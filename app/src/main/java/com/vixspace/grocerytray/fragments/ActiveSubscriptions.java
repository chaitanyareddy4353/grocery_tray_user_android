package com.vixspace.grocerytray.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.MainActivity;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.AcceptLocationActivity;
import com.vixspace.grocerytray.activities.LoginActivity;
import com.vixspace.grocerytray.activities.SignUpActivity;
import com.vixspace.grocerytray.activities.SplashActivity;
import com.vixspace.grocerytray.adapters.ActiveSubsAdapter;
import com.vixspace.grocerytray.adapters.CurrentOrdersAdapter;
import com.vixspace.grocerytray.pojos.FetchAllActiveSubscriptionProductsResponse;
import com.vixspace.grocerytray.pojos.FetchDefaultLocalitySettingResponse;
import com.vixspace.grocerytray.pojos.FetchUserLocality;
import com.vixspace.grocerytray.pojos.UpcomingOrdersResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActiveSubscriptions extends Fragment  implements ActiveSubsAdapter.Subs,View.OnClickListener{
    RecyclerView recyclerSubs;
    LinearLayout linearNull;
    ImageView imgBack;
    ActiveSubsAdapter myJobsAdapter;
    LinearLayoutManager linearLayoutManager;
    List<FetchAllActiveSubscriptionProductsResponse.Datum> datumList = new ArrayList<>();
    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;

    RelativeLayout relativeWhole;
    TextView txtTitleDialog,txtDescriptionDialog,txtNoDialog,txtYesDialog;
    Integer type = 0;
    String subId = "";

    Button btnStartShopping;
    ProgressBar progressBar;
    SwipeRefreshLayout swipeLayout;

    public ActiveSubscriptions() {}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subs,container,false);

        allsharedpreference= new Allsharedpreference(getActivity());
        commonMethods= new CommonMethods(getActivity());

        init(view);

        return view;
    }

    private void init(View view) {
        linearNull = view.findViewById(R.id.linearNull);
        recyclerSubs = view.findViewById(R.id.recyclerSubs);
        relativeWhole = view.findViewById(R.id.relativeWhole);
        txtTitleDialog = view.findViewById(R.id.txtTitleDialog);
        txtDescriptionDialog = view.findViewById(R.id.txtDescriptionDialog);
        txtNoDialog = view.findViewById(R.id.txtNoDialog);
        txtYesDialog = view.findViewById(R.id.txtYesDialog);
        btnStartShopping = view.findViewById(R.id.btnStartShopping);
        progressBar = view.findViewById(R.id.progressBar);
        swipeLayout = view.findViewById(R.id.swipeLayout);

        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryDark));

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(false);
                showJobs();
            }
        });

        relativeWhole.setOnClickListener(null);
        txtNoDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeWhole.setVisibility(View.GONE);
            }
        });

        txtYesDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeWhole.setVisibility(View.GONE);
                if (type!=2){
                    if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                        editSub(type);
                    }else{
                        showNoInterNetMessage();
                    }

                } else {
                    Toast.makeText(getActivity(), "Coming soon", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnStartShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
//                getActivity().finish();
            }
        });


        showJobs();
    }

    private void showJobs() {
        datumList.clear();
        datumList = new ArrayList<>();
        myJobsAdapter = new ActiveSubsAdapter(getActivity(),datumList,ActiveSubscriptions.this);
        linearLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
        recyclerSubs.setLayoutManager(linearLayoutManager);
        recyclerSubs.setAdapter(myJobsAdapter);


        if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
            ListAllJobs();
        }else{
            showNoInterNetMessage();
        }

    }

    private void ListAllJobs() {
        swipeLayout.setRefreshing(false);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.allActiveSubscriptionProducts(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {

                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final FetchAllActiveSubscriptionProductsResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,FetchAllActiveSubscriptionProductsResponse.class);

                    if(listOfProductsResponse.getSuccess()) {
                        if (listOfProductsResponse.getExtras().getData().size()>0){
                            linearNull.setVisibility(View.GONE);
                            datumList.addAll(listOfProductsResponse.getExtras().getData());
                            myJobsAdapter.notifyDataSetChanged();

                        } else {
                            linearNull.setVisibility(View.VISIBLE);
                            recyclerSubs.setVisibility(View.GONE);
                        }

                        progressBar.setVisibility(View.GONE);

                    } else {
                        progressBar.setVisibility(View.GONE);
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(getActivity(), "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            allsharedpreference.clearAllSharedPreferences();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }

                }else {
                    progressBar.setVisibility(View.GONE);
//                    if (response.message().equals("Bad Request")){
                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(getActivity(), SplashActivity.class);
                        allsharedpreference.clearAllSharedPreferences();
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(),t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onModify(String Id, Boolean isPaused, Boolean isModified, Boolean isDelete) {
//        Toast.makeText(getActivity(), "Test", Toast.LENGTH_SHORT).show();
        relativeWhole.setVisibility(View.VISIBLE);
        subId = Id;
        if (isPaused){
            type = 1;
            txtTitleDialog.setText("Pause Subscription");
            txtDescriptionDialog.setText("Are you sure you want to Pause the Subscription ?");
            txtYesDialog.setText("YES, PAUSE");
        } else if (isModified){

//            type = 2;
//            txtTitleDialog.setText("");
//            txtDescriptionDialog.setText("");
//            txtYesDialog.setText("");
        } else if (isDelete) {
            type = 3;
            txtTitleDialog.setText("Terminate Subscription");
            txtDescriptionDialog.setText("Are you sure you want to terminate the Subscription ?");
            txtYesDialog.setText("YES, Terminate");
        }
    }

    private void editSub(Integer type) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("SubscriptionID",subId);
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = null;
        if (type==1){
            apiResponse = apiService.pauseSubscription(jsonObject);
        } else if (type==2){
            apiResponse = apiService.updateSubscriptionInformation(jsonObject);
        } else if (type==3) {
            apiResponse = apiService.terminateSubscription(jsonObject);
        }

        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final UpdateFcmResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,UpdateFcmResponse.class);

                        Toast.makeText(getActivity(), "successful", Toast.LENGTH_SHORT).show();

                        showJobs();


                    } else {

                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }


    @Override
    public void onClick(View v) {

    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(getActivity(), getResources().getString(R.string.no_internet));
    }

}
