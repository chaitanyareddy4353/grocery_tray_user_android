package com.vixspace.grocerytray.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.razorpay.Checkout;
import com.vixspace.grocerytray.MainActivity;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.ConfirmOrder;
import com.vixspace.grocerytray.activities.MyAccoutActivity;
import com.vixspace.grocerytray.activities.SideNavActivity;
import com.vixspace.grocerytray.activities.SplashActivity;
import com.vixspace.grocerytray.pojos.FetchUserWalletInformationResponse;
import com.vixspace.grocerytray.pojos.LastRechargeResponse;
import com.vixspace.grocerytray.pojos.RazorpaypayWalletResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import org.json.JSONObject;

import java.text.DecimalFormat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletFragment extends Fragment implements MainActivity.PaymentResult {

    public WalletFragment() {}

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;
    TextView txtWalletBalance,txtLastRechargeAmount,txtBalanceAfterLasRecharge,txtBal;
    Button btnAddMOney,btn1000,btn2000,btn3000,btn4000;
    EditText edtAddCash;
    Double walletAmt = 0.0;
    Double walletRazorAmt = 0.0;
    ProgressBar progressBar;

    CardView cardRechargeHistory,cardReserveMoney;
    TextView txtReserveAmount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wallet, container, false);

        allsharedpreference = new Allsharedpreference(getActivity());
        commonMethods = new CommonMethods(getActivity());

        init(view);

        return view;
    }

    private void init(View view) {
        txtLastRechargeAmount = view.findViewById(R.id.txtLastRechargeAmount);
        txtBalanceAfterLasRecharge = view.findViewById(R.id.txtBalanceAfterLasRecharge);
        txtBal = view.findViewById(R.id.txtBal);
        cardReserveMoney = view.findViewById(R.id.cardReserveMoney);
        txtWalletBalance = view.findViewById(R.id.txtWalletBalance);
        edtAddCash = view.findViewById(R.id.edtAddCash);
        btnAddMOney = view.findViewById(R.id.btnAddMOney);
        btn1000 = view.findViewById(R.id.btn1000);
        btn2000 = view.findViewById(R.id.btn2000);
        btn3000 = view.findViewById(R.id.btn3000);
        btn4000 = view.findViewById(R.id.btn4000);
        progressBar = view.findViewById(R.id.progressBar);
        cardRechargeHistory = view.findViewById(R.id.cardRechargeHistory);
        txtReserveAmount = view.findViewById(R.id.txtReserveAmount);

        if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){

            if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                getWalletInformation();
                getLastInfo();
            }else{
                showNoInterNetMessage();
            }

        }

        btn1000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtAddCash.setText("1000");
            }
        });

        btn2000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtAddCash.setText("2000");
            }
        });

        btn3000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtAddCash.setText("3000");
            }
        });

        btn4000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtAddCash.setText("4000");
            }
        });

        btnAddMOney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtAddCash.getText().toString().trim().length()>0){

                    if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                        RazorPay(Double.valueOf(edtAddCash.getText().toString().trim()));
                    }else{
                        showNoInterNetMessage();
                    }

//                    makePayment(listOfProductsResponse.getExtras().getData().getTotalAmount(), listOfProductsResponse.getExtras().getData().getAmountRequestID());
                }
            }
        });

        cardReserveMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SideNavActivity.class);
                intent.putExtra("indicator",8);
                startActivity(intent);
            }
        });

        cardRechargeHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SideNavActivity.class);
                intent.putExtra("indicator",9);
                startActivity(intent);
            }
        });


    }

    private void getWalletInformation() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.walletInformation(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final FetchUserWalletInformationResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,FetchUserWalletInformationResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                        txtWalletBalance.setText("₹" +String.valueOf(listOfProductsResponse.getExtras().getData().getAvailableAmount()));
                        walletAmt = listOfProductsResponse.getExtras().getData().getAvailableAmount();
                        txtReserveAmount.setText("₹" +String.valueOf(listOfProductsResponse.getExtras().getData().getReservedAmount()));

                    } else {
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(getActivity(), "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            allsharedpreference.clearAllSharedPreferences();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }

                }else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(getActivity(), SplashActivity.class);
                        allsharedpreference.clearAllSharedPreferences();
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(getActivity(),t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getLastInfo() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.lastWallet(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final LastRechargeResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,LastRechargeResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                        if (!String.valueOf(listOfProductsResponse.getExtras().getData().getBalanceAfterLastRecharge()).equals("null")){
                            txtBalanceAfterLasRecharge.setText("₹" +String.valueOf(listOfProductsResponse.getExtras().getData().getBalanceAfterLastRecharge()));
                        }
                        if (!String.valueOf(listOfProductsResponse.getExtras().getData().getLastRechargeAmount()).equals("null")){
                            txtLastRechargeAmount.setText("₹"+String.valueOf(listOfProductsResponse.getExtras().getData().getLastRechargeAmount()));;
                        }
                        if (!String.valueOf(listOfProductsResponse.getExtras().getData().getLastBillAmount()).equals("null")){
                            txtBal.setText("₹" +String.valueOf(listOfProductsResponse.getExtras().getData().getLastBillAmount()));
                        }

                    } else {
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(getActivity(), "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            allsharedpreference.clearAllSharedPreferences();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }

                }else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(getActivity(), SplashActivity.class);
                        allsharedpreference.clearAllSharedPreferences();
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(getActivity(),t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getWalletInformationRazorPay() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.walletInformation(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final FetchUserWalletInformationResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,FetchUserWalletInformationResponse.class);

                    progressBar.setVisibility(View.GONE);
                    if(listOfProductsResponse.getSuccess()) {

                        txtWalletBalance.setText("₹" +String.valueOf(listOfProductsResponse.getExtras().getData().getAvailableAmount()));
                        walletRazorAmt = listOfProductsResponse.getExtras().getData().getAvailableAmount();
                        if (walletRazorAmt==walletAmt){
                            edtAddCash.setText("");
                            commonMethods.hideSoftKeyboard();
                            Toast.makeText(getActivity(), "Wallet updated", Toast.LENGTH_SHORT).show();
                        } else {
                            edtAddCash.setText("");
                            commonMethods.hideSoftKeyboard();
                            Toast.makeText(getActivity(), "Amount shall be added soon in your wallet", Toast.LENGTH_SHORT).show();
//                            progressBar.setVisibility(View.VISIBLE);
//                            getWalletInformationRazorPay();
                        }

                    } else {
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(getActivity(), "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            allsharedpreference.clearAllSharedPreferences();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }

                }else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(getActivity(), SplashActivity.class);
                        allsharedpreference.clearAllSharedPreferences();
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(),t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void RazorPay(Double finalTransactionAmount) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("Amount",finalTransactionAmount);
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.razorPay(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final RazorpaypayWalletResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,RazorpaypayWalletResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                        if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                            makePayment(listOfProductsResponse.getExtras().getData().getTotalAmount(),listOfProductsResponse.getExtras().getData().getAmountRequestID());
                        }else{
                            showNoInterNetMessage();
                        }

                    } else {
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(getActivity(), "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            allsharedpreference.clearAllSharedPreferences();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }

                }else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(getActivity(), SplashActivity.class);
                        allsharedpreference.clearAllSharedPreferences();
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(getActivity(),t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void addReserveMoney() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.walletInformation(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final FetchUserWalletInformationResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,FetchUserWalletInformationResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                        txtWalletBalance.setText("₹" +String.valueOf(listOfProductsResponse.getExtras().getData().getAvailableAmount()));
                        walletAmt = listOfProductsResponse.getExtras().getData().getAvailableAmount();
                        txtReserveAmount.setText("₹" +String.valueOf(listOfProductsResponse.getExtras().getData().getReservedAmount()));

                    } else {
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(getActivity(), "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            allsharedpreference.clearAllSharedPreferences();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }

                }else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(getActivity(),t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    private void makePayment(Double totalAmount, String amountRequestID) {

        Checkout checkout = new Checkout();
        //checkout.setFullScreenDisable(true);
        /**
         * Set your logo here
         */
        //checkout.setImage(R.drawable.);

        /**
         * Reference to current activity
         */
        final Activity activity = getActivity();

        /**
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */
        try {
            JSONObject options = new JSONObject();

            /**
             * Merchant Name
             * eg: Rentomojo || HasGeek etc.
             */

            /**
             * Description can be anything
             * eg: Order #123123
             *     Invoice Payment
             *     etc.
             */

            JSONObject jsonObject = new JSONObject();

            String newd = jsonObject.toString();
//            jsonObject.put("OrderID",OrderNum);
            jsonObject.put("Amount_RequestID",amountRequestID);
            options.put("notes", jsonObject);


            options.put("currency", "INR");
            JSONObject preFill = new JSONObject();

            options.put("prefill", preFill);

            Double amt = Double.valueOf(totalAmount);
//            Double amt = Double.valueOf(edtAddCash.getText().toString().trim());

            DecimalFormat df = new DecimalFormat("#.00");
            String angleFormated = df.format(amt);
            amt=  Double.valueOf(angleFormated);


            options.put("amount",String.valueOf((amt)*100));


            /**
             * Amount is always passed in PAISE
             * Eg: "500" = Rs 5.00
             */
//            options.put("amount",String.valueOf((RazorPayAmount)*100));

            checkout.open(activity, options);
        } catch(Exception e) {
            Log.e("payment_ERROR", "Error in starting Razorpay Checkout"+ e.toString());
        }
    }

    @Override
    public void onResultRecieved(boolean isSuccess) {
        if (isSuccess){
            progressBar.setVisibility(View.VISIBLE);
//            Toast.makeText(getActivity(), "Try again after sometime", Toast.LENGTH_SHORT).show();
            if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                getWalletInformationRazorPay();
            }else{
                showNoInterNetMessage();
            }

        } else {
            Toast.makeText(getActivity(), "Try again after sometime", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
            getWalletInformation();
            getLastInfo();
        }else{
            showNoInterNetMessage();
        }

    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(getActivity(), getResources().getString(R.string.no_internet));
    }

}
