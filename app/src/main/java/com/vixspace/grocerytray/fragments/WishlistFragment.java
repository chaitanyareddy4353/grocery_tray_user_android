package com.vixspace.grocerytray.fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.MainActivity;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.CartActivity;
import com.vixspace.grocerytray.activities.ConfirmOrder;
import com.vixspace.grocerytray.activities.SplashActivity;
import com.vixspace.grocerytray.adapters.CurrentOrdersAdapter;
import com.vixspace.grocerytray.adapters.SelecteddatesAdapter;
import com.vixspace.grocerytray.adapters.WishListAdapter;
import com.vixspace.grocerytray.pojos.CartInfoModel;
import com.vixspace.grocerytray.pojos.CartModelClass;
import com.vixspace.grocerytray.pojos.FetchAllAvailableDatesResponse;
import com.vixspace.grocerytray.pojos.FetchAllFavProductsResponse;
import com.vixspace.grocerytray.pojos.UpcomingOrdersResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishlistFragment extends Fragment implements WishListAdapter.Delete {

    public WishlistFragment() {}

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;
    LinearLayout layoutNull;
    Button btnStartShopping;
    RecyclerView recyclerWishList;
    ProgressBar progressBar;
    LinearLayoutManager linearLayoutManager;
    WishListAdapter wishListAdapter;
    TextView txtClearAll;
    List<FetchAllFavProductsResponse.Datum> datumList = new ArrayList<>();

    CartInfoModel cartInfoModel = new CartInfoModel();
    List<CartModelClass> cartModelClasses = new ArrayList<>();
    Integer CountRepeat = 0;
    Integer count = 0;
    String data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wishlist, container, false);

        allsharedpreference = new Allsharedpreference(getActivity());
        commonMethods = new CommonMethods(getActivity());

        init(view);

        return view;
    }

    private void init(View view) {
        layoutNull = view.findViewById(R.id.layoutNull);
        btnStartShopping = view.findViewById(R.id.btnStartShopping);
        recyclerWishList = view.findViewById(R.id.recyclerWishList);
        progressBar = view.findViewById(R.id.progressBar);
        txtClearAll = view.findViewById(R.id.txtClearAll);

        btnStartShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
//                HomeFragment fragment2 = new HomeFragment();
//                FragmentManager fragmentManager = getFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.frame_container, fragment2);
//                fragmentTransaction.commit();
            }
        });

        Gson gson = new Gson();
        data = allsharedpreference.getShared_String(Allsharedpreference.CartData);
        if (data!=null){
            cartInfoModel = gson.fromJson(data, CartInfoModel.class);
            if (!data.equals("")){
                if (cartInfoModel.getData().size()>0){
                    cartModelClasses = cartInfoModel.getData();
                }
            }
        }
        if (cartInfoModel.getData()!=null){


        } else {
            cartInfoModel = new CartInfoModel();
        }

        txtClearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count>0){
                    if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                        clearFav();
                    }else{
                        showNoInterNetMessage();
                    }

                } else {

                }
            }
        });

        showJobs();
    }

    private void clearFav() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.removeAllFav(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final UpdateFcmResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,UpdateFcmResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                        showJobs();

                    } else {
                        progressBar.setVisibility(View.GONE);
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(getActivity(), "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }

                }else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(getActivity(), SplashActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        allsharedpreference.clearAllSharedPreferences();
                        startActivity(intent);
                        getActivity().finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(),t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    private void showJobs() {
        datumList.clear();
        datumList = new ArrayList<>();
        wishListAdapter = new WishListAdapter(getActivity(),datumList,2,WishlistFragment.this);
        linearLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
        recyclerWishList.setLayoutManager(linearLayoutManager);
        recyclerWishList.setAdapter(wishListAdapter);

        if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
            ListAllItems();
        }else{
            showNoInterNetMessage();
        }

    }

    private void ListAllItems() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.allFav(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final FetchAllFavProductsResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,FetchAllFavProductsResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                        count = listOfProductsResponse.getExtras().getCount();

                        if (listOfProductsResponse.getExtras().getData().size()>0){
                            datumList.addAll(listOfProductsResponse.getExtras().getData());
                            wishListAdapter.notifyDataSetChanged();
                            progressBar.setVisibility(View.GONE);
                        } else {
                            progressBar.setVisibility(View.GONE);
                        }
                        if (listOfProductsResponse.getExtras().getCount()>0){
                            layoutNull.setVisibility(View.GONE);
                            recyclerWishList.setVisibility(View.VISIBLE);
                        } else {
                            layoutNull.setVisibility(View.VISIBLE);
                            recyclerWishList.setVisibility(View.GONE);
                        }

                    } else {
                        progressBar.setVisibility(View.GONE);
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(getActivity(), "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }

                }else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(getActivity(), SplashActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        allsharedpreference.clearAllSharedPreferences();
                        startActivity(intent);
                        getActivity().finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(),t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onDeleted(String productId, Integer position,Boolean isDelete,FetchAllFavProductsResponse.Datum datum) {

        if (isDelete){
            if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                removeItem(productId);
            }else{
                showNoInterNetMessage();
            }

        } else {
            if (data!=null){
                if (cartInfoModel.getData().size()>0){
                    for (int i =0;i<cartInfoModel.getData().size();i++){
                        if (productId.equals(cartInfoModel.getData().get(i).getLocalityProductID())){
                            CountRepeat = CountRepeat+1;
                        }
                    }

                    if (CountRepeat>0){
                        Toast.makeText(getActivity(), "Item already added to cart", Toast.LENGTH_SHORT).show();
                    } else {
                        CartModelClass cartModelClass = new CartModelClass();
                        cartModelClass.setLocalityProductID(datum.getLocalityProductID());
                        cartModelClass.setMRP(datum.getMRP());
                        cartModelClass.setProductID(datum.getProductID());
                        cartModelClass.setProductName(datum.getProductName());
                        cartModelClass.setItemWeight(datum.getProductUnit());
                        cartModelClass.setBrand(datum.getProductBrand());
                        cartModelClass.setImageId(datum.getImageInformation().getImage250());
                        cartModelClass.setQuantity(1);
                        cartModelClass.setSelling_Price(datum.getSellingPrice());
                        cartModelClass.setSub_Total_Amount(datum.getSellingPrice());
                        cartModelClasses.add(cartModelClass);
                        cartInfoModel.setData(cartModelClasses);
                        Gson gsons = new Gson();
                        String json = gsons.toJson(cartInfoModel); // myObject - instance of MyObject
                        allsharedpreference.setShared_String(Allsharedpreference.CartData,json);
                        Intent intent = new Intent(getActivity(), CartActivity.class);
                        startActivity(intent);
                    }
                } else {
                    CartModelClass cartModelClass = new CartModelClass();
                    cartModelClass.setLocalityProductID(datum.getLocalityProductID());
                    cartModelClass.setMRP(datum.getMRP());
                    cartModelClass.setProductID(datum.getProductID());
                    cartModelClass.setProductName(datum.getProductName());
                    cartModelClass.setItemWeight(datum.getProductUnit());
                    cartModelClass.setBrand(datum.getProductBrand());
                    cartModelClass.setImageId(datum.getImageInformation().getImage250());
                    cartModelClass.setQuantity(1);
                    cartModelClass.setSelling_Price(datum.getSellingPrice());
                    cartModelClass.setSub_Total_Amount(datum.getSellingPrice());
                    cartModelClasses.add(cartModelClass);
                    cartInfoModel.setData(cartModelClasses);
                    Gson gsons = new Gson();
                    String json = gsons.toJson(cartInfoModel); // myObject - instance of MyObject
                    allsharedpreference.setShared_String(Allsharedpreference.CartData,json);
                    Intent intent = new Intent(getActivity(),CartActivity.class);
                    startActivity(intent);

                }
            } else {
                CartModelClass cartModelClass = new CartModelClass();
                cartModelClass.setLocalityProductID(datum.getLocalityProductID());
                cartModelClass.setMRP(datum.getMRP());
                cartModelClass.setProductID(datum.getProductID());
                cartModelClass.setProductName(datum.getProductName());
                cartModelClass.setItemWeight(datum.getProductUnit());
                cartModelClass.setBrand(datum.getProductBrand());
                cartModelClass.setImageId(datum.getImageInformation().getImage250());
                cartModelClass.setQuantity(1);
                cartModelClass.setSelling_Price(datum.getSellingPrice());
                cartModelClass.setSub_Total_Amount(datum.getSellingPrice());
                cartModelClasses.add(cartModelClass);
                cartInfoModel.setData(cartModelClasses);
                Gson gsons = new Gson();
                String json = gsons.toJson(cartInfoModel); // myObject - instance of MyObject
                allsharedpreference.setShared_String(Allsharedpreference.CartData,json);
                Intent intent = new Intent(getActivity(),CartActivity.class);
                startActivity(intent);
            }

        }

        }

    private void removeItem(String cartInfoModel) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("FavouriteID",cartInfoModel);

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.removeFav(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final UpdateFcmResponse loginResponse =
                                gson.fromJson(splashScreenResponse,UpdateFcmResponse.class);

                        if (loginResponse.getSuccess()) {

                            showJobs();

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            getActivity().finish();
                        }

//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        Gson gson = new Gson();
        data = allsharedpreference.getShared_String(Allsharedpreference.CartData);
        if (data!=null){
            cartInfoModel = gson.fromJson(data, CartInfoModel.class);
            if (!data.equals("")){
                if (cartInfoModel.getData().size()>0){
                    cartModelClasses = cartInfoModel.getData();
                }
            }
        }
        if (cartInfoModel.getData()!=null){


        } else {
            cartInfoModel = new CartInfoModel();
        }

    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(getActivity(), getResources().getString(R.string.no_internet));
    }

}
