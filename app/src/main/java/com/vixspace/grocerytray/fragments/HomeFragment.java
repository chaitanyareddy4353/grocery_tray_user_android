package com.vixspace.grocerytray.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.MainActivity;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.AllCategoriesActivity;
import com.vixspace.grocerytray.activities.ChooseLocationActivity;
import com.vixspace.grocerytray.activities.LoginActivity;
import com.vixspace.grocerytray.activities.LoginSignUpActivity;
import com.vixspace.grocerytray.activities.MyAccoutActivity;
import com.vixspace.grocerytray.activities.MySubscriptions;
import com.vixspace.grocerytray.activities.NeedHelpActivity;
import com.vixspace.grocerytray.activities.NotificationsActivity;
import com.vixspace.grocerytray.activities.SearchActivity;
import com.vixspace.grocerytray.activities.SideNavActivity;
import com.vixspace.grocerytray.activities.SplashActivity;
import com.vixspace.grocerytray.adapters.BannersAdapter;
import com.vixspace.grocerytray.adapters.CategoriesAdapter;
import com.vixspace.grocerytray.adapters.DealOfDayAdapter;
import com.vixspace.grocerytray.pojos.CartInfoModel;
import com.vixspace.grocerytray.pojos.CartModelClass;
import com.vixspace.grocerytray.pojos.FetchAllDealOfDayDetailsResponse;
import com.vixspace.grocerytray.pojos.FetchAllHomeSliderItemsResponse;
import com.vixspace.grocerytray.pojos.FetchAllLocalityCategoriesResponse;
import com.vixspace.grocerytray.pojos.FetchDefaultLocalitySettingResponse;
import com.vixspace.grocerytray.pojos.FetchUserLocality;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class HomeFragment extends Fragment implements NavigationView.OnNavigationItemSelectedListener {

    NavigationView side_nav;
    View navView;
    DrawerLayout drawer;

    ImageView imgHam,imgNotification;
    TextView txtUserName,txtAddress;
    EditText edtSearch;

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;

    TextView txtLocation,txtViewAll,txtAllMilk;
    RecyclerView recyclerCategories,recyclerBanners,recyclerMilk,recyclerDealOfTheDay;
    LinearLayout linearLocation;

    String checkLoc = "";

    public HomeFragment(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home,container,false);


        init(view);
        return view;

    }

    private void init(View view) {
        allsharedpreference = new Allsharedpreference(getActivity());
        commonMethods = new CommonMethods(getActivity());

        imgHam = view.findViewById(R.id.imgHam);
        edtSearch = view.findViewById(R.id.edtSearch);

        imgNotification = view.findViewById(R.id.imgNotification);
        linearLocation = view.findViewById(R.id.linearLocation);
        recyclerDealOfTheDay = view.findViewById(R.id.recyclerDealOfTheDay);
        recyclerMilk = view.findViewById(R.id.recyclerMilk);
        txtAllMilk = view.findViewById(R.id.txtAllMilk);
        txtViewAll = view.findViewById(R.id.txtViewAll);
        recyclerCategories = view.findViewById(R.id.recyclerCategories);
        recyclerBanners = view.findViewById(R.id.recyclerBanners);
        drawer = view.findViewById(R.id.drawer);
        side_nav = view.findViewById(R.id.side_nav);
        navView = side_nav.inflateHeaderView(R.layout.layout_header);
        txtUserName = navView.findViewById(R.id.txtUserName);
        txtAddress = navView.findViewById(R.id.txtAddress);

        txtLocation = view.findViewById(R.id.txtLocation);

        if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
            txtUserName.setText(allsharedpreference.getShared_String(Allsharedpreference.UserName));
        } else {
            txtUserName.setText("Guest User");
        }

        txtAddress.setText(allsharedpreference.getShared_String(Allsharedpreference.CurrentLocation));

        checkLoc = allsharedpreference.getShared_String(Allsharedpreference.CurrentLocation);

        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                startActivity(intent);
            }
        });


        if (!allsharedpreference.getShared_Booleen(Allsharedpreference.NotfirstTimeHomeScreen)){
            allsharedpreference.setShared_Booleen(Allsharedpreference.NotfirstTimeHomeScreen,true);
                 txtLocation.setText(allsharedpreference.getShared_String(Allsharedpreference.CurrentLocation));
            txtAddress.setText(allsharedpreference.getShared_String(Allsharedpreference.CurrentLocation));

            if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                allCategories();
            }else{
                showNoInterNetMessage();
            }

        } else {
            if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                allCategories();
            }else{
                showNoInterNetMessage();
            }

            txtLocation.setText(allsharedpreference.getShared_String(Allsharedpreference.CurrentLocation));
            txtAddress.setText(allsharedpreference.getShared_String(Allsharedpreference.CurrentLocation));
        }

        linearLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ChooseLocationActivity.class);
                intent.putExtra("indicator",1);
                startActivityForResult(intent,101);
            }
        });



        imgHam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });

        txtViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AllCategoriesActivity.class);
                startActivity(intent);
            }
        });

        side_nav.setNavigationItemSelectedListener(this);
//        if (!allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
//            side_nav.getMenu().findItem(R.id.logout).setTitle("Signup");
//        } else {
//            side_nav.getMenu().findItem(R.id.logout).setTitle("Logout");
//        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        Fragment fragment;

        if (id==R.id.myAccount){

            if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
                Intent intent = new Intent(getActivity(), MyAccoutActivity.class);
                intent.putExtra("indicator","1");
                startActivity(intent);
            } else {
                Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }


        } else if (id==R.id.mySubscriptions){

            if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
                Intent intent = new Intent(getActivity(), MySubscriptions.class);
                intent.putExtra("indicator","2");
                startActivity(intent);
            } else {
                Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }

        } else if (id==R.id.shopByCategory){

            Intent intent = new Intent(getActivity(), AllCategoriesActivity.class);
            intent.putExtra("indicator","3");
            startActivity(intent);

        }
//        else
//            if (id==R.id.notifications){
//
//            Intent intent = new Intent(getActivity(), NotificationsActivity.class);
//            intent.putExtra("indicator","4");
//            startActivity(intent);
//
//        }
        else if (id==R.id.myWishlist){

            if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
                Intent intent = new Intent(getActivity(), SideNavActivity.class);
                intent.putExtra("indicator", 5);
                getActivity().startActivity(intent);
            } else {
                Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }



        }
//        else if (id==R.id.rateOurApp){
//
//            final String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
//            try {
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//            }
//            catch (android.content.ActivityNotFoundException anfe) {
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//            }
//
//        }
//
        else if (id==R.id.needHelp){

            Intent intent = new Intent(getActivity(), NeedHelpActivity.class);
            intent.putExtra("Indicator", 7);
            getActivity().startActivity(intent);

        }else if (id==R.id.referandEarn){

            final String appPackageName = getActivity().getPackageName();
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,  "Checkout GroceryTray, use my referral code"+" **** " + " https://play.google.com/store/apps/details?id=" + appPackageName);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);

        }else if (id==R.id.share){
            final String appPackageName = getActivity().getPackageName();
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,  "Checkout GroceryTray application " + " https://play.google.com/store/apps/details?id=" + appPackageName);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);

        }

//        else if (id==R.id.logout){
//
//            if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
//                (new AlertDialog.Builder(getActivity())).setTitle("LOGOUT").setMessage("Are you sure you want to logout?").setPositiveButton("yes", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface var1, int var2) {
//
//                        allsharedpreference.clearAllSharedPreferences();
//                        Intent intent = new Intent(getActivity(), SplashActivity.class);
//                        startActivity(intent);
//                        getActivity().finish();
//
//
//                    }
//                }).setNegativeButton( "No",new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface var1, int var2) {
//
//                    }
//                }).show();
//            } else {
//                Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
//            }
//
//
//
//        }

        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==101&& resultCode == RESULT_OK){
            String lat = data.getStringExtra("lat");
            String lon = data.getStringExtra("lon");
            String strAddress = data.getStringExtra("address");
            String city  = data.getStringExtra("city");
            String state = data.getStringExtra("state");
            String pincode = data.getStringExtra("pin");
            String landmark = data.getStringExtra("landmark");

           String newAddress = (strAddress +", " +city+ ", " + state +", " + pincode);

           txtLocation.setText(strAddress);
            txtAddress.setText(allsharedpreference.getShared_String(Allsharedpreference.CurrentLocation));

           if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
               if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                   addAddress(lat,lon,1);
               }else{
                   showNoInterNetMessage();
               }
           } else {
               if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                   addAddress(lat,lon,2);
               }else{
                   showNoInterNetMessage();
               }

           }

        }
    }

    private void addAddress(final String lat, final String lon, final int i) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("Latitude",lat);
        jsonObject.addProperty("Longitude",lon);
        if (i==1){
            jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
            jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        }
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = null;
        if (i==1){
            apiResponse = apiService.userLocality(jsonObject);
        } else {
            apiResponse = apiService.defaultLocality(jsonObject);
        }

        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){
                        //Registered 1;
                        if (i==1){
                            JsonObject splashScreenResponse = response.body();
                            Gson gson = new Gson();
                            FetchUserLocality loginResponse = gson.fromJson(splashScreenResponse,FetchUserLocality.class);

                            if (loginResponse.getSuccess()) {

                                if (loginResponse.getExtras().getWhetherLocalityAvailable()){
                                    allsharedpreference.setShared_String(Allsharedpreference.CurrentLocation,loginResponse.getExtras().getData().getLocalityTitle());
                                    allsharedpreference.setShared_String(Allsharedpreference.CurrentCityID,loginResponse.getExtras().getData().getCityID());
                                    allsharedpreference.setShared_String(Allsharedpreference.LocalityID,loginResponse.getExtras().getData().getLocalityID());
                                    allsharedpreference.setShared_String(Allsharedpreference.Locality_Title,loginResponse.getExtras().getData().getLocalityTitle());

                                    txtLocation.setText(loginResponse.getExtras().getData().getLocalityTitle());
                                    txtAddress.setText(loginResponse.getExtras().getData().getLocalityTitle());

                                    if (checkLoc.equals(loginResponse.getExtras().getData().getLocalityTitle())){

                                    } else {
                                        String data = allsharedpreference.getShared_String(Allsharedpreference.CartData);
                                        if (!data.equals("")&&data!=null){
                                            Toast.makeText(getActivity(), "cart has been cleared", Toast.LENGTH_SHORT).show();
                                        }

                                        CartInfoModel cartInfoModel1 = new CartInfoModel();
                                        List<CartModelClass> cartModelClasses = new ArrayList<>();
                                        cartModelClasses.clear();
                                        cartInfoModel1.setData(cartModelClasses);
                                        Gson gsons = new Gson();
                                        String jsons = gsons.toJson(cartInfoModel1); // myObject - instance of MyObject
                                        allsharedpreference.setShared_String(Allsharedpreference.CartData,jsons);
                                    }

                                    if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                                        allCategories();
                                    }else{
                                        showNoInterNetMessage();
                                    }

                                } else {
                                    if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                                        addAddress(lat,lon,2);
                                    }else{
                                        showNoInterNetMessage();
                                    }

                                }

                            } else{
                                commonMethods.showToast(loginResponse.getExtras().getMsg());
                            }
                        } else {
                            JsonObject splashScreenResponse = response.body();
                            Gson gson = new Gson();
                            FetchDefaultLocalitySettingResponse loginResponse = gson.fromJson(splashScreenResponse,FetchDefaultLocalitySettingResponse.class);

                            if (loginResponse.getSuccess()) {

                                allsharedpreference.setShared_String(Allsharedpreference.CurrentLocation,loginResponse.getExtras().getData().getLocalityData().getLocalityTitle());
                                allsharedpreference.setShared_String(Allsharedpreference.CurrentCityID,loginResponse.getExtras().getData().getLocalityData().getCityID());
                                allsharedpreference.setShared_String(Allsharedpreference.LocalityID,loginResponse.getExtras().getData().getLocalityData().getLocalityID());
                                allsharedpreference.setShared_String(Allsharedpreference.Locality_Title,loginResponse.getExtras().getData().getLocalityData().getLocalityTitle());

                                txtLocation.setText(loginResponse.getExtras().getData().getLocalityData().getLocalityTitle());
                                txtAddress.setText(loginResponse.getExtras().getData().getLocalityData().getLocalityTitle());

                                if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                                    allCategories();
                                }else{
                                    showNoInterNetMessage();
                                }

                            } else{
                                commonMethods.showToast(loginResponse.getExtras().getMsg());
                            }
                        }


                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            getActivity().finish();
                        }

//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    private void allCategories() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.fetchAllLocalityCategories(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final FetchAllLocalityCategoriesResponse loginResponse = gson.fromJson(splashScreenResponse,FetchAllLocalityCategoriesResponse.class);

                        if (loginResponse.getSuccess()) {

                            CategoriesAdapter adapter = new CategoriesAdapter(getActivity(), loginResponse.getExtras().getData());
                            recyclerCategories.setLayoutManager(new LinearLayoutManager(getActivity(),RecyclerView.HORIZONTAL,false));
                            recyclerCategories.setAdapter(adapter);

                            if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                                allBanners();
                                dealOfTheDay();
                            }else{
                                showNoInterNetMessage();
                            }

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());

//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    private void allBanners() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.allHomeSlideritems(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final FetchAllHomeSliderItemsResponse loginResponse = gson.fromJson(splashScreenResponse,FetchAllHomeSliderItemsResponse.class);

                        if (loginResponse.getSuccess()) {

                            if (loginResponse.getExtras().getCount()>0){
                                BannersAdapter adapter = new BannersAdapter(getActivity(), loginResponse.getExtras().getData());
                                recyclerBanners.setLayoutManager(new LinearLayoutManager(getActivity(),RecyclerView.HORIZONTAL,false));
                                recyclerBanners.setAdapter(adapter);
                            } else {
                                recyclerBanners.setVisibility(View.GONE);
                            }

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());

//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    private void dealOfTheDay() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.allDealOfDayProducts(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final FetchAllDealOfDayDetailsResponse loginResponse = gson.fromJson(splashScreenResponse,FetchAllDealOfDayDetailsResponse.class);

                        if (loginResponse.getSuccess()) {

                            DealOfDayAdapter adapter = new DealOfDayAdapter(getActivity(), loginResponse.getExtras().getData());
                            recyclerDealOfTheDay.setLayoutManager(new LinearLayoutManager(getActivity(),RecyclerView.HORIZONTAL,false));
                            recyclerDealOfTheDay.setAdapter(adapter);

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());

//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(getActivity(), getResources().getString(R.string.no_internet));
    }


}
