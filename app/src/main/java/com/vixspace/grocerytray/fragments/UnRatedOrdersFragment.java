package com.vixspace.grocerytray.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.MainActivity;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.ContactUsActivity;
import com.vixspace.grocerytray.activities.SplashActivity;
import com.vixspace.grocerytray.adapters.CurrentOrdersAdapter;
import com.vixspace.grocerytray.adapters.UnRatedOrdersAdapter;
import com.vixspace.grocerytray.pojos.UpcomingOrdersResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UnRatedOrdersFragment extends Fragment implements UnRatedOrdersAdapter.Rate {
    RecyclerView recyclerView;
    LinearLayout linearNull;
    ImageView imgBack;
    UnRatedOrdersAdapter myJobsAdapter;
    LinearLayoutManager linearLayoutManager;
    List<UpcomingOrdersResponse.Datum> datumList = new ArrayList<>();
    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;
    RelativeLayout relativeNotifications;
    Button btnStartShopping;

    ProgressBar progressBar;
    int skipValue = 0;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean loading = true;

    public UnRatedOrdersFragment() {}

    RelativeLayout relativeNoTouch,relativeComplete;
    TextView txtHelp,txtOrderId,txtTotal,txtTime,txtSkip,txtContinue,txtItems;
    RatingBar rating;
    EditText edtComment;
    String OrderNum="";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_unrated_order_history,container,false);

        allsharedpreference= new Allsharedpreference(getActivity());
        commonMethods= new CommonMethods(getActivity());

        init(view);

        return view;
    }

    private void init(View view) {
        progressBar = view.findViewById(R.id.progressBar);
        linearNull = view.findViewById(R.id.linearNull);
        recyclerView = view.findViewById(R.id.recyclerView);
        btnStartShopping = view.findViewById(R.id.btnStartShopping);


        txtItems = view.findViewById(R.id.txtItems);
        txtSkip = view.findViewById(R.id.txtSkip);
        txtTime = view.findViewById(R.id.txtTime);
        txtTotal = view.findViewById(R.id.txtTotal);
        txtOrderId = view.findViewById(R.id.txtOrderId);
        txtHelp = view.findViewById(R.id.txtHelp);
        txtContinue = view.findViewById(R.id.txtContinue);
        relativeNoTouch = view.findViewById(R.id.relativeNoTouch);
        relativeComplete = view.findViewById(R.id.relativeComplete);
        rating = view.findViewById(R.id.rating);
        edtComment = view.findViewById(R.id.edtComment);

        btnStartShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        txtHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ContactUsActivity.class);
                startActivity(intent);
            }
        });

//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//            }
//
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                if(dy > 0) //check for scroll down
//                {
//                    visibleItemCount = linearLayoutManager.getChildCount();
//                    totalItemCount = linearLayoutManager.getItemCount();
//                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
//
//                    if (loading)
//                    {
//                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
//                        {
//                            loading = false;
//                            Log.v("...", "Last Item Wow !");
//                            progressBar.setVisibility(View.VISIBLE);
//
//                            skipValue = skipValue + 30;
//                            Log.e("SkipValue---->", String.valueOf(skipValue));
//
//                            ListAllJobs();
//
//                            loading = true;
//                        }
//                    }
//                }
//            }
//        });

        txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeComplete.setVisibility(View.GONE);
            }
        });

        relativeNoTouch.setOnClickListener(null);

        txtContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rating.getRating()>0){
                    rateOrder();
                }
            }
        });

        showJobs();
    }


    private void showJobs() {
        datumList.clear();
        datumList = new ArrayList<>();
        myJobsAdapter = new UnRatedOrdersAdapter(getActivity(),datumList,3,UnRatedOrdersFragment.this);
        linearLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(myJobsAdapter);

        if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
            ListAllJobs();
        }else{
            showNoInterNetMessage();
        }

    }

    private void ListAllJobs() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.unRatedOrders(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final UpcomingOrdersResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,UpcomingOrdersResponse.class);

                    if(listOfProductsResponse.getSuccess()) {
                        if (listOfProductsResponse.getExtras().getData().size()>0){
                            datumList.addAll(listOfProductsResponse.getExtras().getData());
                            myJobsAdapter.notifyDataSetChanged();
                            progressBar.setVisibility(View.GONE);
                            linearNull.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                        } else {
                            progressBar.setVisibility(View.GONE);
                            linearNull.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        }

                    } else {
                        progressBar.setVisibility(View.GONE);
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(getActivity(), "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            allsharedpreference.clearAllSharedPreferences();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }

                }else {
//                    if (response.message().equals("Bad Request")){
                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(getActivity(), SplashActivity.class);
                        allsharedpreference.clearAllSharedPreferences();
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(),t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(getActivity(), getResources().getString(R.string.no_internet));
    }

    @Override
        public void rate(String orderId,String orderNum,String date,String total,String count) {
        OrderNum = orderId;
        edtComment.setText("");
        rating.setRating(0);
        txtOrderId.setText("Order Id : #"+orderNum);
        txtTime.setText("Delivered on : "+date);
        txtTotal.setText("Rs "+ total);
        txtItems.setText(count+" items");
        relativeComplete.setVisibility(View.VISIBLE);
    }

    private void rateOrder() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("OrderID",OrderNum);
        jsonObject.addProperty("Rating_Points",rating.getRating());
        jsonObject.addProperty("Remarks",edtComment.getText().toString());

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.submitOrderRating(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final UpcomingOrdersResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,UpcomingOrdersResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                        relativeComplete.setVisibility(View.GONE);
                        showJobs();

                    } else {
                        progressBar.setVisibility(View.GONE);
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(getActivity(), "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            allsharedpreference.clearAllSharedPreferences();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }

                }else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(getActivity(), SplashActivity.class);
                        allsharedpreference.clearAllSharedPreferences();
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(),t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


}
