package com.vixspace.grocerytray.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.SplashActivity;
import com.vixspace.grocerytray.adapters.TransactionsAdapter;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.pojos.WalletLogsResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransactionFragment extends Fragment implements TransactionsAdapter.Toucher {

    public TransactionFragment(){
    }

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;

    RecyclerView recyclerTransactions;
    ProgressBar progressBar;
    TextView txtNull;

    TransactionsAdapter transactionsAdapter;
    List<WalletLogsResponse.Datum> datumList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transaction,container,false);

        allsharedpreference = new Allsharedpreference(getActivity());
        commonMethods = new CommonMethods(getActivity());

        init(view);

        return view;
    }

    private void init(View view) {

        recyclerTransactions = view.findViewById(R.id.recyclerTransactions);
        progressBar = view.findViewById(R.id.progressBar);
        txtNull = view.findViewById(R.id.txtNull);

        showLogs();

    }

    private void showLogs() {
        transactionsAdapter = new TransactionsAdapter(getActivity(),datumList,TransactionFragment.this);
        recyclerTransactions.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerTransactions.setAdapter(transactionsAdapter);

        if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
            LoadLogs();
        }else{
            showNoInterNetMessage();
        }

    }

    private void LoadLogs()  {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("Start_Date","2020-09-01");
        jsonObject.addProperty("End_Date","2021-12-31");
        jsonObject.addProperty("Whether_Date_Filter",false);
        jsonObject.addProperty("Type",0);
        jsonObject.addProperty("skip",0);
        jsonObject.addProperty("limit",30);

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.walletLogs(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){
                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final WalletLogsResponse userLoginResponse = gson.fromJson(splashScreenResponse,WalletLogsResponse.class);

                        if (userLoginResponse.getSuccess()){

                            if (userLoginResponse.getExtras().getData().size()>0){
                                txtNull.setVisibility(View.GONE);
                                datumList.addAll(userLoginResponse.getExtras().getData());
                                transactionsAdapter.notifyDataSetChanged();
                            } else {
                                txtNull.setVisibility(View.VISIBLE);
                            }

                            progressBar.setVisibility(View.GONE);

                        }
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            allsharedpreference.clearAllSharedPreferences();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }
//                        Toast.makeText(LoginActivity.this, userLoginResponse.getExtras().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    commonMethods.showToast(t.getMessage());
                }
            });
        }
        catch (Exception e){
            progressBar.setVisibility(View.GONE);
            commonMethods.showToast(e.getMessage());
        }

    }


    @Override
    public void onToucher(String Id, Boolean added) {

    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(getActivity(), getResources().getString(R.string.no_internet));
    }

}
