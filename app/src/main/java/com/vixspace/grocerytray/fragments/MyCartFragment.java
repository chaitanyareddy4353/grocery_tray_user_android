package com.vixspace.grocerytray.fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.MainActivity;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.ApplyCoupon;
import com.vixspace.grocerytray.activities.CartActivity;
import com.vixspace.grocerytray.activities.ChooseLocationActivity;
import com.vixspace.grocerytray.activities.ConfirmOrder;
import com.vixspace.grocerytray.activities.SplashActivity;
import com.vixspace.grocerytray.adapters.CartAdapter;
import com.vixspace.grocerytray.pojos.AddUserAddressResponse;
import com.vixspace.grocerytray.pojos.CartInfoModel;
import com.vixspace.grocerytray.pojos.CartModelClass;
import com.vixspace.grocerytray.pojos.GeneratePriceQuoteRequest;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class MyCartFragment extends Fragment implements CartAdapter.Items{

    public MyCartFragment() {}

    Button btnAddProducts;

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;

    ImageView imgBack;
    TextView txtUpdate,txtAddress,txtCheckOut,txtApply,txtTotalBottom,txtClearCart;
    ScrollView scrollView;
    LinearLayout linearNull,linearAddMore,linearBottom;
    String newAddress,addressID;
    RelativeLayout relativeCoupon;
    String CouponId = "";
    Boolean IsCoponApplied = false;
    RecyclerView recyclerCart;
    List< GeneratePriceQuoteRequest.CartInformation> cartInformation = new ArrayList<>();
    CartAdapter adapter;
    Double total = 0.0;

    TextView txtOrderTotal,txtTax,txtDiscount,txtDeliveryCharges,txtToPay;
    Double orderTotal=0.0,tax=0.0,discount=0.0,deliveryCharges=0.0,toPay=0.0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_cart, container, false);

        init(view);

        return view;
    }

    private void init(View view) {

        allsharedpreference = new Allsharedpreference(getActivity());
        commonMethods = new CommonMethods(getActivity());

        linearBottom = view.findViewById(R.id.linearBottom);
        txtClearCart = view.findViewById(R.id.txtClearCart);
        txtOrderTotal =view.findViewById(R.id.txtOrderTotal);
        txtTax = view.findViewById(R.id.txtTax);
        txtDiscount = view.findViewById(R.id.txtDiscount);
        txtDeliveryCharges = view.findViewById(R.id.txtDeliveryCharges);
        txtToPay = view.findViewById(R.id.txtToPay);


        txtTotalBottom = view.findViewById(R.id.txtTotalBottom);
        recyclerCart = view.findViewById(R.id.recyclerCart);
        txtApply = view.findViewById(R.id.txtApply);
        relativeCoupon = view.findViewById(R.id.relativeCoupon);
        btnAddProducts = view.findViewById(R.id.btnAddProducts);
        imgBack = view.findViewById(R.id.imgBack);
        txtUpdate = view.findViewById(R.id.txtUpdate);
        txtAddress = view.findViewById(R.id.txtAddress);
        txtCheckOut = view.findViewById(R.id.txtCheckOut);
        linearNull = view.findViewById(R.id.linearNull);
        linearAddMore = view.findViewById(R.id.linearAddMore);
        scrollView = view.findViewById(R.id.scrollView);

        imgBack.setVisibility(View.GONE);

        Gson gson = new Gson();
        String data = allsharedpreference.getShared_String(Allsharedpreference.CartData);
        CartInfoModel cartInfoModel = gson.fromJson(data, CartInfoModel.class);


        btnAddProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        txtClearCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CartInfoModel cartInfoModel1 = new CartInfoModel();
                List<CartModelClass> cartModelClasses = new ArrayList<>();
                cartModelClasses.clear();
                cartInfoModel1.setData(cartModelClasses);
                Gson gsons = new Gson();
                String jsons = gsons.toJson(cartInfoModel1); // myObject - instance of MyObject
                allsharedpreference.setShared_String(Allsharedpreference.CartData,jsons);
                scrollView.setVisibility(View.GONE);
                linearNull.setVisibility(View.VISIBLE);
                linearBottom.setVisibility(View.GONE);
            }
        });

        if (data!=null){
            if (cartInfoModel.getData().size()>0){
                CartInfoModel cartInfoModelTest = new CartInfoModel();
                List<CartModelClass> test = new ArrayList<>();
                for (int i =0;i<cartInfoModel.getData().size();i++){
                    if (cartInfoModel.getData().get(i).getQuantity()==0){

                    } else {
                        test.add(cartInfoModel.getData().get(i));
                    }
                }
                cartInfoModelTest.setData(test);
                adapter = new CartAdapter(getActivity(), cartInfoModelTest,MyCartFragment.this);
                recyclerCart.setLayoutManager(new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false));
                recyclerCart.setAdapter(adapter);

                for (int i=0;i<cartInfoModelTest.getData().size();i++){
                    orderTotal = orderTotal+(cartInfoModelTest.getData().get(i).getMRP()*cartInfoModelTest.getData().get(i).getQuantity());
                    toPay = toPay+(cartInfoModelTest.getData().get(i).getSub_Total_Amount()*cartInfoModelTest.getData().get(i).getQuantity());
                    if (i==cartInfoModelTest.getData().size()-1) {
                        total = toPay;
                        txtOrderTotal.setText("₹"+String.valueOf(orderTotal));
                        txtToPay.setText("₹"+String.valueOf(toPay));
                        txtTotalBottom.setText("₹"+String.valueOf(toPay));
                        txtDiscount.setText("- ₹" + String.valueOf(orderTotal-toPay));
                    }
                }
            } else {
                scrollView.setVisibility(View.GONE);
                linearNull.setVisibility(View.VISIBLE);
                linearBottom.setVisibility(View.GONE);
            }
        } else {
            scrollView.setVisibility(View.GONE);
            linearNull.setVisibility(View.VISIBLE);
            linearBottom.setVisibility(View.GONE);
        }



//        for (int i =0;i<cartInfoModel.getData().size();i++){
//            GeneratePriceQuoteRequest.CartInformation cartInformation1 = new GeneratePriceQuoteRequest.CartInformation();
//            cartInformation1.setLocalityProductID(cartInfoModel.getData().get(i).getLocalityProductID());
//            cartInformation1.setQuantity(cartInfoModel.getData().get(i).getQuantity());
//            cartInformation.add(cartInformation1);
//        }



        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
//                finish();
            }
        });

        txtUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ChooseLocationActivity.class);
                intent.putExtra("indicator",2);
                startActivityForResult(intent,101);
            }
        });

        btnAddProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        linearAddMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        txtCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ConfirmOrder.class);
                intent.putExtra("addressId",addressID);
                intent.putExtra("total",total);
                intent.putExtra("addressint",newAddress);
                intent.putExtra("discount",txtDiscount.getText().toString().trim());
                intent.putExtra("couponapplied",IsCoponApplied);
                if (IsCoponApplied){
                    intent.putExtra("couponID",CouponId);
                }

                startActivity(intent);
            }
        });

        relativeCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ApplyCoupon.class);
                startActivityForResult(intent,199);
            }
        });

        txtApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ApplyCoupon.class);
                startActivityForResult(intent,199);
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==101&& resultCode == RESULT_OK){
            String lat = data.getStringExtra("lat");
            String lon = data.getStringExtra("lon");
            String strAddress = data.getStringExtra("address");
            String city  = data.getStringExtra("city");
            String state = data.getStringExtra("state");
            String pincode = data.getStringExtra("pin");
            String landmark = data.getStringExtra("landmark");
            String addressint = data.getStringExtra("addressint");
            Integer radio = data.getIntExtra("radio",0);

            allsharedpreference.setShared_String(Allsharedpreference.SELECTED_LON, String.valueOf(lon));
            allsharedpreference.setShared_String(Allsharedpreference.SELECTED_LAT, String.valueOf(lat));

            newAddress = (landmark+strAddress +", " +city+ ", " + state +", " + pincode);


            if (NetWorkUtil.IsNetworkAvailable(getActivity())) {
                validateAddress(lat,lon,1,newAddress,addressint,radio);
            }else{
                showNoInterNetMessage();
            }

        } else if (requestCode==199&& resultCode == RESULT_OK){
//            CouponAmt = data.getStringExtra("couponAmt");
            CouponId = data.getStringExtra("couponId");
//            Total = data.getStringExtra("total");
            IsCoponApplied = true;
//            txtApplyCoupon.setText("₹ "+CouponAmt);
//            txtOrderTotal.setText("₹ "+Total);
//            txtbottomOrderTotal.setText("₹ "+Total);
        }



    }

    private void validateAddress(String lat, String lon, Integer indicator, final String newAddress, String addressint, Integer radio) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("Name",allsharedpreference.getShared_String(Allsharedpreference.UserName));
        jsonObject.addProperty("PhoneNumber",allsharedpreference.getShared_String(Allsharedpreference.PhoneNumber));
        jsonObject.addProperty("AddressType",radio);
        jsonObject.addProperty("Address",addressint);
        jsonObject.addProperty("Latitude",lat);
        jsonObject.addProperty("Longitude",lon);

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.addAddress(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final AddUserAddressResponse loginResponse = gson.fromJson(splashScreenResponse,AddUserAddressResponse.class);

                        if (loginResponse.getSuccess()) {

                            addressID = loginResponse.getExtras().getData().getAddressID();
//                            allsharedpreference.setShared_String(Allsharedpreference.CurrentLocation,loginResponse.getExtras().getData().getLocalityData().getCityTitle());
//                            allsharedpreference.setShared_String(Allsharedpreference.CurrentCityID,loginResponse.getExtras().getData().getLocalityData().getCityID());
//                            allsharedpreference.setShared_String(Allsharedpreference.LocalityID,loginResponse.getExtras().getData().getLocalityData().getLocalityID());
//                            allsharedpreference.setShared_String(Allsharedpreference.Locality_Title,loginResponse.getExtras().getData().getLocalityData().getLocalityTitle());
                            txtAddress.setText(newAddress);

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            getActivity().finish();
                        }

//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    @Override
    public void onItemTouched(CartInfoModel cartInfoModel, Integer position,Boolean isAdd,  List<CartModelClass> cartModelClasses) {
        Double orderTotal= 0.0;
        Double toPay= 0.0;

        if (cartInfoModel.getData().size()>0){
            Gson gson = new Gson();
            String json = gson.toJson(cartInfoModel); // myObject - instance of MyObject
            allsharedpreference.setShared_String(Allsharedpreference.CartData,json);
            for (int i=0;i<cartInfoModel.getData().size();i++){
                orderTotal = orderTotal+(cartInfoModel.getData().get(i).getMRP()*cartInfoModel.getData().get(position).getQuantity());
                toPay = toPay+(cartInfoModel.getData().get(i).getSub_Total_Amount()*cartInfoModel.getData().get(position).getQuantity());
                if (i==cartInfoModel.getData().size()-1) {
                    total = toPay;
                    txtOrderTotal.setText("₹"+String.valueOf(orderTotal));
                    txtToPay.setText("₹"+String.valueOf(toPay));
                    txtTotalBottom.setText("₹"+String.valueOf(toPay));
                    txtDiscount.setText("- ₹" + String.valueOf(orderTotal-toPay));
                }
            }
        }  else {
            CartInfoModel cartInfoModel1 = new CartInfoModel();
            List<CartModelClass> list = new ArrayList<>();
            cartInfoModel1.setData(list);
            Gson gsons = new Gson();
            String jsons = gsons.toJson(cartInfoModel1); // myObject - instance of MyObject
            allsharedpreference.setShared_String(Allsharedpreference.CartData,jsons);
            Intent intent = new Intent(getActivity(),MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(getActivity(), getResources().getString(R.string.no_internet));
    }

}
