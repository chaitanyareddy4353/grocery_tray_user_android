package com.vixspace.grocerytray.services;


import com.vixspace.grocerytray.utils.AllVariables;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestApiClientImage {

    public static final String BASE_URL = AllVariables.URL_HOST1;
//    public static final String BASE_URL_UPLOAD = "https://api.belt2door.com/upload/";

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(1000, TimeUnit.SECONDS)
                .readTimeout(1000, TimeUnit.SECONDS).build();
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL).client(client)
                    .addConverterFactory(GsonConverterFactory.create(new Gson()))
                    .build();
        }
        return retrofit;
    }

//    public static Retrofit getImageClient() {
//        OkHttpClient client = new OkHttpClient.Builder()
//                .connectTimeout(1000, TimeUnit.SECONDS)
//                .readTimeout(1000, TimeUnit.SECONDS).build();
//        if (retrofit == null) {
//            retrofit = new Retrofit.Builder()
//                    .baseUrl(BASE_URL_UPLOAD).client(client)
//                    .addConverterFactory(GsonConverterFactory.create(new Gson()))
//                    .build();
//        }
//        return retrofit;
//    }

}

