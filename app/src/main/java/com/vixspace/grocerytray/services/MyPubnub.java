package com.vixspace.grocerytray.services;

import android.content.Context;
import android.util.Log;

import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

public class MyPubnub {

    private static final String TAG = "MyPubnubMessage";
    private static  MyPubnub myPubnub;
    private static PNConfiguration pnConfiguration;
    private static PubNub pubnub;
    private Allsharedpreference sessionMgr;
    private static Context appContext;

    public static MyPubnub getInstance(Context context)
    {
        if(myPubnub == null)
        {
            //Log.i("MyPubnubTest MyPubnub", "getInstance myPubnub == null ");
            myPubnub = new MyPubnub(context);
            appContext=context.getApplicationContext();
        }
        return myPubnub;
    }
    /******************************************************/

    private MyPubnub(Context context)
    {
        sessionMgr = new Allsharedpreference(context);
        //pnConfiguration = getPubnubConfiguration();
        initPubNub();
    }
    /************************************************************/

    public void initPubNub()
    {
        //Log.i("MyPubnubTest MyPubnub", "initPubNub ");
        pnConfiguration = getPubnubConfiguration();
        pubnub = getPubnubInstance();
    }
    /************************************************************/
    public void subscribeRequestChannel() {
        try {
            pubnub.subscribe()
                    .withPresence()
                    .channels(Arrays.asList("Oct_Order_Request_Status"))
                    .execute(); // subscribe to channels.execute();
            pubnub.addListener(subscribeCallbackOrderRequest);
        } catch (Exception e) {
            Log.i("MyPubnubTest MyPubnub", e.toString());
            e.printStackTrace();
        }
    }
    public void unSubscriveReqestChannel(){
        try{
            pubnub.removeListener(subscribeCallbackOrderRequest);
            pubnub.unsubscribe()
                    .channels(Arrays.asList(sessionMgr.getPresenceChannel(), sessionMgr.getListenerChannel()))
                    .execute();
            sessionMgr.setIsPubnubSubscribed(false);
        }
        catch (Exception e){

        }
    }
    SubscribeCallback subscribeCallbackOrderRequest = new SubscribeCallback() {
        @Override
        public void status(PubNub pubnub, PNStatus status)
        {
            Log.v("Pubnub Response", String.valueOf(status.getCategory()));
        }

        @Override
        public void message(PubNub pubnub, PNMessageResult message) {

            Log.v("Pubnub Response", String.valueOf(message.getMessage()));
            try {
                JSONObject pubnubMessage= new JSONObject(String.valueOf(message.getMessage()));

                JSONObject InstantOrderRequestData = pubnubMessage.getJSONObject("InstantOrderRequestData");
                int type = pubnubMessage.getInt("type");
                String InstantOrderRequestID = InstantOrderRequestData.getString("InstantOrderRequestID");
//                if (InstantOrderRequestID.equals(sessionMgr.getInstantOrderRequestID())){
//                    if(type==1){
//                        if (sessionMgr.getShared_Int(Allsharedpreference.Trader_Order_Type)==1){
//                            Intent intent = new Intent("com.evontex.octopus.singleorderrequeststatus");
//                            intent.putExtra("type",1);
//                            appContext.sendBroadcast(intent);
//                        }
//                        else if(sessionMgr.getShared_Int(Allsharedpreference.Trader_Order_Type)==2||sessionMgr.getShared_Int(Allsharedpreference.Trader_Order_Type)==3){
//                            Intent intent = new Intent("com.evontex.octopus.multipleorderrequeststatus");
//                            intent.putExtra("type",1);
//                            appContext.sendBroadcast(intent);
//                        }
//
//                    }
//                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        @Override
        public void presence(PubNub pubnub, PNPresenceEventResult presence) {
            Log.d(TAG, "presence " + presence.getEvent());
        }
    };
    private PNConfiguration getPubnubConfiguration()
    {
        PNConfiguration pnConfig = new PNConfiguration();
        pnConfig.setPublishKey(sessionMgr.getPubnubPublishKey());
        pnConfig.setSubscribeKey(sessionMgr.getPubnubSubscribeKey());

       // pnConfig.setPresenceTimeoutWithCustomInterval(30, 10);
        //Log.i("MyPubnub", "setPNConfig emailId: "+emailId +" pnConf"+pnConfiguration.getUuid());

        //Log.i("MyPubnubTest MyPubnub", "getPubnubConfiguration");
        return pnConfig;
    }
    public synchronized PubNub getPubnubInstance()
    {
        if(pubnub == null)
        {
            if (pnConfiguration == null)
            {
                pnConfiguration = getPubnubConfiguration();
            }
            pubnub = new PubNub(pnConfiguration);
        }
        return pubnub;
    }
    SubscribeCallback subscribeCallback = new SubscribeCallback() {
        @Override
        public void status(PubNub pubnub, PNStatus status)
        {

        }

        @Override
        public void message(PubNub pubnub, PNMessageResult message) {

            Log.v("Pubnub Response", String.valueOf(message.getMessage()));

        }
        @Override
        public void presence(PubNub pubnub, PNPresenceEventResult presence) {
            Log.d(TAG, "presence " + presence.getEvent());
        }
    };
    public void subscribe(String ChannelID) {
        try {
            pubnub.subscribe()
                    .withPresence()
                    .channels(Arrays.asList(ChannelID))
                    .execute(); // subscribe to channels.execute();

            pubnub.addListener(subscribeCallback);
        } catch (Exception e) {
            //Log.i("MyPubnubTest MyPubnub", "subscribe() exception");
            e.printStackTrace();
        }
    }


    /***************************************************************/

    /************************************************************/
    public void publishOnPubnub(final String channelName, final HashMap message)
    {
        if (pubnub != null)
        {
            Log.v("MyPubnub", "publishOnPubnub: "+message);
            pubnub.publish()
                    .channel(channelName).message(message)
                    .async(new PNCallback<PNPublishResult>()
                    {
                        //PublishOnPubnubCallBack publishOnPubnubCallBack;
                        @Override
                        public void onResponse(PNPublishResult result, PNStatus status)

                        {

                            String statusdata = " statuscode " + status.getStatusCode() + "statuserrdata " + status.getErrorData()
                                    + " statuseffchanel " + status.getAffectedChannelGroups() + " statuscat " + status.getCategory()
                                    + " statusclientrequest " + status.getClientRequest() ;


                            // Check whether request successfully completed or not.
                            /*if (!status.isError())
                            {
                                // Message successfully published to specified channel.
                                //Log.i("MyPubnub", "publishOnPubnub channelName: " + channelName + "  result: " +
                                result.getTimetoken()+"  status: " + status+"  message: "+message.toString());
                                //pubnubPublishCallBack.pubnubPublishedStatusCallBack(true);
                            }
                            // Request processing failed.
                            else
                            {

                                // Handle message publishOnPubnub error. Check 'category' property to find out possible issue
                                // because of which request did fail.
                                // Request can be resent using: [status retry];
                                Log.i("MyPubnub", "publishOnPubnub error: " +   result+ "  status: " + status+"  message: "+message.toString());

                            }*/
                            if (result != null)
                            {

                                Log.v("Loggingapp 5","newsetofdata" + " result " + result.getTimetoken() +  " status " + statusdata);

                                Log.i("MyPubnub", "publishOnPubnub timeToken: "+result.getTimetoken());
                            }

                        }
                    });
        }
    }
    /************************************************************/

    public void stopPubnub()
    {

        pnConfiguration = null;
        pubnub = null;
        myPubnub = null;
    }
    /************************************************************/

}
