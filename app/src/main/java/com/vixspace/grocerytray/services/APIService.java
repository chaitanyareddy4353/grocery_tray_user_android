package com.vixspace.grocerytray.services;

import com.vixspace.grocerytray.pojos.ImageUploadResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.utils.AllVariables;
import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIService {

    @POST(AllVariables.Splash_Screen)
    Call<JsonObject> getSplashScreenResponse(@Body JsonObject jsonObject);

    @POST(AllVariables.Customer_Update_FCM_Token)
    Call<JsonObject> fcm(@Body JsonObject jsonObject);

    @POST(AllVariables.Generate_Login_OTP)
    Call<JsonObject> generateOTP(@Body JsonObject jsonObject);

    @POST(AllVariables.Validate_Login_OTP)
    Call<JsonObject> validateotp (@Body JsonObject jsonObject);

    @POST(AllVariables.Update_User_Basic_Information)
    Call<JsonObject> updateUserinfo (@Body JsonObject jsonObject);

    @POST(AllVariables.Update_User_Image_Information)
    Call<UpdateFcmResponse> updateUserImage (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_Default_Locality_Setting)
    Call<JsonObject> defaultLocality(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_User_Locality_From_Lat_Long)
    Call<JsonObject> userLocality(@Body JsonObject jsonObject);

    @POST(AllVariables.Razorpay_Add_Amount_To_Wallet_Fetch_Complete_Information)
    Call<JsonObject> razorPay(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_User_Wallet_Information)
    Call<JsonObject> walletInformation(@Body JsonObject jsonObject);

    @POST(AllVariables.Filter_All_User_Wallet_Logs)
    Call<JsonObject> walletLogs(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Subscription_Products)
    Call<JsonObject> allSubscriptionProducts(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Home_Slider_Items)
    Call<JsonObject> allHomeSlideritems(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Deal_of_Day_Products)
    Call<JsonObject> allDealOfDayProducts(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Locality_Categories )
    Call<JsonObject> fetchAllLocalityCategories (@Body JsonObject jsonObject);

    @POST(AllVariables.Add_User_Address)
    Call<JsonObject> addAddress(@Body JsonObject jsonObject);

    @POST(AllVariables.List_All_User_Address)
    Call<JsonObject> allAddresses(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_Category_Complete_Information)
    Call<JsonObject> categoryCompleteInformation (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Category_Collections )
    Call<JsonObject> allCategoryCollections(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_Collection_Complete_Information)
    Call<JsonObject> collectionCompleteInformation(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Collection_Products)
    Call<JsonObject> allCollectionProducts(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_Product_Complete_Information )
    Call<JsonObject> productCompleteInformation(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_Locality_Product_Complete_Information)
    Call<JsonObject> localityProductCompleteInformation(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Available_Offer_Coupons)
    Call<JsonObject> availableOfferCoupons(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_Offer_Coupon_Complete_Information)
    Call<JsonObject> couponCompleteInformation(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_Complete_City_Settings)
    Call<JsonObject> citySettings(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Available_Slots)
    Call<JsonObject> allAvailableSlots(@Body JsonObject jsonObject);

    @POST(AllVariables.Subscribe_Subscription_Product)
    Call<JsonObject> subscribe(@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Active_Subscription_Products)
    Call<JsonObject> allActiveSubscriptionProducts(@Body JsonObject jsonObject);

    @POST(AllVariables.Update_Subscription_Information)
    Call<JsonObject> updateSubscriptionInformation(@Body JsonObject jsonObject);

    @POST(AllVariables.Pause_Subscription)
    Call<JsonObject> pauseSubscription(@Body JsonObject jsonObject);

    @POST(AllVariables.Activate_Subscription_Service)
    Call<JsonObject> activateSubscription (@Body JsonObject jsonObject);

    @POST(AllVariables.Terminate_Subscription)
    Call<JsonObject> terminateSubscription (@Body JsonObject jsonObject);

    @POST(AllVariables.Send_Next_Available_Dates)
    Call<JsonObject> sendNextAvailableDates (@Body JsonObject jsonObject);

    @POST(AllVariables.Generate_Order_Price_Quote)
    Call<JsonObject> generateOrderPriceQuote (@Body JsonObject jsonObject);

    @POST(AllVariables.Apply_Offer_Coupon_Code)
    Call<JsonObject> applyOfferCouponCode (@Body JsonObject jsonObject);

    @POST(AllVariables.Place_Order)
    Call<JsonObject> placeOrder (@Body JsonObject jsonObject);

    @POST(AllVariables.Logout)
    Call<JsonObject> logOut (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_Subscription_Complete_Information)
    Call<JsonObject> subscriptionCompleteInformation (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Subscription_Product_Deliveries)
    Call<JsonObject> allSubscriptionProductDeliveries (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_Subscription_Task_Complete_Information)
    Call<JsonObject> subscriptionTaskCompleteInformation (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Upcoming_Orders)
    Call<JsonObject> upcomingOrders (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Past_Orders)
    Call<JsonObject> pastOrders (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Active_Orders)
    Call<JsonObject> activeOrders (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_Order_Complete_Information)
    Call<JsonObject> orderComplete (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_Unrated_Orders)
    Call<JsonObject> unRatedOrders (@Body JsonObject jsonObject);

    @POST(AllVariables.Submit_Order_Rating)
    Call<JsonObject> submitOrderRating (@Body JsonObject jsonObject);

    @POST(AllVariables.Cancel_Order)
    Call<JsonObject> cancelOrders (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Favourite_Products)
    Call<JsonObject> allFav (@Body JsonObject jsonObject);

    @POST(AllVariables.User_Remove_Favourite_Product)
    Call<JsonObject> removeFav (@Body JsonObject jsonObject);

    @POST(AllVariables.Save_Favourite_Product)
    Call<JsonObject> saveFav (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_User_Complete_Information)
    Call<JsonObject> userInfo (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Paused_Subscription_Products)
    Call<JsonObject> pausedSubs (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Terminated_Subscription_Products)
    Call<JsonObject> terminatedSubs (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_All_Related_Products)
    Call<JsonObject> related (@Body JsonObject jsonObject);

    @POST(AllVariables.Search_All_Products)
    Call<JsonObject> search (@Body JsonObject jsonObject);

    @POST(AllVariables.Remove_All_Favourite_Products)
    Call<JsonObject> removeAllFav (@Body JsonObject jsonObject);

    @POST(AllVariables.Reserve_Amount)
    Call<JsonObject> reserve (@Body JsonObject jsonObject);

    @POST(AllVariables.Fetch_Last_Wallet_Information)
    Call<JsonObject> lastWallet (@Body JsonObject jsonObject);

    @POST(AllVariables.Edit_Whether_Ring)
    Call<JsonObject> ring (@Body JsonObject jsonObject);

    @Multipart
    @POST(AllVariables.URL_Upload_Image)
    Call<ImageUploadResponse> uploadImage(@Part MultipartBody.Part body);

}
