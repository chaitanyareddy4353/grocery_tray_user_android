package com.vixspace.grocerytray.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;

import com.google.android.material.textfield.TextInputLayout;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Utility {
    public static void printLog(String... msg)
    {
        boolean logs = true;
        String str="";
        for(String i : msg)
        {
            str= str+"\n"+i;
        }
        if(logs)
        {
        }
    }



    public static double StringtoDecimalConvert(String Number){

        if(!Number.equals("")) {
            double decimalNumber = Double.parseDouble(Number);

            NumberFormat nf_out = NumberFormat.getNumberInstance(Locale.US);
            nf_out.setMaximumFractionDigits(2);
            nf_out.setMinimumFractionDigits(2);
            nf_out.setGroupingUsed(false);

            Number = nf_out.format(decimalNumber);

            //Log.i("W99l", "Number Value"+Number+" "+decimalNumber);
            decimalNumber = Double.parseDouble(Number);

            return Double.valueOf(decimalNumber);
        }else {
            return 0;
        }

    }


    /************************************************    for checking internet connection*******/
    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager connectivity = null;
        boolean isNetworkAvail = false;
        try {
            connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for(int i = 0; i < info.length; i++)
                        if (info[i].getState() == NetworkInfo.State.CONNECTED)
                        {
                            printLog("info for network",info[i].getTypeName());
                            return true;
                        }
                }
            }
            return false;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if (connectivity != null) {
                connectivity = null;
            }
        }
        return isNetworkAvail;
    }
    /************************************************ Round Off the decimal values*******/

    public static double roundTwoDecimals(double d)
    {
        NumberFormat nf_out = NumberFormat.getNumberInstance(Locale.US);
        nf_out.setMaximumFractionDigits(2);
        nf_out.setGroupingUsed(false);
        //DecimalFormat twoDForm = new DecimalFormat("#####################.##");
        return Double.valueOf(nf_out.format(d));
    }

    public static String roundOfDoubleValue(double number){

        DecimalFormat twoDForm = new DecimalFormat("#.00");
        return String.format("%.2f", Double.valueOf(twoDForm.format(number)));
        //return Double.valueOf(twoDForm.format(decimalNumber))+"";
    }
    /************************************************   show the alert*******/

    public static void showVisibillityAnimation(final View view){

        // Prepare the View for the animation

//        view.setAlpha(0.0f);

        // Start the animation
        view.animate()
                .translationX(0)
                .alpha(1.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationStart(animation);
                        view.setVisibility(View.VISIBLE);
                    }
                });
    }
    public static void showGoneAnimation(final View view){
        view.animate()
                .translationY(view.getHeight())
                .alpha(1.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        view.setVisibility(View.GONE);
                    }
                });


    }

    public static void hideSoftKeyboard(Activity mActivity) {

        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = mActivity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(mActivity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }

    public static Animation inFromRightAnimation() {

        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }
    public static Animation outToRightAnimation() {
        Animation outtoRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoRight.setDuration(500);
        outtoRight.setInterpolator(new AccelerateInterpolator());
        return outtoRight;
    }
    public static Animation outToLeftAnimation() {
        Animation outtoLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoLeft.setDuration(500);
        outtoLeft.setInterpolator(new AccelerateInterpolator());
        return outtoLeft;
    }
    public static  Animation inFromLeftAnimation() {
        Animation inFromLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromLeft.setDuration(500);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;
    }
    public static void validateUsername(Editable editable, TextInputLayout inputLayout){
        if (editable.length()==0){
            inputLayout.setError("Name can't be empty");
        }
        else{
            inputLayout.setError(null);
        }
    }
    public static void validateQuantity(Editable editable, TextInputLayout inputLayout){
        if (editable.length()==0){
            inputLayout.setError("Quantity can't be empty");
        }
        else{
            inputLayout.setError(null);
        }
    }
    public static void validateMobileNumber(Editable editable, TextInputLayout inputLayout){
        if (editable.length()==0){
            inputLayout.setError("Mobile number can't be empty");
        }
        else if (editable.length()<10){
            inputLayout.setError("That was an invalid phone number");
        }
        else{
            inputLayout.setError(null);
        }
    }
    public static void validatePassword(Editable editable, TextInputLayout inputLayout){
        if (editable.length()==0){
            inputLayout.setError("Password cant be empty");
        }
        else{
            inputLayout.setError(null);
        }
    }
    public static void validateNumberOfConsultations(Editable editable, TextInputLayout inputLayout){
        if (editable.length()==0){
            inputLayout.setError("Number of consultations an't be empty");
        }
        else{
            inputLayout.setError(null);
        }
    }
    public static void ConsultaionFee(Editable editable, TextInputLayout inputLayout){
        if (editable.length()==0){
            inputLayout.setError("Consultation fee can't be empty");
        }
        else{
            inputLayout.setError(null);
        }
    }
    public static void validateEmail(Editable editable, TextInputLayout inputLayout){
        if (editable.length()==0){
            inputLayout.setError("email can't be empty");
        }
        else{
            inputLayout.setError(null);
        }
    }
    public static void validateDob(Editable editable, TextInputLayout inputLayout){
        if (editable.length()==0){
            inputLayout.setError("Date of birth can't be empty");
        }
        else{
            inputLayout.setError(null);
        }
    }
    public static void ShowAlert(String msg, final Context context)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // set title
        alertDialogBuilder.setTitle("alert");

        // set dialog message
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)

                .setNegativeButton("ok",new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        //closing the application

                        dialog.dismiss();

                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();

    }

    public static String getOrderDate(String getdate){
        String returnStringDate="";

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat fort=new SimpleDateFormat("MMM dd , hh:mm a");
        Date date;

        try {
            date = format.parse(getdate);
            String eventDate = fort.format(date);
            returnStringDate=eventDate;

        }
        catch (Exception e){
            Utility.printLog(" error  in datte" + e);
        }



        return returnStringDate;

    }
    public static String getSelectedDate(int year,int monthOfYear,int dayOfMonth){
        String mSelectedDate="";
        if (monthOfYear >= 9) {
            mSelectedDate = String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                    + "-" + String.valueOf(year);
        } else {

            mSelectedDate = String.valueOf(dayOfMonth) + "-" + "0" + String.valueOf(monthOfYear + 1)
                    + "-" + String.valueOf(year);
        }

        return  mSelectedDate;
    }

    /********************************************************************************************************************/
    //GETTING CURRENT DATE
    public static String date_month(){
        Calendar calendar = Calendar.getInstance(Locale.US);
        Date date = calendar.getTime();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        return formater.format(date);
    }
    /********************************************************************************************************************/

    /************************************************    for checking location service*******/
    public static boolean isLocationAvailable(Context context){
        LocationManager locationManager = null;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * *******************************************************************************************
     * for getting location coordinates, it will provide last location and will call refresh
     * ************************************************************************************************
     */



    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }


    public static String getLocallity(double lat, double lang,Context context){


        String cityName="";
        try {
            String location = "theNameOfTheLocation";
            Geocoder gc = new Geocoder(context);
            List<Address> addresses= gc.getFromLocation(lat, lang,1); // get the found Address Objects
            cityName = addresses.get(0).getLocality();
        } catch (IOException e) {
            // handle the exception
        }
        return cityName;

    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }



    //network connection status
    public static boolean isConnectingToInternet(Context mcontext) {

        ConnectivityManager connectivity = (ConnectivityManager) mcontext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        //Log.d("Network", "NETWORKnAME: "+info[i].getTypeName());
                        return true;
                    }
        }
        return false;
    }
    @SuppressLint("MissingPermission")
    public static String getDeviceId(Context context)
    {

        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();

        //return "123456789101112DummyId2";

    }


    //GETTING CURRENT DATE
    public static String date(){
        Calendar calendar = Calendar.getInstance(Locale.US);
        Date date = calendar.getTime();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        return formater.format(date);
    }
    /********************************************************************************************************************/

    //Process dialog

    public static ProgressDialog GetProcessDialog(Activity activity)
    {
        // prepare the dialog box
        ProgressDialog dialog = new ProgressDialog(activity);
        // make the progress bar cancelable
        dialog.setCancelable(true);
        // set a message text
        dialog.setMessage("Loading...");

        // show it
        return dialog;
    }


//    public static boolean checkPlayServices(Activity activity, int PLAY_SERVICES_RESOLUTION_REQUEST)
//    {
//        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
//        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
//        if (resultCode != ConnectionResult.SUCCESS)
//        {
//            if (apiAvailability.isUserResolvableError(resultCode))
//            {
//
//                apiAvailability.getErrorDialog(activity,
//                        PLAY_SERVICES_RESOLUTION_REQUEST,resultCode).show();
//            }
//            else
//            {
//                activity.finish();
//            }
//            return false;
//        }
//        return true;
//    }

    public   static boolean isApplicationSentToBackground(final Context context)
    {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty())
        {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName()))
            {
                return true;
            }
        }
        return false;
    }
}
