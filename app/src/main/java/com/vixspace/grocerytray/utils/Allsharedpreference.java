package com.vixspace.grocerytray.utils;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.util.HashMap;

public class Allsharedpreference {

    public static final String PREFS_NAME = "B2DUser";
    public static final String PhoneNumber = "PhoneNumber";
    public static final String IS_ALREADY_LOGIN = "IS_ALREADY_LOGIN";
    public static final String Registered = "Registered";
    public static final String AgentID = "AgentID";
    public static final String API_KEY = "API_KEY";
    public static final String UserName = "UserName";
    public static final String NOT_FIRST_TIME_LOGIN = "FIRST_TIME_LOGIN";

    public static final String SessionId = "SessionId";
    public static final String USER_ID = "User_Id";
    public static final String EmailId = "EmailId";
    public static final String JoinedDate = "JoinedDate";
    public static final String img = "img";
    public static final String imgAvailable = "bagPicker";
    public static final String salesExecutive = "salesExecutive";
    public static final String viewSales = "viewSales";
    public static final String firstName = "firstName";
    public static final String secondName = "secondName";
    public static final String Role = "Role";
    public static final String Airport = "Airport";
    public static final String CurrentLocation = "CurrentLocation";
    public static final String CurrentCityID = "CurrentCityID";
    public static final String LocalityID = "LocalityID";
    public static final String Locality_Title = "Locality_Title";
    public static final String Skip = "Skip";
    public static final String Location = "Location";
    public static final String SELECTED_LON = "SELECTED_LON";
    public static final String SELECTED_LAT = "SELECTED_LAT";
    public static final String NotfirstTimeHomeScreen = "NotfirstTimeHomeScreen";
    public static final String CartData = "CartData";
    public static final String ring = "ring";

    public HashMap<String, Integer> mDiffrentRoadSideds;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    public Allsharedpreference(Context context) {

        this._context = context;
        pref = _context.getSharedPreferences(PREFS_NAME, PRIVATE_MODE);
        editor = pref.edit();

    }

    public void setShared_String(String key, String sKEY_IS_EDITTEXT) {
        editor.putString(key, sKEY_IS_EDITTEXT);
        editor.commit();
    }

    public String getShared_String(String key) {
        return pref.getString(key, null);
    }

    public void setShared_Int(String key, int sKEY_IS_EDITTEXT) {
        editor.putInt(key, sKEY_IS_EDITTEXT);
        editor.commit();
    }

    public int getShared_Int(String key) {
        return pref.getInt(key, 0);
    }

    public void setShared_Booleen(String key, Boolean sKEY_IS_EDITTEXT) {
        editor.putBoolean(key, sKEY_IS_EDITTEXT);
        editor.commit();
    }

    public Boolean getShared_Booleen(String key) {
        return pref.getBoolean(key, false);
    }


    public void movetoactivtywithfinish(Class movetoanotheractivty) {

        Intent moveintent = new Intent(_context.getApplicationContext(), movetoanotheractivty);
        moveintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        _context.startActivity(moveintent);

    }



    public void movetoactivtywithoutfinish(Class movetoanotheractivty) {

        Intent moveintent = new Intent(_context.getApplicationContext(), movetoanotheractivty);
        _context.startActivity(moveintent);


    }

    public void showToast(String sdf) {

        Toast.makeText(_context, sdf, Toast.LENGTH_LONG).show();


    }
    ////////////////////////////////////////////

    public String getInstantOrderRequestID()
    {
        return pref.getString("InstantOrderRequestID", "");
    }
    /***************************************************************/

    public void setInstantOrderRequestID(String pasChn)
    {
        editor.putString("InstantOrderRequestID", pasChn);
        editor.apply();
    }

    public void setPubnubUserUID(String UUID)
    {
        editor.putString("UUID", UUID);
        editor.apply();
    }

    public String getPubnubUserUID()
    {
        return pref.getString("UUID", "");
    }
    /***************************************************************/

    public void setRazorpayKeyID(String razorpayKeyID)
    {
        editor.putString("RazorpayKeyID", razorpayKeyID);
        editor.apply();
    }

    public String getRazorpayKeyID()
    {
        return pref.getString("RazorpayKeyID", "");
    }
    /***************************************************************/


    public void setPubnubPublishKey(String pubnubPublishKey)
    {
        editor.putString("PUBNUB_PUBLISH_KEY", pubnubPublishKey);
        editor.apply();
    }

    public String getPubnubPublishKey()
    {
        return pref.getString("PUBNUB_PUBLISH_KEY", "");
    }
    /***************************************************************/

    public void setPubnubSubscribeKey(String pubnubSubscribeKey)
    {
        editor.putString("PUBNUB_SUBSCRIBE_KEY", pubnubSubscribeKey);
        editor.apply();
    }

    public String getPubnubSubscribeKey()
    {
        return pref.getString("PUBNUB_SUBSCRIBE_KEY", "");
    }
    /***************************************************************/

    // used to listen and notify our presence and leave to server
    public void setPresenceChannel(String presenceChannel)
    {
        editor.putString("PRESENCE_CHANNEL", presenceChannel);
        editor.apply();
    }

    public String getPresenceChannel()
    {
        return pref.getString("PRESENCE_CHANNEL", "");
    }
    /***************************************************************/

    // this channel is used to listen from server
    public void setListenerChannel(String listenerChannel)
    {
        editor.putString("LISTENER_CHANNEL", listenerChannel);
        editor.apply();
    }
    public String getListenerChannel()
    {
        return pref.getString("LISTENER_CHANNEL", "");
    }
    /***************************************************************/

    // to publish our location to server
    public void setServerChannel(String serverChannel)
    {
        editor.putString("SERVER_CHANNEL", serverChannel);
        editor.apply();
    }
    public String getServerChannel()
    {
        return pref.getString("SERVER_CHANNEL", "");
    }
    /***************************************************************/

    // to publish our location to client on driver channel for live tracking
    public void setDriverChannel(String driverChannel)
    {
        editor.putString("DRIVER_CHANNEL", driverChannel);
        editor.apply();
    }
    public String getDriverChannel()
    {
        return pref.getString("DRIVER_CHANNEL", "");
    }
    /***************************************************************/

    // used to send order status notifications to passenger when in booking
    public void setPasChannel(String pasChn)
    {
        editor.putString("PASSENGER_CHANNEL", pasChn);
        editor.apply();
    }
    public String getPasChannel()
    {
        return pref.getString("PASSENGER_CHANNEL", "");
    }
    /***************************************************************/

    // used to send order status notifications to passenger when in booking
    public void setIsPubnubSubscribed(boolean isPresenceSubscribed)
    {
        editor.putBoolean("IS_PUBNUB_SUBSCRIBED", isPresenceSubscribed);
        editor.apply();
    }
    public boolean getIsPubnubSubscribed()
    {
        return pref.getBoolean("IS_PUBNUB_SUBSCRIBED", false);
    }

    /***************************************************/

    public void setMinimumWeight(int minimumWeight)
    {
        editor.putInt("MINIMUM_WEIGHT", minimumWeight);
        editor.apply();
    }

    public int getMinimumWeight()
    {
        return pref.getInt("MINIMUM_WEIGHT", 0);
    }
    public void setMaximumWeight(int maximumWeight)
    {
        editor.putInt("MAXIMUM_WEIGHT", maximumWeight);
        editor.apply();
    }

    public int getMaximumWeight()
    {
        return pref.getInt("MAXIMUM_WEIGHT", 0);
    }

    public void clearAllSharedPreferences() {
        editor.clear();
        editor.apply();
    }
}
