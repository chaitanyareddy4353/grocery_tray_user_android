package com.vixspace.grocerytray.utils;

import android.content.Context;
import android.widget.Toast;

public class PrintMsg {
    public static void printLongToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void printShortToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

}
