package com.vixspace.grocerytray.utils;

public class Constants {

    public static String PREF_NAME = "my_pref";

    public static String ApiKEY = "ApiKey";

    public static String DeviceID = "DeviceID";

    public static  String IS_USER_ALREADY_LOGIN = "IS_USER_ALREADY_LOGIN";

    public static  String IS_ALREADY_LOGIN = "IS_ALREADY_LOGIN";

    public static  String NOT_FIRST_TIME_LOGIN = "FIRST_TIME_LOGIN";

    public static String ReferralAmount = "ReferralAmount";

    public static String DepositAmount = "DepositAmount";

    public static String WinningsAmount ="WinningsAmount";

    public static String WalletAmount ="WalletAmount";

    public static String Referral ="Referral";

    public static String Ref_PhoneNumber ="Ref_PhoneNumber";

    public static String Time = "Time";

    public static String ProductID = "ProductID" ;

    public static String Indicator ="Indicator";

    public static  String Lat="Latitude";

    public static  String Long="Longitude";

    public static  String AddressId="AddressID";

    public static String match_id_Rapid = "match_id_Rapid";

    public static String SeriesID = "SeriesID";

    public  static  String MatchType = "MatchType";

    public static String Away = "Away";

    public static String Home = "Home";

    public static String MatchDate = "MatchDate";

    public static String Created_TeamID = "Created_TeamID";
    //
    public static String Bet_Slip_id = "Bet_Slip_id";
    //
    public static String AddMinus = "AddMinus";

    public static String Players = "Players";

    public static String Members = "Members";

    public static String Progress = "Progress";

    public  static String AmtLeft = "AmtLeft";

    public  static String BeneficiaryId = "BeneficiaryId";

    public  static String DeleteBeneficiaryId = "DeleteBeneficiaryId";

    public  static String Test = "test";

    public  static String FromAddress = "fromAddress";

    public  static String FromEdit = "fromedit";

    public  static String WishListId = "WishListId";

    public  static String TransactionID = "TransactionID";

    public  static String Token = "Token";

    public  static String referralId = "referralId";

    public  static String Location = "Location";

}
