package com.vixspace.grocerytray.utils;

public class AllVariables {

    public static final String URL_HOST = "https://api.grocerytray.com/app/";
    public static final String URL_HOST1 = "https://api.grocerytray.com/upload/";

    public static final String Cancel_Order = "Cancel_Order";
    public static final String Submit_Order_Rating = "Submit_Order_Rating";
    public static final String Fetch_Unrated_Orders = "Fetch_Unrated_Orders";
    public static final String Fetch_Order_Complete_Information = "Fetch_Order_Complete_Information";
    public static final String Fetch_All_Past_Orders = "Fetch_All_Past_Orders";
    public static final String Fetch_All_Active_Orders = "Fetch_All_Active_Orders";
    public static final String Fetch_All_Upcoming_Orders = "Fetch_All_Upcoming_Orders";
    public static final String Fetch_Subscription_Task_Complete_Information = "Fetch_Subscription_Task_Complete_Information";
    public static final String Fetch_All_Subscription_Product_Deliveries = "Fetch_All_Subscription_Product_Deliveries";
    public static final String Fetch_Subscription_Complete_Information = "Fetch_Subscription_Complete_Information";
    public static final String Logout = "Logout";
    public static final String Place_Order = "Place_Order ";
    public static final String Apply_Offer_Coupon_Code = "Apply_Offer_Coupon_Code ";
    public static final String Generate_Order_Price_Quote = "Generate_Order_Price_Quote ";
    public static final String Send_Next_Available_Dates = "Send_Next_Available_Dates ";
    public static final String Terminate_Subscription = "Terminate_Subscription ";
    public static final String Activate_Subscription_Service = "Activate_Subscription_Service ";
    public static final String Pause_Subscription = "Pause_Subscription ";
    public static final String Update_Subscription_Information = "Update_Subscription_Information ";
    public static final String Fetch_All_Active_Subscription_Products = "Fetch_All_Active_Subscription_Products ";
    public static final String Subscribe_Subscription_Product = "Subscribe_Subscription_Product ";
    public static final String Fetch_All_Available_Slots = "Fetch_All_Available_Slots ";
    public static final String Fetch_Complete_City_Settings = "Fetch_Complete_City_Settings ";
    public static final String Fetch_Offer_Coupon_Complete_Information = "Fetch_Offer_Coupon_Complete_Information";
    public static final String Fetch_All_Available_Offer_Coupons = "Fetch_All_Available_Offer_Coupons";
    public static final String Fetch_Locality_Product_Complete_Information = "Fetch_Locality_Product_Complete_Information";
    public static final String Fetch_Product_Complete_Information = "Fetch_Product_Complete_Information";
    public static final String Fetch_All_Collection_Products = "Fetch_All_Collection_Products";
    public static final String Fetch_Collection_Complete_Information = "Fetch_Collection_Complete_Information";
    public static final String Fetch_All_Category_Collections = "Fetch_All_Category_Collections";
    public static final String Fetch_Category_Complete_Information = "Fetch_Category_Complete_Information";
    public static final String List_All_User_Address = "List_All_User_Address";
    public static final String Add_User_Address = "Add_User_Address";
    public static final String Fetch_All_Locality_Categories = "Fetch_All_Locality_Categories";
    public static final String Fetch_All_Deal_of_Day_Products = "Fetch_All_Deal_of_Day_Products";
    public static final String Fetch_All_Home_Slider_Items = "Fetch_All_Home_Slider_Items";
    public static final String Fetch_All_Subscription_Products = "Fetch_All_Subscription_Products";
    public static final String Filter_All_User_Wallet_Logs = "Filter_All_User_Wallet_Logs";
    public static final String Fetch_User_Wallet_Information = "Fetch_User_Wallet_Information";
    public static final String Razorpay_Add_Amount_To_Wallet_Fetch_Complete_Information = "Razorpay_Add_Amount_To_Wallet_Fetch_Complete_Information";
    public static final String Fetch_User_Locality_From_Lat_Long = "Fetch_User_Locality_From_Lat_Long";
    public static final String Fetch_Default_Locality_Setting = "Fetch_Default_Locality_Setting";
    public static final String Update_User_Image_Information = "Update_User_Image_Information";
    public static final String Update_User_Basic_Information = "Update_User_Basic_Information";
    public static final String Validate_Login_OTP = "Validate_Login_OTP";
    public static final String Generate_Login_OTP = "Generate_Login_OTP";
    public static final String Customer_Update_FCM_Token = "Customer_Update_FCM_Token";
    public static final String Splash_Screen = "Splash_Screen";
    public static final String Fetch_All_Favourite_Products = "Fetch_All_Favourite_Products";
    public static final String User_Remove_Favourite_Product = "User_Remove_Favourite_Product";
    public static final String Save_Favourite_Product = "Save_Favourite_Product";
    public static final String Fetch_User_Complete_Information = "Fetch_User_Complete_Information";
    public static final String Fetch_All_Paused_Subscription_Products = "Fetch_All_Paused_Subscription_Products";
    public static final String Fetch_All_Terminated_Subscription_Products = "Fetch_All_Terminated_Subscription_Products";
    public static final String Fetch_All_Related_Products = "Fetch_All_Related_Products";
    public static final String Search_All_Products = "Search_All_Products";
    public static final String Remove_All_Favourite_Products = "Remove_All_Favourite_Products";
    public static final String Reserve_Amount = "Reserve_Amount";
    public static final String Fetch_Last_Wallet_Information = "Fetch_Last_Wallet_Information";
    public static final String Edit_Whether_Ring = "Edit_Whether_Ring";

//UploadImage
    public static final String URL_Upload_Image  = "Upload_Image";

}