package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.utils.PrintMsg;

public class ContactUsActivity extends AppCompatActivity {

    ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        imgBack = findViewById(R.id.imgBack);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(ContactUsActivity.this, getResources().getString(R.string.no_internet));
    }
}
