package com.vixspace.grocerytray.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.MainActivity;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.FetchDefaultLocalitySettingResponse;
import com.vixspace.grocerytray.pojos.FetchUserLocality;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.GPSTracker;
import com.vixspace.grocerytray.utils.PrintMsg;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AcceptLocationActivity extends AppCompatActivity {

    private static final int SEARCH_SCREEN_REQ_CODE = 201;
    private static final int PERMISSIONS_REQUEST = 100;
    private static final int REQUEST_CHECK_SETTINGS = 1010;
    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;
    TextView txtSkip,txtSetManually;

    RelativeLayout relativeCurrentLlocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_location);

        allsharedpreference = new Allsharedpreference(AcceptLocationActivity.this);
        commonMethods = new CommonMethods(AcceptLocationActivity.this);

        relativeCurrentLlocation = findViewById(R.id.relativeCurrentLlocation);
        txtSkip = findViewById(R.id.txtSkip);
        txtSetManually = findViewById(R.id.txtSetManually);

        CheckPermission();

        relativeCurrentLlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckPermission();
            }
        });

        txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                    validateAddress("lat","lon",2);
                }else{
                    showNoInterNetMessage();
                }


            }
        });

        txtSetManually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AcceptLocationActivity.this, ChooseLocationActivity.class);
                intent.putExtra("indicator",1);
                startActivityForResult(intent,101);
            }
        });
    }

    private void CheckPermission(){
        Log.e("log", String.valueOf(1));
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
//                displayLocationSettingsRequest(this);
                getLocationUpdates();

            } else {
                //Request Location Permission
                requestPermissions(
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST);
            }
        }
        else {
            getLocationUpdates();
//            displayLocationSettingsRequest(this);
        }
    }

    private void getLocationUpdates(){
        Log.e("log", String.valueOf(4));
//        Intent intent = new Intent(getApplicationContext(), GoogleApiClientLocationUpdates.class);
//        startService(intent);
        getLocation();
    }

    private void getLocation() {

        GPSTracker gpsTracker = new GPSTracker(getApplicationContext());

        Double  Lat = gpsTracker.getLatitude();
        Double Long = gpsTracker.getLongitude();

        allsharedpreference.setShared_String(Allsharedpreference.SELECTED_LON, String.valueOf(Long));
        allsharedpreference.setShared_String(Allsharedpreference.SELECTED_LAT, String.valueOf(Lat));

        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

            addresses = geocoder.getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            if(addresses.size() > 0) {

                if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
                    if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                        validateAddress(String.valueOf(Lat), String.valueOf(Long),1);
                    }else{
                        showNoInterNetMessage();
                    }

                } else {
                    if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                        validateAddress(String.valueOf(Lat), String.valueOf(Long),2);
                    }else{
                        showNoInterNetMessage();
                    }
                }

//                allsharedpreference.setShared_String(Allsharedpreference.CurrentLocation,addresses.get(0).getLocality());
//                Intent intent = new Intent(AcceptLocationActivity.this, MainActivity.class);
//                startActivity(intent);
//                finish();

                Log.e("add", String.valueOf(addresses.get(0).getLocality()));
            } else {

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("log", String.valueOf(2));
        if (requestCode == PERMISSIONS_REQUEST && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            displayLocationSettingsRequest(this);
        }
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (shouldShowRequestPermissionRationale(permissions[0])){
//                txt_tanzania.setText("Choose Location");
                Toast.makeText(getApplicationContext(), "Show Popup Message Regarding this Permission Usage", Toast.LENGTH_SHORT).show();
            }
            else{
//                txt_tanzania.setText("Choose Location");
                Toast.makeText(getApplicationContext(), "Denied Permissions", Toast.LENGTH_SHORT).show();
            }
        }
        else{
            CheckPermission();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("onActivityResult()", Integer.toString(resultCode));

        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        // All required changes were successfully made
                        getLocationUpdates();
                        Toast.makeText(getApplicationContext(), "Location enabled by user!", Toast.LENGTH_LONG).show();
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        // The user was asked to change settings, but chose not to
                        Toast.makeText(getApplicationContext(), "Location not enabled, user cancelled.", Toast.LENGTH_LONG).show();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
            case SEARCH_SCREEN_REQ_CODE :
                if(resultCode == RESULT_OK) {
                    String address = data.getStringExtra("Address");
                    String lat = data.getStringExtra("lat");
                    String lon = data.getStringExtra("lon");
                    allsharedpreference.setShared_String(Allsharedpreference.SELECTED_LON, String.valueOf(lon));
                    allsharedpreference.setShared_String(Allsharedpreference.SELECTED_LAT, String.valueOf(lat));
//                    allsharedpreference.setShared_String(Allsharedpreference.CurrentLocation, String.valueOf(address));
                    allsharedpreference.setShared_Booleen(Allsharedpreference.Location,true);

                    Intent intent = new Intent(AcceptLocationActivity.this,MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();

//                    validateAddress(lat, lon);
                }
        }

        if (requestCode==101&& resultCode == RESULT_OK){
            String lat = data.getStringExtra("lat");
            String lon = data.getStringExtra("lon");
            String strAddress = data.getStringExtra("address");
            String city  = data.getStringExtra("city");
            String state = data.getStringExtra("state");
            String pincode = data.getStringExtra("pin");

            allsharedpreference.setShared_String(Allsharedpreference.SELECTED_LON, String.valueOf(lon));
            allsharedpreference.setShared_String(Allsharedpreference.SELECTED_LAT, String.valueOf(lat));

            String newAddress = (strAddress +", " +city+ ", " + state +", " + pincode);

            if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
                if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                    validateAddress(lat,lon,1);
                }else{
                    showNoInterNetMessage();
                }
            } else {
                if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                    validateAddress(lat,lon,2);
                }else{
                    showNoInterNetMessage();
                }
            }


        }
    }

    private void validateAddress(final String lat, final String lon, final Integer indicator) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("Latitude",lat);
        jsonObject.addProperty("Longitude",lon);
        if (indicator==1){
            jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
            jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        }
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = null;
        if (indicator==1){
            apiResponse = apiService.userLocality(jsonObject);
        } else {
            apiResponse = apiService.defaultLocality(jsonObject);
        }
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){
                        allsharedpreference.setShared_Booleen(Allsharedpreference.Location,true);
                        if (indicator==1){

                            JsonObject splashScreenResponse = response.body();
                            Gson gson = new Gson();
                            final FetchUserLocality loginResponse = gson.fromJson(splashScreenResponse,FetchUserLocality.class);

                            if (loginResponse.getSuccess()) {

                                if (loginResponse.getExtras().getWhetherLocalityAvailable()){
                                    allsharedpreference.setShared_String(Allsharedpreference.CurrentLocation,loginResponse.getExtras().getData().getLocalityTitle());
                                    allsharedpreference.setShared_String(Allsharedpreference.CurrentCityID,loginResponse.getExtras().getData().getCityID());
                                    allsharedpreference.setShared_String(Allsharedpreference.LocalityID,loginResponse.getExtras().getData().getLocalityID());
                                    allsharedpreference.setShared_String(Allsharedpreference.Locality_Title,loginResponse.getExtras().getData().getLocalityTitle());

                                    Intent intent = new Intent(AcceptLocationActivity.this,MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                }   else {
                                    if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                                        validateAddress(lat,lon,2);
                                    }else{
                                        showNoInterNetMessage();
                                    }
                                }


                            }
                            else{
                                commonMethods.showToast(loginResponse.getExtras().getMsg());
                            }
                        } else if (indicator==2){

                            JsonObject splashScreenResponse = response.body();
                            Gson gson = new Gson();
                            final FetchDefaultLocalitySettingResponse loginResponse = gson.fromJson(splashScreenResponse,FetchDefaultLocalitySettingResponse.class);

                            if (loginResponse.getSuccess()) {

                                allsharedpreference.setShared_String(Allsharedpreference.CurrentLocation,loginResponse.getExtras().getData().getLocalityData().getLocalityTitle());
                                allsharedpreference.setShared_String(Allsharedpreference.CurrentCityID,loginResponse.getExtras().getData().getLocalityData().getCityID());
                                allsharedpreference.setShared_String(Allsharedpreference.LocalityID,loginResponse.getExtras().getData().getLocalityData().getLocalityID());
                                allsharedpreference.setShared_String(Allsharedpreference.Locality_Title,loginResponse.getExtras().getData().getLocalityData().getLocalityTitle());

                                Intent intent = new Intent(AcceptLocationActivity.this,MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                            else{
                                commonMethods.showToast(loginResponse.getExtras().getMsg());
                            }
                        }

                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(AcceptLocationActivity.this, SplashActivity.class);
                            allsharedpreference.clearAllSharedPreferences();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    private void displayLocationSettingsRequest(final FragmentActivity activity) {
        Log.e("log", String.valueOf(3));
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(activity)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i("Location Settings ", "All location settings are satisfied.");
                        getLocationUpdates();
//                        Toast.makeText(OnBoardingScreensActivity.this, "Allowed", Toast.LENGTH_SHORT).show();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i("Location Settings ", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i("Location Settings ", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("Location Settings ", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(AcceptLocationActivity.this, getResources().getString(R.string.no_internet));
    }

}
