package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.material.tabs.TabLayout;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.fragments.ActiveSubscriptions;
import com.vixspace.grocerytray.fragments.CurrentOrders;
import com.vixspace.grocerytray.fragments.OrderHistory;
import com.vixspace.grocerytray.fragments.OrdersFragment;
import com.vixspace.grocerytray.fragments.PausedSubscriptions;
import com.vixspace.grocerytray.fragments.TerminatedSubscriptions;

import java.util.ArrayList;
import java.util.List;

public class MySubscriptions extends AppCompatActivity {

    ImageView imgBack;
    LinearLayout linearNull;
    private TabLayout tabLayout;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_subscriptions);

        imgBack = findViewById(R.id.imgBack);
        linearNull = findViewById(R.id.linearNull);
        viewPager =  findViewById(R.id.viewpager);
        tabLayout =  findViewById(R.id.tabs);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ActiveSubscriptions(), "Active Subscriptions");
        adapter.addFragment(new PausedSubscriptions(), "Paused Subscriptions");
        adapter.addFragment(new TerminatedSubscriptions(), "Terminated Subscriptions");

        viewPager.setAdapter(adapter);
    }

    public static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
