package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.adapters.TrackAdapter;
import com.vixspace.grocerytray.pojos.FetchOrderCompleteResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ImageView imgBack;
    TextView txtOrderId,txtOrderDetails;
    String OrderId,OrderNum;
    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;
    TrackAdapter trackAdapter;
    TextView txtTrackOnMap;
    String DriverId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        txtTrackOnMap = findViewById(R.id.txtTrackOnMap);
        txtOrderDetails = findViewById(R.id.txtOrderDetails);
        txtOrderId = findViewById(R.id.txtOrderId);
        imgBack = findViewById(R.id.imgBack);
        recyclerView = findViewById(R.id.recyclerView);

        OrderNum = getIntent().getStringExtra("OrderNum");
        OrderId = getIntent().getStringExtra("OrderId");

        txtOrderId.setText(OrderNum);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        txtTrackOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(TrackActivity.this,TrackOrderStatusActivity.class);
//                intent.putExtra("DriverID",DriverId);
//                startActivity(intent);
            }
        });

        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            LoadOrderDetails(OrderId);
        }else{
            showNoInterNetMessage();
        }

    }

    private void LoadOrderDetails(String orderId) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("OrderID",orderId);

        APIService apiService = RestApiClient.getClient().create(APIService.class);

        Call<JsonObject> all_products = apiService.orderComplete(jsonObject);

        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if(response.isSuccessful()) {

                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final FetchOrderCompleteResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,FetchOrderCompleteResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                            if (listOfProductsResponse.getExtras().getData().getOrderStatusLogs().size()>0){

                                Integer i = listOfProductsResponse.getExtras().getData().getOrderStatusLogs().size();
                                txtOrderDetails.setText(listOfProductsResponse.getExtras().getData().getOrderStatusLogs().get(i-1).getComment());
                                trackAdapter = new TrackAdapter(TrackActivity.this,listOfProductsResponse.getExtras().getData().getOrderStatusLogs());
                                recyclerView.setLayoutManager(new LinearLayoutManager(TrackActivity.this,LinearLayoutManager.VERTICAL,true));
                                recyclerView.setAdapter(trackAdapter);
                                if (listOfProductsResponse.getExtras().getData().getOrderStatus()==5){
                                    txtTrackOnMap.setVisibility(View.VISIBLE);
                                    DriverId = listOfProductsResponse.getExtras().getData().getDriverID();
                                }
                            }

                    } else {
//                        progressBar.setVisibility(View.GONE);
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(TrackActivity.this, "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(TrackActivity.this, SplashActivity.class);
                            allsharedpreference.clearAllSharedPreferences();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }


                } else {
//                    if (response.message().equals("Bad Request")){
                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(TrackActivity.this, SplashActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        allsharedpreference.clearAllSharedPreferences();
                        startActivity(intent);
                        finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
//                progressBar.setVisibility(View.GONE);
                Toast.makeText(TrackActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });


    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(TrackActivity.this, getResources().getString(R.string.no_internet));
    }

}