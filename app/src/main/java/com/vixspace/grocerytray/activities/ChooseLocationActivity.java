package com.vixspace.grocerytray.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.GPSTracker;
import com.vixspace.grocerytray.utils.PrintMsg;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class ChooseLocationActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private Allsharedpreference mAllsharedpreference;
    private CommonMethods mCommonMethods;

    CardView cardViewChoosePlace;
    String address, lat, lon,city,state,loc,pincode,addressint;
    ImageView imageBack;
    TextView txtCurrentAddress;
    Button btnNext;
    LatLng currentLatlangs;
    private GoogleMap mMap;
    protected GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    protected LocationManager locationManager;
    boolean isEditAddress;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    Marker mCurrLocationMarker;

    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    private static final int PERMISSIONS_REQUEST = 100;
    public static final int REQUEST_PERMISSION_SETTING = 300;
    public static final int GPS_SETTINGS_REQUEST = 400;
    public static final int AUTOCOMPLETE_REQUEST_CODE = 4000;
    public  static final int CUSTOM_PLACE_PICKER_REQUEST = 1000;

    RadioButton radioOther,radioWork,radioHome;

    RelativeLayout relativeLandMark,relativeAlpha;
    EditText edtLandmark;
    TextView txtContinue;

    Integer indicator = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_location);

        mCommonMethods = new CommonMethods(this);
        mAllsharedpreference = new Allsharedpreference(getApplicationContext());

        txtContinue = findViewById(R.id.txtContinue);
        edtLandmark = findViewById(R.id.edtLandmark);
        relativeLandMark = findViewById(R.id.relativeLandMark);
        relativeAlpha = findViewById(R.id.relativeAlpha);

        radioHome = findViewById(R.id.radioHome);
        radioWork = findViewById(R.id.radioWork);
        radioOther = findViewById(R.id.radioOther);
        cardViewChoosePlace = findViewById(R.id.CardView);
        imageBack = findViewById(R.id.imgBack);
        btnNext = findViewById(R.id.btnNxt);
        txtCurrentAddress = findViewById(R.id.txtCurrentLocation);

        indicator = getIntent().getIntExtra("indicator",0);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        String apiKey = getResources().getString(R.string.google_maps_key);

        /**
         * Initialize Places. For simplicity, the API key is hard-coded. In a production
         * environment we recommend using a secure mechanism to manage API keys.
         */
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }

        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(this);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeLandMark.setVisibility(View.VISIBLE);
            }
        });

        txtContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("lat", lat);
                intent.putExtra("lon", lon);
                intent.putExtra("address", address);
                intent.putExtra("addressint", addressint);
                intent.putExtra("city",city);
                intent.putExtra("state",state);
                intent.putExtra("loc",loc);
                intent.putExtra("pin",pincode);
                intent.putExtra("landmark",edtLandmark.getText().toString().trim());
                if (radioHome.isChecked()){
                    intent.putExtra("radio",1);
                } else if (radioWork.isChecked()){
                    intent.putExtra("radio",2);
                } else if (radioOther.isChecked()){
                    intent.putExtra("radio",3);
                }


                setResult(RESULT_OK, intent);
                finish();
            }
        });
//       btnNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent();
//                intent.putExtra("lat", lat);
//                intent.putExtra("lon", lon);
//                intent.putExtra("address", address);
//                intent.putExtra("addressint", addressint);
//                intent.putExtra("city",city);
//                intent.putExtra("state",state);
//                intent.putExtra("loc",loc);
//                intent.putExtra("pin",pincode);
//                if (radioHome.isChecked()){
//                    intent.putExtra("radio",1);
//                } else if (radioWork.isChecked()){
//                    intent.putExtra("radio",2);
//                } else if (radioOther.isChecked()){
//                    intent.putExtra("radio",3);
//                }
//
//
//                setResult(RESULT_OK, intent);
//                finish();
//            }
//        });



        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                    Intent intent = new Intent();
//                    setResult(RESULT_OK, intent);
                finish();
            }
        });

        cardViewChoosePlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    getDestination();
                onSearchCalled();
            }
        });
    }

    @SuppressLint("MissingPermission")
    public void getCurrentLocation(){
        GPSTracker gpsTracker = new GPSTracker(ChooseLocationActivity.this);
        mLastLocation = gpsTracker.getLocation();
        if (mLastLocation!=null){
            currentLatlangs = new LatLng(gpsTracker.getLatitude(),gpsTracker.getLongitude());
            mMap.setMyLocationEnabled(true);
            try{
                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLatlangs).zoom(18).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                btnNext.setVisibility(View.VISIBLE);
                mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    @Override
                    public void onCameraIdle() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                currentLatlangs = mMap.getCameraPosition().target;
                                if(mCurrLocationMarker!=null){
                                    mCurrLocationMarker.remove();
                                    mCurrLocationMarker = mMap.addMarker(new MarkerOptions().position(currentLatlangs).title("New Position").snippet("Marker Description"));

                                }
                                Log.d("TAG123", mMap.getCameraPosition().target.toString() + "\n" );
                            }
                        });
                    }
                });
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
        else{
            checkLocationPermision();
        }
    }

    private boolean checkLocationPermision(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();

                return true;
            } else {
                //Request Location Permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST);
            }
        }
        return false;
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(ChooseLocationActivity.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public boolean checkForProvider(){
        try {
            locationManager = (LocationManager) ChooseLocationActivity.this
                    .getSystemService(LOCATION_SERVICE);


            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (!isGPSEnabled && !isNetworkEnabled) {
                return false;
            } else {
                return true;
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return  false;
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ChooseLocationActivity.this);


        alertDialog.setTitle("Warning");


        alertDialog.setMessage("Please allow location permission to select location");


        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
            }
        });


        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    public void setLocation(){
        try{
            btnNext.setVisibility(View.VISIBLE);
            mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            currentLatlangs = mMap.getCameraPosition().target;
                            try {
                                lat = String.valueOf(currentLatlangs.latitude);
                                lon = String.valueOf(currentLatlangs.longitude);
                                Geocoder geo = new Geocoder(ChooseLocationActivity.this.getApplicationContext(), Locale.getDefault());
                                List<Address> addresses = geo.getFromLocation(currentLatlangs.latitude, currentLatlangs.longitude, 1);

                                if (addresses.size() > 0) {
                                    address = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getLocality() + ", "
                                            + addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName();
                                    addressint = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getLocality() ;

//                                        Toast.makeText(ChooseLocationActivity.this, addresses.get(0).getSubLocality(), Toast.LENGTH_SHORT).show();
                                    loc = addresses.get(0).getSubLocality();
                                    city =  addresses.get(0).getLocality();
                                    state = addresses.get(0).getAdminArea();
                                    pincode = addresses.get(0).getPostalCode();
                                    txtCurrentAddress.setText(address);
//                                    mAllsharedpreference.setShared_String(Allsharedpreference.CurrentLocation,address);

                                }
                            }
                            catch (Exception e) {
                                e.printStackTrace(); // getFromLocation() may sometimes fail
                            }
                            isEditAddress = false;
                            if(mCurrLocationMarker!=null){
                                mCurrLocationMarker.remove();
                                mCurrLocationMarker = mMap.addMarker(new MarkerOptions().position(currentLatlangs).title("New Position").snippet("Marker Description"));
                            }
                            Log.d("TAG123", mMap.getCameraPosition().target.toString() + "\n" );
                        }
                    });
                }
            });
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public void showEnableGpsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ChooseLocationActivity.this);


        alertDialog.setTitle("Warning");


        alertDialog.setMessage("Please enable GPS from settings");


        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent,GPS_SETTINGS_REQUEST);
            }
        });


        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });


        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        //If the permission has been granted...//
        if (requestCode == PERMISSIONS_REQUEST && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient();
        } else {
            //If the user denies the permission request, then display a toast with some more information
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                // user rejected the permission
                String permission = permissions[0];
                boolean showRationale = false;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    showRationale = shouldShowRequestPermissionRationale( permission );
                }
                if (! showRationale) {
                    // user also CHECKED "never ask again"
                    // you can either enable some fall back,
                    // disable features of your app
                    // or open another dialog explaining
                    // again the permission and directing to
                    // the app setting
                    showSettingsAlert();
                } else if (Manifest.permission.ACCESS_FINE_LOCATION.equals(permission)) {
                    showSettingsAlert();
                    // user did NOT check "never ask again"
                    // this is a good place to explain the user
                    // why you need the permission and ask if he wants
                    // to accept it (the rationale)
                } else {
                    showSettingsAlert();
                }
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap=googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setOnCameraIdleListener(onCameraIdleListener);
        mMap.getUiSettings().setCompassEnabled(true);

        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                if (checkForProvider()){
                    checkLocationPermision();
                }
                else{
                    showEnableGpsAlert();
                }

            } else {
                //Request Location Permission
                requestPermissions(
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST);
            }
        }
        else {
            if (checkForProvider()){
                checkLocationPermision();
            }
            else{
                showEnableGpsAlert();
            }
        }
        mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {

            }
        });

        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                setLocation();
            }
        });
    }

    public void onSearchCalled() {
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields).setCountry("IN") //NIGERIA
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i("Tag", "Place: " + place.getName() + ", " + place.getId() + ", " + place.getAddress());
//                Toast.makeText(ChooseLocationActivity.this, "ID: " + place.getId() + "address:" + place.getAddress() + "Name:" + place.getName() + " latlong: " + place.getLatLng(), Toast.LENGTH_LONG).show();
                LatLng currentLatlangs = place.getLatLng();
                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLatlangs).zoom(18).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                isEditAddress = true;
                setCustomLocation(currentLatlangs);
                // do query with address

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Toast.makeText(ChooseLocationActivity.this, "Error: " + status.getStatusMessage(), Toast.LENGTH_LONG).show();
                Log.i("Tags", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void setCustomLocation(LatLng latLng) {
        try{
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(18).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


            mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            currentLatlangs = mMap.getCameraPosition().target;

                            if(mCurrLocationMarker!=null){
                                mCurrLocationMarker.remove();
//
                            }
                            Log.d("TAG123", mMap.getCameraPosition().target.toString() + "\n" );
                        }
                    });
                }
            });
        }
        catch(Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        if (relativeLandMark.getVisibility()==View.VISIBLE){
            relativeLandMark.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
            finish();
        }
    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(ChooseLocationActivity.this, getResources().getString(R.string.no_internet));
    }
}
