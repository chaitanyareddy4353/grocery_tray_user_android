package com.vixspace.grocerytray.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
import com.vixspace.grocerytray.Application.AppController;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.GPSTracker;
import com.vixspace.grocerytray.utils.PrintMsg;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class TrackOrderStatusActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
private GoogleMap mMap;
protected GoogleApiClient mGoogleApiClient;
        Location mLastLocation;
        Marker mCurrLocationMarker;
        private static final int PERMISSIONS_REQUEST = 100;
        private SupportMapFragment mMapFragment; // MapView UI element
        public static PubNub pubnub; // Pubnub instance
        private Allsharedpreference mAllsharedpreference;
        private CommonMethods mCommonMethods;
        private JSONArray EventLogs;
        ImageView imgBackButton;
        String DriverID;  //from order status logs
        double DriverLatitude,DriverLongitude;
        TextView txtTime,txtDistance,txtDriverName;
        CircleImageView imageViewDriver;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_order_status);
        mCommonMethods = new CommonMethods(this);
        mAllsharedpreference = new Allsharedpreference(getApplicationContext());
        imgBackButton = findViewById(R.id.imgBackButton);
        imgBackButton.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View view) {
        onBackPressed();
        }
        });
        mMapFragment = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
        txtTime = findViewById(R.id.txtTime);
        txtDistance = findViewById(R.id.txtDistance);
        txtDriverName = findViewById(R.id.txtDriverName);
        imageViewDriver = findViewById(R.id.imageViewDriver);

        initPubnub();
        Bundle bundle= getIntent().getExtras();
        if (bundle!=null){
//        try {
//        EventLogs = new JSONArray(bundle.getString("EventLogs"));
        DriverID= bundle.getString("DriverID");
//        Log.v("EeventLogs",EventLogs.toString());
//        } catch (JSONException e) {
//        e.printStackTrace();
//        }
        }
        try {
        pubnub.subscribe()
        .channels(Arrays.asList(mAllsharedpreference.getPresenceChannel())) // subscribe to channels
        .execute();
        } catch (Exception e) {
        e.printStackTrace();
        }
        pubnub.addListener(new SubscribeCallback() {
@Override
public void status(PubNub pub, PNStatus status) {

        }

@Override
public void message(PubNub pub, final PNMessageResult message) {
        runOnUiThread(new Runnable() {
@Override
public void run() {
        try {
        Log.v("pubnubMessage",message.toString());
//                                    updateUI(newLocation);
        JSONObject pubnubMessage= new JSONObject(String.valueOf(message.getMessage()));

//                            String CurrentDriver = EventLogs.getJSONObject(EventLogs.length()-1).getString("DriverID");
        String DriverId = pubnubMessage.getString("DriverID");

        if (DriverID.equals(DriverId)){
        Log.v("JsonObject",pubnubMessage.toString());
        updateUI(pubnubMessage.getDouble("Latitude"),pubnubMessage.getDouble("Longitude"));
        }

        } catch (Exception e) {
        e.printStackTrace();
        }
        }
        });
        }

@Override
public void presence(PubNub pub, PNPresenceEventResult presence) {

        }
        });

        }
public void getCurrentLocation(){
        GPSTracker gpsTracker = new GPSTracker(TrackOrderStatusActivity.this);
        mLastLocation = gpsTracker.getLocation();
        if (mLastLocation!=null){
        LatLng currentLatlangs = new LatLng(gpsTracker.getLatitude(),gpsTracker.getLongitude());
        try{
        mCurrLocationMarker = mMap.addMarker(new MarkerOptions().position(currentLatlangs).
        icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_baseline_two_wheeler_24)));
//                mCurrLocationMarker =mMap.addMarker(new MarkerOptions().position(currentLatlangs).title("Marker Title").snippet("Marker Description"));
        // For zooming automatically to the location of the marker
        CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLatlangs).zoom(15).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        JSONObject eventLogs = EventLogs.getJSONObject(EventLogs.length()-1);
        DriverLatitude = eventLogs.getDouble("lat");
        DriverLongitude = eventLogs.getDouble("lng");
        getPricingDetails(String.valueOf(currentLatlangs.latitude),String.valueOf(currentLatlangs.longitude),String.valueOf(DriverLatitude),String.valueOf(DriverLongitude));


        }
        catch(Exception e){
        e.printStackTrace();
        }
        }
        }
private void getPricingDetails(String latitudeOne,String longitudeOne,String latitudeTwo,String longitudeTwo){

        String tag_string_req = "req_login";

        StringRequest strReq = new StringRequest(Request.Method.GET,
        "http://maps.googleapis.com/maps/api/distancematrix/json?units=metric&mode=driving&origins="+latitudeOne+","+longitudeOne+"&destinations="+latitudeTwo+","+longitudeTwo+"&key=AIzaSyAZwPjErk9fYi27QctZ3VjJRb2SRLvWe-Q", new Response.Listener<String>() {
@Override
public void onResponse(String response) {

        Log.v("doctorslist", "Roles Response: " + response);
        try {
        JSONObject jObj = new JSONObject(response);
        JSONArray extras = jObj.getJSONArray("rows");
        JSONObject rowsObject = extras.getJSONObject(0);
        JSONArray elementsArray = rowsObject.getJSONArray("elements");
        JSONObject elementsObject = elementsArray.getJSONObject(0);
        JSONObject distanceObject = elementsObject.getJSONObject("distance");
        JSONObject durationObject = elementsObject.getJSONObject("duration");
        String distance = distanceObject.getString("text");
        String Time = durationObject.getString("text");
        txtTime.setText(Time);
        txtDistance.setText(distance);
//                    Toast.makeText(TrackOrderStatusActivity.this, distance +" , "+Time, Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
        Log.v("responcedf", e.toString());
        }
        }
        }, new Response.ErrorListener() {

@Override
public void onErrorResponse(VolleyError error) {
        }
        }) {
@Override
protected Map<String, String> getParams() {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<String, String>();
//                "Sub_Trip_Type": 1, //1.SP-SD 2.SP-MD 3.MP-SD
//                "Service_Type": 1, // 1. Instant 2.Schedule
//                "Order_Type": 1, //1.Cash 2.Online 3.Month Invocie
//                Collection_Type: 1 // 1.Cash at Pickup  2. Drop
//                "Payment_Type": 1 //.Razorpay

        return params;
        }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(
        100000,
        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

        }
private void initPubnub() {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey(mAllsharedpreference.getPubnubSubscribeKey());
        pnConfiguration.setPublishKey(mAllsharedpreference.getPubnubPublishKey());
        pnConfiguration.setSecure(true);
        pubnub = new PubNub(pnConfiguration);
        }
@Override
public void onConnected(@Nullable Bundle bundle) {
        getCurrentLocation();
        }

@Override
public void onConnectionSuspended(int i) {

        }

@Override
public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        }

@Override
public void onMapReady(GoogleMap googleMap) {
        mMap=googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setCompassEnabled(false);

        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
        Manifest.permission.ACCESS_FINE_LOCATION)
        == PackageManager.PERMISSION_GRANTED) {
        //Location Permission already granted
        buildGoogleApiClient();
        mMap.setMyLocationEnabled(false);
        } else {
        //Request Location Permission
        requestPermissions(
        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
        PERMISSIONS_REQUEST);
        }
        }
        else {
        buildGoogleApiClient();
        mMap.setMyLocationEnabled(true);
        }
        }
private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(LocationServices.API)
        .build();
        mGoogleApiClient.connect();
        }
private void updateUI(double latitude,double longitude) {
        LatLng newLocation = new LatLng(latitude,longitude);
        if (mCurrLocationMarker != null) {
        animateCar(newLocation);
        boolean contains = mMap.getProjection()
        .getVisibleRegion()
        .latLngBounds
        .contains(newLocation);
        if (!contains) {
        mMap.moveCamera(CameraUpdateFactory.newLatLng(newLocation));
        }
        } else {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
        newLocation, 15.5f));
        mCurrLocationMarker = mMap.addMarker(new MarkerOptions().position(newLocation).
        icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_baseline_two_wheeler_24)));
        }
        }
private void animateCar(final LatLng destination) {
final LatLng startPosition = mCurrLocationMarker.getPosition();
final LatLng endPosition = new LatLng(destination.latitude, destination.longitude);


final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(5000); // duration 5 seconds
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
@Override
public void onAnimationUpdate(ValueAnimator animation) {
        try {
        float v = animation.getAnimatedFraction();
        LatLng newPosition = latLngInterpolator.interpolate( v,startPosition, endPosition);
        mCurrLocationMarker.setPosition(newPosition);
        } catch (Exception ex) {
        Log.v("JSONObject",ex.toString());
        }
        }
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {
@Override
public void onAnimationEnd(Animator animation) {
        super.onAnimationEnd(animation);
        }
        });
        valueAnimator.start();
        }
@Override
public void onResume() {
        super.onResume();
        mMapFragment.onResume();
        }

@Override
public void onPause() {
        super.onPause();
        mMapFragment.onPause();
        }

@Override
public void onDestroy() {
        super.onDestroy();
        mMapFragment.onDestroy();
        }

@Override
public void onLowMemory() {
        super.onLowMemory();
        mMapFragment.onLowMemory();
        }
private interface LatLngInterpolator {
    LatLng interpolate(float fraction, LatLng a, LatLng b);

    class LinearFixed implements LatLngInterpolator {
        @Override
        public LatLng interpolate(float fraction, LatLng a, LatLng b) {
            double lat = (b.latitude - a.latitude) * fraction + a.latitude;
            double lngDelta = b.longitude - a.longitude;
            if (Math.abs(lngDelta) > 180) {
                lngDelta -= Math.signum(lngDelta) * 360;
            }
            double lng = lngDelta * fraction + a.longitude;
            return new LatLng(lat, lng);
        }
    }
}

        public void showNoInterNetMessage(){
                PrintMsg.printLongToast(TrackOrderStatusActivity.this, getResources().getString(R.string.no_internet));
        }

}
