package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.adapters.CollectionProductsAdapter;
import com.vixspace.grocerytray.adapters.RelatedProductsAdapter;
import com.vixspace.grocerytray.adapters.SelecteddatesAdapter;
import com.vixspace.grocerytray.pojos.CartInfoModel;
import com.vixspace.grocerytray.pojos.CartModelClass;
import com.vixspace.grocerytray.pojos.FetchAllAvailableDatesResponse;
import com.vixspace.grocerytray.pojos.FetchAllCollectionproductsResponse;
import com.vixspace.grocerytray.pojos.FetchAllRelatedProducts;
import com.vixspace.grocerytray.pojos.FetchLocalityProductCompleteInformationResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailsActivity extends AppCompatActivity {

    ImageView imgBack,imgProduct,imgBookmark;
    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;
    Button btnAdd,btnSub,btnOutOfStock;
    ImageView imgCart,imgBookmarkHeart;
    CartInfoModel cartInfoModel = new CartInfoModel();
    List<CartModelClass> cartModelClasses = new ArrayList<>();
    FetchLocalityProductCompleteInformationResponse fetch = new FetchLocalityProductCompleteInformationResponse();
    TextView txtBrand,txtQuantity,txtProductDetails,txtsellingPrice,txtMrp,txtRelated,txtProductName;
    String productID = "";
    Boolean isRepeated = false;
    Integer CountRepeat = 0;
    RecyclerView recyclerRelated;
    ProgressBar progressBar;

    TextView txtViewCart,txtTotal,txtTotalQty;
    RelativeLayout relativeBottomSheet;
    Double orderTotal=0.0,tax=0.0,discount=0.0,deliveryCharges=0.0;

    TextView txtPercent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        txtPercent = findViewById(R.id.txtPercent);
        relativeBottomSheet = findViewById(R.id.relativeBottomSheet);
        txtTotalQty = findViewById(R.id.txtTotalQty);
        txtTotal = findViewById(R.id.txtTotal);
        txtViewCart = findViewById(R.id.txtViewCart);
        btnSub = findViewById(R.id.btnSub);
        recyclerRelated = findViewById(R.id.recyclerRelated);
        txtRelated = findViewById(R.id.txtRelated);
        imgBookmark = findViewById(R.id.imgBookmark);
        imgCart = findViewById(R.id.imgCart);
        imgBack = findViewById(R.id.imgBack);
        imgProduct = findViewById(R.id.imgProduct);
        btnAdd = findViewById(R.id.btnAdd);
        txtBrand = findViewById(R.id.txtBrand);
        txtQuantity = findViewById(R.id.txtQuantity);
        txtProductDetails = findViewById(R.id.txtProductDetails);
        txtsellingPrice = findViewById(R.id.txtsellingPrice);
        txtMrp = findViewById(R.id.txtMrp);
        imgBookmarkHeart = findViewById(R.id.imgBookmarkHeart);
        progressBar = findViewById(R.id.progressBar);
        btnOutOfStock = findViewById(R.id.btnOutOfStock);
        txtProductName = findViewById(R.id.txtProductName);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        productID = getIntent().getStringExtra("productID");

        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            getProductDetails(getIntent().getStringExtra("productID"));
        }else{
            showNoInterNetMessage();
        }
//        String data = allsharedpreference.getShared_String(Allsharedpreference.CartData);
//        if (!data.equals("")&&data!=null){
//            Gson gson = new Gson();
//            String Translation = allsharedpreference.getShared_String(Allsharedpreference.CartData);
//            cartInfoModel = gson.fromJson(Translation, CartInfoModel.class);
//        }
        Gson gson = new Gson();
        final String data = allsharedpreference.getShared_String(Allsharedpreference.CartData);
        if (data!=null){
            cartInfoModel = gson.fromJson(data, CartInfoModel.class);
            if (!data.equals("")){
                CartInfoModel cartInfoModelTest = new CartInfoModel();
                List<CartModelClass> test = new ArrayList<>();
                for (int i =0;i<cartInfoModel.getData().size();i++){
                    if (cartInfoModel.getData().get(i).getQuantity()==0){

                    } else {
                        test.add(cartInfoModel.getData().get(i));
                    }
                }
                cartInfoModelTest.setData(test);
                if (cartInfoModelTest.getData().size()>0){
                    cartModelClasses = cartInfoModelTest.getData();
                    Double toPay=0.0;
                    for (int i=0;i<cartInfoModelTest.getData().size();i++){
                        toPay = toPay+(cartInfoModelTest.getData().get(i).getSub_Total_Amount()*cartInfoModelTest.getData().get(i).getQuantity());
                        if (i==cartInfoModelTest.getData().size()-1) {
                            if (i==0){
                                txtTotalQty.setText(cartInfoModelTest.getData().size()+"item");
                            } else {
                                txtTotalQty.setText(cartInfoModelTest.getData().size()+"items");
                            }
                            txtTotal.setText("₹"+String.valueOf(toPay));
                            relativeBottomSheet.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
          }
            if (cartInfoModel.getData()!=null){

            } else {
                cartInfoModel = new CartInfoModel();
            }

            txtViewCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ProductDetailsActivity.this,CartActivity.class);
                    startActivity(intent);
                    finish();
                }
            });


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
                    CountRepeat =0;
                    if (data!=null){
                        if (cartInfoModel.getData().size()>0){
                            for (int i =0;i<cartInfoModel.getData().size();i++){
                                if (fetch.getExtras().getData().getLocalityProductID().equals(cartInfoModel.getData().get(i).getLocalityProductID())){
                                    CountRepeat = CountRepeat+1;
                                }
                            }

                            if (CountRepeat>0){
                                Toast.makeText(ProductDetailsActivity.this, "Item already added to cart", Toast.LENGTH_SHORT).show();
                            } else {
                                CartModelClass cartModelClass = new CartModelClass();
                                cartModelClass.setLocalityProductID(fetch.getExtras().getData().getLocalityProductID());
                                cartModelClass.setMRP(fetch.getExtras().getData().getMRP());
                                cartModelClass.setProductID(fetch.getExtras().getData().getProductID());
                                cartModelClass.setProductName(fetch.getExtras().getData().getProductName());
                                cartModelClass.setItemWeight(fetch.getExtras().getData().getProductUnit());
                                cartModelClass.setBrand(fetch.getExtras().getData().getProductBrand());
                                cartModelClass.setImageId(fetch.getExtras().getData().getImageInformation().getImage250());
                                cartModelClass.setQuantity(1);
                                cartModelClass.setSelling_Price(fetch.getExtras().getData().getSellingPrice());
                                cartModelClass.setSub_Total_Amount(fetch.getExtras().getData().getSellingPrice());
                                cartModelClasses.add(cartModelClass);
                                cartInfoModel.setData(cartModelClasses);
                                Gson gson = new Gson();
                                String json = gson.toJson(cartInfoModel); // myObject - instance of MyObject
                                allsharedpreference.setShared_String(Allsharedpreference.CartData,json);
                                Intent intent = new Intent(ProductDetailsActivity.this,CartActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            CartModelClass cartModelClass = new CartModelClass();
                            cartModelClass.setLocalityProductID(fetch.getExtras().getData().getLocalityProductID());
                            cartModelClass.setMRP(fetch.getExtras().getData().getMRP());
                            cartModelClass.setProductID(fetch.getExtras().getData().getProductID());
                            cartModelClass.setProductName(fetch.getExtras().getData().getProductName());
                            cartModelClass.setItemWeight(fetch.getExtras().getData().getProductUnit());
                            cartModelClass.setBrand(fetch.getExtras().getData().getProductBrand());
                            cartModelClass.setImageId(fetch.getExtras().getData().getImageInformation().getImage250());
                            cartModelClass.setQuantity(1);
                            cartModelClass.setSelling_Price(fetch.getExtras().getData().getSellingPrice());
                            cartModelClass.setSub_Total_Amount(fetch.getExtras().getData().getSellingPrice());
                            cartModelClasses.add(cartModelClass);
                            cartInfoModel.setData(cartModelClasses);
                            Gson gson = new Gson();
                            String json = gson.toJson(cartInfoModel); // myObject - instance of MyObject
                            allsharedpreference.setShared_String(Allsharedpreference.CartData,json);
                            Intent intent = new Intent(ProductDetailsActivity.this,CartActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        CartModelClass cartModelClass = new CartModelClass();
                        cartModelClass.setLocalityProductID(fetch.getExtras().getData().getLocalityProductID());
                        cartModelClass.setMRP(fetch.getExtras().getData().getMRP());
                        cartModelClass.setProductID(fetch.getExtras().getData().getProductID());
                        cartModelClass.setProductName(fetch.getExtras().getData().getProductName());
                        cartModelClass.setItemWeight(fetch.getExtras().getData().getProductUnit());
                        cartModelClass.setBrand(fetch.getExtras().getData().getProductBrand());
                        cartModelClass.setImageId(fetch.getExtras().getData().getImageInformation().getImage250());
                        cartModelClass.setQuantity(1);
                        cartModelClass.setSelling_Price(fetch.getExtras().getData().getSellingPrice());
                        cartModelClass.setSub_Total_Amount(fetch.getExtras().getData().getSellingPrice());
                        cartModelClasses.add(cartModelClass);
                        cartInfoModel.setData(cartModelClasses);
                        Gson gson = new Gson();
                        String json = gson.toJson(cartInfoModel); // myObject - instance of MyObject
                        allsharedpreference.setShared_String(Allsharedpreference.CartData,json);
                        Intent intent = new Intent(ProductDetailsActivity.this,CartActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Intent intent = new Intent(ProductDetailsActivity.this, LoginSignUpActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        });

            imgBookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
                        progressBar.setVisibility(View.VISIBLE);

                        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                            bookMark();
                        }else{
                            showNoInterNetMessage();
                        }

                    }

                }
            });

        btnSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
                    Intent intent = new Intent(ProductDetailsActivity.this,CreateSubscriptionActivity.class);
                    intent.putExtra("productID",productID);
                    intent.putExtra("brand",fetch.getExtras().getData().getProductBrand());
                    intent.putExtra("name",fetch.getExtras().getData().getProductName());
                    intent.putExtra("size",fetch.getExtras().getData().getProductUnit());
                    intent.putExtra("img",fetch.getExtras().getData().getImageInformation().getImage250());
                    intent.putExtra("sp","₹"+fetch.getExtras().getData().getSellingPrice());
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(ProductDetailsActivity.this, LoginSignUpActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }

//                Toast.makeText(ProductDetailsActivity.this, "coming soon", Toast.LENGTH_SHORT).show();
            }
        });

        imgCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductDetailsActivity.this,CartActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void bookMark() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("ProductID",fetch.getExtras().getData().getProductID());
        jsonObject.addProperty("Remark","my favourite");

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.saveFav(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final UpdateFcmResponse loginResponse =
                                gson.fromJson(splashScreenResponse,UpdateFcmResponse.class);

                        progressBar.setVisibility(View.GONE);

                        if (loginResponse.getSuccess()) {

                            if (imgBookmarkHeart.getVisibility()==View.VISIBLE){
                                imgBookmarkHeart.setVisibility(View.GONE);
                                imgBookmark.setVisibility(View.VISIBLE);
                                Toast.makeText(ProductDetailsActivity.this, "Bookmark removed", Toast.LENGTH_SHORT).show();
                            } else {
                                imgBookmarkHeart.setVisibility(View.VISIBLE);
                                imgBookmark.setVisibility(View.GONE);
                                Toast.makeText(ProductDetailsActivity.this, "Bookmark added", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                            progressBar.setVisibility(View.GONE);
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        progressBar.setVisibility(View.GONE);
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            allsharedpreference.clearAllSharedPreferences();
                            Intent intent = new Intent(ProductDetailsActivity.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
            progressBar.setVisibility(View.GONE);
        }

    }

    private void getProductDetails(String productID) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("LocalityProductID",productID);

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.localityProductCompleteInformation(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final FetchLocalityProductCompleteInformationResponse loginResponse =
                                gson.fromJson(splashScreenResponse,FetchLocalityProductCompleteInformationResponse.class);

                        if (loginResponse.getSuccess()) {

                            fetch = loginResponse;

                            Picasso.get().load(loginResponse.getExtras().getData().getImageInformation().getImageOriginal()).into(imgProduct);

//                            txtBrand.setText(loginResponse.getExtras().getData().getProductBrand() +
//                                    " ("+ loginResponse.getExtras().getData().getProductUnit() +")");
                            txtBrand.setText(loginResponse.getExtras().getData().getProductBrand());
                            txtQuantity.setText("Qty "+loginResponse.getExtras().getData().getProductUnit());
                            txtProductDetails.setText(loginResponse.getExtras().getData().getProductDescription());
                            txtMrp.setText("₹"+loginResponse.getExtras().getData().getMRP());
                            txtsellingPrice.setText("₹"+loginResponse.getExtras().getData().getSellingPrice());
                            txtProductName.setText(loginResponse.getExtras().getData().getProductName());

//                            if (loginResponse.getExtras().getData().ge)


                            if (loginResponse.getExtras().getData().getAvailableStock()==0){
                                btnAdd.setVisibility(View.GONE);
                                btnOutOfStock.setVisibility(View.VISIBLE);
                            } else {
                                btnAdd.setVisibility(View.VISIBLE);
                                btnOutOfStock.setVisibility(View.GONE);
                            }

                            if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
                               if (loginResponse.getExtras().getData().getWhetherFavouriteProduct()!=null){
                                   if (loginResponse.getExtras().getData().getWhetherFavouriteProduct()){
                                       imgBookmarkHeart.setVisibility(View.VISIBLE);
                                       imgBookmark.setVisibility(View.GONE);
                                   } else {
                                       imgBookmarkHeart.setVisibility(View.GONE);
                                       imgBookmark.setVisibility(View.VISIBLE);
                                   }
                               } else {
                                   imgBookmarkHeart.setVisibility(View.GONE);
                                   imgBookmark.setVisibility(View.VISIBLE);
                               }
                            }

                            if (loginResponse.getExtras().getData().getWhetherSubscription()){
                                btnSub.setVisibility(View.VISIBLE);
                            } else {
                                btnSub.setVisibility(View.GONE);
                            }

                            if (loginResponse.getExtras().getData().getCategoryTitle().equals("News Paper")||loginResponse.getExtras().getData().getCategoryTitle().equals("Water Cans")){
                                if (loginResponse.getExtras().getData().getWhetherSubscription()){
                                    btnAdd.setVisibility(View.INVISIBLE);
                                    btnSub.setVisibility(View.VISIBLE);
                                } else {
                                    btnAdd.setVisibility(View.INVISIBLE);
                                    btnSub.setVisibility(View.INVISIBLE);
                                }
                            }

                            if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
                                if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                                    relatedProducts(loginResponse.getExtras().getData().getProductID());
                                }else{
                                    showNoInterNetMessage();
                                }
                            } else {
                                progressBar.setVisibility(View.GONE);
                            }


                            if (loginResponse.getExtras().getData().getMRP().equals(loginResponse.getExtras().getData().getSellingPrice())){
                                txtMrp.setVisibility(View.GONE);
                            } else {
                                txtMrp.setVisibility(View.VISIBLE);
                            }


                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());
                            progressBar.setVisibility(View.GONE);
//
//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
            progressBar.setVisibility(View.GONE);
        }

    }

    private void relatedProducts(String productID) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("ProductID",productID);

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.related(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final FetchAllRelatedProducts loginResponse =
                                gson.fromJson(splashScreenResponse,FetchAllRelatedProducts.class);

                        progressBar.setVisibility(View.GONE);

                        if (loginResponse.getSuccess()) {

                            if (loginResponse.getExtras().getData().size()>0){
                                RelatedProductsAdapter adapter = new RelatedProductsAdapter(ProductDetailsActivity.this, loginResponse.getExtras().getData());
                                recyclerRelated.setLayoutManager(new LinearLayoutManager(ProductDetailsActivity.this,RecyclerView.HORIZONTAL,false));
                                recyclerRelated.setAdapter(adapter);
                                txtRelated.setVisibility(View.VISIBLE);
                            } else {
                                txtRelated.setVisibility(View.GONE);
                            }

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            allsharedpreference.clearAllSharedPreferences();
                            Toast.makeText(ProductDetailsActivity.this, "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ProductDetailsActivity.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        CartInfoModel cartInfoModels = new CartInfoModel();
        Gson gson = new Gson();
        final String data = allsharedpreference.getShared_String(Allsharedpreference.CartData);
        if (data!=null){
            cartInfoModels = gson.fromJson(data, CartInfoModel.class);
        } else {
            relativeBottomSheet.setVisibility(View.GONE);
        }

//        if (data!=null){
//            cartInfoModel = gson.fromJson(data, CartInfoModel.class);
//            if (!data.equals("")){
//                if (cartInfoModel.getData().size()>0){
//                    cartModelClasses = cartInfoModel.getData();
//                    for (int i=0;i<cartInfoModel.getData().size();i++){
//                        toPay = toPay+(cartInfoModel.getData().get(i).getSub_Total_Amount()*cartInfoModel.getData().get(i).getQuantity());
//                        if (i==cartInfoModel.getData().size()-1) {
//                            if (i==0){
//                                txtTotalQty.setText(cartInfoModel.getData().size()+"item");
//                            } else {
//                                txtTotalQty.setText(cartInfoModel.getData().size()+"items");
//                            }
//                            txtTotal.setText("₹"+String.valueOf(toPay));
//                            relativeBottomSheet.setVisibility(View.VISIBLE);
//                        }
//                    }
//                }
//            }
//        }
        if (cartInfoModels.getData()!=null){
            CartInfoModel cartInfoModelTest = new CartInfoModel();
            List<CartModelClass> test = new ArrayList<>();
            for (int i =0;i<cartInfoModels.getData().size();i++){
                if (cartInfoModels.getData().get(i).getQuantity()==0){

                } else {
                    test.add(cartInfoModels.getData().get(i));
                }
            }
            cartInfoModelTest.setData(test);

            Double toPay=0.0;
            for (int i=0;i<cartInfoModelTest.getData().size();i++){
                toPay = toPay+(cartInfoModelTest.getData().get(i).getSub_Total_Amount()*cartInfoModelTest.getData().get(i).getQuantity());
                if (i==cartInfoModelTest.getData().size()-1) {
                    if (i==0){
                        txtTotalQty.setText(cartInfoModelTest.getData().size()+"item");
                    } else {
                        txtTotalQty.setText(cartInfoModelTest.getData().size()+"items");
                    }
                    txtTotal.setText("₹"+String.valueOf(toPay));
                    relativeBottomSheet.setVisibility(View.VISIBLE);
                }
            }
        } else {
            cartInfoModel = new CartInfoModel();
            relativeBottomSheet.setVisibility(View.GONE);
        }

    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(ProductDetailsActivity.this, getResources().getString(R.string.no_internet));
    }
}
