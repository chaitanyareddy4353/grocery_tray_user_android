package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;

public class TermsAndConditionsActivity extends AppCompatActivity {
    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;

    WebView webView;
    ImageView imgBack;
    Integer Indicator=0;
//    TextView txtHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);

        allsharedpreference =  new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        Indicator = getIntent().getIntExtra("indicator",0);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Data...");
        progressDialog.setCancelable(false);

        webView = findViewById(R.id.webView);
        imgBack = findViewById(R.id.imgBack);
//        txtHeader = findViewById(R.id.txtHeader);
        webView.setWebViewClient(new MyBrowser());

        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if(Indicator == 1) {
//            txtHeader.setText("Terms and Conditions");
            webView.loadUrl(getIntent().getStringExtra("web"));
        } else if(Indicator == 2) {
//            txtHeader.setText("Privacy policy");
//            webView.loadUrl("https://trakb.com/privacy.html");
        } else if(Indicator == 3) {
//            txtHeader.setText("Legality");
//            webView.loadUrl("http://www.belt2door.com/disclaimer.html");
        }


        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100) {
                    progressDialog.show();
                }
                if (progress == 100) {
                    progressDialog.dismiss();
                }
            }
        });


    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}