package com.vixspace.grocerytray.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.squareup.picasso.Picasso;
import com.vixspace.grocerytray.MainActivity;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.adapters.CategoriesAdapter;
import com.vixspace.grocerytray.adapters.CollectionsAdapter;
import com.vixspace.grocerytray.adapters.SelecteddatesAdapter;
import com.vixspace.grocerytray.adapters.TimeSlotsAdapter;
import com.vixspace.grocerytray.pojos.AddUserAddressResponse;
import com.vixspace.grocerytray.pojos.ApplyOfferCouponCodeResponse;
import com.vixspace.grocerytray.pojos.CartInfoModel;
import com.vixspace.grocerytray.pojos.CartModelClass;
import com.vixspace.grocerytray.pojos.FetchAllAvailableDatesResponse;
import com.vixspace.grocerytray.pojos.FetchAllAvailableSlotsResponse;
import com.vixspace.grocerytray.pojos.FetchLocalityProductCompleteInformationResponse;
import com.vixspace.grocerytray.pojos.FetchUserWalletInformationResponse;
import com.vixspace.grocerytray.pojos.GenerateOrderPriceQuoteResponse;
import com.vixspace.grocerytray.pojos.GeneratePriceQuoteRequest;
import com.vixspace.grocerytray.pojos.ListAllUserAddressResponse;
import com.vixspace.grocerytray.pojos.PlaceOrderResponse;
import com.vixspace.grocerytray.pojos.RazorpaypayWalletResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmOrder extends AppCompatActivity implements TimeSlotsAdapter.Toucher,SelecteddatesAdapter.Dates, PaymentResultListener {

    Button btnConfirm,btnUpdate;
    ImageView imgBack;
    RecyclerView recycleDates,recyclerTimings;
    TextView txtAddress;
    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;
    String newAddress;
    RadioButton radioOnline,radioCOD;
    ProgressBar progressBar;

    String timeslot="", availabledate="",time ="",addressID="",CouponId="",orderId="",priceQuoteID="";
    Boolean couponapplied = false;
    Double amt = 0.0;
    Double walletAmt = 0.0;
    Double delivery = 0.0;
    Double discount = 0.0;
    Double toPay = 0.0;

    TextView txtApply;
    RelativeLayout relativeCoupon;
//    Boolean IsCoponApplied = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        imgBack = findViewById(R.id.imgBack);
        radioCOD = findViewById(R.id.radioCOD);
        radioOnline = findViewById(R.id.radioOnline);
        btnUpdate = findViewById(R.id.btnUpdate);
        txtAddress = findViewById(R.id.txtAddress);
        btnConfirm = findViewById(R.id.btnConfirm);
        recycleDates = findViewById(R.id.recycleDates);
        recyclerTimings = findViewById(R.id.recyclerTimings);
        progressBar = findViewById(R.id.progressBar);
        txtApply = findViewById(R.id.txtApply);
        relativeCoupon = findViewById(R.id.relativeCoupon);

//        txtAddress.setText(getIntent().getStringExtra("addressint"));
//        addressID = getIntent().getStringExtra("addressId");
        couponapplied = getIntent().getBooleanExtra("couponapplied",false);
        if (couponapplied){
            CouponId = getIntent().getStringExtra("couponID");
        }

        amt = getIntent().getDoubleExtra("total",0.0);

        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            selectTimeSlot();
            selectDates();
        }else{
            showNoInterNetMessage();
        }


//        txtAddress.setText("Opp Gachibowli traffic police station - SV, Hyderabad");

//        addressID = "a64c9e5a-901e-4890-9763-5e851e655ef0";

        txtApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmOrder.this,ApplyCoupon.class);
                startActivityForResult(intent,199);
            }
        });

        relativeCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmOrder.this,ApplyCoupon.class);
                startActivityForResult(intent,199);
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!addressID.equals("")){
                    if (timeslot.length()>0){
                        if (availabledate.length()>0){
                            if (radioOnline.isChecked()){
//                                if (amt<=walletAmt){
//                                    GeneratePriceQuote();
//                                } else {
//                                    progressBar.setVisibility(View.VISIBLE);
//                                    RazorPay(amt-walletAmt);
//                                }
//                                getWalletInformation();
                                if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                                    GeneratePriceQuote();
                                }else{
                                    showNoInterNetMessage();
                                }

                            } else {
                                if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                                    GeneratePriceQuote();
                                }else{
                                    showNoInterNetMessage();
                                }

                            }

                        } else {
                            Toast.makeText(ConfirmOrder.this, "please select a date", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ConfirmOrder.this, "please select a time slot", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ConfirmOrder.this, "Please add an address", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmOrder.this, AddressActivity.class);
                startActivityForResult(intent,102);
//                Intent intent = new Intent(ConfirmOrder.this, ChooseLocationActivity.class);
//                startActivityForResult(intent,101);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });


        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            getWalletInformation();
        }else{
            showNoInterNetMessage();
        }

        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            listAllAddress();
        }else{
            showNoInterNetMessage();
        }
    }

    private void getWalletInformation() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.walletInformation(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final FetchUserWalletInformationResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,FetchUserWalletInformationResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                        walletAmt = listOfProductsResponse.getExtras().getData().getAvailableAmount();

                    } else {
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(ConfirmOrder.this, "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ConfirmOrder.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
                    }

                }else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(ConfirmOrder.this, SplashActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        allsharedpreference.clearAllSharedPreferences();
                        startActivity(intent);
                        finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(ConfirmOrder.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getWalletInformationRazor() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.walletInformation(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final FetchUserWalletInformationResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,FetchUserWalletInformationResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                       Double walletAmts = listOfProductsResponse.getExtras().getData().getAvailableAmount();

                       walletAmt = listOfProductsResponse.getExtras().getData().getAvailableAmount();

                     if (walletAmts>=amt){
                         if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                             placeOrder(priceQuoteID);
                         }else{
                             showNoInterNetMessage();
                         }

                     } else {
                         progressBar.setVisibility(View.VISIBLE);
                         if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                             getWalletInformationRazor();
                         }else{
                             showNoInterNetMessage();
                         }
                     }

                    } else {
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(ConfirmOrder.this, "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ConfirmOrder.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
                    }

                }else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(ConfirmOrder.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void GeneratePriceQuote() {
        JsonObject hola = home();
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.generateOrderPriceQuote(hola);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final GenerateOrderPriceQuoteResponse loginResponse = gson.fromJson(splashScreenResponse,GenerateOrderPriceQuoteResponse.class);

                        if (loginResponse.getSuccess()) {

                            priceQuoteID = loginResponse.getExtras().getData().getPriceQuoteID();
                            delivery =loginResponse.getExtras().getData().getPricingInformation().getDeliveryPrice();
                            discount =loginResponse.getExtras().getData().getPricingInformation().getDiscountAmount();
                            toPay =loginResponse.getExtras().getData().getPricingInformation().getFinalTransactionAmount();

                            if (couponapplied){

//                                if (couponapplied){
                                if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                                    ApplyCouponCode(CouponId,loginResponse.getExtras().getData().getPriceQuoteID());
                                }else{
                                    showNoInterNetMessage();
                                }
                                //                                } else {
//
//                                    placeOrder(loginResponse.getExtras().getData().getPriceQuoteID());
//                                }
                                Toast.makeText(ConfirmOrder.this, "generate price quote generated", Toast.LENGTH_SHORT).show();

                            } else {
//                                GeneratePriceQuote();
//                                RazorPay(loginResponse.getExtras().getData().getPricingInformation().getFinalTransactionAmount());
//
//                                placeOrder(loginResponse.getExtras().getData().getPriceQuoteID());
                                Intent intent = new Intent(ConfirmOrder.this,PlaceOrder.class);
                                intent.putExtra("amt",amt);
                                intent.putExtra("priceQuoteId",priceQuoteID);
                                intent.putExtra("delivery",loginResponse.getExtras().getData().getPricingInformation().getDeliveryPrice());
                                intent.putExtra("discount",getIntent().getStringExtra("discount"));
                                intent.putExtra("toPay",loginResponse.getExtras().getData().getPricingInformation().getFinalTransactionAmount());
                                intent.putExtra("couponapplied",couponapplied);
                                if (radioOnline.isChecked()){
                                    intent.putExtra("online",true);
                                } else {
                                    intent.putExtra("online",false);
                                }
                                if (couponapplied){
                                    intent.putExtra("coupon",CouponId);
                                }
                                startActivity(intent);
                            }

                            // allsharedpreference.setShared_String(Allsharedpreference.CurrentLocation,loginResponse.getExtras().getData().getLocalityData().getCityTitle());
//                            allsharedpreference.setShared_String(Allsharedpreference.CurrentCityID,loginResponse.getExtras().getData().getLocalityData().getCityID());
//                            allsharedpreference.setShared_String(Allsharedpreference.LocalityID,loginResponse.getExtras().getData().getLocalityData().getLocalityID());
//                            allsharedpreference.setShared_String(Allsharedpreference.Locality_Title,loginResponse.getExtras().getData().getLocalityData().getLocalityTitle());

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(ConfirmOrder.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    private void ApplyCouponCode(String couponId, final String priceQuoteID) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("PriceQuoteID",priceQuoteID);
        jsonObject.addProperty("Coupon_Code",couponId);
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.applyOfferCouponCode(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final ApplyOfferCouponCodeResponse splash = gson.fromJson(splashScreenResponse,ApplyOfferCouponCodeResponse.class);

//                        ApplyCouponCodeResponse splash = response.body();

                        if (splash.getSuccess()) {

                            Toast.makeText(ConfirmOrder.this, "Coupon Code Applied", Toast.LENGTH_SHORT).show();

//                            placeOrder(priceQuoteID);
                            Intent intent = new Intent(ConfirmOrder.this,PlaceOrder.class);
                            intent.putExtra("amt",amt);
                            intent.putExtra("priceQuoteId",priceQuoteID);
                            intent.putExtra("delivery",delivery);
                            intent.putExtra("discount",discount);
                            intent.putExtra("toPay",toPay);
                            intent.putExtra("couponapplied",couponapplied);
                            if (radioOnline.isChecked()){
                                intent.putExtra("online",true);
                            } else {
                                intent.putExtra("online",false);
                            }
                            if (couponapplied){
                                intent.putExtra("coupon",CouponId);
                            }
                            startActivity(intent);

                        }
                        else{
                            commonMethods.showToast(splash.getExtras().getMsg());
//                            placeOrder(priceQuoteID);
                            Intent intent = new Intent(ConfirmOrder.this,PlaceOrder.class);
                            intent.putExtra("amt",amt);
                            intent.putExtra("priceQuoteId",priceQuoteID);
                            intent.putExtra("delivery",delivery);
                            intent.putExtra("discount",discount);
                            intent.putExtra("toPay",toPay);
                            intent.putExtra("address",txtAddress.getText().toString());
                            intent.putExtra("couponapplied",couponapplied);
                            if (radioOnline.isChecked()){
                                intent.putExtra("online",true);
                            } else {
                                intent.putExtra("online",false);
                            }
                            if (couponapplied){
                                intent.putExtra("coupon",CouponId);
                            }
                            startActivity(intent);

                        }
                    } else {
//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final ApplyOfferCouponCodeResponse splash = gson.fromJson(response.errorBody().charStream(),ApplyOfferCouponCodeResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(ConfirmOrder.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
//                        }
//                        commonMethods.showToast("Something went wrong");
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());
                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }
    }

    private void placeOrder(String priceQuoteID) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("PriceQuoteID",priceQuoteID);
        if (couponapplied){
            jsonObject.addProperty("Whether_Coupon_Applied",true);
            jsonObject.addProperty("AppliedCouponID",CouponId);
        } else {
            jsonObject.addProperty("Whether_Coupon_Applied",false);
            jsonObject.addProperty("AppliedCouponID","");
        }

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.placeOrder(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final PlaceOrderResponse loginResponse = gson.fromJson(splashScreenResponse,PlaceOrderResponse.class);

                        if (loginResponse.getSuccess()) {

                            orderId = loginResponse.getExtras().getData().getOrderID();
//                            RazorPay(loginResponse.getExtras().getData().getPricingInformation().getFinalTransactionAmount());

                            CartInfoModel cartInfoModel = new CartInfoModel();
                            List<CartModelClass> cartModelClasses = new ArrayList<>();
                            cartInfoModel.setData(cartModelClasses);
                            Gson gsons = new Gson();
                            String json = gsons.toJson(cartInfoModel); // myObject - instance of MyObject
                            allsharedpreference.setShared_String(Allsharedpreference.CartData,json);
                            Toast.makeText(ConfirmOrder.this, "order created succesfully    ", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ConfirmOrder.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

//                            allsharedpreference.setShared_String(Allsharedpreference.CurrentLocation,loginResponse.getExtras().getData().getLocalityData().getCityTitle());
//                            allsharedpreference.setShared_String(Allsharedpreference.CurrentCityID,loginResponse.getExtras().getData().getLocalityData().getCityID());
//                            allsharedpreference.setShared_String(Allsharedpreference.LocalityID,loginResponse.getExtras().getData().getLocalityData().getLocalityID());
//                            allsharedpreference.setShared_String(Allsharedpreference.Locality_Title,loginResponse.getExtras().getData().getLocalityData().getLocalityTitle());

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(ConfirmOrder.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    private JsonObject home() {
        GeneratePriceQuoteRequest requestModel = new GeneratePriceQuoteRequest();
        requestModel.setApiKey(allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        requestModel.setUSERID(allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        requestModel.setSessionID(allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        requestModel.setCityID(allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        requestModel.setLocalityID(allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        requestModel.setAddressID(addressID);
        requestModel.setSLOTID(timeslot);
        requestModel.setSelectedDateTime(availabledate+" "+time);
        if (radioOnline.isChecked()){
            requestModel.setPaymentMode(1);
        } else {
            requestModel.setPaymentMode(2);
        }
       List< GeneratePriceQuoteRequest.CartInformation> cartInformation = new ArrayList<>();
        Gson gson = new Gson();
        String data = allsharedpreference.getShared_String(Allsharedpreference.CartData);
        CartInfoModel cartInfoModel = gson.fromJson(data, CartInfoModel.class);

        for (int i =0;i<cartInfoModel.getData().size();i++){
            GeneratePriceQuoteRequest.CartInformation cartInformation1 = new GeneratePriceQuoteRequest.CartInformation();
            cartInformation1.setLocalityProductID(cartInfoModel.getData().get(i).getLocalityProductID());
            cartInformation1.setQuantity(cartInfoModel.getData().get(i).getQuantity());
            cartInformation.add(cartInformation1);
        }
        requestModel.setCartInformation(cartInformation);

        return new Gson().toJsonTree(requestModel).getAsJsonObject();

    }

    private void selectTimeSlot(){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.allAvailableSlots(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final FetchAllAvailableSlotsResponse loginResponse =
                                gson.fromJson(splashScreenResponse,FetchAllAvailableSlotsResponse.class);

                        if (loginResponse.getSuccess()) {

                            TimeSlotsAdapter adapter = new TimeSlotsAdapter(ConfirmOrder.this, loginResponse.getExtras().getData(),ConfirmOrder.this);
                            recyclerTimings.setLayoutManager(new GridLayoutManager(ConfirmOrder.this,2,GridLayoutManager.VERTICAL,false));
                            recyclerTimings.setAdapter(adapter);

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());
//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    private void selectDates() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.sendNextAvailableDates(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final FetchAllAvailableDatesResponse loginResponse =
                                gson.fromJson(splashScreenResponse,FetchAllAvailableDatesResponse.class);

                        if (loginResponse.getSuccess()) {

                            SelecteddatesAdapter adapter = new SelecteddatesAdapter(ConfirmOrder.this, loginResponse.getExtras().getData(),ConfirmOrder.this);
                            recycleDates.setLayoutManager(new LinearLayoutManager(ConfirmOrder.this,RecyclerView.HORIZONTAL,false));
                            recycleDates.setAdapter(adapter);

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());

//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    @Override
    public void onToucher(String Id,String times) {
        timeslot = Id;
        time = times;
    }

    @Override
    public void onDateTouched(String Id) {
        availabledate = Id;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==101&& resultCode == RESULT_OK){
            String lat = data.getStringExtra("lat");
            String lon = data.getStringExtra("lon");
            String strAddress = data.getStringExtra("address");
            String city  = data.getStringExtra("city");
            String state = data.getStringExtra("state");
            String pincode = data.getStringExtra("pin");
            String addressint = data.getStringExtra("addressint");

            allsharedpreference.setShared_String(Allsharedpreference.SELECTED_LON, String.valueOf(lon));
            allsharedpreference.setShared_String(Allsharedpreference.SELECTED_LAT, String.valueOf(lat));

            newAddress = (strAddress +", " +city+ ", " + state +", " + pincode);


            if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                validateAddress(lat,lon,1,newAddress,addressint);
            }else{
                showNoInterNetMessage();
            }


        } else if (requestCode==102&&resultCode==RESULT_OK) {
            addressID = data.getStringExtra("addressId");
            String address = data.getStringExtra("address");
            txtAddress.setText(address);
        } else if (requestCode==199&& resultCode == RESULT_OK){
//            CouponAmt = data.getStringExtra("couponAmt");
            CouponId = data.getStringExtra("couponId");
//            Total = data.getStringExtra("total");
            couponapplied = true;
        }
    }

    private void validateAddress(String lat, String lon, Integer indicator, final String newAddress, String addressint) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("Name",allsharedpreference.getShared_String(Allsharedpreference.UserName));
        jsonObject.addProperty("PhoneNumber",allsharedpreference.getShared_String(Allsharedpreference.PhoneNumber));
        jsonObject.addProperty("AddressType",1);
        jsonObject.addProperty("Address",addressint);
        jsonObject.addProperty("Latitude",lat);
        jsonObject.addProperty("Longitude",lon);

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.addAddress(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final AddUserAddressResponse loginResponse = gson.fromJson(splashScreenResponse,AddUserAddressResponse.class);

                        if (loginResponse.getSuccess()) {

                            addressID = loginResponse.getExtras().getData().getAddressID();
//                            allsharedpreference.setShared_String(Allsharedpreference.CurrentLocation,loginResponse.getExtras().getData().getLocalityData().getCityTitle());
//                            allsharedpreference.setShared_String(Allsharedpreference.CurrentCityID,loginResponse.getExtras().getData().getLocalityData().getCityID());
//                            allsharedpreference.setShared_String(Allsharedpreference.LocalityID,loginResponse.getExtras().getData().getLocalityData().getLocalityID());
//                            allsharedpreference.setShared_String(Allsharedpreference.Locality_Title,loginResponse.getExtras().getData().getLocalityData().getLocalityTitle());
                            txtAddress.setText(loginResponse.getExtras().getData().getAddress());

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(ConfirmOrder.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }

//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    @Override
    public void onPaymentSuccess(String s) {
//        getWalletInformationRazor();
        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            getWalletInformationRazor();
        }else{
            showNoInterNetMessage();
        }
//        placeOrder(priceQuoteID);
//                            CartInfoModel cartInfoModel = new CartInfoModel();
//                            List<CartModelClass> cartModelClasses = new ArrayList<>();
//                            cartInfoModel.setData(cartModelClasses);
//                            Gson gsons = new Gson();
//                            String json = gsons.toJson(cartInfoModel); // myObject - instance of MyObject
//                            allsharedpreference.setShared_String(Allsharedpreference.CartData,json);
//                            Toast.makeText(ConfirmOrder.this, "order created succesfully    ", Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent(ConfirmOrder.this, MainActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            startActivity(intent);
    }

    @Override
    public void onPaymentError(int i, String s) {
        Toast.makeText(ConfirmOrder.this, "oops something went wrong ", Toast.LENGTH_SHORT).show();

    }

    private void RazorPay(Double finalTransactionAmount) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("Amount",finalTransactionAmount);
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.razorPay(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final RazorpaypayWalletResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,RazorpaypayWalletResponse.class);

                    if(listOfProductsResponse.getSuccess()) {


                        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                            makePayment(listOfProductsResponse.getExtras().getData().getTotalAmount(),listOfProductsResponse.getExtras().getData().getAmountRequestID());
                        }else{
                            showNoInterNetMessage();
                        }

                    } else {
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(ConfirmOrder.this, "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ConfirmOrder.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
                    }

                }else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(ConfirmOrder.this, SplashActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(ConfirmOrder.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void makePayment(Double totalAmount, String amountRequestID) {

        Checkout checkout = new Checkout();
        //checkout.setFullScreenDisable(true);
        /**
         * Set your logo here
         */
        //checkout.setImage(R.drawable.);

        /**
         * Reference to current activity
         */
        final Activity activity = this;

        /**
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */
        try {
            JSONObject options = new JSONObject();

            /**
             * Merchant Name
             * eg: Rentomojo || HasGeek etc.
             */

            /**
             * Description can be anything
             * eg: Order #123123
             *     Invoice Payment
             *     etc.
             */

            JSONObject jsonObject = new JSONObject();

            String newd = jsonObject.toString();
            jsonObject.put("Amount_RequestID",amountRequestID);
            options.put("notes", jsonObject);


            options.put("currency", "INR");
            JSONObject preFill = new JSONObject();

            options.put("prefill", preFill);

            Double amt = totalAmount;

            DecimalFormat df = new DecimalFormat("#.00");
            String angleFormated = df.format(amt);
            amt=  Double.valueOf(angleFormated);


            options.put("amount",String.valueOf((amt)*100));


            /**
             * Amount is always passed in PAISE
             * Eg: "500" = Rs 5.00
             */
//            options.put("amount",String.valueOf((RazorPayAmount)*100));

            checkout.open(activity, options);
        } catch(Exception e) {
            Log.e("payment_ERROR", "Error in starting Razorpay Checkout"+ e.toString());
        }
    }

    private void listAllAddress() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.allAddresses(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final ListAllUserAddressResponse loginResponse = gson.fromJson(splashScreenResponse,ListAllUserAddressResponse.class);

                        if (loginResponse.getSuccess()) {

                            if (loginResponse.getExtras().getData().size()>0){

                                txtAddress.setText(loginResponse.getExtras().getData().get(0).getAddress());
                                addressID = loginResponse.getExtras().getData().get(0).getAddressID();
                            }
                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }

                    } else {

                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(ConfirmOrder.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }

                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(ConfirmOrder.this, getResources().getString(R.string.no_internet));
    }

}
