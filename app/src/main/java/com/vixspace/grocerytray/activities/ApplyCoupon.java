package com.vixspace.grocerytray.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.adapters.CouponsAdapter;
import com.vixspace.grocerytray.pojos.ApplyOfferCouponCodeResponse;
import com.vixspace.grocerytray.pojos.FetchAllAvailableOfferCouponsResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApplyCoupon extends AppCompatActivity implements CouponsAdapter.Toucher {

    ImageView imgBack;
    RecyclerView recyclerCoupons;
    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;
    List<FetchAllAvailableOfferCouponsResponse.Datum> datumList = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    ProgressBar progressBar;
    LinearLayout linearNull;

    int skipValue = 0;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean loading = true;
    CouponsAdapter couponsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_coupon);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        recyclerCoupons = findViewById(R.id.recyclerCoupons);
        imgBack = findViewById(R.id.imgBack);
        linearNull = findViewById(R.id.linearNull);
        progressBar = findViewById(R.id.progressBar);

        recyclerCoupons.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.v("...", "Last Item Wow !");
                            //Do pagination.. i.e. fetch new data
//                            progressbar.setVisibility(View.VISIBLE);

                            skipValue = skipValue + 30;
                            Log.e("SkipValue---->", String.valueOf(skipValue));

                            ListCoupons();

                            loading = true;
                        }
                    }
                }
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        ShowCoupons();


    }

    private void ListCoupons() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("skip",skipValue);
        jsonObject.addProperty("limit",30);

        APIService apiService = RestApiClient.getClient().create(APIService.class);

        Call<JsonObject> all_products = apiService.availableOfferCoupons(jsonObject);

        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if(response.isSuccessful()) {

                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final FetchAllAvailableOfferCouponsResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,FetchAllAvailableOfferCouponsResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                        if (listOfProductsResponse.getExtras().getData().size()>0){
                            linearNull.setVisibility(View.GONE);
                            datumList.addAll(listOfProductsResponse.getExtras().getData());
                            couponsAdapter.notifyDataSetChanged();
                            progressBar.setVisibility(View.GONE);
                        } else {
//                            Toast.makeText(getActivity(), "No orders yet", Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                            linearNull.setVisibility(View.VISIBLE);
                        }


                    } else {
                        progressBar.setVisibility(View.GONE);
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(ApplyCoupon.this, "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ApplyCoupon.this, SplashActivity.class);
                            allsharedpreference.clearAllSharedPreferences();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }

                } else {
//                    if (response.message().equals("Bad Request")){
                        Gson gson = new Gson();
                        final FetchAllAvailableOfferCouponsResponse splash = gson.fromJson(response.errorBody().charStream(),FetchAllAvailableOfferCouponsResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ApplyCoupon.this,t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });


    }

    private void ShowCoupons() {
        couponsAdapter = new CouponsAdapter(ApplyCoupon.this,datumList,this);
        linearLayoutManager = new LinearLayoutManager(ApplyCoupon.this, LinearLayoutManager.VERTICAL,false);
        recyclerCoupons.setLayoutManager(linearLayoutManager);
        recyclerCoupons.setAdapter(couponsAdapter);


        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            ListCoupons();
        }else{
            showNoInterNetMessage();
        }

    }

    @Override
    public void onToucher(String Id) {
//        ApplyCouponCode(Id);
        Toast.makeText(ApplyCoupon.this, "Coupon Code Copied", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent();
//        intent.putExtra("couponAmt",String.valueOf(splash.getExtras().getData().getPricingInformation().getDiscountAmount()));
        intent.putExtra("couponId",Id);
//        intent.putExtra("couponId",splash.getExtras().getData().getAppliedCouponID());
//        intent.putExtra("total",String.valueOf(splash.getExtras().getData().getPricingInformation().getFinalTransactionAmount()));
        setResult(RESULT_OK, intent);
        finish();

    }

    private void ApplyCouponCode(final String id) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("PriceQuoteID","");
        jsonObject.addProperty("Coupon_Code",id);
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.applyOfferCouponCode(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final ApplyOfferCouponCodeResponse splash = gson.fromJson(splashScreenResponse,ApplyOfferCouponCodeResponse.class);

//                        ApplyCouponCodeResponse splash = response.body();

                        if (splash.getSuccess()) {

                            Toast.makeText(ApplyCoupon.this, "Coupon Code Applied", Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent();
                            intent.putExtra("couponAmt",String.valueOf(splash.getExtras().getData().getPricingInformation().getDiscountAmount()));
                            intent.putExtra("couponId",splash.getExtras().getData().getAppliedCouponID());
                            intent.putExtra("total",String.valueOf(splash.getExtras().getData().getPricingInformation().getFinalTransactionAmount()));
                            setResult(RESULT_OK, intent);
                            finish();

                        }
                        else{
                            commonMethods.showToast(splash.getExtras().getMsg());
                        }
                    } else {
//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final ApplyOfferCouponCodeResponse splash = gson.fromJson(response.errorBody().charStream(),ApplyOfferCouponCodeResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(ApplyCoupon.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }

//                        }
//                        commonMethods.showToast("Something went wrong");
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());
                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }
    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(ApplyCoupon.this, getResources().getString(R.string.no_internet));
    }
}