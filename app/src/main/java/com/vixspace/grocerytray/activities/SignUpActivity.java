package com.vixspace.grocerytray.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.ImageUploadResponse;
import com.vixspace.grocerytray.pojos.SplashScreenResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.services.RestApiClientImage;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {
    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;

    ProgressBar progressBar;
    ImageView imgProfile;
    EditText edtFirstName,edtLastName,edtMailAddress;
    Button btnSubmit;
    private static final int STORAGE_PERMISSION_CODE = 123 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        imgProfile = findViewById(R.id.imgProfile);
        edtFirstName = findViewById(R.id.edtFirstName);
        edtLastName = findViewById(R.id.edtLastName);
        edtMailAddress = findViewById(R.id.edtMailAddress);
        btnSubmit = findViewById(R.id.btnSubmit);
        progressBar = findViewById(R.id.progressBar);

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestStoragePermission();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtFirstName.getText().toString().trim().length()!=0){
                    if (edtLastName.getText().toString().trim().length()!=0){
                        progressBar.setVisibility(View.VISIBLE);
                        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                            userInfoUpdate();
                        }else{
                            showNoInterNetMessage();
                        }
                    } else {
                        Toast.makeText(SignUpActivity.this, "Last name can't be empty", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SignUpActivity.this, "First name can't be empty", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void requestStoragePermission() {
        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                showFileChooser();
            } else {
                //Request Location Permission
                requestPermissions(
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, STORAGE_PERMISSION_CODE);
            }
        } else {
            showFileChooser();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == STORAGE_PERMISSION_CODE && grantResults.length == 2
                && (grantResults[0] == PackageManager.PERMISSION_GRANTED)&&(grantResults[1] == PackageManager.PERMISSION_GRANTED)) {

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    //Location Permission already granted
                    showFileChooser();
                } else {
                    //Request Location Permission
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA},
                            STORAGE_PERMISSION_CODE);
                }
            }
        } else {
            //If the user denies the permission request, then display a toast with some more information
            Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
//            requestStoragePermission();
        }
    }

    private void showFileChooser() {
        CropImage.activity()
                .start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Log.e("ImageURI", String.valueOf(resultUri));
                try {
                    progressBar.setVisibility(View.VISIBLE);
//                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), resultUri);
                    InputStream is = getApplicationContext().getContentResolver().openInputStream(resultUri);

                    if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                        imageUpload(getBytes(is),resultUri);
                    }else{
                        showNoInterNetMessage();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public byte[] getBytes(InputStream is) throws IOException {
        ByteArrayOutputStream byteBuff = new ByteArrayOutputStream();

        int buffSize = 1024;
        byte[] buff = new byte[buffSize];

        int len = 0;
        while ((len = is.read(buff)) != -1) {
            byteBuff.write(buff, 0, len);
        }

        return byteBuff.toByteArray();
    }

    private void imageUpload(byte[] imageBytes, final Uri resultUri) {

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), imageBytes);

        MultipartBody.Part body = MultipartBody.Part.createFormData("image", "image.jpg", requestFile);
        APIService apiService = RestApiClientImage.getClient().create(APIService.class);
        Call<ImageUploadResponse> apiResponse = apiService.uploadImage(body);
        try{
            apiResponse.enqueue(new Callback<ImageUploadResponse>() {
                @Override
                public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                    if(response.isSuccessful()){

                        ImageUploadResponse imageUploadResponse = response.body();

                        if(imageUploadResponse.isSuccess()) {

                            imgProfile.setImageURI(resultUri);
                            if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                                updateImage(imageUploadResponse.getExtras().getImageID(),imageUploadResponse.getExtras().getImage550());
                            }else{
                                showNoInterNetMessage();
                            }
                        }

                    } else {
                        commonMethods.showToast("Something went wrong");
                        progressBar.setVisibility(View.GONE);
                    }
                }
                @Override
                public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    commonMethods.showToast(t.getMessage());
                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
            progressBar.setVisibility(View.GONE);
        }
    }

    private void updateImage(final String imageID, final String image550) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("ImageID",imageID);
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<UpdateFcmResponse> apiResponse = apiService.updateUserImage(jsonObject);
        try{
            apiResponse.enqueue(new Callback<UpdateFcmResponse>() {
                @Override
                public void onResponse(Call<UpdateFcmResponse> call, Response<UpdateFcmResponse> response) {

                    if(response.isSuccessful()){

                        UpdateFcmResponse loginResponse = response.body();

                        if (loginResponse.getSuccess()) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(SignUpActivity.this, "Image updated", Toast.LENGTH_SHORT).show();
                            allsharedpreference.setShared_String(Allsharedpreference.img,image550);
                            allsharedpreference.setShared_Booleen(Allsharedpreference.imgAvailable,true);
                        }
                        else {
                            progressBar.setVisibility(View.GONE);
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                            progressBar.setVisibility(View.GONE);
                            Log.e("Failed Msg", "Success False Msg");
                            int code = loginResponse.getExtras().getCode();
                            Log.e("msg", String.valueOf(code));
                            if (code == 1) {
                                Toast.makeText(SignUpActivity.this, "Session Expired", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(SignUpActivity.this, SplashActivity.class);
                                allsharedpreference.clearAllSharedPreferences();
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        }
                    }
//                    else {

//                        commonMethods.showToast("Something went wrong");
                     else {
//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final SplashScreenResponse splash = gson.fromJson(response.errorBody().charStream(),SplashScreenResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(SignUpActivity.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }

//                        }
                        progressBar.setVisibility(View.GONE);
                    }

                }
                @Override
                public void onFailure(Call<UpdateFcmResponse> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());
                    progressBar.setVisibility(View.GONE);

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
            progressBar.setVisibility(View.GONE);
        }
    }

    private void userInfoUpdate() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("Name",edtFirstName.getText().toString().trim()+" "+edtLastName.getText().toString().trim());
        jsonObject.addProperty("EmailID",edtMailAddress.getText().toString());

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.updateUserinfo(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){
                        progressBar.setVisibility(View.GONE);

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final UpdateFcmResponse loginResponse = gson.fromJson(splashScreenResponse,UpdateFcmResponse.class);

                        if (loginResponse.getSuccess()) {

                            allsharedpreference.setShared_String(Allsharedpreference.UserName,edtFirstName.getText().toString().trim()+" "+edtLastName.getText().toString().trim());
                            allsharedpreference.setShared_String(Allsharedpreference.EmailId,edtMailAddress.getText().toString().trim());
                            allsharedpreference.setShared_Booleen(Allsharedpreference.Registered,true);
                            startActivity(new Intent(SignUpActivity.this, AcceptLocationActivity.class));
                            finish();

                        }
                        else{

                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(SignUpActivity.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
//                        }
                        progressBar.setVisibility(View.GONE);
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }
    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(SignUpActivity.this, getResources().getString(R.string.no_internet));
    }

}