package com.vixspace.grocerytray.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginSignUpActivity extends AppCompatActivity {

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;

    TextView txtSkip;
    EditText edtMobileNo;
    TextView btnContinue,btnContinueGo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        txtSkip = findViewById(R.id.txtSkip);
        edtMobileNo = findViewById(R.id.edtMobileNo);
        btnContinue = findViewById(R.id.btnContinue);
        btnContinueGo = findViewById(R.id.btnContinueGo);

        edtMobileNo.addTextChangedListener(new GenericTextWatcher(edtMobileNo));

        txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginSignUpActivity.this,AcceptLocationActivity.class);
                allsharedpreference.setShared_Booleen(Allsharedpreference.Skip,true);
                startActivity(intent);
                finish();
            }
        });

        btnContinueGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtMobileNo.getText().toString().trim().length()==10){
                    if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                        ValidateMobile();
                    }else{
                        showNoInterNetMessage();
                    }

                } else {
                    Toast.makeText(LoginSignUpActivity.this, "Invalid mobile number", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void ValidateMobile() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("PhoneNumber",edtMobileNo.getText().toString());
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.generateOTP(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final UpdateFcmResponse loginResponse = gson.fromJson(splashScreenResponse,UpdateFcmResponse.class);

                        if (loginResponse.getSuccess()) {

                            allsharedpreference.setShared_String(Allsharedpreference.PhoneNumber,edtMobileNo.getText().toString().trim());
                            startActivity(new Intent(LoginSignUpActivity.this,OtpActivity.class));

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());

//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }
    }

    public class GenericTextWatcher implements TextWatcher {
        private View view;
        private GenericTextWatcher(View view)
        {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString().trim();
            switch (view.getId()) {
                case R.id.edtMobileNo:
                    if (editable.length()==10){
                        btnContinueGo.setVisibility(View.VISIBLE);
                        btnContinue.setVisibility(View.GONE);
                    } else if (editable.length()<10){
                        btnContinueGo.setVisibility(View.GONE);
                        btnContinue.setVisibility(View.VISIBLE);
                    }
                    break;

            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(LoginSignUpActivity.this, getResources().getString(R.string.no_internet));
    }
}
