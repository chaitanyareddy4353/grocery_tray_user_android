package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.adapters.CollectionProductsAdapter;
import com.vixspace.grocerytray.adapters.CollectionsAdapter;
import com.vixspace.grocerytray.pojos.FetchAllCategoryCollectionResponse;
import com.vixspace.grocerytray.pojos.FetchAllCollectionproductsResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllCollectionProductsActivity extends AppCompatActivity implements CollectionsAdapter.Collect {

    RecyclerView recyclerCollectionProducts,recyclerCollections;
    ImageView imgBack;

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;
    TextView txtHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_collection_products);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        txtHeader = findViewById(R.id.txtHeader);
        imgBack = findViewById(R.id.imgBack);
        recyclerCollectionProducts = findViewById(R.id.recyclerCollectionProducts);
        recyclerCollections = findViewById(R.id.recyclerCollections);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            allCollections(getIntent().getStringExtra("CategoryID"));
        }else{
            showNoInterNetMessage();
        }


        txtHeader.setText(getIntent().getStringExtra("CategoryName"));

//        allCollectionProducts(getIntent().getStringExtra("CollectionID"));

    }

    private void allCollections(String categoryID) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("CategoryID",categoryID);

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.allCategoryCollections(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final FetchAllCategoryCollectionResponse loginResponse =
                                gson.fromJson(splashScreenResponse,FetchAllCategoryCollectionResponse.class);

                        if (loginResponse.getSuccess()) {

                            CollectionsAdapter adapter = new CollectionsAdapter(AllCollectionProductsActivity.this, loginResponse.getExtras().getData(),AllCollectionProductsActivity.this);
                            recyclerCollections.setLayoutManager(new LinearLayoutManager(AllCollectionProductsActivity.this,RecyclerView.HORIZONTAL,false));
                            recyclerCollections.setAdapter(adapter);

                            if (loginResponse.getExtras().getData().size()>0){

                                if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                                    allCollectionProducts(loginResponse.getExtras().getData().get(0).getCollectionID());
                                }else{
                                    showNoInterNetMessage();
                                }

                            } else {
                                Toast.makeText(AllCollectionProductsActivity.this, "no items in this collection", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());

//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    private void allCollectionProducts(String collectionID) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("CollectionID",collectionID);

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.allCollectionProducts(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final FetchAllCollectionproductsResponse loginResponse =
                                gson.fromJson(splashScreenResponse,FetchAllCollectionproductsResponse.class);

                        if (loginResponse.getSuccess()) {

                            CollectionProductsAdapter adapter = new CollectionProductsAdapter(AllCollectionProductsActivity.this, loginResponse.getExtras().getData());
                            recyclerCollectionProducts.setLayoutManager(new GridLayoutManager(AllCollectionProductsActivity.this,2,GridLayoutManager.VERTICAL,false));
                            recyclerCollectionProducts.setAdapter(adapter);

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());

//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    @Override
    public void onCollect(String Id, String name) {

        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            allCollectionProducts(Id);
        }else{
            showNoInterNetMessage();
        }

    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(AllCollectionProductsActivity.this, getResources().getString(R.string.no_internet));
    }

}