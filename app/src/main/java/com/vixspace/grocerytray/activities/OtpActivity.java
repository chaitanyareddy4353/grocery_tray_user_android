package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.MainActivity;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.pojos.ValidateLoginOtpResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpActivity extends AppCompatActivity {

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;
    Button btnContinue;

    LinearLayout linearResend;
    EditText editText1,editText2,editText3,editText4;
    private EditText[] editTexts;
    public StringBuilder finalOtp;
    TextView txtOtp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        commonMethods = new CommonMethods(this);
        allsharedpreference = new Allsharedpreference(this);

        linearResend = findViewById(R.id.linearResend);
        btnContinue = findViewById(R.id.btnContinue);
        txtOtp = findViewById(R.id.txtOtp);
        editText1 = findViewById(R.id.editText1);
        editText2 = findViewById(R.id.editText2);
        editText3 = findViewById(R.id.editText3);
        editText4 = findViewById(R.id.editText4);

        txtOtp.setText("+91 " + allsharedpreference.getShared_String(Allsharedpreference.PhoneNumber));

        editText1.requestFocus();

        OTP();

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StringBuilder otp = new StringBuilder();
                for(int i=0;i<editTexts.length;i++){
                    otp=otp.append(editTexts[i].getText().toString().trim());
                }

                finalOtp = otp;
                String One = finalOtp.toString();
                if (One.length()==4){
                    ValidateForgotUserPassword(One);
                } else {
                    Toast.makeText(OtpActivity.this, "Invalid OTP", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void ValidateForgotUserPassword(String one) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("PhoneNumber",allsharedpreference.getShared_String(Allsharedpreference.PhoneNumber));
        jsonObject.addProperty("OTP",one);
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.validateotp(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final ValidateLoginOtpResponse loginResponse = gson.fromJson(splashScreenResponse,ValidateLoginOtpResponse.class);


                        if (loginResponse.getSuccess()) {

                            allsharedpreference.setShared_String(Allsharedpreference.USER_ID,loginResponse.getExtras().getData().getUSERID());
                            allsharedpreference.setShared_String(Allsharedpreference.SessionId,loginResponse.getExtras().getData().getSessionID());

                            if (loginResponse.getExtras().getData().getWhetherBasicInformationAvailable()){
                                allsharedpreference.setShared_Booleen(Allsharedpreference.IS_ALREADY_LOGIN,true);
                                allsharedpreference.setShared_Booleen(Allsharedpreference.Registered,true);
//                                startActivity(new Intent(OtpActivity.this, MainActivity.class));
                                if (allsharedpreference.getShared_Booleen(Allsharedpreference.Location)){
                                    startActivity(new Intent(OtpActivity.this, MainActivity.class));
                                    finish();
                                } else {
                                    startActivity(new Intent(OtpActivity.this, AcceptLocationActivity.class));
                                    finish();
                                }

                                if (loginResponse.getExtras().getData().getWhetherImageAvailable()){
                                    allsharedpreference.setShared_Booleen(Allsharedpreference.imgAvailable,true);
                                    allsharedpreference.setShared_String(Allsharedpreference.img,loginResponse.getExtras().getData().getImageData().getImage550());
                                }

                                allsharedpreference.setShared_String(Allsharedpreference.UserName,loginResponse.getExtras().getData().getName());
                                allsharedpreference.setShared_String(Allsharedpreference.EmailId,loginResponse.getExtras().getData().getEmailID());
//                                finish();
                            } else {
                                allsharedpreference.setShared_Booleen(Allsharedpreference.IS_ALREADY_LOGIN,true);
                                if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
                                    if (allsharedpreference.getShared_Booleen(Allsharedpreference.Location)){
                                        startActivity(new Intent(OtpActivity.this, MainActivity.class));
                                        finish();
                                    } else {
                                        startActivity(new Intent(OtpActivity.this, AcceptLocationActivity.class));
                                        finish();
                                    }
                                } else {
                                        startActivity(new Intent(OtpActivity.this, SignUpActivity.class));
                                    finish();
                                }
                            }

                        }
                        else{

                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(OtpActivity.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }
    }

    private void OTP() {
        editTexts = new EditText[]{editText1, editText2, editText3, editText4};
        editText1.addTextChangedListener(new PinTextWatcher(0));
        editText2.addTextChangedListener(new PinTextWatcher(1));
        editText3.addTextChangedListener(new PinTextWatcher(2));
        editText4.addTextChangedListener(new PinTextWatcher(3));

        editText1.setOnKeyListener(new PinOnKeyListener(0));
        editText2.setOnKeyListener(new PinOnKeyListener(1));
        editText3.setOnKeyListener(new PinOnKeyListener(2));
        editText4.setOnKeyListener(new PinOnKeyListener(3));
    }

    public class PinTextWatcher implements TextWatcher {

        private int currentIndex;
        private boolean isFirst = false, isLast = false;
        private String newTypedString = "";

        PinTextWatcher(int currentIndex) {
            this.currentIndex = currentIndex;

            if (currentIndex == 0)
                this.isFirst = true;
            else if (currentIndex == editTexts.length - 1)
                this.isLast = true;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            newTypedString = s.subSequence(start, start + count).toString().trim();
        }

        @Override
        public void afterTextChanged(Editable s) {

            String text = newTypedString;

            /* Detect paste event and set first char */
            if (text.length() > 1)
                text = String.valueOf(text.charAt(0)); // TODO: We can fill out other EditTexts

            editTexts[currentIndex].removeTextChangedListener(this);
            editTexts[currentIndex].setText(text);
            editTexts[currentIndex].setSelection(text.length());
            editTexts[currentIndex].addTextChangedListener(this);

            if (text.length() == 1)
                moveToNext();
            else if (text.length() == 0)
                moveToPrevious();
        }

        private void moveToNext() {
            if (!isLast)
                editTexts[currentIndex + 1].requestFocus();

            if (isAllEditTextsFilled() && isLast) { // isLast is optional
                editTexts[currentIndex].setFocusable(true);
//                hideKeyboard();
            }
        }

        private void moveToPrevious() {
            if (!isFirst)
                editTexts[currentIndex - 1].requestFocus();
        }

        private boolean isAllEditTextsFilled() {
            for (EditText editText : editTexts)
                if (editText.getText().toString().trim().length() == 0)
                    return false;
            return true;
        }

        private void hideKeyboard() {
            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        }

    }

    public class PinOnKeyListener implements View.OnKeyListener {

        private int currentIndex;

        PinOnKeyListener(int currentIndex) {
            this.currentIndex = currentIndex;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                if (editTexts[currentIndex].getText().toString().isEmpty() && currentIndex != 0)
                    editTexts[currentIndex - 1].requestFocus();
            }
            return false;
        }

    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(OtpActivity.this, getResources().getString(R.string.no_internet));
    }
}
