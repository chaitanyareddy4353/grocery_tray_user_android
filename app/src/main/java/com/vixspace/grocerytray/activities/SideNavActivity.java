package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.razorpay.PaymentResultListener;
import com.vixspace.grocerytray.MainActivity;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.fragments.OrdersFragment;
import com.vixspace.grocerytray.fragments.ReserveFragment;
import com.vixspace.grocerytray.fragments.TransactionFragment;
import com.vixspace.grocerytray.fragments.UnRatedOrdersFragment;
import com.vixspace.grocerytray.fragments.WalletFragment;
import com.vixspace.grocerytray.fragments.WishlistFragment;

public class SideNavActivity extends AppCompatActivity implements PaymentResultListener {

    FrameLayout frame_container;
    Integer Indicator;
    ImageView imgBack;
    RelativeLayout relativeHeader;
    TextView txtHeader;
    PaymentResult paymentResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_side_nav);

        frame_container = findViewById(R.id.frame_container);
        imgBack = findViewById(R.id.imgBack);
        relativeHeader = findViewById(R.id.relativeHeader);
        txtHeader = findViewById(R.id.txtHeader);

        Bundle extras = getIntent().getExtras();

        Indicator = extras.getInt("indicator",0);
        Fragment fragment;

        if (Indicator==1){
            txtHeader.setText("Orders");
            fragment = new OrdersFragment();
            loadFragment(fragment);
        } else if (Indicator==2){
            txtHeader.setText("My Wallet");
            fragment = new WalletFragment();
            loadFragment(fragment);
        }  else if (Indicator==5){
            relativeHeader.setVisibility(View.GONE);
            txtHeader.setText("My Wishlist");
            fragment = new WishlistFragment();
            loadFragment(fragment);
        }  else if (Indicator==8){
//            relativeHeader.setVisibility(View.GONE);
            txtHeader.setText("Reserve Money");
            fragment = new ReserveFragment();
            loadFragment(fragment);
        } else if (Indicator==9){
            txtHeader.setText("Transaction History");
            fragment = new TransactionFragment();
            loadFragment(fragment);
        }  else if (Indicator==10){
            txtHeader.setText("UnRated Orders");
            fragment = new UnRatedOrdersFragment();
            loadFragment(fragment);
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onPaymentSuccess(String s) {
        paymentResult.onResultRecieved(true);
    }

    @Override
    public void onPaymentError(int i, String s) {
        paymentResult.onResultRecieved(false);
    }

    public interface PaymentResult {
        void onResultRecieved(boolean isSuccess);
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof PaymentResult) {
            paymentResult = (PaymentResult) fragment;
        }
    }
}