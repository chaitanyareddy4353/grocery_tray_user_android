package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.AddUserAddressResponse;
import com.vixspace.grocerytray.pojos.ListAllUserAddressResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.pojos.UserInfoResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyAccoutActivity extends AppCompatActivity {

    ImageView imgBack,imgUser,imgMoreAddress;
    TextView txtuserName,txtPhone,txtEmail,txtAddress;
    Switch switchRing;
    Button btnEdit;
    RelativeLayout relativeMySubs,relativeMyOrders,relativeMyWallet,relativeUnrated;
    LinearLayout linearLogout;

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_accout);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        linearLogout = findViewById(R.id.linearLogout);
        relativeMyWallet = findViewById(R.id.relativeMyWallet);
        relativeMyOrders = findViewById(R.id.relativeMyOrders);
        relativeMySubs = findViewById(R.id.relativeMySubs);
        imgBack = findViewById(R.id.imgBack);
        imgUser = findViewById(R.id.imgUser);
        txtuserName = findViewById(R.id.txtuserName);
        txtPhone = findViewById(R.id.txtPhone);
        txtAddress = findViewById(R.id.txtAddress);
        txtEmail = findViewById(R.id.txtEmail);
        imgMoreAddress = findViewById(R.id.imgMoreAddress);
        switchRing = findViewById(R.id.switchRing);
        btnEdit = findViewById(R.id.btnEdit);
        relativeUnrated = findViewById(R.id.relativeUnrated);

        switchRing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchRing.isChecked()){
                    ring(true);
                    switchRing.setEnabled(false);
                } else {
                    ring(false);
                    switchRing.setEnabled(false);
                }
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccoutActivity.this,ProfileEditActivity.class);
                startActivity(intent);
            }
        });

        txtuserName.setText(allsharedpreference.getShared_String(Allsharedpreference.UserName));
        txtEmail.setText(allsharedpreference.getShared_String(Allsharedpreference.EmailId));
        txtPhone.setText(allsharedpreference.getShared_String(Allsharedpreference.PhoneNumber));

        relativeMySubs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccoutActivity.this,MySubscriptions.class);
                startActivity(intent);
            }
        });

        relativeMyOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccoutActivity.this,SideNavActivity.class);
                intent.putExtra("indicator",1);
                startActivity(intent);
            }
        });

        relativeMyWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccoutActivity.this,SideNavActivity.class);
                intent.putExtra("indicator",2);
                startActivity(intent);
            }
        });

        relativeUnrated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccoutActivity.this,SideNavActivity.class);
                intent.putExtra("indicator",10);
                startActivity(intent);
            }
        });

        imgMoreAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccoutActivity.this,AddressActivity.class);
                startActivity(intent);
            }
        });

        linearLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    allsharedpreference.clearAllSharedPreferences();
                    Intent intent = new Intent(MyAccoutActivity.this, SplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                allsharedpreference.clearAllSharedPreferences();
                    startActivity(intent);
                    finish();
            }
        });

        if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
            relativeUnrated.setVisibility(View.VISIBLE);
        }


        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            LoadInfo();
        }else{
            showNoInterNetMessage();
        }

        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            LoadAddress();
        }else{
            showNoInterNetMessage();
        }

        if (allsharedpreference.getShared_Booleen(Allsharedpreference.ring)){
            switchRing.setChecked(true);
        } else {
            switchRing.setChecked(false);
        }

    }

    private void ring(final boolean b){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.userInfo(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final UpdateFcmResponse loginResponse = gson.fromJson(splashScreenResponse,UpdateFcmResponse.class);

                        if (loginResponse.getSuccess()) {

                            if (b){
                                Toast.makeText(MyAccoutActivity.this, "activated", Toast.LENGTH_SHORT).show();
                                allsharedpreference.setShared_Booleen(Allsharedpreference.ring,b);
                            } else {
                                Toast.makeText(MyAccoutActivity.this, "deactivated", Toast.LENGTH_SHORT).show();
                                allsharedpreference.setShared_Booleen(Allsharedpreference.ring,b);
                            }

                            switchRing.setEnabled(true);
                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                            switchRing.setEnabled(true);
                        }

                    } else {

                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        switchRing.setEnabled(true);
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(MyAccoutActivity.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }

                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());
                    switchRing.setEnabled(true);

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    private void LoadInfo() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.userInfo(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final UserInfoResponse loginResponse = gson.fromJson(splashScreenResponse,UserInfoResponse.class);

                        if (loginResponse.getSuccess()) {

                            if (loginResponse.getExtras().getData().getWhetherImageAvailable()){
                                Picasso.get().load(loginResponse.getExtras().getData().getImageData().getImageOriginal()).into(imgUser);
                            }

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }

                    } else {

                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(MyAccoutActivity.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }


                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    private void LoadAddress() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.allAddresses(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final ListAllUserAddressResponse loginResponse = gson.fromJson(splashScreenResponse,ListAllUserAddressResponse.class);

                        if (loginResponse.getSuccess()) {

                            if (loginResponse.getExtras().getData().size()>0){
                                txtAddress.setText(loginResponse.getExtras().getData().get(0).getAddress());
                            } else {
                                txtAddress.setText("");
                            }

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }

                    } else {

                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(MyAccoutActivity.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }


                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(MyAccoutActivity.this, getResources().getString(R.string.no_internet));
    }

}
