package com.vixspace.grocerytray.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.adapters.MonthlyAdapter;
import com.vixspace.grocerytray.adapters.WeeklyAdapter;
import com.vixspace.grocerytray.pojos.AddUserAddressResponse;
import com.vixspace.grocerytray.pojos.CreateSubRequestPojo;
import com.vixspace.grocerytray.pojos.FetchAllActiveSubscriptionProductsResponse;
import com.vixspace.grocerytray.pojos.MonthlyPojo;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.pojos.WeeklyPojo;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditSubscriptionActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener,WeeklyAdapter.Toucher,MonthlyAdapter.ToucherM {

    ImageView imgBack,imgSub,imgAdd,imgProduct;
    TextView txtCount,txtAddress,txtSubDate,txtDaily,txtAlternateDays, txtEvery3Days,txtSP,
            txtWeekly,txtMonthly,txtBrand,txtproductName,txtProductQty,txtSubscriptionStartDate,txtCreateSub;
    Button btnUpdate;

    String selectedDate;
    int mYear,mMonth,mDay;

    LinearLayoutManager linearLayoutManager;

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;
    String addressId="",newAddress="",LocalityProductID="",SubID="",address="";
    Integer Required_Frequency=0;
    Integer subType=0;


    List<WeeklyPojo> weeklyPojos = new ArrayList<>();
    List<MonthlyPojo> monthlyPojos = new ArrayList<>();

    List<String> weekDays = new ArrayList<>();
    List<Integer> monthDays = new ArrayList<>();

    List<String> week = new ArrayList<>();

    RelativeLayout relativeWeekMonth,relativeAlpha;
    RecyclerView recyclerView;
    TextView txtDone,txtWeekDays;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_subscription);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        txtWeekDays = findViewById(R.id.txtWeekDays);
        txtSP = findViewById(R.id.txtSP);
        txtSubDate = findViewById(R.id.txtSubDate);
        txtCreateSub = findViewById(R.id.txtCreateSub);
        txtSubscriptionStartDate = findViewById(R.id.txtSubscriptionStartDate);
        imgBack = findViewById(R.id.imgBack);
        imgSub = findViewById(R.id.imgSub);
        imgAdd = findViewById(R.id.imgAdd);
        imgProduct = findViewById(R.id.imgProduct);
        txtCount = findViewById(R.id.txtCount);
        txtAddress = findViewById(R.id.txtAddress);
        txtDaily = findViewById(R.id.txtDaily);
        txtAlternateDays = findViewById(R.id.txtAlternateDays);
        txtEvery3Days = findViewById(R.id.txtEvery3Days);
        txtWeekly = findViewById(R.id.txtWeekly);
        txtMonthly = findViewById(R.id.txtMonthly);
        txtBrand = findViewById(R.id.txtBrand);
        txtproductName = findViewById(R.id.txtproductName);
        txtProductQty = findViewById(R.id.txtProductQty);
        btnUpdate = findViewById(R.id.btnUpdate);

        relativeWeekMonth = findViewById(R.id.relativeWeekMonth);
        recyclerView = findViewById(R.id.recyclerView);
        relativeAlpha = findViewById(R.id.relativeAlpha);
        txtDone = findViewById(R.id.txtDone);

        Picasso.get().load(getIntent().getStringExtra("img")).into(imgProduct);
        txtBrand.setText(getIntent().getStringExtra("brand"));
        txtproductName.setText(getIntent().getStringExtra("name"));
        txtProductQty.setText(getIntent().getStringExtra("size"));
        txtSP.setText(getIntent().getStringExtra("sp"));
        SubID = getIntent().getStringExtra("SubId");
        addressId = getIntent().getStringExtra("addressId");
        txtAddress.setText(getIntent().getStringExtra("address"));
        txtCount.setText(getIntent().getStringExtra("Num"));
        LocalityProductID = getIntent().getStringExtra("productID");
        subType = getIntent().getIntExtra("subType",0);
        selectedDate = getIntent().getStringExtra("date");

//        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss:SSS'Z'");
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = form.parse(selectedDate);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("yyyy-MM-dd");
        String newDateStr = postFormater.format(date);

        txtSubDate.setText(newDateStr);

        if (subType==1){
            Required_Frequency = 1;
            subType=1;
            txtDaily.setTextColor(getApplicationContext().getResources().getColor(R.color.tomato));
            txtAlternateDays.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            txtEvery3Days.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            txtWeekly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            txtMonthly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
        } else if (subType==2) {
            Required_Frequency = 2;
            subType = 2;
            txtDaily.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            txtAlternateDays.setTextColor(getApplicationContext().getResources().getColor(R.color.tomato));
            txtEvery3Days.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            txtWeekly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            txtMonthly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
        } else if (subType==3) {
            Required_Frequency = 3;
            subType = 3;
            txtDaily.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            txtAlternateDays.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            txtEvery3Days.setTextColor(getApplicationContext().getResources().getColor(R.color.tomato));
            txtWeekly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            txtMonthly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
        } else if (subType==4) {
            Required_Frequency = 7;
            subType = 4;
            txtDaily.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            txtAlternateDays.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            txtEvery3Days.setTextColor(getApplicationContext().getResources().getColor(R.color.black));

            txtMonthly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            Gson gson = new Gson();
            String data = getIntent().getStringExtra("weekArray");
            FetchAllActiveSubscriptionProductsResponse.Datum datum = gson.fromJson(data,FetchAllActiveSubscriptionProductsResponse.Datum.class);
            if (datum.getWeekArray()!=null){
                weekDays.addAll(datum.getWeekArray());
                if (datum.getWeekArray().size()==2){
                    if (datum.getWeekArray().get(0).equals("Saturday")||datum.getWeekArray().get(0).equals("Sunday")){
                        if (datum.getWeekArray().get(1).equals("Saturday")||datum.getWeekArray().get(1).equals("Sunday")){
                            txtWeekly.setTextColor(getApplicationContext().getResources().getColor(R.color.tomato));
                            txtWeekDays.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                        }
                    } else {
                        txtWeekly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                        txtWeekDays.setTextColor(getApplicationContext().getResources().getColor(R.color.tomato));
                    }
                }
            } else {
                weekDays.clear();
            }

    } else if (subType==5) {
            Required_Frequency = 30;
            subType = 5;
            txtDaily.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            txtAlternateDays.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            txtEvery3Days.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            txtWeekly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            txtMonthly.setTextColor(getApplicationContext().getResources().getColor(R.color.tomato));
            Gson gson = new Gson();
            String data = getIntent().getStringExtra("month");
            FetchAllActiveSubscriptionProductsResponse.Datum datum = gson.fromJson(data,FetchAllActiveSubscriptionProductsResponse.Datum.class);
            if (datum.getDaySelectionArray()!=null){
                monthDays.addAll(datum.getDaySelectionArray());
            } else {
                monthDays.clear();
            }
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        txtDaily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Required_Frequency = 1;
                subType=1;
                txtDaily.setTextColor(getApplicationContext().getResources().getColor(R.color.tomato));
                txtAlternateDays.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtEvery3Days.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtWeekly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtMonthly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            }
        });

        txtAlternateDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Required_Frequency = 2;
                subType = 2;
                txtDaily.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtAlternateDays.setTextColor(getApplicationContext().getResources().getColor(R.color.tomato));
                txtEvery3Days.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtWeekly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtMonthly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            }
        });


        txtEvery3Days.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Required_Frequency = 3;
                subType = 3;
                txtDaily.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtAlternateDays.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtEvery3Days.setTextColor(getApplicationContext().getResources().getColor(R.color.tomato));
                txtWeekly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtMonthly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
            }
        });

        txtWeekDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weeklyPojos.clear();
                week.clear();
                Required_Frequency = 7;
                subType = 4;
                txtDaily.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtAlternateDays.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtEvery3Days.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtWeekly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtWeekDays.setTextColor(getApplicationContext().getResources().getColor(R.color.tomato));
                txtMonthly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                //code
                week.add("Monday");
                week.add("Tuesday");
                week.add("Wednesday");
                week.add("Thursday");
                week.add("Friday");
                week.add("Saturday");
                week.add("Sunday");
                for (int i = 0;i<week.size();i++){
                    WeeklyPojo weeklyPojo = new WeeklyPojo();
                    weeklyPojo.setName(week.get(i));
//                    weeklyPojo.setSelect(true);

                    if (i<5){
                        weeklyPojo.setSelect(true);
                    } else {
                        weeklyPojo.setSelect(false);
                    }

                    weeklyPojos.add(weeklyPojo);
                }

                WeeklyAdapter adapter = new WeeklyAdapter(EditSubscriptionActivity.this, weeklyPojos,EditSubscriptionActivity.this);
                recyclerView.setLayoutManager(new LinearLayoutManager(EditSubscriptionActivity.this, RecyclerView.VERTICAL,false));
                recyclerView.setAdapter(adapter);

                weekDays.clear();
                for (int i=0;i<weeklyPojos.size();i++){
                    if (weeklyPojos.get(i).getSelect()){
                        weekDays.add(weeklyPojos.get(i).getName());
                    }
                }

                relativeWeekMonth.setVisibility(View.VISIBLE);
            }
        });




        txtWeekly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weeklyPojos.clear();
                week.clear();
                Required_Frequency = 7;
                subType = 4;
                txtDaily.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtAlternateDays.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtEvery3Days.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtWeekly.setTextColor(getApplicationContext().getResources().getColor(R.color.tomato));
                txtWeekDays.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtMonthly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                //code
                week.add("Monday");
                week.add("Tuesday");
                week.add("Wednesday");
                week.add("Thursday");
                week.add("Friday");
                week.add("Saturday");
                week.add("Sunday");
                for (int i = 0;i<week.size();i++){
                    WeeklyPojo weeklyPojo = new WeeklyPojo();
                    weeklyPojo.setName(week.get(i));
//                    weeklyPojo.setSelect(true);
                    if (i==5||i==6){
                        weeklyPojo.setSelect(true);
                    } else {
                        weeklyPojo.setSelect(false);
                    }
                    weeklyPojos.add(weeklyPojo);
                }

                WeeklyAdapter adapter = new WeeklyAdapter(EditSubscriptionActivity.this, weeklyPojos,EditSubscriptionActivity.this);
                recyclerView.setLayoutManager(new LinearLayoutManager(EditSubscriptionActivity.this, RecyclerView.VERTICAL,false));
                recyclerView.setAdapter(adapter);

                weekDays.clear();
                for (int i=0;i<weeklyPojos.size();i++){
                    if (weeklyPojos.get(i).getSelect()){
                        weekDays.add(weeklyPojos.get(i).getName());
                    }
                }

                relativeWeekMonth.setVisibility(View.VISIBLE);
            }
        });

        txtMonthly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                monthlyPojos.clear();
                weeklyPojos.clear();
                Required_Frequency = 30;
                subType = 5;
                txtDaily.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtAlternateDays.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtEvery3Days.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtWeekly.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtWeekDays.setTextColor(getApplicationContext().getResources().getColor(R.color.black));
                txtMonthly.setTextColor(getApplicationContext().getResources().getColor(R.color.tomato));
                //code
                Calendar c = Calendar.getInstance();
                int maxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);

                for (int i=0;i<maxDays;i++){
                    MonthlyPojo monthlyPojo = new MonthlyPojo();
                    monthlyPojo.setPos(i+1);
                    monthlyPojo.setSelect(true);

                    monthlyPojos.add(monthlyPojo);
                }

                MonthlyAdapter adapter = new MonthlyAdapter(EditSubscriptionActivity.this, monthlyPojos,EditSubscriptionActivity.this);
                recyclerView.setLayoutManager(new GridLayoutManager(EditSubscriptionActivity.this,8,GridLayoutManager.VERTICAL,false));
                recyclerView.setAdapter(adapter);

                monthDays.clear();
                for (int i=0;i<monthlyPojos.size();i++){
                    if (monthlyPojos.get(i).getSelect()){
                        monthDays.add(i+1);
                    }
                }

                relativeWeekMonth.setVisibility(View.VISIBLE);
            }
        });

        txtSubDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });
        txtSubscriptionStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        txtCreateSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Required_Frequency!=0){
                    if (!addressId.equals("")){

                        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                            createSubscription();
                        }else{
                            showNoInterNetMessage();
                        }

                    } else {
                        Toast.makeText(EditSubscriptionActivity.this, "Please select the address ", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(EditSubscriptionActivity.this, "Please select the frequency ", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(EditSubscriptionActivity.this, ChooseLocationActivity.class);
//                startActivityForResult(intent,101);
                Intent intent = new Intent(EditSubscriptionActivity.this, AddressActivity.class);
                startActivityForResult(intent,102);
            }
        });

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(txtCount.getText().toString().trim())>0){
                    int count = Integer.parseInt(txtCount.getText().toString().trim()) + 1;
                    txtCount.setText(String.valueOf(count));
                }
            }
        });

        imgSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(txtCount.getText().toString().trim())!=1){
                    int count = Integer.parseInt(txtCount.getText().toString().trim()) - 1;
                    txtCount.setText(String.valueOf(count));
                } else {
                    finish();
                }
            }
        });

        txtDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeWeekMonth.setVisibility(View.GONE);
            }
        });

    }

    private void createSubscription() {
        JsonObject jsonObject = sub();
//        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
//        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
//        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
//        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
//        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
//        jsonObject.addProperty("SubscriptionID",SubID);
//        jsonObject.addProperty("AddressID",addressId);
//        jsonObject.addProperty("Required_Quantity",Integer.parseInt(txtCount.getText().toString().trim()));
//        jsonObject.addProperty("Required_Frequency",Required_Frequency);
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.updateSubscriptionInformation(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final UpdateFcmResponse loginResponse = gson.fromJson(splashScreenResponse,UpdateFcmResponse.class);

                        if (loginResponse.getSuccess()) {
//                            Intent intent = new Intent(CreateSubscriptionActivity.this, MainActivity.class);
//                            Toast.makeText(CreateSubscriptionActivity.this, "Subscription created successfully", Toast.LENGTH_SHORT).show();
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            startActivity(intent);
                            Intent intent = new Intent(EditSubscriptionActivity.this, SubscriptionSuccessfulActivity.class);
                            intent.putExtra("indicator",2);
                            intent.putExtra("date",txtSubDate.getText().toString());
                            Toast.makeText(EditSubscriptionActivity.this, "Subscription modified successfully", Toast.LENGTH_SHORT).show();
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(EditSubscriptionActivity.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    private JsonObject sub() {
        CreateSubRequestPojo requestModel = new CreateSubRequestPojo();
        requestModel.setApiKey(allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        requestModel.setUSERID(allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        requestModel.setSessionID(allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        requestModel.setCityID(allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        requestModel.setLocalityID(allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        requestModel.setSubscriptionID(SubID);
        requestModel.setLocalityProductID(LocalityProductID);
        requestModel.setAddressID(addressId);
        requestModel.setRequiredQuantity(Integer.parseInt(txtCount.getText().toString().trim()));
        requestModel.setSubscriptionScheduleType(subType);
        requestModel.setSubscriptionStartDate(txtSubDate.getText().toString());
        if (subType==4){
            requestModel.setWeekArray(weekDays);
        } else if (subType==5){
            requestModel.setDaySelectionArray(monthDays);
        }
//        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
//        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
//        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
//        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
//        jsonObject.addProperty("LocalityProductID",LocalityProductID);
//        jsonObject.addProperty("AddressID",addressId);
//        jsonObject.addProperty("Required_Quantity",Integer.parseInt(txtCount.getText().toString().trim()));
////        jsonObject.addProperty("Required_Frequency",Required_Frequency);
//        jsonObject.addProperty("Subscription_Schedule_Type",subType);
//        jsonObject.addProperty("Subscription_Start_Date",subType);
//        if (subType==4){
//            jsonObject.addProperty("Week_Array",weeklyPojos);
//        } else if (subType==5){
//            jsonObject.addProperty("Day_Selection_Array",MonthlyPojo);
//        }
        return new Gson().toJsonTree(requestModel).getAsJsonObject();
    }

    public void showDatePicker(){
        // Show date picker dialog.
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        GregorianCalendar gc = new GregorianCalendar();
        gc.add(Calendar.DATE, 1);
        DatePickerDialog datePickerDialog =
                new DatePickerDialog(EditSubscriptionActivity.this, EditSubscriptionActivity.this, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(gc.getTimeInMillis());
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        SimpleDateFormat simpledateformat = null;

        simpledateformat = new SimpleDateFormat("dd-MM-yyyy", getResources().getConfiguration().locale);
//        Date date = new Date(year, month, dayOfMonth);
        GregorianCalendar gc = new GregorianCalendar(year, month, dayOfMonth);

//        selectedDate = simpledateformat.format(gc.getTime());
//        month = month+1;
//        selectedDate= dayOfMonth+"-"+month+"-"+year;
        mYear = year;
        mMonth = month;
        mDay  = dayOfMonth;
        selectedDate = simpledateformat.format(gc.getTime());
        txtSubDate.setText(selectedDate);


//        Toast.makeText(getApplicationContext(),selectedDate, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==101&& resultCode == RESULT_OK){
            String lat = data.getStringExtra("lat");
            String lon = data.getStringExtra("lon");
            String strAddress = data.getStringExtra("address");
            String city  = data.getStringExtra("city");
            String state = data.getStringExtra("state");
            String pincode = data.getStringExtra("pin");
            String addressint = data.getStringExtra("addressint");

            allsharedpreference.setShared_String(Allsharedpreference.SELECTED_LON, String.valueOf(lon));
            allsharedpreference.setShared_String(Allsharedpreference.SELECTED_LAT, String.valueOf(lat));

            newAddress = (strAddress +", " +city+ ", " + state +", " + pincode);
            if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                validateAddress(lat,lon,1,newAddress,addressint);
            }else{
                showNoInterNetMessage();
            }
        } else if (requestCode==102&&resultCode==RESULT_OK) {
            addressId = data.getStringExtra("addressId");
            String address = data.getStringExtra("address");
            txtAddress.setText(address);
        }
    }

    private void validateAddress(String lat, String lon, Integer indicator, final String newAddress, String addressint) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("Name",allsharedpreference.getShared_String(Allsharedpreference.UserName));
        jsonObject.addProperty("PhoneNumber",allsharedpreference.getShared_String(Allsharedpreference.PhoneNumber));
        jsonObject.addProperty("AddressType",1);
        jsonObject.addProperty("Address",addressint);
        jsonObject.addProperty("Latitude",lat);
        jsonObject.addProperty("Longitude",lon);

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.addAddress(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final AddUserAddressResponse loginResponse = gson.fromJson(splashScreenResponse,AddUserAddressResponse.class);

                        if (loginResponse.getSuccess()) {

                            addressId = loginResponse.getExtras().getData().getAddressID();
                          txtAddress.setText(newAddress);

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(EditSubscriptionActivity.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    @Override
    public void onToucherm(Integer pos, List<MonthlyPojo> datum) {
        monthDays.clear();
        for (int i=0;i<datum.size();i++){
            if (datum.get(i).getSelect()){
                monthDays.add(i+1);
            }
        }
    }

    @Override
    public void onToucher(String Id, Integer pos, List<WeeklyPojo> weeklyPojos) {
        weekDays.clear();
        for (int i=0;i<weeklyPojos.size();i++){
            if (weeklyPojos.get(i).getSelect()){
                weekDays.add(weeklyPojos.get(i).getName());
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (relativeWeekMonth.getVisibility()==View.VISIBLE){
            relativeWeekMonth.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
            finish();
        }
    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(EditSubscriptionActivity.this, getResources().getString(R.string.no_internet));
    }
}
