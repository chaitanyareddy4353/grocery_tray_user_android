package com.vixspace.grocerytray.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.adapters.AddressAdapter;
import com.vixspace.grocerytray.adapters.CouponsAdapter;
import com.vixspace.grocerytray.pojos.AddUserAddressResponse;
import com.vixspace.grocerytray.pojos.FetchAllAvailableOfferCouponsResponse;
import com.vixspace.grocerytray.pojos.ListAllUserAddressResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

 public class AddressActivity extends AppCompatActivity implements AddressAdapter.Dates {

    LinearLayout linearUseCurrentAddress;
    ImageView imgBack;
    TextView txtAddNewAddress;
    RecyclerView recyclerAddress;

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;
    String newAddress= "";
    AddressAdapter addressAdapter;
    List<ListAllUserAddressResponse.Datum> datumList = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        linearUseCurrentAddress = findViewById(R.id.linearUseCurrentAddress);
        imgBack = findViewById(R.id.imgBack);
        txtAddNewAddress = findViewById(R.id.txtAddNewAddress);
        recyclerAddress = findViewById(R.id.recyclerAddress);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        linearUseCurrentAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddressActivity.this, ChooseLocationActivity.class);
                intent.putExtra("indicator",2);
                startActivityForResult(intent,101);
            }
        });

        txtAddNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddressActivity.this, ChooseLocationActivity.class);
                intent.putExtra("indicator",2);
                startActivityForResult(intent,101);
            }
        });

        ShowCoupons();
    }

    private void ShowCoupons() {
        datumList = new ArrayList<>();
        addressAdapter = new AddressAdapter(AddressActivity.this,datumList,this);
        linearLayoutManager = new LinearLayoutManager(AddressActivity.this, LinearLayoutManager.VERTICAL,false);
        recyclerAddress.setLayoutManager(linearLayoutManager);
        recyclerAddress.setAdapter(addressAdapter);

        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            listAllAddress();
        }else{
            showNoInterNetMessage();
        }

    }

    private void listAllAddress() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.allAddresses(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final ListAllUserAddressResponse loginResponse = gson.fromJson(splashScreenResponse,ListAllUserAddressResponse.class);

                        if (loginResponse.getSuccess()) {

                            if (loginResponse.getExtras().getData().size()>0){

                                datumList.addAll(loginResponse.getExtras().getData());
                                addressAdapter.notifyDataSetChanged();

                            }
                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }

                    } else {

                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(AddressActivity.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }

                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==101&& resultCode == RESULT_OK){
            String lat = data.getStringExtra("lat");
            String lon = data.getStringExtra("lon");
            String strAddress = data.getStringExtra("address");
            String city  = data.getStringExtra("city");
            String state = data.getStringExtra("state");
            String pincode = data.getStringExtra("pin");
            String addressint = data.getStringExtra("addressint");
            String landmark = data.getStringExtra("landmark");
            Integer radio = data.getIntExtra("radio",0);

            allsharedpreference.setShared_String(Allsharedpreference.SELECTED_LON, String.valueOf(lon));
            allsharedpreference.setShared_String(Allsharedpreference.SELECTED_LAT, String.valueOf(lat));

            newAddress = (landmark+strAddress +", " +city+ ", " + state +", " + pincode);

            if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                validateAddress(lat,lon,1,newAddress,addressint,radio);
            }else{
                showNoInterNetMessage();
            }

        }
    }

    private void validateAddress(String lat, String lon, Integer indicator, final String newAddress, String addressint, Integer radio) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("Name",allsharedpreference.getShared_String(Allsharedpreference.UserName));
        jsonObject.addProperty("PhoneNumber",allsharedpreference.getShared_String(Allsharedpreference.PhoneNumber));
        jsonObject.addProperty("AddressType",radio);
        jsonObject.addProperty("Address",newAddress);
        jsonObject.addProperty("Latitude",lat);
        jsonObject.addProperty("Longitude",lon);

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.addAddress(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final AddUserAddressResponse loginResponse = gson.fromJson(splashScreenResponse,AddUserAddressResponse.class);

                        if (loginResponse.getSuccess()) {

                            ShowCoupons();

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(AddressActivity.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }


//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    @Override
    public void onDateTouched(String Id,String Name) {
        Intent intent = new Intent();
        intent.putExtra("addressId", Id);
        intent.putExtra("address", Name);
        setResult(RESULT_OK, intent);
        finish();
    }

        @Override
        public void onBackPressed() {
            super.onBackPressed();
            finish();
        }

     public void showNoInterNetMessage(){
         PrintMsg.printLongToast(AddressActivity.this, getResources().getString(R.string.no_internet));
     }
 }