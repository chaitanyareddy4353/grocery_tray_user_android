package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.adapters.RelatedProductsAdapter;
import com.vixspace.grocerytray.adapters.SearchAdapter;
import com.vixspace.grocerytray.pojos.FetchAllRelatedProducts;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {

    EditText edtSearch;
    RecyclerView recyclerSearch;
    ImageView imgBack;

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        recyclerSearch = findViewById(R.id.recyclerSearch);
        edtSearch = findViewById(R.id.edtSearch);
        imgBack = findViewById(R.id.imgBack);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        edtSearch.addTextChangedListener(new GenericTextWatcher(edtSearch));
    }

    public class GenericTextWatcher implements TextWatcher
    {
        private View view;
        private GenericTextWatcher(View view)
        {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString().trim();
            switch (view.getId()) {
                case R.id.edtSearch:
                    if (editable.length()>=1){
                        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                            LoadOrders();
                        }else{
                            showNoInterNetMessage();
                        }

//                        txt_search_context.setVisibility(View.GONE);
//                        txt_steve_jobs.setVisibility(View.GONE);
                    } else if (editable.length()==0){
//                        txt_search_context.setVisibility(View.VISIBLE);
//                        txt_steve_jobs.setVisibility(View.VISIBLE);
                        recyclerSearch.setVisibility(View.GONE);


                    }
                    break;

            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub

        }
    }

        private void LoadOrders() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("Search_Input",edtSearch.getText().toString().trim());

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.search(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final FetchAllRelatedProducts loginResponse =
                                gson.fromJson(splashScreenResponse,FetchAllRelatedProducts.class);

                        if (loginResponse.getSuccess()) {

                            if (loginResponse.getExtras().getData().size()>0){

                                SearchAdapter adapter = new SearchAdapter(SearchActivity.this, loginResponse.getExtras().getData());
                                recyclerSearch.setLayoutManager(new LinearLayoutManager(SearchActivity.this,RecyclerView.VERTICAL,false));
                                recyclerSearch.setAdapter(adapter);
                                recyclerSearch.setVisibility(View.VISIBLE);

                            }  else {
                                Toast.makeText(SearchActivity.this, "No items found", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(SearchActivity.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(SearchActivity.this, getResources().getString(R.string.no_internet));
    }
}