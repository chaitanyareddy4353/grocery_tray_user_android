package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.adapters.CategoriesAdapter;
import com.vixspace.grocerytray.adapters.CollectionsAdapter;
import com.vixspace.grocerytray.pojos.FetchAllCategoryCollectionResponse;
import com.vixspace.grocerytray.pojos.FetchAllLocalityCategoriesResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllCollectionsActivity extends AppCompatActivity implements CollectionsAdapter.Collect{

    ImageView imgBack;
    RecyclerView recyclerCollections;
    LinearLayoutManager linearLayoutManager;
    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_collections);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        recyclerCollections = findViewById(R.id.recyclerCollections);
        imgBack = findViewById(R.id.imgBack);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            allCollections(getIntent().getStringExtra("CategoryID"));
        }else{
            showNoInterNetMessage();
        }

    }

    private void allCollections(String categoryID) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("CategoryID",categoryID);

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.allCategoryCollections(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final FetchAllCategoryCollectionResponse loginResponse =
                                gson.fromJson(splashScreenResponse,FetchAllCategoryCollectionResponse.class);

                        if (loginResponse.getSuccess()) {

                            CollectionsAdapter adapter = new CollectionsAdapter(AllCollectionsActivity.this, loginResponse.getExtras().getData(),AllCollectionsActivity.this);
                            recyclerCollections.setLayoutManager(new GridLayoutManager(AllCollectionsActivity.this,3,GridLayoutManager.VERTICAL,false));
                            recyclerCollections.setAdapter(adapter);

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());

//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    @Override
    public void onCollect(String Id, String name) {

    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(AllCollectionsActivity.this, getResources().getString(R.string.no_internet));
    }
}