package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.adapters.CategoriesAdapter;
import com.vixspace.grocerytray.adapters.CategoriesNewAdapter;
import com.vixspace.grocerytray.pojos.FetchAllLocalityCategoriesResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllCategoriesActivity extends AppCompatActivity {

    ImageView imgBack;
    RecyclerView recyclerCategories;

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_categories);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        imgBack = findViewById(R.id.imgBack);
        recyclerCategories = findViewById(R.id.recyclerCategories);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            allCategories();
        }else{
            showNoInterNetMessage();
        }



    }

    private void allCategories() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.fetchAllLocalityCategories(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final FetchAllLocalityCategoriesResponse loginResponse = gson.fromJson(splashScreenResponse,FetchAllLocalityCategoriesResponse.class);

                        if (loginResponse.getSuccess()) {

                            CategoriesNewAdapter adapter = new CategoriesNewAdapter(AllCategoriesActivity.this, loginResponse.getExtras().getData());
                            recyclerCategories.setLayoutManager(new GridLayoutManager(AllCategoriesActivity.this,3,GridLayoutManager.VERTICAL,false));
                            recyclerCategories.setAdapter(adapter);

                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {

//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());

//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(AllCategoriesActivity.this, getResources().getString(R.string.no_internet));
    }
}
