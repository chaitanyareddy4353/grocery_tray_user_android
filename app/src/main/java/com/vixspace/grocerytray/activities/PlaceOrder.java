package com.vixspace.grocerytray.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.libraries.places.api.model.Place;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.vixspace.grocerytray.MainActivity;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.CartInfoModel;
import com.vixspace.grocerytray.pojos.CartModelClass;
import com.vixspace.grocerytray.pojos.FetchUserWalletInformationResponse;
import com.vixspace.grocerytray.pojos.PlaceOrderResponse;
import com.vixspace.grocerytray.pojos.RazorpaypayWalletResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceOrder extends AppCompatActivity implements PaymentResultListener {

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;

    ImageView imgBack;
    TextView txtOrderTotal,txtDiscount,txtDeliveryCharges,txtToPay,txtTotalBottom,txtCheckOut;
    ProgressBar progressBar;

    Double walletAmt = 0.0;
    Double amt = 0.0;
    Double toPay = 0.0;
    String orderId="",priceQuoteID="",couponId="";
    Boolean isOnline = false;
    Boolean couponApplied = false;

    TextView txtCouponName,txtApply,txtAddress;
    RelativeLayout relativeCoupon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        txtCouponName = findViewById(R.id.txtCouponName);
        txtApply = findViewById(R.id.txtApply);
        imgBack = findViewById(R.id.imgBack);
        txtOrderTotal = findViewById(R.id.txtOrderTotal);
        txtDiscount = findViewById(R.id.txtDiscount);
        txtDeliveryCharges = findViewById(R.id.txtDeliveryCharges);
        txtToPay = findViewById(R.id.txtToPay);
        txtTotalBottom = findViewById(R.id.txtTotalBottom);
        txtCheckOut = findViewById(R.id.txtCheckOut);
        progressBar = findViewById(R.id.progressBar);
        relativeCoupon = findViewById(R.id.relativeCoupon);
        txtAddress = findViewById(R.id.txtAddress);

        amt = getIntent().getDoubleExtra("amt",0.0);
        toPay = getIntent().getDoubleExtra("toPay",0.0);
        couponApplied = getIntent().getBooleanExtra("couponapplied",false);
        priceQuoteID = getIntent().getStringExtra("priceQuoteId");
        txtAddress.setText(getIntent().getStringExtra("address"));
        txtOrderTotal.setText("₹"+amt);
        txtTotalBottom.setText("₹"+getIntent().getDoubleExtra("toPay",0.0));
        txtToPay.setText("₹"+getIntent().getDoubleExtra("toPay",0.0));
        txtDiscount.setText(getIntent().getStringExtra("discount"));
//        txtDiscount.setText("-₹"+getIntent().getDoubleExtra("discount",0.0));
        txtDeliveryCharges.setText("+₹"+getIntent().getDoubleExtra("delivery",0.0));
        isOnline = getIntent().getBooleanExtra("online",false);
        if (couponApplied){
            couponId = getIntent().getStringExtra("coupon");
            txtCouponName.setText(couponId);
            txtApply.setText("X");
        }
//9160067117
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        txtCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline){
//                    if (amt<=walletAmt){
                    if (toPay<=walletAmt){

                        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                            placeOrder(priceQuoteID);
                        }else{
                            showNoInterNetMessage();
                        }

                    } else {
                        progressBar.setVisibility(View.VISIBLE);
                        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                            RazorPay(toPay-walletAmt);
//                            RazorPay(amt-walletAmt);
                        }else{
                            showNoInterNetMessage();
                        }

                    }
                } else {
                    if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                        placeOrder(priceQuoteID);
                    }else{
                        showNoInterNetMessage();
                    }
                }
            }
        });

        txtApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtApply.getText().toString().trim().equals("X")){
                    txtApply.setText("Apply");
                    txtCouponName.setText("Got a Coupon Code?");
                    couponId="";
                    couponApplied=false;
                    Toast.makeText(PlaceOrder.this, "Coupon code removed", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(PlaceOrder.this,ApplyCoupon.class);
                    startActivityForResult(intent,199);
                }

            }
        });

        relativeCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PlaceOrder.this,ApplyCoupon.class);
                startActivityForResult(intent,199);
            }
        });

        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            getWalletInformation();
        }else{
            showNoInterNetMessage();
        }


    }

    private void getWalletInformation() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.walletInformation(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final FetchUserWalletInformationResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,FetchUserWalletInformationResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                        walletAmt = listOfProductsResponse.getExtras().getData().getAvailableAmount();

                    } else {
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(PlaceOrder.this, "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(PlaceOrder.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
                    }

                }else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(PlaceOrder.this, SplashActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        allsharedpreference.clearAllSharedPreferences();
                        startActivity(intent);
                        finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(PlaceOrder.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getWalletInformationRazor() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.walletInformation(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final FetchUserWalletInformationResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,FetchUserWalletInformationResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                        Double walletAmts = listOfProductsResponse.getExtras().getData().getAvailableAmount();

                        walletAmt = listOfProductsResponse.getExtras().getData().getAvailableAmount();

//                        if (walletAmts>=amt){
                        if (walletAmts>=toPay){
                            if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                                placeOrder(priceQuoteID);
                            }else{
                                showNoInterNetMessage();
                            }
//                            placeOrder(priceQuoteID);
                        } else {
                            progressBar.setVisibility(View.VISIBLE);
                            if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                                getWalletInformationRazor();
                            }else{
                                showNoInterNetMessage();
                            }

                        }

                    } else {
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(PlaceOrder.this, "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(PlaceOrder.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
                    }

                }else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(PlaceOrder.this, SplashActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        allsharedpreference.clearAllSharedPreferences();
                        startActivity(intent);
                        finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(PlaceOrder.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void placeOrder(String priceQuoteID) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("PriceQuoteID",priceQuoteID);
        if (couponApplied){
            jsonObject.addProperty("Whether_Coupon_Applied",true);
            jsonObject.addProperty("AppliedCouponID",couponId);
        } else {
            jsonObject.addProperty("Whether_Coupon_Applied",false);
            jsonObject.addProperty("AppliedCouponID","");
        }

        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.placeOrder(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final PlaceOrderResponse loginResponse = gson.fromJson(splashScreenResponse,PlaceOrderResponse.class);

                        if (loginResponse.getSuccess()) {

                            orderId = loginResponse.getExtras().getData().getOrderID();
//                            RazorPay(loginResponse.getExtras().getData().getPricingInformation().getFinalTransactionAmount());
                            progressBar.setVisibility(View.GONE);
                            CartInfoModel cartInfoModel = new CartInfoModel();
                            List<CartModelClass> cartModelClasses = new ArrayList<>();
                            cartModelClasses.clear();
                            cartInfoModel.setData(cartModelClasses);
                            Gson gsons = new Gson();
                            String json = gsons.toJson(cartInfoModel); // myObject - instance of MyObject
                            allsharedpreference.setShared_String(Allsharedpreference.CartData,json);
                            Toast.makeText(PlaceOrder.this, "order created succesfully    ", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(PlaceOrder.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

//                            allsharedpreference.setShared_String(Allsharedpreference.CurrentLocation,loginResponse.getExtras().getData().getLocalityData().getCityTitle());
//                            allsharedpreference.setShared_String(Allsharedpreference.CurrentCityID,loginResponse.getExtras().getData().getLocalityData().getCityID());
//                            allsharedpreference.setShared_String(Allsharedpreference.LocalityID,loginResponse.getExtras().getData().getLocalityData().getLocalityID());
//                            allsharedpreference.setShared_String(Allsharedpreference.Locality_Title,loginResponse.getExtras().getData().getLocalityData().getLocalityTitle());
                        }
                        else{
                            commonMethods.showToast(loginResponse.getExtras().getMsg());
                        }
                    } else {
                        progressBar.setVisibility(View.GONE);
//                        if (response.message().equals("Bad Request")){
                        Gson gson = new Gson();
                        final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());
                        if (splash.getExtras().getMsg().equals("Invalid Applied Coupon")){
                            couponApplied=false;
                        }

                        int code = splash.getExtras().getCode();
                        if(code == 1) {
                            Intent intent = new Intent(PlaceOrder.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());

                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onPaymentSuccess(String s) {
        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            getWalletInformationRazor();
        }else{
            showNoInterNetMessage();
        }
//        getWalletInformationRazor();
//        placeOrder(priceQuoteID);
//                            CartInfoModel cartInfoModel = new CartInfoModel();
//                            List<CartModelClass> cartModelClasses = new ArrayList<>();
//                            cartInfoModel.setData(cartModelClasses);
//                            Gson gsons = new Gson();
//                            String json = gsons.toJson(cartInfoModel); // myObject - instance of MyObject
//                            allsharedpreference.setShared_String(Allsharedpreference.CartData,json);
//                            Toast.makeText(ConfirmOrder.this, "order created succesfully    ", Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent(ConfirmOrder.this, MainActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            startActivity(intent);
    }

    @Override
    public void onPaymentError(int i, String s) {
        Toast.makeText(PlaceOrder.this, s, Toast.LENGTH_SHORT).show();
//        Toast.makeText(PlaceOrder.this, "oops something went wrong ", Toast.LENGTH_SHORT).show();

    }

    private void RazorPay(Double finalTransactionAmount) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("Amount",finalTransactionAmount);
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> all_products = apiService.razorPay(jsonObject);
        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final RazorpaypayWalletResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,RazorpaypayWalletResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                            makePayment(listOfProductsResponse.getExtras().getData().getTotalAmount(),listOfProductsResponse.getExtras().getData().getAmountRequestID());
                        }else{
                            showNoInterNetMessage();
                        }

                    } else {
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(PlaceOrder.this, "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(PlaceOrder.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            allsharedpreference.clearAllSharedPreferences();
                            startActivity(intent);
                            finish();
                        }
                    }

                }else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(), UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(PlaceOrder.this, SplashActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        allsharedpreference.clearAllSharedPreferences();
                        startActivity(intent);
                        finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(PlaceOrder.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void makePayment(Double totalAmount, String amountRequestID) {

        Checkout checkout = new Checkout();
        //checkout.setFullScreenDisable(true);
        /**
         * Set your logo here
         */
        //checkout.setImage(R.drawable.);

        /**
         * Reference to current activity
         */
        final Activity activity = this;

        /**
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */
        try {
            JSONObject options = new JSONObject();

            /**
             * Merchant Name
             * eg: Rentomojo || HasGeek etc.
             */

            /**
             * Description can be anything
             * eg: Order #123123
             *     Invoice Payment
             *     etc.
             */

            JSONObject jsonObject = new JSONObject();

            String newd = jsonObject.toString();
            jsonObject.put("Amount_RequestID",amountRequestID);
            options.put("notes", jsonObject);


            options.put("currency", "INR");
            JSONObject preFill = new JSONObject();

            options.put("prefill", preFill);

            Double amts = totalAmount*100;

//            DecimalFormat df = new DecimalFormat("#.00");
            DecimalFormat df = new DecimalFormat("#");
            String angleFormated = df.format(amts);
            amts=  Double.valueOf(angleFormated);


            options.put("amount",String.valueOf((amts)));
//            options.put("amount",String.valueOf((amts)*100));


            /**
             * Amount is always passed in PAISE
             * Eg: "500" = Rs 5.00
             */
//            options.put("amount",String.valueOf((RazorPayAmount)*100));

            checkout.open(activity, options);
        } catch(Exception e) {
            Log.e("payment_ERROR", "Error in starting Razorpay Checkout"+ e.toString());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==199&& resultCode == RESULT_OK){
//            CouponAmt = data.getStringExtra("couponAmt");
            couponId = data.getStringExtra("couponId");
//            Total = data.getStringExtra("total");
            couponApplied = true;
            txtCouponName.setText(couponId);
            txtApply.setText("X");
        }
    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(PlaceOrder.this, getResources().getString(R.string.no_internet));
    }
}