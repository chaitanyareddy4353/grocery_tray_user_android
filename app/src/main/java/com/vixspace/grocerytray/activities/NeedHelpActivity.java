package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.vixspace.grocerytray.R;

public class NeedHelpActivity extends AppCompatActivity {

    ImageView imgBack;
    CardView cardOfferTerms,cardPrivacy,cardTerms,cardContact,cardSub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_need_help);

        imgBack = findViewById(R.id.imgBack);
        cardOfferTerms = findViewById(R.id.cardOfferTerms);
        cardPrivacy = findViewById(R.id.cardPrivacy);
        cardTerms = findViewById(R.id.cardTerms);
        cardContact = findViewById(R.id.cardContact);
        cardSub = findViewById(R.id.cardSub);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        cardTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        cardOfferTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        cardContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NeedHelpActivity.this,ContactUsActivity.class);
                startActivity(intent);
            }
        });

        cardPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        cardSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NeedHelpActivity.this,MySubscriptions.class);
                startActivity(intent);
            }
        });

    }
}
