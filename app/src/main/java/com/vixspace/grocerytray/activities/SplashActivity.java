package com.vixspace.grocerytray.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.vixspace.grocerytray.MainActivity;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.SplashScreenResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.utils.PrintMsg;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;
    public String OSName, DeviceName, DeviceID, versionid, DeviceType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        getOSandDeviceId();

    }

    private void getOSandDeviceId() {

        PackageManager manager = getApplicationContext().getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(
                    getApplicationContext().getPackageName(), 0);
            double version = info.versionCode;

            versionid = String.valueOf(version);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        OSName = "ANDROID";
        DeviceType = "1";
        DeviceName = manufacturer + model;
        DeviceID = Settings.Secure.getString(getApplicationContext().getContentResolver(),Settings.Secure.ANDROID_ID);


        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("FCM Token", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        // Log and toast
                        String msg =  token;
                        Log.d("FCM MSg", msg);
                        //   Toast.makeText(Splash.this, msg, Toast.LENGTH_SHORT).show();

                        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                            sendSplashData(token);
                        }else{
                            showNoInterNetMessage();
                        }

                    }
                });

    }

    private void sendSplashData(final String token){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("DeviceID",DeviceID);
        jsonObject.addProperty("DeviceType",1);
        jsonObject.addProperty("DeviceName",DeviceName);
        jsonObject.addProperty("AppVersion",1);
        APIService apiService = RestApiClient.getClient().create(APIService.class);
        Call<JsonObject> apiResponse = apiService.getSplashScreenResponse(jsonObject);
        try{
            apiResponse.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if(response.isSuccessful()){

                        JsonObject splashScreenResponse = response.body();
                        Gson gson = new Gson();
                        final SplashScreenResponse splashResponse = gson.fromJson(splashScreenResponse,SplashScreenResponse.class);

                        if (splashResponse.getSuccess()) {

                            String ApiKey = splashResponse.getExtras().getData().getApiKey();
                            allsharedpreference.setShared_String(Allsharedpreference.API_KEY, ApiKey);

                            allsharedpreference.setPresenceChannel(splashResponse.getExtras().getData().getPubnubData().getMasterChannelName());
                            allsharedpreference.setPubnubSubscribeKey(splashResponse.getExtras().getData().getPubnubData().getSubscribeKey());
                            allsharedpreference.setPubnubPublishKey(splashResponse.getExtras().getData().getPubnubData().getPublishKey());

                            if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                                UpdateFcm(ApiKey,token);
                            }else{
                                showNoInterNetMessage();
                            }

                        }
                        else{
                            commonMethods.showToast(splashResponse.getExtras().getMsg());
                        }
                    }  else {
//                        if (response.message().equals("Bad Request")){
                            Gson gson = new Gson();
                            final SplashScreenResponse splash = gson.fromJson(response.errorBody().charStream(),SplashScreenResponse.class);
                            commonMethods.showToast(splash.getExtras().getMsg());
//                        }
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    commonMethods.showToast(t.getMessage());
                }
            });
        }
        catch (Exception e){
            commonMethods.showToast(e.getMessage());
        }
    }

    private void UpdateFcm(String apiKey, String token) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",apiKey);
        jsonObject.addProperty("FCM_Token",token);

        APIService apiService = RestApiClient.getClient().create(APIService.class);

        Call<JsonObject> all_products = apiService.fcm(jsonObject);

        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if(response.isSuccessful()) {

                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final UpdateFcmResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,UpdateFcmResponse.class);


                    if(listOfProductsResponse.getSuccess()) {

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                if (allsharedpreference.getShared_Booleen(Allsharedpreference.IS_ALREADY_LOGIN)){

                                    if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){

                                        if (allsharedpreference.getShared_Booleen(Allsharedpreference.Location)){

                                            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent);

                                            finish();

                                        } else {
                                            Intent intent = new Intent(SplashActivity.this, AcceptLocationActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent);
                                            finish();
                                        }

                                    } else {
                                        Intent intent = new Intent(SplashActivity.this, SignUpActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        finish();
                                    }


                                } else {
                                    if (allsharedpreference.getShared_Booleen(Allsharedpreference.Skip)){

                                        if (allsharedpreference.getShared_Booleen(Allsharedpreference.Location)){
                                            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            Intent intent = new Intent(SplashActivity.this, AcceptLocationActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent);
                                            finish();
                                        }
                                    } else {
                                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        finish();
                                    }

                                }

                            }
                        }, 3000);



                    } else {

                        commonMethods.showToast(listOfProductsResponse.getExtras().getMsg());

                    }

                } else {
//                    if (response.message().equals("Bad Request")){
                        Gson gson = new Gson();
                        final SplashScreenResponse splash = gson.fromJson(response.errorBody().charStream(),SplashScreenResponse.class);
                        commonMethods.showToast(splash.getExtras().getMsg());

//                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
//                progressBar.setVisibility(View.GONE);
                Toast.makeText(SplashActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(SplashActivity.this, getResources().getString(R.string.no_internet));
    }

}
