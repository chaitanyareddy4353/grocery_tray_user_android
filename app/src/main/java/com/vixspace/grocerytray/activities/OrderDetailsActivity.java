package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.adapters.OrderDeliveryAdapter;
import com.vixspace.grocerytray.adapters.RelatedProductsAdapter;
import com.vixspace.grocerytray.adapters.TrackAdapter;
import com.vixspace.grocerytray.pojos.FetchOrderCompleteResponse;
import com.vixspace.grocerytray.pojos.UpdateFcmResponse;
import com.vixspace.grocerytray.services.APIService;
import com.vixspace.grocerytray.services.NetWorkUtil;
import com.vixspace.grocerytray.services.RestApiClient;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;
import com.vixspace.grocerytray.utils.PrintMsg;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsActivity extends AppCompatActivity {

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;

    TextView txtOrderNumber,txtOrderTime,txtOrderTotal,txtTax,txtDiscount,
            txtDeliveryCharge,txtAddressType,txtAddress,txtModeOfPayment,txtCancel,txtHelp,txtToPay;
    RecyclerView recyclerCart;
    ImageView imgBack;
    Integer indicator = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        imgBack = findViewById(R.id.imgBack);
        txtHelp = findViewById(R.id.txtHelp);
        txtToPay = findViewById(R.id.txtToPay);
        txtCancel = findViewById(R.id.txtCancel);
        txtModeOfPayment = findViewById(R.id.txtModeOfPayment);
        txtAddress = findViewById(R.id.txtAddress);
        txtAddressType = findViewById(R.id.txtAddressType);
        txtDeliveryCharge = findViewById(R.id.txtDeliveryCharge);
        txtOrderNumber = findViewById(R.id.txtOrderNumber);
        txtOrderTime = findViewById(R.id.txtOrderTime);
        txtOrderTotal = findViewById(R.id.txtOrderTotal);
        txtTax = findViewById(R.id.txtTax);
        txtDiscount = findViewById(R.id.txtDiscount);
        recyclerCart = findViewById(R.id.recyclerCart);

        indicator = getIntent().getIntExtra("indicator",0);

        if (indicator==3){
            txtCancel.setVisibility(View.GONE);
        }

        txtHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel: 8008632017"));
                startActivity(intent);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
                    cancelOrder(getIntent().getStringExtra("OrderId"));
                }else{
                    showNoInterNetMessage();
                }
            }
        });


        if (NetWorkUtil.IsNetworkAvailable(getApplicationContext())) {
            LoadOrderDetails(getIntent().getStringExtra("OrderId"));
        }else{
            showNoInterNetMessage();
        }
    }

    private void LoadOrderDetails(String orderNum) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("OrderID",orderNum);

        APIService apiService = RestApiClient.getClient().create(APIService.class);

        Call<JsonObject> all_products = apiService.orderComplete(jsonObject);

        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if(response.isSuccessful()) {

                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final FetchOrderCompleteResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,FetchOrderCompleteResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                        if (listOfProductsResponse.getExtras().getData().getPaymentMode()==1){
                            txtModeOfPayment.setText("GroceryTray Wallet");
                        } else {
                            txtModeOfPayment.setText("COD");
                        }

                        if (listOfProductsResponse.getExtras().getData().getDeliveryInformation().getAddressType()==1){
                            txtAddressType.setText("Home");
                        } else  if (listOfProductsResponse.getExtras().getData().getDeliveryInformation().getAddressType()==2) {
                            txtAddressType.setText("work");
                        } else {
                            txtAddressType.setText("Others");
                        }

                        txtAddress.setText(listOfProductsResponse.getExtras().getData().getDeliveryInformation().getAddress());
                        txtDeliveryCharge.setText("+ ₹"+listOfProductsResponse.getExtras().getData().getPricingInformation().getDeliveryPrice());
                        txtDiscount.setText("+ ₹"+listOfProductsResponse.getExtras().getData().getPricingInformation().getDiscountAmount());
                        txtOrderNumber.setText("#"+listOfProductsResponse.getExtras().getData().getOrderNumber());
                        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = null;
                        try {
                            date = form.parse(listOfProductsResponse.getExtras().getData().getSelectedDateTime());
                        } catch (ParseException e) {

                            e.printStackTrace();
                        }
                        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM yyyy");
                        String newDateStr = postFormater.format(date);

                        txtOrderTime.setText("Ordered on "+newDateStr);
                        txtOrderTotal.setText("₹ "+listOfProductsResponse.getExtras().getData().getPricingInformation().getTotalCartAmount());
                        txtToPay.setText("₹ "+listOfProductsResponse.getExtras().getData().getPricingInformation().getFinalTransactionAmount());
//                        txtTax.setText("₹ "+ listOfProductsResponse.getExtras().getData().getPricingInformation().get );

                        OrderDeliveryAdapter adapter = new OrderDeliveryAdapter(OrderDetailsActivity.this, listOfProductsResponse.getExtras().getData().getCartInformation());
                        recyclerCart.setLayoutManager(new LinearLayoutManager(OrderDetailsActivity.this,RecyclerView.VERTICAL,false));
                        recyclerCart.setAdapter(adapter);

//                        if (listOfProductsResponse.getExtras().getData().getWh)

//                        if (listOfProductsResponse.getExtras().getData().getOrderStatusLogs().size()>0){
//
//                        }
                    } else {
//                        progressBar.setVisibility(View.GONE);
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(OrderDetailsActivity.this, "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(OrderDetailsActivity.this, SplashActivity.class);
                            allsharedpreference.clearAllSharedPreferences();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }


                } else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(OrderDetailsActivity.this, SplashActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        allsharedpreference.clearAllSharedPreferences();
                        startActivity(intent);
                        finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
//                progressBar.setVisibility(View.GONE);
                Toast.makeText(OrderDetailsActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });


    }

    private void cancelOrder(String orderNum) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ApiKey",allsharedpreference.getShared_String(Allsharedpreference.API_KEY));
        jsonObject.addProperty("USERID",allsharedpreference.getShared_String(Allsharedpreference.USER_ID));
        jsonObject.addProperty("SessionID",allsharedpreference.getShared_String(Allsharedpreference.SessionId));
        jsonObject.addProperty("CityID",allsharedpreference.getShared_String(Allsharedpreference.CurrentCityID));
        jsonObject.addProperty("LocalityID",allsharedpreference.getShared_String(Allsharedpreference.LocalityID));
        jsonObject.addProperty("OrderID",orderNum);

        APIService apiService = RestApiClient.getClient().create(APIService.class);

        Call<JsonObject> all_products = apiService.cancelOrders(jsonObject);

        all_products.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if(response.isSuccessful()) {

                    JsonObject splashScreenResponse = response.body();
                    Gson gson = new Gson();
                    final UpdateFcmResponse listOfProductsResponse = gson.fromJson(splashScreenResponse,UpdateFcmResponse.class);

                    if(listOfProductsResponse.getSuccess()) {

                        finish();

                    } else {
//                        progressBar.setVisibility(View.GONE);
                        Log.e("Failed Msg", "Success False Msg");
                        int code = listOfProductsResponse.getExtras().getCode();
                        Log.e("msg", String.valueOf(code));
                        if(code == 1) {
                            Toast.makeText(OrderDetailsActivity.this, "Session Expired", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(OrderDetailsActivity.this, SplashActivity.class);
                            allsharedpreference.clearAllSharedPreferences();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }


                } else {
//                    if (response.message().equals("Bad Request")){
                    Gson gson = new Gson();
                    final UpdateFcmResponse splash = gson.fromJson(response.errorBody().charStream(),UpdateFcmResponse.class);
                    commonMethods.showToast(splash.getExtras().getMsg());
                    int code = splash.getExtras().getCode();
                    if(code == 1) {
                        Intent intent = new Intent(OrderDetailsActivity.this, SplashActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        allsharedpreference.clearAllSharedPreferences();
                        startActivity(intent);
                        finish();
                    }
//                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
//                progressBar.setVisibility(View.GONE);
                Toast.makeText(OrderDetailsActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });


    }

    public void showNoInterNetMessage(){
        PrintMsg.printLongToast(OrderDetailsActivity.this, getResources().getString(R.string.no_internet));
    }

}
