package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vixspace.grocerytray.MainActivity;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;

public class SubscriptionSuccessfulActivity extends AppCompatActivity {

    ImageView imgBack;
    Button btnAddMoney,btnHome;
    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;
    TextView txtDate;
    Integer indicator=0;
    LinearLayout layoutDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_successful);

        allsharedpreference = new Allsharedpreference(this);
        commonMethods = new CommonMethods(this);

        imgBack = findViewById(R.id.imgBack);
        btnAddMoney = findViewById(R.id.btnAddMoney);
        txtDate = findViewById(R.id.txtDate);
        layoutDate = findViewById(R.id.layoutDate);
        btnHome = findViewById(R.id.btnHome);

        indicator= getIntent().getIntExtra("indicator",0);

        if (indicator==1){
            txtDate.setText(getIntent().getStringExtra("date"));
        } else if (indicator==2){
            layoutDate.setVisibility(View.GONE);
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubscriptionSuccessfulActivity.this, MainActivity.class);
                Toast.makeText(SubscriptionSuccessfulActivity.this, "Subscription created successfully", Toast.LENGTH_SHORT).show();
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        btnAddMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubscriptionSuccessfulActivity.this, SideNavActivity.class);
                intent.putExtra("indicator",8);
                startActivity(intent);
//                Intent intent = new Intent(SubscriptionSuccessfulActivity.this,SideNavActivity.class);
//                intent.putExtra("indicator",2);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
//                Intent intent = new Intent(SubscriptionSuccessfulActivity.this,MainActivity.class);
//                intent.putExtra("indicator",2);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
            }
        });

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(SubscriptionSuccessfulActivity.this, SideNavActivity.class);
//                intent.putExtra("indicator",8);
//                startActivity(intent);
//                Intent intent = new Intent(SubscriptionSuccessfulActivity.this,SideNavActivity.class);
//                intent.putExtra("indicator",2);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
                Intent intent = new Intent(SubscriptionSuccessfulActivity.this,MainActivity.class);
//                intent.putExtra("indicator",2);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SubscriptionSuccessfulActivity.this,MainActivity.class);
//                intent.putExtra("indicator",2);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
