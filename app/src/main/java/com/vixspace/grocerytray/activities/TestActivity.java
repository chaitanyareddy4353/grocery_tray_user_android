package com.vixspace.grocerytray.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;

public class TestActivity extends AppCompatActivity {

    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        allsharedpreference = new Allsharedpreference(getApplicationContext());
        commonMethods = new CommonMethods(TestActivity.this);


    }
}