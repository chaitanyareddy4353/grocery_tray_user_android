package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class WalletLogsResponse implements Serializable {

    public class Data {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("Amount_RequestID")
        @Expose
        private String amountRequestID;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("Amount")
        @Expose
        private Double amount;
        @SerializedName("Payment_Gateway_Percentage")
        @Expose
        private Double paymentGatewayPercentage;
        @SerializedName("Payment_Gateway_Amount")
        @Expose
        private Double paymentGatewayAmount;
        @SerializedName("Total_Amount")
        @Expose
        private Double totalAmount;
        @SerializedName("Whether_Request")
        @Expose
        private Boolean whetherRequest;
        @SerializedName("Request_Time")
        @Expose
        private String requestTime;
        @SerializedName("Whether_Payment_Done")
        @Expose
        private Boolean whetherPaymentDone;
        @SerializedName("Payment_Time")
        @Expose
        private String paymentTime;
        @SerializedName("PaymentID")
        @Expose
        private String paymentID;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("__v")
        @Expose
        private Integer v;
//        @SerializedName("PaymentData")
//        @Expose
//        private PaymentData paymentData;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAmountRequestID() {
            return amountRequestID;
        }

        public void setAmountRequestID(String amountRequestID) {
            this.amountRequestID = amountRequestID;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public Double getAmount() {
            return amount;
        }

        public void setAmount(Double amount) {
            this.amount = amount;
        }

        public Double getPaymentGatewayPercentage() {
            return paymentGatewayPercentage;
        }

        public void setPaymentGatewayPercentage(Double paymentGatewayPercentage) {
            this.paymentGatewayPercentage = paymentGatewayPercentage;
        }

        public Double getPaymentGatewayAmount() {
            return paymentGatewayAmount;
        }

        public void setPaymentGatewayAmount(Double paymentGatewayAmount) {
            this.paymentGatewayAmount = paymentGatewayAmount;
        }

        public Double getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Double totalAmount) {
            this.totalAmount = totalAmount;
        }

        public Boolean getWhetherRequest() {
            return whetherRequest;
        }

        public void setWhetherRequest(Boolean whetherRequest) {
            this.whetherRequest = whetherRequest;
        }

        public String getRequestTime() {
            return requestTime;
        }

        public void setRequestTime(String requestTime) {
            this.requestTime = requestTime;
        }

        public Boolean getWhetherPaymentDone() {
            return whetherPaymentDone;
        }

        public void setWhetherPaymentDone(Boolean whetherPaymentDone) {
            this.whetherPaymentDone = whetherPaymentDone;
        }

        public String getPaymentTime() {
            return paymentTime;
        }

        public void setPaymentTime(String paymentTime) {
            this.paymentTime = paymentTime;
        }

        public String getPaymentID() {
            return paymentID;
        }

        public void setPaymentID(String paymentID) {
            this.paymentID = paymentID;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

//        public PaymentData getPaymentData() {
//            return paymentData;
//        }
//
//        public void setPaymentData(PaymentData paymentData) {
//            this.paymentData = paymentData;
//        }

    }

    public class Datum {

        @SerializedName("LogID")
        @Expose
        private String logID;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("Type")
        @Expose
        private Integer type;
        @SerializedName("Amount")
        @Expose
        private Double amount;
        @SerializedName("Available_Amount")
        @Expose
        private Double availableAmount;
        @SerializedName("Time")
        @Expose
        private String time;
        @SerializedName("Data")
        @Expose
        private Data data;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public String getLogID() {
            return logID;
        }

        public void setLogID(String logID) {
            this.logID = logID;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public Double getAmount() {
            return amount;
        }

        public void setAmount(Double amount) {
            this.amount = amount;
        }

        public Double getAvailableAmount() {
            return availableAmount;
        }

        public void setAvailableAmount(Double availableAmount) {
            this.availableAmount = availableAmount;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Count")
        @Expose
        private Integer count;
        @SerializedName("Data")
        @Expose
        private List<Datum> data = null;

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }

    }

    public class PaymentData {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("entity")
        @Expose
        private String entity;
        @SerializedName("amount")
        @Expose
        private Double amount;
        @SerializedName("currency")
        @Expose
        private String currency;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("order_id")
        @Expose
        private String orderId;
        @SerializedName("invoice_id")
        @Expose
        private String invoiceId;
        @SerializedName("international")
        @Expose
        private Boolean international;
        @SerializedName("method")
        @Expose
        private String method;
        @SerializedName("amount_refunded")
        @Expose
        private Double amountRefunded;
        @SerializedName("refund_status")
        @Expose
        private Object refundStatus;
        @SerializedName("captured")
        @Expose
        private Boolean captured;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("card_id")
        @Expose
        private Object cardId;
        @SerializedName("bank")
        @Expose
        private Object bank;
        @SerializedName("wallet")
        @Expose
        private String wallet;
        @SerializedName("vpa")
        @Expose
        private Object vpa;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("contact")
        @Expose
        private String contact;
        @SerializedName("customer_id")
        @Expose
        private String customerId;
        @SerializedName("notes")
        @Expose
        private List<Object> notes = null;
        @SerializedName("fee")
        @Expose
        private Double fee;
        @SerializedName("tax")
        @Expose
        private Double tax;
        @SerializedName("error_code")
        @Expose
        private Object errorCode;
        @SerializedName("error_description")
        @Expose
        private Object errorDescription;
        @SerializedName("error_source")
        @Expose
        private Object errorSource;
        @SerializedName("error_step")
        @Expose
        private Object errorStep;
        @SerializedName("error_reason")
        @Expose
        private Object errorReason;
        @SerializedName("created_at")
        @Expose
        private String createdAt;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEntity() {
            return entity;
        }

        public void setEntity(String entity) {
            this.entity = entity;
        }

        public Double getAmount() {
            return amount;
        }

        public void setAmount(Double amount) {
            this.amount = amount;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getInvoiceId() {
            return invoiceId;
        }

        public void setInvoiceId(String invoiceId) {
            this.invoiceId = invoiceId;
        }

        public Boolean getInternational() {
            return international;
        }

        public void setInternational(Boolean international) {
            this.international = international;
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public Double getAmountRefunded() {
            return amountRefunded;
        }

        public void setAmountRefunded(Double amountRefunded) {
            this.amountRefunded = amountRefunded;
        }

        public Object getRefundStatus() {
            return refundStatus;
        }

        public void setRefundStatus(Object refundStatus) {
            this.refundStatus = refundStatus;
        }

        public Boolean getCaptured() {
            return captured;
        }

        public void setCaptured(Boolean captured) {
            this.captured = captured;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Object getCardId() {
            return cardId;
        }

        public void setCardId(Object cardId) {
            this.cardId = cardId;
        }

        public Object getBank() {
            return bank;
        }

        public void setBank(Object bank) {
            this.bank = bank;
        }

        public String getWallet() {
            return wallet;
        }

        public void setWallet(String wallet) {
            this.wallet = wallet;
        }

        public Object getVpa() {
            return vpa;
        }

        public void setVpa(Object vpa) {
            this.vpa = vpa;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getContact() {
            return contact;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public List<Object> getNotes() {
            return notes;
        }

        public void setNotes(List<Object> notes) {
            this.notes = notes;
        }

        public Double getFee() {
            return fee;
        }

        public void setFee(Double fee) {
            this.fee = fee;
        }

        public Double getTax() {
            return tax;
        }

        public void setTax(Double tax) {
            this.tax = tax;
        }

        public Object getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(Object errorCode) {
            this.errorCode = errorCode;
        }

        public Object getErrorDescription() {
            return errorDescription;
        }

        public void setErrorDescription(Object errorDescription) {
            this.errorDescription = errorDescription;
        }

        public Object getErrorSource() {
            return errorSource;
        }

        public void setErrorSource(Object errorSource) {
            this.errorSource = errorSource;
        }

        public Object getErrorStep() {
            return errorStep;
        }

        public void setErrorStep(Object errorStep) {
            this.errorStep = errorStep;
        }

        public Object getErrorReason() {
            return errorReason;
        }

        public void setErrorReason(Object errorReason) {
            this.errorReason = errorReason;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

    }
}
