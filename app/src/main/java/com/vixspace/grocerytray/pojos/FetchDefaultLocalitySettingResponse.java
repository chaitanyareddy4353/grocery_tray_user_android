package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FetchDefaultLocalitySettingResponse implements Serializable {

      public class Data {

        @SerializedName("Whether_Locality_Available")
        @Expose
        private Boolean whetherLocalityAvailable;
        @SerializedName("LocalityData")
        @Expose
        private LocalityData localityData;

        public Boolean getWhetherLocalityAvailable() {
            return whetherLocalityAvailable;
        }

        public void setWhetherLocalityAvailable(Boolean whetherLocalityAvailable) {
            this.whetherLocalityAvailable = whetherLocalityAvailable;
        }

        public LocalityData getLocalityData() {
            return localityData;
        }

        public void setLocalityData(LocalityData localityData) {
            this.localityData = localityData;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Data")
        @Expose
        private Data data;

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }

    public class LocalityData {

        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("Locality_Title")
        @Expose
        private String localityTitle;
        @SerializedName("City_Title")
        @Expose
        private String cityTitle;

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityTitle() {
            return localityTitle;
        }

        public void setLocalityTitle(String localityTitle) {
            this.localityTitle = localityTitle;
        }

        public String getCityTitle() {
            return cityTitle;
        }

        public void setCityTitle(String cityTitle) {
            this.cityTitle = cityTitle;
        }

    }
}
