package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ListAllUserAddressResponse implements Serializable {

    public class Datum {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("PhoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("Address")
        @Expose
        private String address;
        @SerializedName("AddressID")
        @Expose
        private String addressID;
        @SerializedName("AddressType")
        @Expose
        private Integer addressType;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("Latitude")
        @Expose
        private Double latitude;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("Locality_Information")
        @Expose
        private LocalityInformation localityInformation;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAddressID() {
            return addressID;
        }

        public void setAddressID(String addressID) {
            this.addressID = addressID;
        }

        public Integer getAddressType() {
            return addressType;
        }

        public void setAddressType(Integer addressType) {
            this.addressType = addressType;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public LocalityInformation getLocalityInformation() {
            return localityInformation;
        }

        public void setLocalityInformation(LocalityInformation localityInformation) {
            this.localityInformation = localityInformation;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {
        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Data")
        @Expose
        private List<Datum> data = null;

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }

    }

    public class LocalityInformation {

        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("Locality_Title")
        @Expose
        private String localityTitle;
        @SerializedName("City_Title")
        @Expose
        private String cityTitle;

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityTitle() {
            return localityTitle;
        }

        public void setLocalityTitle(String localityTitle) {
            this.localityTitle = localityTitle;
        }

        public String getCityTitle() {
            return cityTitle;
        }

        public void setCityTitle(String cityTitle) {
            this.cityTitle = cityTitle;
        }

    }
}
