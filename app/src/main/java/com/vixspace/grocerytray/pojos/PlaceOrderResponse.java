package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PlaceOrderResponse implements Serializable {

    public class CartInformation {

        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("ProductID")
        @Expose
        private String productID;
        @SerializedName("LocalityProductID")
        @Expose
        private String localityProductID;
        @SerializedName("MRP")
        @Expose
        private Double mRP;
        @SerializedName("Selling_Price")
        @Expose
        private Double sellingPrice;
        @SerializedName("Quantity")
        @Expose
        private Double quantity;
        @SerializedName("Sub_Total_Amount")
        @Expose
        private Double subTotalAmount;
        @SerializedName("_id")
        @Expose
        private String id;

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public String getProductID() {
            return productID;
        }

        public void setProductID(String productID) {
            this.productID = productID;
        }

        public String getLocalityProductID() {
            return localityProductID;
        }

        public void setLocalityProductID(String localityProductID) {
            this.localityProductID = localityProductID;
        }

        public Double getMRP() {
            return mRP;
        }

        public void setMRP(Double mRP) {
            this.mRP = mRP;
        }

        public Double getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(Double sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public Double getQuantity() {
            return quantity;
        }

        public void setQuantity(Double quantity) {
            this.quantity = quantity;
        }

        public Double getSubTotalAmount() {
            return subTotalAmount;
        }

        public void setSubTotalAmount(Double subTotalAmount) {
            this.subTotalAmount = subTotalAmount;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

    }

    public class CouponInformation {

        @SerializedName("AppliedCouponID")
        @Expose
        private String appliedCouponID;
        @SerializedName("CouponID")
        @Expose
        private String couponID;

        public String getAppliedCouponID() {
            return appliedCouponID;
        }

        public void setAppliedCouponID(String appliedCouponID) {
            this.appliedCouponID = appliedCouponID;
        }

        public String getCouponID() {
            return couponID;
        }

        public void setCouponID(String couponID) {
            this.couponID = couponID;
        }

    }

    public class Data {

        @SerializedName("Delivery_Information")
        @Expose
        private DeliveryInformation deliveryInformation;
        @SerializedName("Device_Information")
        @Expose
        private DeviceInformation deviceInformation;
        @SerializedName("Coupon_Information")
        @Expose
        private CouponInformation couponInformation;
        @SerializedName("Pricing_Information")
        @Expose
        private PricingInformation pricingInformation;
        @SerializedName("OrderID")
        @Expose
        private String orderID;
        @SerializedName("Order_Number")
        @Expose
        private String orderNumber;
        @SerializedName("PriceQuoteID")
        @Expose
        private String priceQuoteID;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("AddressID")
        @Expose
        private String addressID;
        @SerializedName("SLOTID")
        @Expose
        private String sLOTID;
        @SerializedName("Selected_Date_Time")
        @Expose
        private String selectedDateTime;
        @SerializedName("Payment_Mode")
        @Expose
        private Integer paymentMode;
        @SerializedName("Whether_Coupon_Applied")
        @Expose
        private Boolean whetherCouponApplied;
        @SerializedName("Order_Status")
        @Expose
        private Integer orderStatus;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("Delivery_Point")
        @Expose
        private List<Double> deliveryPoint = null;
        @SerializedName("Cart_Information")
        @Expose
        private List<CartInformation> cartInformation = null;
        @SerializedName("Order_Status_Logs")
        @Expose
        private List<OrderStatusLog> orderStatusLogs = null;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public DeliveryInformation getDeliveryInformation() {
            return deliveryInformation;
        }

        public void setDeliveryInformation(DeliveryInformation deliveryInformation) {
            this.deliveryInformation = deliveryInformation;
        }

        public DeviceInformation getDeviceInformation() {
            return deviceInformation;
        }

        public void setDeviceInformation(DeviceInformation deviceInformation) {
            this.deviceInformation = deviceInformation;
        }

        public CouponInformation getCouponInformation() {
            return couponInformation;
        }

        public void setCouponInformation(CouponInformation couponInformation) {
            this.couponInformation = couponInformation;
        }

        public PricingInformation getPricingInformation() {
            return pricingInformation;
        }

        public void setPricingInformation(PricingInformation pricingInformation) {
            this.pricingInformation = pricingInformation;
        }

        public String getOrderID() {
            return orderID;
        }

        public void setOrderID(String orderID) {
            this.orderID = orderID;
        }

        public String getOrderNumber() {
            return orderNumber;
        }

        public void setOrderNumber(String orderNumber) {
            this.orderNumber = orderNumber;
        }

        public String getPriceQuoteID() {
            return priceQuoteID;
        }

        public void setPriceQuoteID(String priceQuoteID) {
            this.priceQuoteID = priceQuoteID;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getAddressID() {
            return addressID;
        }

        public void setAddressID(String addressID) {
            this.addressID = addressID;
        }

        public String getSLOTID() {
            return sLOTID;
        }

        public void setSLOTID(String sLOTID) {
            this.sLOTID = sLOTID;
        }

        public String getSelectedDateTime() {
            return selectedDateTime;
        }

        public void setSelectedDateTime(String selectedDateTime) {
            this.selectedDateTime = selectedDateTime;
        }

        public Integer getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(Integer paymentMode) {
            this.paymentMode = paymentMode;
        }

        public Boolean getWhetherCouponApplied() {
            return whetherCouponApplied;
        }

        public void setWhetherCouponApplied(Boolean whetherCouponApplied) {
            this.whetherCouponApplied = whetherCouponApplied;
        }

        public Integer getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(Integer orderStatus) {
            this.orderStatus = orderStatus;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<Double> getDeliveryPoint() {
            return deliveryPoint;
        }

        public void setDeliveryPoint(List<Double> deliveryPoint) {
            this.deliveryPoint = deliveryPoint;
        }

        public List<CartInformation> getCartInformation() {
            return cartInformation;
        }

        public void setCartInformation(List<CartInformation> cartInformation) {
            this.cartInformation = cartInformation;
        }

        public List<OrderStatusLog> getOrderStatusLogs() {
            return orderStatusLogs;
        }

        public void setOrderStatusLogs(List<OrderStatusLog> orderStatusLogs) {
            this.orderStatusLogs = orderStatusLogs;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }

    public class DeliveryInformation {

        @SerializedName("AddressID")
        @Expose
        private String addressID;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("PhoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("AddressType")
        @Expose
        private Integer addressType;
        @SerializedName("Address")
        @Expose
        private String address;
        @SerializedName("Latitude")
        @Expose
        private Double latitude;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;

        public String getAddressID() {
            return addressID;
        }

        public void setAddressID(String addressID) {
            this.addressID = addressID;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public Integer getAddressType() {
            return addressType;
        }

        public void setAddressType(Integer addressType) {
            this.addressType = addressType;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

    }

    public class DeviceInformation {

        @SerializedName("DeviceID")
        @Expose
        private String deviceID;
        @SerializedName("DeviceType")
        @Expose
        private Integer deviceType;
        @SerializedName("DeviceName")
        @Expose
        private String deviceName;
        @SerializedName("AppVersion")
        @Expose
        private Integer appVersion;
        @SerializedName("IPAddress")
        @Expose
        private String iPAddress;

        public String getDeviceID() {
            return deviceID;
        }

        public void setDeviceID(String deviceID) {
            this.deviceID = deviceID;
        }

        public Integer getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(Integer deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceName() {
            return deviceName;
        }

        public void setDeviceName(String deviceName) {
            this.deviceName = deviceName;
        }

        public Integer getAppVersion() {
            return appVersion;
        }

        public void setAppVersion(Integer appVersion) {
            this.appVersion = appVersion;
        }

        public String getIPAddress() {
            return iPAddress;
        }

        public void setIPAddress(String iPAddress) {
            this.iPAddress = iPAddress;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Data")
        @Expose
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }

    public class OrderStatusLog {

        @SerializedName("LogID")
        @Expose
        private String logID;
        @SerializedName("DriverID")
        @Expose
        private String driverID;
        @SerializedName("Order_Status")
        @Expose
        private Integer orderStatus;
        @SerializedName("Comment")
        @Expose
        private String comment;
        @SerializedName("Time")
        @Expose
        private String time;

        public String getLogID() {
            return logID;
        }

        public void setLogID(String logID) {
            this.logID = logID;
        }

        public String getDriverID() {
            return driverID;
        }

        public void setDriverID(String driverID) {
            this.driverID = driverID;
        }

        public Integer getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(Integer orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

    }

    public class PricingInformation {

        @SerializedName("Total_Cart_Amount")
        @Expose
        private Double totalCartAmount;
        @SerializedName("Delivery_Price")
        @Expose
        private Double deliveryPrice;
        @SerializedName("Whether_Offer_Coupon_Applied")
        @Expose
        private Boolean whetherOfferCouponApplied;
        @SerializedName("Discount_Percentage")
        @Expose
        private Double discountPercentage;
        @SerializedName("Discount_Amount")
        @Expose
        private Double discountAmount;
        @SerializedName("Final_Transaction_Amount")
        @Expose
        private Double finalTransactionAmount;

        public Double getTotalCartAmount() {
            return totalCartAmount;
        }

        public void setTotalCartAmount(Double totalCartAmount) {
            this.totalCartAmount = totalCartAmount;
        }

        public Double getDeliveryPrice() {
            return deliveryPrice;
        }

        public void setDeliveryPrice(Double deliveryPrice) {
            this.deliveryPrice = deliveryPrice;
        }

        public Boolean getWhetherOfferCouponApplied() {
            return whetherOfferCouponApplied;
        }

        public void setWhetherOfferCouponApplied(Boolean whetherOfferCouponApplied) {
            this.whetherOfferCouponApplied = whetherOfferCouponApplied;
        }

        public Double getDiscountPercentage() {
            return discountPercentage;
        }

        public void setDiscountPercentage(Double discountPercentage) {
            this.discountPercentage = discountPercentage;
        }

        public Double getDiscountAmount() {
            return discountAmount;
        }

        public void setDiscountAmount(Double discountAmount) {
            this.discountAmount = discountAmount;
        }

        public Double getFinalTransactionAmount() {
            return finalTransactionAmount;
        }

        public void setFinalTransactionAmount(Double finalTransactionAmount) {
            this.finalTransactionAmount = finalTransactionAmount;
        }

    }
}
