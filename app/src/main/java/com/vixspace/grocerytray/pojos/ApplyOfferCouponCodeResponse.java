package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ApplyOfferCouponCodeResponse implements Serializable {

    public class Data {

        @SerializedName("Pricing_Information")
        @Expose
        private PricingInformation pricingInformation;
        @SerializedName("AppliedCouponID")
        @Expose
        private String appliedCouponID;
        @SerializedName("PriceQuoteID")
        @Expose
        private String priceQuoteID;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("AddressID")
        @Expose
        private String addressID;
        @SerializedName("SLOTID")
        @Expose
        private String sLOTID;
        @SerializedName("Selected_Date_Time")
        @Expose
        private String selectedDateTime;
        @SerializedName("CouponID")
        @Expose
        private String couponID;
        @SerializedName("Coupon_Code")
        @Expose
        private String couponCode;
        @SerializedName("Discount_Percentage")
        @Expose
        private Integer discountPercentage;
        @SerializedName("Whether_Order_Placed")
        @Expose
        private Boolean whetherOrderPlaced;
        @SerializedName("Order_Time")
        @Expose
        private Object orderTime;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public PricingInformation getPricingInformation() {
            return pricingInformation;
        }

        public void setPricingInformation(PricingInformation pricingInformation) {
            this.pricingInformation = pricingInformation;
        }

        public String getAppliedCouponID() {
            return appliedCouponID;
        }

        public void setAppliedCouponID(String appliedCouponID) {
            this.appliedCouponID = appliedCouponID;
        }

        public String getPriceQuoteID() {
            return priceQuoteID;
        }

        public void setPriceQuoteID(String priceQuoteID) {
            this.priceQuoteID = priceQuoteID;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getAddressID() {
            return addressID;
        }

        public void setAddressID(String addressID) {
            this.addressID = addressID;
        }

        public String getSLOTID() {
            return sLOTID;
        }

        public void setSLOTID(String sLOTID) {
            this.sLOTID = sLOTID;
        }

        public String getSelectedDateTime() {
            return selectedDateTime;
        }

        public void setSelectedDateTime(String selectedDateTime) {
            this.selectedDateTime = selectedDateTime;
        }

        public String getCouponID() {
            return couponID;
        }

        public void setCouponID(String couponID) {
            this.couponID = couponID;
        }

        public String getCouponCode() {
            return couponCode;
        }

        public void setCouponCode(String couponCode) {
            this.couponCode = couponCode;
        }

        public Integer getDiscountPercentage() {
            return discountPercentage;
        }

        public void setDiscountPercentage(Integer discountPercentage) {
            this.discountPercentage = discountPercentage;
        }

        public Boolean getWhetherOrderPlaced() {
            return whetherOrderPlaced;
        }

        public void setWhetherOrderPlaced(Boolean whetherOrderPlaced) {
            this.whetherOrderPlaced = whetherOrderPlaced;
        }

        public Object getOrderTime() {
            return orderTime;
        }

        public void setOrderTime(Object orderTime) {
            this.orderTime = orderTime;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Data")
        @Expose
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }

    public class PricingInformation {

        @SerializedName("Total_Cart_Amount")
        @Expose
        private Integer totalCartAmount;
        @SerializedName("Delivery_Price")
        @Expose
        private Integer deliveryPrice;
        @SerializedName("Whether_Offer_Coupon_Applied")
        @Expose
        private Boolean whetherOfferCouponApplied;
        @SerializedName("Discount_Percentage")
        @Expose
        private Integer discountPercentage;
        @SerializedName("Discount_Amount")
        @Expose
        private Integer discountAmount;
        @SerializedName("Final_Transaction_Amount")
        @Expose
        private Integer finalTransactionAmount;

        public Integer getTotalCartAmount() {
            return totalCartAmount;
        }

        public void setTotalCartAmount(Integer totalCartAmount) {
            this.totalCartAmount = totalCartAmount;
        }

        public Integer getDeliveryPrice() {
            return deliveryPrice;
        }

        public void setDeliveryPrice(Integer deliveryPrice) {
            this.deliveryPrice = deliveryPrice;
        }

        public Boolean getWhetherOfferCouponApplied() {
            return whetherOfferCouponApplied;
        }

        public void setWhetherOfferCouponApplied(Boolean whetherOfferCouponApplied) {
            this.whetherOfferCouponApplied = whetherOfferCouponApplied;
        }

        public Integer getDiscountPercentage() {
            return discountPercentage;
        }

        public void setDiscountPercentage(Integer discountPercentage) {
            this.discountPercentage = discountPercentage;
        }

        public Integer getDiscountAmount() {
            return discountAmount;
        }

        public void setDiscountAmount(Integer discountAmount) {
            this.discountAmount = discountAmount;
        }

        public Integer getFinalTransactionAmount() {
            return finalTransactionAmount;
        }

        public void setFinalTransactionAmount(Integer finalTransactionAmount) {
            this.finalTransactionAmount = finalTransactionAmount;
        }

    }
}
