package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FetchAllAvailableOfferCouponsResponse implements Serializable {

    public class Datum {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("Image_Information")
        @Expose
        private ImageInformation imageInformation;
        @SerializedName("CouponID")
        @Expose
        private String couponID;
        @SerializedName("Coupon_Code")
        @Expose
        private String couponCode;
        @SerializedName("Description")
        @Expose
        private String description;
        @SerializedName("Terms_and_Condition")
        @Expose
        private String termsAndCondition;
        @SerializedName("Start_Date_Time")
        @Expose
        private String startDateTime;
        @SerializedName("End_Date_Time")
        @Expose
        private String endDateTime;
        @SerializedName("Discount_Percentage")
        @Expose
        private Integer discountPercentage;
        @SerializedName("Minimum_Billing_Amount")
        @Expose
        private Integer minimumBillingAmount;
        @SerializedName("MAX_User_Applicable_Orders")
        @Expose
        private Integer mAXUserApplicableOrders;
        @SerializedName("MAX_Discount_Amount")
        @Expose
        private Integer mAXDiscountAmount;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public ImageInformation getImageInformation() {
            return imageInformation;
        }

        public void setImageInformation(ImageInformation imageInformation) {
            this.imageInformation = imageInformation;
        }

        public String getCouponID() {
            return couponID;
        }

        public void setCouponID(String couponID) {
            this.couponID = couponID;
        }

        public String getCouponCode() {
            return couponCode;
        }

        public void setCouponCode(String couponCode) {
            this.couponCode = couponCode;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTermsAndCondition() {
            return termsAndCondition;
        }

        public void setTermsAndCondition(String termsAndCondition) {
            this.termsAndCondition = termsAndCondition;
        }

        public String getStartDateTime() {
            return startDateTime;
        }

        public void setStartDateTime(String startDateTime) {
            this.startDateTime = startDateTime;
        }

        public String getEndDateTime() {
            return endDateTime;
        }

        public void setEndDateTime(String endDateTime) {
            this.endDateTime = endDateTime;
        }

        public Integer getDiscountPercentage() {
            return discountPercentage;
        }

        public void setDiscountPercentage(Integer discountPercentage) {
            this.discountPercentage = discountPercentage;
        }

        public Integer getMinimumBillingAmount() {
            return minimumBillingAmount;
        }

        public void setMinimumBillingAmount(Integer minimumBillingAmount) {
            this.minimumBillingAmount = minimumBillingAmount;
        }

        public Integer getMAXUserApplicableOrders() {
            return mAXUserApplicableOrders;
        }

        public void setMAXUserApplicableOrders(Integer mAXUserApplicableOrders) {
            this.mAXUserApplicableOrders = mAXUserApplicableOrders;
        }

        public Integer getMAXDiscountAmount() {
            return mAXDiscountAmount;
        }

        public void setMAXDiscountAmount(Integer mAXDiscountAmount) {
            this.mAXDiscountAmount = mAXDiscountAmount;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Count")
        @Expose
        private Integer count;
        @SerializedName("Data")
        @Expose
        private List<Datum> data = null;

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }

    }

    public class ImageInformation {

        @SerializedName("ImageID")
        @Expose
        private String imageID;
        @SerializedName("Image50")
        @Expose
        private String image50;
        @SerializedName("Image100")
        @Expose
        private String image100;
        @SerializedName("Image250")
        @Expose
        private String image250;
        @SerializedName("Image550")
        @Expose
        private String image550;
        @SerializedName("Image900")
        @Expose
        private String image900;
        @SerializedName("ImageOriginal")
        @Expose
        private String imageOriginal;

        public String getImageID() {
            return imageID;
        }

        public void setImageID(String imageID) {
            this.imageID = imageID;
        }

        public String getImage50() {
            return image50;
        }

        public void setImage50(String image50) {
            this.image50 = image50;
        }

        public String getImage100() {
            return image100;
        }

        public void setImage100(String image100) {
            this.image100 = image100;
        }

        public String getImage250() {
            return image250;
        }

        public void setImage250(String image250) {
            this.image250 = image250;
        }

        public String getImage550() {
            return image550;
        }

        public void setImage550(String image550) {
            this.image550 = image550;
        }

        public String getImage900() {
            return image900;
        }

        public void setImage900(String image900) {
            this.image900 = image900;
        }

        public String getImageOriginal() {
            return imageOriginal;
        }

        public void setImageOriginal(String imageOriginal) {
            this.imageOriginal = imageOriginal;
        }

    }
}
