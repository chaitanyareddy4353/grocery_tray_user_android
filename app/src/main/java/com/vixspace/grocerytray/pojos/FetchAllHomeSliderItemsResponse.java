package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FetchAllHomeSliderItemsResponse implements Serializable {

    public class CategoryData {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("Image_Information")
        @Expose
        private ImageInformation imageInformation;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("Category_Number")
        @Expose
        private Integer categoryNumber;
        @SerializedName("Category_Title")
        @Expose
        private String categoryTitle;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public ImageInformation getImageInformation() {
            return imageInformation;
        }

        public void setImageInformation(ImageInformation imageInformation) {
            this.imageInformation = imageInformation;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public Integer getCategoryNumber() {
            return categoryNumber;
        }

        public void setCategoryNumber(Integer categoryNumber) {
            this.categoryNumber = categoryNumber;
        }

        public String getCategoryTitle() {
            return categoryTitle;
        }

        public void setCategoryTitle(String categoryTitle) {
            this.categoryTitle = categoryTitle;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }

    public class CollectionData {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("CollectionID")
        @Expose
        private String collectionID;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("Collection_Number")
        @Expose
        private Integer collectionNumber;
        @SerializedName("Collection_Name")
        @Expose
        private String collectionName;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCollectionID() {
            return collectionID;
        }

        public void setCollectionID(String collectionID) {
            this.collectionID = collectionID;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public Integer getCollectionNumber() {
            return collectionNumber;
        }

        public void setCollectionNumber(Integer collectionNumber) {
            this.collectionNumber = collectionNumber;
        }

        public String getCollectionName() {
            return collectionName;
        }

        public void setCollectionName(String collectionName) {
            this.collectionName = collectionName;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }

    public class CouponData {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("Image_Information")
        @Expose
        private ImageInformation imageInformation;
        @SerializedName("CouponID")
        @Expose
        private String couponID;
        @SerializedName("Coupon_Code")
        @Expose
        private String couponCode;
        @SerializedName("Description")
        @Expose
        private String description;
        @SerializedName("Terms_and_Condition")
        @Expose
        private String termsAndCondition;
        @SerializedName("Start_Date_Time")
        @Expose
        private String startDateTime;
        @SerializedName("End_Date_Time")
        @Expose
        private String endDateTime;
        @SerializedName("Discount_Percentage")
        @Expose
        private Integer discountPercentage;
        @SerializedName("Minimum_Billing_Amount")
        @Expose
        private Integer minimumBillingAmount;
        @SerializedName("MAX_User_Applicable_Orders")
        @Expose
        private Integer mAXUserApplicableOrders;
        @SerializedName("MAX_Discount_Amount")
        @Expose
        private Integer mAXDiscountAmount;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public ImageInformation getImageInformation() {
            return imageInformation;
        }

        public void setImageInformation(ImageInformation imageInformation) {
            this.imageInformation = imageInformation;
        }

        public String getCouponID() {
            return couponID;
        }

        public void setCouponID(String couponID) {
            this.couponID = couponID;
        }

        public String getCouponCode() {
            return couponCode;
        }

        public void setCouponCode(String couponCode) {
            this.couponCode = couponCode;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTermsAndCondition() {
            return termsAndCondition;
        }

        public void setTermsAndCondition(String termsAndCondition) {
            this.termsAndCondition = termsAndCondition;
        }

        public String getStartDateTime() {
            return startDateTime;
        }

        public void setStartDateTime(String startDateTime) {
            this.startDateTime = startDateTime;
        }

        public String getEndDateTime() {
            return endDateTime;
        }

        public void setEndDateTime(String endDateTime) {
            this.endDateTime = endDateTime;
        }

        public Integer getDiscountPercentage() {
            return discountPercentage;
        }

        public void setDiscountPercentage(Integer discountPercentage) {
            this.discountPercentage = discountPercentage;
        }

        public Integer getMinimumBillingAmount() {
            return minimumBillingAmount;
        }

        public void setMinimumBillingAmount(Integer minimumBillingAmount) {
            this.minimumBillingAmount = minimumBillingAmount;
        }

        public Integer getMAXUserApplicableOrders() {
            return mAXUserApplicableOrders;
        }

        public void setMAXUserApplicableOrders(Integer mAXUserApplicableOrders) {
            this.mAXUserApplicableOrders = mAXUserApplicableOrders;
        }

        public Integer getMAXDiscountAmount() {
            return mAXDiscountAmount;
        }

        public void setMAXDiscountAmount(Integer mAXDiscountAmount) {
            this.mAXDiscountAmount = mAXDiscountAmount;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }

    public class Datum {

        @SerializedName("Document_Information")
        @Expose
        private DocumentInformation documentInformation;
        @SerializedName("ItemID")
        @Expose
        private String itemID;
        @SerializedName("Item_Type")
        @Expose
        private String itemType;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("CollectionID")
        @Expose
        private String collectionID;
        @SerializedName("ProductID")
        @Expose
        private String productID;
        @SerializedName("CouponID")
        @Expose
        private String couponID;
        @SerializedName("Page_URL")
        @Expose
        private String pageURL;
        @SerializedName("Status")
        @Expose
        private Boolean status;

        public Boolean getWhetherFavouriteProduct() {
            return whetherFavouriteProduct;
        }

        public void setWhetherFavouriteProduct(Boolean whetherFavouriteProduct) {
            this.whetherFavouriteProduct = whetherFavouriteProduct;
        }

        @SerializedName("Whether_Favourite_Product")
        @Expose
        private Boolean whetherFavouriteProduct;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public CategoryData getCategoryData() {
            return categoryData;
        }

        public void setCategoryData(CategoryData categoryData) {
            this.categoryData = categoryData;
        }

        public CollectionData getCollectionData() {
            return collectionData;
        }

        public void setCollectionData(CollectionData collectionData) {
            this.collectionData = collectionData;
        }

        public ProductData getProductData() {
            return productData;
        }

        public void setProductData(ProductData productData) {
            this.productData = productData;
        }

        public CouponData getCouponData() {
            return couponData;
        }

        public void setCouponData(CouponData couponData) {
            this.couponData = couponData;
        }

        @SerializedName("CategoryData")
        @Expose
        private CategoryData categoryData;
        @SerializedName("CollectionData")
        @Expose
        private CollectionData collectionData;
        @SerializedName("ProductData")
        @Expose
        private ProductData productData;
        @SerializedName("CouponData")
        @Expose
        private CouponData couponData;

        public DocumentInformation getDocumentInformation() {
            return documentInformation;
        }

        public void setDocumentInformation(DocumentInformation documentInformation) {
            this.documentInformation = documentInformation;
        }

        public String getItemID() {
            return itemID;
        }

        public void setItemID(String itemID) {
            this.itemID = itemID;
        }

        public String getItemType() {
            return itemType;
        }

        public void setItemType(String itemType) {
            this.itemType = itemType;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public String getCollectionID() {
            return collectionID;
        }

        public void setCollectionID(String collectionID) {
            this.collectionID = collectionID;
        }

        public String getProductID() {
            return productID;
        }

        public void setProductID(String productID) {
            this.productID = productID;
        }

        public String getCouponID() {
            return couponID;
        }

        public void setCouponID(String couponID) {
            this.couponID = couponID;
        }

        public String getPageURL() {
            return pageURL;
        }

        public void setPageURL(String pageURL) {
            this.pageURL = pageURL;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

//        public CategoryData getCategoryData() {
//            return categoryData;
//        }
//
//        public void setCategoryData(CategoryData categoryData) {
//            this.categoryData = categoryData;
//        }
//
//        public CollectionData getCollectionData() {
//            return collectionData;
//        }
//
//        public void setCollectionData(CollectionData collectionData) {
//            this.collectionData = collectionData;
//        }
//
//        public ProductData getProductData() {
//            return productData;
//        }
//
//        public void setProductData(ProductData productData) {
//            this.productData = productData;
//        }
//
//        public CouponData getCouponData() {
//            return couponData;
//        }
//
//        public void setCouponData(CouponData couponData) {
//            this.couponData = couponData;
//        }

    }

    public class DocumentInformation {

        @SerializedName("DocumentID")
        @Expose
        private String documentID;
        @SerializedName("Document_URL")
        @Expose
        private String documentURL;

        public String getDocumentID() {
            return documentID;
        }

        public void setDocumentID(String documentID) {
            this.documentID = documentID;
        }

        public String getDocumentURL() {
            return documentURL;
        }

        public void setDocumentURL(String documentURL) {
            this.documentURL = documentURL;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {


        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;


        @SerializedName("Count")
        @Expose
        private Integer count;
        @SerializedName("Data")
        @Expose
        private List<Datum> data = null;

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }

    }

    public class ImageInformation {

        @SerializedName("ImageID")
        @Expose
        private String imageID;
        @SerializedName("Image50")
        @Expose
        private String image50;
        @SerializedName("Image100")
        @Expose
        private String image100;
        @SerializedName("Image250")
        @Expose
        private String image250;
        @SerializedName("Image550")
        @Expose
        private String image550;
        @SerializedName("Image900")
        @Expose
        private String image900;
        @SerializedName("ImageOriginal")
        @Expose
        private String imageOriginal;

        public String getImageID() {
            return imageID;
        }

        public void setImageID(String imageID) {
            this.imageID = imageID;
        }

        public String getImage50() {
            return image50;
        }

        public void setImage50(String image50) {
            this.image50 = image50;
        }

        public String getImage100() {
            return image100;
        }

        public void setImage100(String image100) {
            this.image100 = image100;
        }

        public String getImage250() {
            return image250;
        }

        public void setImage250(String image250) {
            this.image250 = image250;
        }

        public String getImage550() {
            return image550;
        }

        public void setImage550(String image550) {
            this.image550 = image550;
        }

        public String getImage900() {
            return image900;
        }

        public void setImage900(String image900) {
            this.image900 = image900;
        }

        public String getImageOriginal() {
            return imageOriginal;
        }

        public void setImageOriginal(String imageOriginal) {
            this.imageOriginal = imageOriginal;
        }

    }

    public class ProductData {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("Image_Information")
        @Expose
        private ImageInformation imageInformation;
        @SerializedName("ProductID")
        @Expose
        private String productID;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("Product_Brand")
        @Expose
        private String productBrand;
        @SerializedName("Product_Name")
        @Expose
        private String productName;
        @SerializedName("Product_Unit")
        @Expose
        private String productUnit;
        @SerializedName("Product_Description")
        @Expose
        private String productDescription;
        @SerializedName("Product_Type")
        @Expose
        private Integer productType;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public ImageInformation getImageInformation() {
            return imageInformation;
        }

        public void setImageInformation(ImageInformation imageInformation) {
            this.imageInformation = imageInformation;
        }

        public String getProductID() {
            return productID;
        }

        public void setProductID(String productID) {
            this.productID = productID;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public String getProductBrand() {
            return productBrand;
        }

        public void setProductBrand(String productBrand) {
            this.productBrand = productBrand;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductUnit() {
            return productUnit;
        }

        public void setProductUnit(String productUnit) {
            this.productUnit = productUnit;
        }

        public String getProductDescription() {
            return productDescription;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }

        public Integer getProductType() {
            return productType;
        }

        public void setProductType(Integer productType) {
            this.productType = productType;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }
}
