package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FetchOrderCompleteResponse implements Serializable {

    public class CartInformation {

        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("ProductID")
        @Expose
        private String productID;
        @SerializedName("LocalityProductID")
        @Expose
        private String localityProductID;
        @SerializedName("MRP")
        @Expose
        private Double mRP;
        @SerializedName("Selling_Price")
        @Expose
        private Double sellingPrice;
        @SerializedName("Quantity")
        @Expose
        private Integer quantity;
        @SerializedName("Sub_Total_Amount")
        @Expose
        private Double subTotalAmount;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("LocalityProductData")
        @Expose
        private LocalityProductData localityProductData;

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public String getProductID() {
            return productID;
        }

        public void setProductID(String productID) {
            this.productID = productID;
        }

        public String getLocalityProductID() {
            return localityProductID;
        }

        public void setLocalityProductID(String localityProductID) {
            this.localityProductID = localityProductID;
        }

        public Double getMRP() {
            return mRP;
        }

        public void setMRP(Double mRP) {
            this.mRP = mRP;
        }

        public Double getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(Double sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public Double getSubTotalAmount() {
            return subTotalAmount;
        }

        public void setSubTotalAmount(Double subTotalAmount) {
            this.subTotalAmount = subTotalAmount;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public LocalityProductData getLocalityProductData() {
            return localityProductData;
        }

        public void setLocalityProductData(LocalityProductData localityProductData) {
            this.localityProductData = localityProductData;
        }

    }

    public class CouponAppliedDetails {

        @SerializedName("Pricing_Information")
        @Expose
        private PricingInformation pricingInformation;
        @SerializedName("AppliedCouponID")
        @Expose
        private String appliedCouponID;
        @SerializedName("PriceQuoteID")
        @Expose
        private String priceQuoteID;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("AddressID")
        @Expose
        private String addressID;
        @SerializedName("SLOTID")
        @Expose
        private String sLOTID;
        @SerializedName("Selected_Date_Time")
        @Expose
        private String selectedDateTime;
        @SerializedName("CouponID")
        @Expose
        private String couponID;
        @SerializedName("Coupon_Code")
        @Expose
        private String couponCode;
        @SerializedName("Discount_Percentage")
        @Expose
        private Double discountPercentage;
        @SerializedName("Whether_Order_Placed")
        @Expose
        private Boolean whetherOrderPlaced;
        @SerializedName("Order_Time")
        @Expose
        private Object orderTime;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public PricingInformation getPricingInformation() {
            return pricingInformation;
        }

        public void setPricingInformation(PricingInformation pricingInformation) {
            this.pricingInformation = pricingInformation;
        }

        public String getAppliedCouponID() {
            return appliedCouponID;
        }

        public void setAppliedCouponID(String appliedCouponID) {
            this.appliedCouponID = appliedCouponID;
        }

        public String getPriceQuoteID() {
            return priceQuoteID;
        }

        public void setPriceQuoteID(String priceQuoteID) {
            this.priceQuoteID = priceQuoteID;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getAddressID() {
            return addressID;
        }

        public void setAddressID(String addressID) {
            this.addressID = addressID;
        }

        public String getSLOTID() {
            return sLOTID;
        }

        public void setSLOTID(String sLOTID) {
            this.sLOTID = sLOTID;
        }

        public String getSelectedDateTime() {
            return selectedDateTime;
        }

        public void setSelectedDateTime(String selectedDateTime) {
            this.selectedDateTime = selectedDateTime;
        }

        public String getCouponID() {
            return couponID;
        }

        public void setCouponID(String couponID) {
            this.couponID = couponID;
        }

        public String getCouponCode() {
            return couponCode;
        }

        public void setCouponCode(String couponCode) {
            this.couponCode = couponCode;
        }

        public Double getDiscountPercentage() {
            return discountPercentage;
        }

        public void setDiscountPercentage(Double discountPercentage) {
            this.discountPercentage = discountPercentage;
        }

        public Boolean getWhetherOrderPlaced() {
            return whetherOrderPlaced;
        }

        public void setWhetherOrderPlaced(Boolean whetherOrderPlaced) {
            this.whetherOrderPlaced = whetherOrderPlaced;
        }

        public Object getOrderTime() {
            return orderTime;
        }

        public void setOrderTime(Object orderTime) {
            this.orderTime = orderTime;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }

    public class CouponData {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("Image_Information")
        @Expose
        private ImageInformation imageInformation;
        @SerializedName("CouponID")
        @Expose
        private String couponID;
        @SerializedName("Coupon_Code")
        @Expose
        private String couponCode;
        @SerializedName("Description")
        @Expose
        private String description;
        @SerializedName("Terms_and_Condition")
        @Expose
        private String termsAndCondition;
        @SerializedName("Start_Date_Time")
        @Expose
        private String startDateTime;
        @SerializedName("End_Date_Time")
        @Expose
        private String endDateTime;
        @SerializedName("Discount_Percentage")
        @Expose
        private Double discountPercentage;
        @SerializedName("Minimum_Billing_Amount")
        @Expose
        private Double minimumBillingAmount;
        @SerializedName("MAX_User_Applicable_Orders")
        @Expose
        private Integer mAXUserApplicableOrders;
        @SerializedName("MAX_Discount_Amount")
        @Expose
        private Double mAXDiscountAmount;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public ImageInformation getImageInformation() {
            return imageInformation;
        }

        public void setImageInformation(ImageInformation imageInformation) {
            this.imageInformation = imageInformation;
        }

        public String getCouponID() {
            return couponID;
        }

        public void setCouponID(String couponID) {
            this.couponID = couponID;
        }

        public String getCouponCode() {
            return couponCode;
        }

        public void setCouponCode(String couponCode) {
            this.couponCode = couponCode;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTermsAndCondition() {
            return termsAndCondition;
        }

        public void setTermsAndCondition(String termsAndCondition) {
            this.termsAndCondition = termsAndCondition;
        }

        public String getStartDateTime() {
            return startDateTime;
        }

        public void setStartDateTime(String startDateTime) {
            this.startDateTime = startDateTime;
        }

        public String getEndDateTime() {
            return endDateTime;
        }

        public void setEndDateTime(String endDateTime) {
            this.endDateTime = endDateTime;
        }

        public Double getDiscountPercentage() {
            return discountPercentage;
        }

        public void setDiscountPercentage(Double discountPercentage) {
            this.discountPercentage = discountPercentage;
        }

        public Double getMinimumBillingAmount() {
            return minimumBillingAmount;
        }

        public void setMinimumBillingAmount(Double minimumBillingAmount) {
            this.minimumBillingAmount = minimumBillingAmount;
        }

        public Integer getMAXUserApplicableOrders() {
            return mAXUserApplicableOrders;
        }

        public void setMAXUserApplicableOrders(Integer mAXUserApplicableOrders) {
            this.mAXUserApplicableOrders = mAXUserApplicableOrders;
        }

        public Double getMAXDiscountAmount() {
            return mAXDiscountAmount;
        }

        public void setMAXDiscountAmount(Double mAXDiscountAmount) {
            this.mAXDiscountAmount = mAXDiscountAmount;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }

    public class CouponInformation {

        @SerializedName("AppliedCouponID")
        @Expose
        private String appliedCouponID;
        @SerializedName("CouponID")
        @Expose
        private String couponID;

        public String getAppliedCouponID() {
            return appliedCouponID;
        }

        public void setAppliedCouponID(String appliedCouponID) {
            this.appliedCouponID = appliedCouponID;
        }

        public String getCouponID() {
            return couponID;
        }

        public void setCouponID(String couponID) {
            this.couponID = couponID;
        }

    }

    public class CurrentLocation {

        @SerializedName("Latitude")
        @Expose
        private Double latitude;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

    }

    public class Data {

        @SerializedName("Delivery_Information")
        @Expose
        private DeliveryInformation deliveryInformation;
        @SerializedName("Device_Information")
        @Expose
        private DeviceInformation deviceInformation;
        @SerializedName("Coupon_Information")
        @Expose
        private CouponInformation couponInformation;
        @SerializedName("Pricing_Information")
        @Expose
        private PricingInformation pricingInformation;
        @SerializedName("OrderID")
        @Expose
        private String orderID;
        @SerializedName("Order_Number")
        @Expose
        private String orderNumber;
        @SerializedName("PriceQuoteID")
        @Expose
        private String priceQuoteID;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("AddressID")
        @Expose
        private String addressID;
        @SerializedName("SLOTID")
        @Expose
        private String sLOTID;
        @SerializedName("Selected_Date_Time")
        @Expose
        private String selectedDateTime;
        @SerializedName("Payment_Mode")
        @Expose
        private Integer paymentMode;
        @SerializedName("Whether_Coupon_Applied")
        @Expose
        private Boolean whetherCouponApplied;
        @SerializedName("Order_Status")
        @Expose
        private Integer orderStatus;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("Delivery_Point")
        @Expose
        private List<Double> deliveryPoint = null;
        @SerializedName("Cart_Information")
        @Expose
        private List<CartInformation> cartInformation = null;
        @SerializedName("DriverID")
        @Expose
        private String driverID;
        @SerializedName("Order_Status_Logs")
        @Expose
        private List<OrderStatusLog> orderStatusLogs = null;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("Receiver_Signature_Image_Data")
        @Expose
        private ReceiverSignatureImageData receiverSignatureImageData;
        @SerializedName("Whether_Receiver_Signature_Image_Available")
        @Expose
        private Boolean whetherReceiverSignatureImageAvailable;
        @SerializedName("Not_Available_Items_Cart_Information")
        @Expose
        private List<NotAvailableItemsCartInformation> notAvailableItemsCartInformation = null;
        @SerializedName("Total_Not_Available_Cart_Amount")
        @Expose
        private Double totalNotAvailableCartAmount;
        @SerializedName("Whether_Some_Not_Available_Items")
        @Expose
        private Boolean whetherSomeNotAvailableItems;
        @SerializedName("Delivery_Date_Time")
        @Expose
        private String deliveryDateTime;
        @SerializedName("CouponData")
        @Expose
        private CouponData couponData;
        @SerializedName("Coupon_Applied_Details")
        @Expose
        private CouponAppliedDetails couponAppliedDetails;
        @SerializedName("UserData")
        @Expose
        private UserData userData;
        @SerializedName("Whether_Delivery_Driver")
        @Expose
        private Boolean whetherDeliveryDriver;
        @SerializedName("DriverData")
        @Expose
        private DriverData driverData;

        public DeliveryInformation getDeliveryInformation() {
            return deliveryInformation;
        }

        public void setDeliveryInformation(DeliveryInformation deliveryInformation) {
            this.deliveryInformation = deliveryInformation;
        }

        public DeviceInformation getDeviceInformation() {
            return deviceInformation;
        }

        public void setDeviceInformation(DeviceInformation deviceInformation) {
            this.deviceInformation = deviceInformation;
        }

        public CouponInformation getCouponInformation() {
            return couponInformation;
        }

        public void setCouponInformation(CouponInformation couponInformation) {
            this.couponInformation = couponInformation;
        }

        public PricingInformation getPricingInformation() {
            return pricingInformation;
        }

        public void setPricingInformation(PricingInformation pricingInformation) {
            this.pricingInformation = pricingInformation;
        }

        public String getOrderID() {
            return orderID;
        }

        public void setOrderID(String orderID) {
            this.orderID = orderID;
        }

        public String getOrderNumber() {
            return orderNumber;
        }

        public void setOrderNumber(String orderNumber) {
            this.orderNumber = orderNumber;
        }

        public String getPriceQuoteID() {
            return priceQuoteID;
        }

        public void setPriceQuoteID(String priceQuoteID) {
            this.priceQuoteID = priceQuoteID;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getAddressID() {
            return addressID;
        }

        public void setAddressID(String addressID) {
            this.addressID = addressID;
        }

        public String getSLOTID() {
            return sLOTID;
        }

        public void setSLOTID(String sLOTID) {
            this.sLOTID = sLOTID;
        }

        public String getSelectedDateTime() {
            return selectedDateTime;
        }

        public void setSelectedDateTime(String selectedDateTime) {
            this.selectedDateTime = selectedDateTime;
        }

        public Integer getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(Integer paymentMode) {
            this.paymentMode = paymentMode;
        }

        public Boolean getWhetherCouponApplied() {
            return whetherCouponApplied;
        }

        public void setWhetherCouponApplied(Boolean whetherCouponApplied) {
            this.whetherCouponApplied = whetherCouponApplied;
        }

        public Integer getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(Integer orderStatus) {
            this.orderStatus = orderStatus;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public List<Double> getDeliveryPoint() {
            return deliveryPoint;
        }

        public void setDeliveryPoint(List<Double> deliveryPoint) {
            this.deliveryPoint = deliveryPoint;
        }

        public List<CartInformation> getCartInformation() {
            return cartInformation;
        }

        public void setCartInformation(List<CartInformation> cartInformation) {
            this.cartInformation = cartInformation;
        }

        public String getDriverID() {
            return driverID;
        }

        public void setDriverID(String driverID) {
            this.driverID = driverID;
        }

        public List<OrderStatusLog> getOrderStatusLogs() {
            return orderStatusLogs;
        }

        public void setOrderStatusLogs(List<OrderStatusLog> orderStatusLogs) {
            this.orderStatusLogs = orderStatusLogs;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public ReceiverSignatureImageData getReceiverSignatureImageData() {
            return receiverSignatureImageData;
        }

        public void setReceiverSignatureImageData(ReceiverSignatureImageData receiverSignatureImageData) {
            this.receiverSignatureImageData = receiverSignatureImageData;
        }

        public Boolean getWhetherReceiverSignatureImageAvailable() {
            return whetherReceiverSignatureImageAvailable;
        }

        public void setWhetherReceiverSignatureImageAvailable(Boolean whetherReceiverSignatureImageAvailable) {
            this.whetherReceiverSignatureImageAvailable = whetherReceiverSignatureImageAvailable;
        }

        public List<NotAvailableItemsCartInformation> getNotAvailableItemsCartInformation() {
            return notAvailableItemsCartInformation;
        }

        public void setNotAvailableItemsCartInformation(List<NotAvailableItemsCartInformation> notAvailableItemsCartInformation) {
            this.notAvailableItemsCartInformation = notAvailableItemsCartInformation;
        }

        public Double getTotalNotAvailableCartAmount() {
            return totalNotAvailableCartAmount;
        }

        public void setTotalNotAvailableCartAmount(Double totalNotAvailableCartAmount) {
            this.totalNotAvailableCartAmount = totalNotAvailableCartAmount;
        }

        public Boolean getWhetherSomeNotAvailableItems() {
            return whetherSomeNotAvailableItems;
        }

        public void setWhetherSomeNotAvailableItems(Boolean whetherSomeNotAvailableItems) {
            this.whetherSomeNotAvailableItems = whetherSomeNotAvailableItems;
        }

        public String getDeliveryDateTime() {
            return deliveryDateTime;
        }

        public void setDeliveryDateTime(String deliveryDateTime) {
            this.deliveryDateTime = deliveryDateTime;
        }

        public CouponData getCouponData() {
            return couponData;
        }

        public void setCouponData(CouponData couponData) {
            this.couponData = couponData;
        }

        public CouponAppliedDetails getCouponAppliedDetails() {
            return couponAppliedDetails;
        }

        public void setCouponAppliedDetails(CouponAppliedDetails couponAppliedDetails) {
            this.couponAppliedDetails = couponAppliedDetails;
        }

        public UserData getUserData() {
            return userData;
        }

        public void setUserData(UserData userData) {
            this.userData = userData;
        }

        public Boolean getWhetherDeliveryDriver() {
            return whetherDeliveryDriver;
        }

        public void setWhetherDeliveryDriver(Boolean whetherDeliveryDriver) {
            this.whetherDeliveryDriver = whetherDeliveryDriver;
        }

        public DriverData getDriverData() {
            return driverData;
        }

        public void setDriverData(DriverData driverData) {
            this.driverData = driverData;
        }

    }

    public class DeliveryInformation {

        @SerializedName("AddressID")
        @Expose
        private String addressID;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("PhoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("AddressType")
        @Expose
        private Integer addressType;
        @SerializedName("Address")
        @Expose
        private String address;
        @SerializedName("Latitude")
        @Expose
        private Double latitude;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;

        public String getAddressID() {
            return addressID;
        }

        public void setAddressID(String addressID) {
            this.addressID = addressID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public Integer getAddressType() {
            return addressType;
        }

        public void setAddressType(Integer addressType) {
            this.addressType = addressType;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

    }

    public class DeviceInformation {

        @SerializedName("DeviceID")
        @Expose
        private String deviceID;
        @SerializedName("DeviceType")
        @Expose
        private Integer deviceType;
        @SerializedName("DeviceName")
        @Expose
        private String deviceName;
        @SerializedName("AppVersion")
        @Expose
        private Integer appVersion;
        @SerializedName("IPAddress")
        @Expose
        private String iPAddress;

        public String getDeviceID() {
            return deviceID;
        }

        public void setDeviceID(String deviceID) {
            this.deviceID = deviceID;
        }

        public Integer getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(Integer deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceName() {
            return deviceName;
        }

        public void setDeviceName(String deviceName) {
            this.deviceName = deviceName;
        }

        public Integer getAppVersion() {
            return appVersion;
        }

        public void setAppVersion(Integer appVersion) {
            this.appVersion = appVersion;
        }

        public String getIPAddress() {
            return iPAddress;
        }

        public void setIPAddress(String iPAddress) {
            this.iPAddress = iPAddress;
        }

    }

    public class DriverData {

        @SerializedName("Image_Information")
        @Expose
        private ImageInformation imageInformation;
        @SerializedName("Current_Location")
        @Expose
        private CurrentLocation currentLocation;
        @SerializedName("DriverID")
        @Expose
        private String driverID;
        @SerializedName("Driver_QR")
        @Expose
        private String driverQR;
        @SerializedName("SessionID")
        @Expose
        private String sessionID;
        @SerializedName("DeviceID")
        @Expose
        private String deviceID;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("Localities_Array")
        @Expose
        private List<String> localitiesArray = null;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("EmailID")
        @Expose
        private String emailID;
        @SerializedName("PhoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("Vehicle_Type")
        @Expose
        private Integer vehicleType;
        @SerializedName("Vehicle_Title")
        @Expose
        private String vehicleTitle;
        @SerializedName("Vehicle_Number")
        @Expose
        private String vehicleNumber;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("PasswordHash")
        @Expose
        private String passwordHash;
        @SerializedName("PasswordSalt")
        @Expose
        private String passwordSalt;
        @SerializedName("Whether_First_Login")
        @Expose
        private Boolean whetherFirstLogin;
        @SerializedName("First_DeviceID")
        @Expose
        private String firstDeviceID;
        @SerializedName("First_Time")
        @Expose
        private Object firstTime;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("Document_Information")
        @Expose
        private List<Object> documentInformation = null;
        @SerializedName("Point")
        @Expose
        private List<Double> point = null;
        @SerializedName("City_Title")
        @Expose
        private String cityTitle;

        public ImageInformation getImageInformation() {
            return imageInformation;
        }

        public void setImageInformation(ImageInformation imageInformation) {
            this.imageInformation = imageInformation;
        }

        public CurrentLocation getCurrentLocation() {
            return currentLocation;
        }

        public void setCurrentLocation(CurrentLocation currentLocation) {
            this.currentLocation = currentLocation;
        }

        public String getDriverID() {
            return driverID;
        }

        public void setDriverID(String driverID) {
            this.driverID = driverID;
        }

        public String getDriverQR() {
            return driverQR;
        }

        public void setDriverQR(String driverQR) {
            this.driverQR = driverQR;
        }

        public String getSessionID() {
            return sessionID;
        }

        public void setSessionID(String sessionID) {
            this.sessionID = sessionID;
        }

        public String getDeviceID() {
            return deviceID;
        }

        public void setDeviceID(String deviceID) {
            this.deviceID = deviceID;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public List<String> getLocalitiesArray() {
            return localitiesArray;
        }

        public void setLocalitiesArray(List<String> localitiesArray) {
            this.localitiesArray = localitiesArray;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmailID() {
            return emailID;
        }

        public void setEmailID(String emailID) {
            this.emailID = emailID;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public Integer getVehicleType() {
            return vehicleType;
        }

        public void setVehicleType(Integer vehicleType) {
            this.vehicleType = vehicleType;
        }

        public String getVehicleTitle() {
            return vehicleTitle;
        }

        public void setVehicleTitle(String vehicleTitle) {
            this.vehicleTitle = vehicleTitle;
        }

        public String getVehicleNumber() {
            return vehicleNumber;
        }

        public void setVehicleNumber(String vehicleNumber) {
            this.vehicleNumber = vehicleNumber;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public String getPasswordHash() {
            return passwordHash;
        }

        public void setPasswordHash(String passwordHash) {
            this.passwordHash = passwordHash;
        }

        public String getPasswordSalt() {
            return passwordSalt;
        }

        public void setPasswordSalt(String passwordSalt) {
            this.passwordSalt = passwordSalt;
        }

        public Boolean getWhetherFirstLogin() {
            return whetherFirstLogin;
        }

        public void setWhetherFirstLogin(Boolean whetherFirstLogin) {
            this.whetherFirstLogin = whetherFirstLogin;
        }

        public String getFirstDeviceID() {
            return firstDeviceID;
        }

        public void setFirstDeviceID(String firstDeviceID) {
            this.firstDeviceID = firstDeviceID;
        }

        public Object getFirstTime() {
            return firstTime;
        }

        public void setFirstTime(Object firstTime) {
            this.firstTime = firstTime;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public List<Object> getDocumentInformation() {
            return documentInformation;
        }

        public void setDocumentInformation(List<Object> documentInformation) {
            this.documentInformation = documentInformation;
        }

        public List<Double> getPoint() {
            return point;
        }

        public void setPoint(List<Double> point) {
            this.point = point;
        }

        public String getCityTitle() {
            return cityTitle;
        }

        public void setCityTitle(String cityTitle) {
            this.cityTitle = cityTitle;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Data")
        @Expose
        private Data data;

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }

    public class ImageData {

        @SerializedName("ImageID")
        @Expose
        private String imageID;
        @SerializedName("Image50")
        @Expose
        private String image50;
        @SerializedName("Image100")
        @Expose
        private String image100;
        @SerializedName("Image250")
        @Expose
        private String image250;
        @SerializedName("Image550")
        @Expose
        private String image550;
        @SerializedName("Image900")
        @Expose
        private String image900;
        @SerializedName("ImageOriginal")
        @Expose
        private String imageOriginal;

        public String getImageID() {
            return imageID;
        }

        public void setImageID(String imageID) {
            this.imageID = imageID;
        }

        public String getImage50() {
            return image50;
        }

        public void setImage50(String image50) {
            this.image50 = image50;
        }

        public String getImage100() {
            return image100;
        }

        public void setImage100(String image100) {
            this.image100 = image100;
        }

        public String getImage250() {
            return image250;
        }

        public void setImage250(String image250) {
            this.image250 = image250;
        }

        public String getImage550() {
            return image550;
        }

        public void setImage550(String image550) {
            this.image550 = image550;
        }

        public String getImage900() {
            return image900;
        }

        public void setImage900(String image900) {
            this.image900 = image900;
        }

        public String getImageOriginal() {
            return imageOriginal;
        }

        public void setImageOriginal(String imageOriginal) {
            this.imageOriginal = imageOriginal;
        }

    }

    public class ImageInformation {

        @SerializedName("ImageID")
        @Expose
        private String imageID;
        @SerializedName("Image50")
        @Expose
        private String image50;
        @SerializedName("Image100")
        @Expose
        private String image100;
        @SerializedName("Image250")
        @Expose
        private String image250;
        @SerializedName("Image550")
        @Expose
        private String image550;
        @SerializedName("Image900")
        @Expose
        private String image900;
        @SerializedName("ImageOriginal")
        @Expose
        private String imageOriginal;

        public String getImageID() {
            return imageID;
        }

        public void setImageID(String imageID) {
            this.imageID = imageID;
        }

        public String getImage50() {
            return image50;
        }

        public void setImage50(String image50) {
            this.image50 = image50;
        }

        public String getImage100() {
            return image100;
        }

        public void setImage100(String image100) {
            this.image100 = image100;
        }

        public String getImage250() {
            return image250;
        }

        public void setImage250(String image250) {
            this.image250 = image250;
        }

        public String getImage550() {
            return image550;
        }

        public void setImage550(String image550) {
            this.image550 = image550;
        }

        public String getImage900() {
            return image900;
        }

        public void setImage900(String image900) {
            this.image900 = image900;
        }

        public String getImageOriginal() {
            return imageOriginal;
        }

        public void setImageOriginal(String imageOriginal) {
            this.imageOriginal = imageOriginal;
        }

    }

    public class LocalityProductData {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("ProductID")
        @Expose
        private String productID;
        @SerializedName("Available_Stock")
        @Expose
        private Integer availableStock;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("LocalityProductID")
        @Expose
        private String localityProductID;
        @SerializedName("MRP")
        @Expose
        private Double mRP;
        @SerializedName("Selling_Price")
        @Expose
        private Double sellingPrice;
        @SerializedName("Sold_Stock")
        @Expose
        private Integer soldStock;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("Total_Stock")
        @Expose
        private Integer totalStock;
        @SerializedName("Whether_Inventory")
        @Expose
        private Boolean whetherInventory;
        @SerializedName("Whether_Subscription")
        @Expose
        private Boolean whetherSubscription;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("Image_Information")
        @Expose
        private ImageInformation imageInformation;
        @SerializedName("Product_Brand")
        @Expose
        private String productBrand;
        @SerializedName("Product_Name")
        @Expose
        private String productName;
        @SerializedName("Product_Unit")
        @Expose
        private String productUnit;
        @SerializedName("Product_Description")
        @Expose
        private String productDescription;
        @SerializedName("Product_Type")
        @Expose
        private Integer productType;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("Category_Title")
        @Expose
        private String categoryTitle;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getProductID() {
            return productID;
        }

        public void setProductID(String productID) {
            this.productID = productID;
        }

        public Integer getAvailableStock() {
            return availableStock;
        }

        public void setAvailableStock(Integer availableStock) {
            this.availableStock = availableStock;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public String getLocalityProductID() {
            return localityProductID;
        }

        public void setLocalityProductID(String localityProductID) {
            this.localityProductID = localityProductID;
        }

        public Double getMRP() {
            return mRP;
        }

        public void setMRP(Double mRP) {
            this.mRP = mRP;
        }

        public Double getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(Double sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public Integer getSoldStock() {
            return soldStock;
        }

        public void setSoldStock(Integer soldStock) {
            this.soldStock = soldStock;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public Integer getTotalStock() {
            return totalStock;
        }

        public void setTotalStock(Integer totalStock) {
            this.totalStock = totalStock;
        }

        public Boolean getWhetherInventory() {
            return whetherInventory;
        }

        public void setWhetherInventory(Boolean whetherInventory) {
            this.whetherInventory = whetherInventory;
        }

        public Boolean getWhetherSubscription() {
            return whetherSubscription;
        }

        public void setWhetherSubscription(Boolean whetherSubscription) {
            this.whetherSubscription = whetherSubscription;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public ImageInformation getImageInformation() {
            return imageInformation;
        }

        public void setImageInformation(ImageInformation imageInformation) {
            this.imageInformation = imageInformation;
        }

        public String getProductBrand() {
            return productBrand;
        }

        public void setProductBrand(String productBrand) {
            this.productBrand = productBrand;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductUnit() {
            return productUnit;
        }

        public void setProductUnit(String productUnit) {
            this.productUnit = productUnit;
        }

        public String getProductDescription() {
            return productDescription;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }

        public Integer getProductType() {
            return productType;
        }

        public void setProductType(Integer productType) {
            this.productType = productType;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public String getCategoryTitle() {
            return categoryTitle;
        }

        public void setCategoryTitle(String categoryTitle) {
            this.categoryTitle = categoryTitle;
        }

    }

    public class NotAvailableItemsCartInformation {

        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("ProductID")
        @Expose
        private String productID;
        @SerializedName("LocalityProductID")
        @Expose
        private String localityProductID;
        @SerializedName("MRP")
        @Expose
        private Double mRP;
        @SerializedName("Selling_Price")
        @Expose
        private Double sellingPrice;
        @SerializedName("Quantity")
        @Expose
        private Integer quantity;
        @SerializedName("Sub_Total_Amount")
        @Expose
        private Double subTotalAmount;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("LocalityProductData")
        @Expose
        private LocalityProductData localityProductData;

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public String getProductID() {
            return productID;
        }

        public void setProductID(String productID) {
            this.productID = productID;
        }

        public String getLocalityProductID() {
            return localityProductID;
        }

        public void setLocalityProductID(String localityProductID) {
            this.localityProductID = localityProductID;
        }

        public Double getMRP() {
            return mRP;
        }

        public void setMRP(Double mRP) {
            this.mRP = mRP;
        }

        public Double getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(Double sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public Double getSubTotalAmount() {
            return subTotalAmount;
        }

        public void setSubTotalAmount(Double subTotalAmount) {
            this.subTotalAmount = subTotalAmount;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public LocalityProductData getLocalityProductData() {
            return localityProductData;
        }

        public void setLocalityProductData(LocalityProductData localityProductData) {
            this.localityProductData = localityProductData;
        }

    }

    public class OrderStatusLog {

        @SerializedName("LogID")
        @Expose
        private String logID;
        @SerializedName("DriverID")
        @Expose
        private String driverID;
        @SerializedName("Order_Status")
        @Expose
        private Integer orderStatus;
        @SerializedName("Comment")
        @Expose
        private String comment;
        @SerializedName("Time")
        @Expose
        private String time;
        @SerializedName("Whether_Delivery_Driver")
        @Expose
        private Boolean whetherDeliveryDriver;
        @SerializedName("DriverData")
        @Expose
        private DriverData driverData;

        public String getLogID() {
            return logID;
        }

        public void setLogID(String logID) {
            this.logID = logID;
        }

        public String getDriverID() {
            return driverID;
        }

        public void setDriverID(String driverID) {
            this.driverID = driverID;
        }

        public Integer getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(Integer orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public Boolean getWhetherDeliveryDriver() {
            return whetherDeliveryDriver;
        }

        public void setWhetherDeliveryDriver(Boolean whetherDeliveryDriver) {
            this.whetherDeliveryDriver = whetherDeliveryDriver;
        }

        public DriverData getDriverData() {
            return driverData;
        }

        public void setDriverData(DriverData driverData) {
            this.driverData = driverData;
        }

    }

    public class PricingInformation {

        @SerializedName("Total_Cart_Amount")
        @Expose
        private Double totalCartAmount;
        @SerializedName("Delivery_Price")
        @Expose
        private Double deliveryPrice;
        @SerializedName("Whether_Offer_Coupon_Applied")
        @Expose
        private Boolean whetherOfferCouponApplied;
        @SerializedName("Discount_Percentage")
        @Expose
        private Double discountPercentage;
        @SerializedName("Discount_Amount")
        @Expose
        private Double discountAmount;
        @SerializedName("Final_Transaction_Amount")
        @Expose
        private Double finalTransactionAmount;

        public Double getTotalCartAmount() {
            return totalCartAmount;
        }

        public void setTotalCartAmount(Double totalCartAmount) {
            this.totalCartAmount = totalCartAmount;
        }

        public Double getDeliveryPrice() {
            return deliveryPrice;
        }

        public void setDeliveryPrice(Double deliveryPrice) {
            this.deliveryPrice = deliveryPrice;
        }

        public Boolean getWhetherOfferCouponApplied() {
            return whetherOfferCouponApplied;
        }

        public void setWhetherOfferCouponApplied(Boolean whetherOfferCouponApplied) {
            this.whetherOfferCouponApplied = whetherOfferCouponApplied;
        }

        public Double getDiscountPercentage() {
            return discountPercentage;
        }

        public void setDiscountPercentage(Double discountPercentage) {
            this.discountPercentage = discountPercentage;
        }

        public Double getDiscountAmount() {
            return discountAmount;
        }

        public void setDiscountAmount(Double discountAmount) {
            this.discountAmount = discountAmount;
        }

        public Double getFinalTransactionAmount() {
            return finalTransactionAmount;
        }

        public void setFinalTransactionAmount(Double finalTransactionAmount) {
            this.finalTransactionAmount = finalTransactionAmount;
        }

    }

    public class ReceiverSignatureImageData {

        @SerializedName("ImageID")
        @Expose
        private String imageID;
        @SerializedName("Image50")
        @Expose
        private String image50;
        @SerializedName("Image100")
        @Expose
        private String image100;
        @SerializedName("Image250")
        @Expose
        private String image250;
        @SerializedName("Image550")
        @Expose
        private String image550;
        @SerializedName("Image900")
        @Expose
        private String image900;
        @SerializedName("ImageOriginal")
        @Expose
        private String imageOriginal;

        public String getImageID() {
            return imageID;
        }

        public void setImageID(String imageID) {
            this.imageID = imageID;
        }

        public String getImage50() {
            return image50;
        }

        public void setImage50(String image50) {
            this.image50 = image50;
        }

        public String getImage100() {
            return image100;
        }

        public void setImage100(String image100) {
            this.image100 = image100;
        }

        public String getImage250() {
            return image250;
        }

        public void setImage250(String image250) {
            this.image250 = image250;
        }

        public String getImage550() {
            return image550;
        }

        public void setImage550(String image550) {
            this.image550 = image550;
        }

        public String getImage900() {
            return image900;
        }

        public void setImage900(String image900) {
            this.image900 = image900;
        }

        public String getImageOriginal() {
            return imageOriginal;
        }

        public void setImageOriginal(String imageOriginal) {
            this.imageOriginal = imageOriginal;
        }

    }

    public class UserData {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("PhoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("DeviceID")
        @Expose
        private String deviceID;
        @SerializedName("EmailID")
        @Expose
        private String emailID;
        @SerializedName("Image_Data")
        @Expose
        private ImageData imageData;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("SessionID")
        @Expose
        private String sessionID;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("Wallet_Information")
        @Expose
        private WalletInformation walletInformation;
        @SerializedName("Whether_Basic_Information_Available")
        @Expose
        private Boolean whetherBasicInformationAvailable;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getDeviceID() {
            return deviceID;
        }

        public void setDeviceID(String deviceID) {
            this.deviceID = deviceID;
        }

        public String getEmailID() {
            return emailID;
        }

        public void setEmailID(String emailID) {
            this.emailID = emailID;
        }

        public ImageData getImageData() {
            return imageData;
        }

        public void setImageData(ImageData imageData) {
            this.imageData = imageData;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSessionID() {
            return sessionID;
        }

        public void setSessionID(String sessionID) {
            this.sessionID = sessionID;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public WalletInformation getWalletInformation() {
            return walletInformation;
        }

        public void setWalletInformation(WalletInformation walletInformation) {
            this.walletInformation = walletInformation;
        }

        public Boolean getWhetherBasicInformationAvailable() {
            return whetherBasicInformationAvailable;
        }

        public void setWhetherBasicInformationAvailable(Boolean whetherBasicInformationAvailable) {
            this.whetherBasicInformationAvailable = whetherBasicInformationAvailable;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public class WalletInformation {

        @SerializedName("Available_Amount")
        @Expose
        private Double availableAmount;
        @SerializedName("Credited_Amount")
        @Expose
        private Double creditedAmount;
        @SerializedName("Debited_Amount")
        @Expose
        private Double debitedAmount;

        public Double getAvailableAmount() {
            return availableAmount;
        }

        public void setAvailableAmount(Double availableAmount) {
            this.availableAmount = availableAmount;
        }

        public Double getCreditedAmount() {
            return creditedAmount;
        }

        public void setCreditedAmount(Double creditedAmount) {
            this.creditedAmount = creditedAmount;
        }

        public Double getDebitedAmount() {
            return debitedAmount;
        }

        public void setDebitedAmount(Double debitedAmount) {
            this.debitedAmount = debitedAmount;
        }

    }
}
