package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ReserveResponse implements Serializable {

    public class Data {

        @SerializedName("PhoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("DeviceID")
        @Expose
        private String deviceID;
        @SerializedName("EmailID")
        @Expose
        private String emailID;
        @SerializedName("Image_Data")
        @Expose
        private ImageData imageData;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("SessionID")
        @Expose
        private String sessionID;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("Wallet_Information")
        @Expose
        private WalletInformation walletInformation;
        @SerializedName("Whether_Basic_Information_Available")
        @Expose
        private Boolean whetherBasicInformationAvailable;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getDeviceID() {
            return deviceID;
        }

        public void setDeviceID(String deviceID) {
            this.deviceID = deviceID;
        }

        public String getEmailID() {
            return emailID;
        }

        public void setEmailID(String emailID) {
            this.emailID = emailID;
        }

        public ImageData getImageData() {
            return imageData;
        }

        public void setImageData(ImageData imageData) {
            this.imageData = imageData;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSessionID() {
            return sessionID;
        }

        public void setSessionID(String sessionID) {
            this.sessionID = sessionID;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public WalletInformation getWalletInformation() {
            return walletInformation;
        }

        public void setWalletInformation(WalletInformation walletInformation) {
            this.walletInformation = walletInformation;
        }

        public Boolean getWhetherBasicInformationAvailable() {
            return whetherBasicInformationAvailable;
        }

        public void setWhetherBasicInformationAvailable(Boolean whetherBasicInformationAvailable) {
            this.whetherBasicInformationAvailable = whetherBasicInformationAvailable;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {
        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Data")
        @Expose
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }

    public class ImageData {

        @SerializedName("ImageID")
        @Expose
        private String imageID;
        @SerializedName("Image50")
        @Expose
        private String image50;
        @SerializedName("Image100")
        @Expose
        private String image100;
        @SerializedName("Image250")
        @Expose
        private String image250;
        @SerializedName("Image550")
        @Expose
        private String image550;
        @SerializedName("Image900")
        @Expose
        private String image900;
        @SerializedName("ImageOriginal")
        @Expose
        private String imageOriginal;

        public String getImageID() {
            return imageID;
        }

        public void setImageID(String imageID) {
            this.imageID = imageID;
        }

        public String getImage50() {
            return image50;
        }

        public void setImage50(String image50) {
            this.image50 = image50;
        }

        public String getImage100() {
            return image100;
        }

        public void setImage100(String image100) {
            this.image100 = image100;
        }

        public String getImage250() {
            return image250;
        }

        public void setImage250(String image250) {
            this.image250 = image250;
        }

        public String getImage550() {
            return image550;
        }

        public void setImage550(String image550) {
            this.image550 = image550;
        }

        public String getImage900() {
            return image900;
        }

        public void setImage900(String image900) {
            this.image900 = image900;
        }

        public String getImageOriginal() {
            return imageOriginal;
        }

        public void setImageOriginal(String imageOriginal) {
            this.imageOriginal = imageOriginal;
        }

    }

    public class WalletInformation {

        @SerializedName("Available_Amount")
        @Expose
        private Double availableAmount;
        @SerializedName("Credited_Amount")
        @Expose
        private Double creditedAmount;
        @SerializedName("Debited_Amount")
        @Expose
        private Double debitedAmount;
        @SerializedName("Reserved_Amount")
        @Expose
        private Double reservedAmount;

        public Double getAvailableAmount() {
            return availableAmount;
        }

        public void setAvailableAmount(Double availableAmount) {
            this.availableAmount = availableAmount;
        }

        public Double getCreditedAmount() {
            return creditedAmount;
        }

        public void setCreditedAmount(Double creditedAmount) {
            this.creditedAmount = creditedAmount;
        }

        public Double getDebitedAmount() {
            return debitedAmount;
        }

        public void setDebitedAmount(Double debitedAmount) {
            this.debitedAmount = debitedAmount;
        }

        public Double getReservedAmount() {
            return reservedAmount;
        }

        public void setReservedAmount(Double reservedAmount) {
            this.reservedAmount = reservedAmount;
        }

    }
}
