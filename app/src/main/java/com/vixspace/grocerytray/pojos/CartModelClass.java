package com.vixspace.grocerytray.pojos;

public class CartModelClass {

    String LocalityProductID;
    String ProductID;
    String ProductName;
    String Brand;
    String itemWeight;
    String ImageId;
    Integer Quantity;
    Double MRP,Selling_Price,Sub_Total_Amount;


    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getItemWeight() {
        return itemWeight;
    }

    public void setItemWeight(String itemWeight) {
        this.itemWeight = itemWeight;
    }


    public String getLocalityProductID() {
        return LocalityProductID;
    }

    public void setLocalityProductID(String localityProductID) {
        LocalityProductID = localityProductID;
    }

    public String getProductID() {
        return ProductID;
    }

    public String getImageId() {
        return ImageId;
    }

    public void setImageId(String imageId) {
        ImageId = imageId;
    }


    public void setProductID(String productID) {
        ProductID = productID;
    }

    public Integer getQuantity() {
        return Quantity;
    }

    public void setQuantity(Integer quantity) {
        Quantity = quantity;
    }

    public Double getMRP() {
        return MRP;
    }

    public void setMRP(Double MRP) {
        this.MRP = MRP;
    }

    public Double getSelling_Price() {
        return Selling_Price;
    }

    public void setSelling_Price(Double selling_Price) {
        Selling_Price = selling_Price;
    }

    public Double getSub_Total_Amount() {
        return Sub_Total_Amount;
    }

    public void setSub_Total_Amount(Double sub_Total_Amount) {
        Sub_Total_Amount = sub_Total_Amount;
    }

}
