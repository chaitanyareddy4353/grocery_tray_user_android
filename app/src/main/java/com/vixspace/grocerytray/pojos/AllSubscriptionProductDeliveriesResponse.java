package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AllSubscriptionProductDeliveriesResponse implements Serializable {

    public class AddressData {

        @SerializedName("PhoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("Address")
        @Expose
        private String address;
        @SerializedName("AddressID")
        @Expose
        private String addressID;
        @SerializedName("AddressType")
        @Expose
        private Integer addressType;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("Latitude")
        @Expose
        private Double latitude;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("Point")
        @Expose
        private List<Double> point = null;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAddressID() {
            return addressID;
        }

        public void setAddressID(String addressID) {
            this.addressID = addressID;
        }

        public Integer getAddressType() {
            return addressType;
        }

        public void setAddressType(Integer addressType) {
            this.addressType = addressType;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Double> getPoint() {
            return point;
        }

        public void setPoint(List<Double> point) {
            this.point = point;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public class CurrentLocation {

        @SerializedName("Latitude")
        @Expose
        private Double latitude;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

    }

    public class Datum {

        @SerializedName("Subscription_Price_Information")
        @Expose
        private SubscriptionPriceInformation subscriptionPriceInformation;
        @SerializedName("Delivery_Information")
        @Expose
        private DeliveryInformation deliveryInformation;
        @SerializedName("TaskID")
        @Expose
        private String taskID;
        @SerializedName("Task_Number")
        @Expose
        private String taskNumber;
        @SerializedName("DriverID")
        @Expose
        private String driverID;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("Task_Type")
        @Expose
        private Integer taskType;
        @SerializedName("SubscriptionID")
        @Expose
        private String subscriptionID;
        @SerializedName("OrderID")
        @Expose
        private String orderID;
        @SerializedName("Order_Number")
        @Expose
        private String orderNumber;
        @SerializedName("Selected_Date_Time")
        @Expose
        private String selectedDateTime;
        @SerializedName("Payment_Mode")
        @Expose
        private Integer paymentMode;
        @SerializedName("Task_Status")
        @Expose
        private Integer taskStatus;
        @SerializedName("Whether_To_Collect_Amount")
        @Expose
        private Boolean whetherToCollectAmount;
        @SerializedName("Collection_Amount")
        @Expose
        private Integer collectionAmount;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("Task_Status_Logs")
        @Expose
        private List<TaskStatusLog> taskStatusLogs = null;
        @SerializedName("Point")
        @Expose
        private List<Double> point = null;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("SubscriptionData")
        @Expose
        private SubscriptionData subscriptionData;
        @SerializedName("DriverData")
        @Expose
        private DriverData driverData;

        public SubscriptionPriceInformation getSubscriptionPriceInformation() {
            return subscriptionPriceInformation;
        }

        public void setSubscriptionPriceInformation(SubscriptionPriceInformation subscriptionPriceInformation) {
            this.subscriptionPriceInformation = subscriptionPriceInformation;
        }

        public DeliveryInformation getDeliveryInformation() {
            return deliveryInformation;
        }

        public void setDeliveryInformation(DeliveryInformation deliveryInformation) {
            this.deliveryInformation = deliveryInformation;
        }

        public String getTaskID() {
            return taskID;
        }

        public void setTaskID(String taskID) {
            this.taskID = taskID;
        }

        public String getTaskNumber() {
            return taskNumber;
        }

        public void setTaskNumber(String taskNumber) {
            this.taskNumber = taskNumber;
        }

        public String getDriverID() {
            return driverID;
        }

        public void setDriverID(String driverID) {
            this.driverID = driverID;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public Integer getTaskType() {
            return taskType;
        }

        public void setTaskType(Integer taskType) {
            this.taskType = taskType;
        }

        public String getSubscriptionID() {
            return subscriptionID;
        }

        public void setSubscriptionID(String subscriptionID) {
            this.subscriptionID = subscriptionID;
        }

        public String getOrderID() {
            return orderID;
        }

        public void setOrderID(String orderID) {
            this.orderID = orderID;
        }

        public String getOrderNumber() {
            return orderNumber;
        }

        public void setOrderNumber(String orderNumber) {
            this.orderNumber = orderNumber;
        }

        public String getSelectedDateTime() {
            return selectedDateTime;
        }

        public void setSelectedDateTime(String selectedDateTime) {
            this.selectedDateTime = selectedDateTime;
        }

        public Integer getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(Integer paymentMode) {
            this.paymentMode = paymentMode;
        }

        public Integer getTaskStatus() {
            return taskStatus;
        }

        public void setTaskStatus(Integer taskStatus) {
            this.taskStatus = taskStatus;
        }

        public Boolean getWhetherToCollectAmount() {
            return whetherToCollectAmount;
        }

        public void setWhetherToCollectAmount(Boolean whetherToCollectAmount) {
            this.whetherToCollectAmount = whetherToCollectAmount;
        }

        public Integer getCollectionAmount() {
            return collectionAmount;
        }

        public void setCollectionAmount(Integer collectionAmount) {
            this.collectionAmount = collectionAmount;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public List<TaskStatusLog> getTaskStatusLogs() {
            return taskStatusLogs;
        }

        public void setTaskStatusLogs(List<TaskStatusLog> taskStatusLogs) {
            this.taskStatusLogs = taskStatusLogs;
        }

        public List<Double> getPoint() {
            return point;
        }

        public void setPoint(List<Double> point) {
            this.point = point;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public SubscriptionData getSubscriptionData() {
            return subscriptionData;
        }

        public void setSubscriptionData(SubscriptionData subscriptionData) {
            this.subscriptionData = subscriptionData;
        }

        public DriverData getDriverData() {
            return driverData;
        }

        public void setDriverData(DriverData driverData) {
            this.driverData = driverData;
        }

    }

    public class DeliveryInformation {

        @SerializedName("AddressID")
        @Expose
        private String addressID;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("PhoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("AddressType")
        @Expose
        private Integer addressType;
        @SerializedName("Address")
        @Expose
        private String address;
        @SerializedName("Latitude")
        @Expose
        private Double latitude;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;

        public String getAddressID() {
            return addressID;
        }

        public void setAddressID(String addressID) {
            this.addressID = addressID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public Integer getAddressType() {
            return addressType;
        }

        public void setAddressType(Integer addressType) {
            this.addressType = addressType;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

    }

    public class DriverData {

        @SerializedName("Image_Information")
        @Expose
        private ImageInformation imageInformation;
        @SerializedName("Current_Location")
        @Expose
        private CurrentLocation currentLocation;
        @SerializedName("DriverID")
        @Expose
        private String driverID;
        @SerializedName("Driver_QR")
        @Expose
        private String driverQR;
        @SerializedName("SessionID")
        @Expose
        private String sessionID;
        @SerializedName("DeviceID")
        @Expose
        private String deviceID;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("Localities_Array")
        @Expose
        private List<String> localitiesArray = null;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("EmailID")
        @Expose
        private String emailID;
        @SerializedName("PhoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("Vehicle_Type")
        @Expose
        private Integer vehicleType;
        @SerializedName("Vehicle_Title")
        @Expose
        private String vehicleTitle;
        @SerializedName("Vehicle_Number")
        @Expose
        private String vehicleNumber;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("PasswordHash")
        @Expose
        private String passwordHash;
        @SerializedName("PasswordSalt")
        @Expose
        private String passwordSalt;
        @SerializedName("Whether_First_Login")
        @Expose
        private Boolean whetherFirstLogin;
        @SerializedName("First_DeviceID")
        @Expose
        private String firstDeviceID;
        @SerializedName("First_Time")
        @Expose
        private Object firstTime;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("Document_Information")
        @Expose
        private List<Object> documentInformation = null;
        @SerializedName("Point")
        @Expose
        private List<Double> point = null;
        @SerializedName("City_Title")
        @Expose
        private String cityTitle;

        public ImageInformation getImageInformation() {
            return imageInformation;
        }

        public void setImageInformation(ImageInformation imageInformation) {
            this.imageInformation = imageInformation;
        }

        public CurrentLocation getCurrentLocation() {
            return currentLocation;
        }

        public void setCurrentLocation(CurrentLocation currentLocation) {
            this.currentLocation = currentLocation;
        }

        public String getDriverID() {
            return driverID;
        }

        public void setDriverID(String driverID) {
            this.driverID = driverID;
        }

        public String getDriverQR() {
            return driverQR;
        }

        public void setDriverQR(String driverQR) {
            this.driverQR = driverQR;
        }

        public String getSessionID() {
            return sessionID;
        }

        public void setSessionID(String sessionID) {
            this.sessionID = sessionID;
        }

        public String getDeviceID() {
            return deviceID;
        }

        public void setDeviceID(String deviceID) {
            this.deviceID = deviceID;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public List<String> getLocalitiesArray() {
            return localitiesArray;
        }

        public void setLocalitiesArray(List<String> localitiesArray) {
            this.localitiesArray = localitiesArray;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmailID() {
            return emailID;
        }

        public void setEmailID(String emailID) {
            this.emailID = emailID;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public Integer getVehicleType() {
            return vehicleType;
        }

        public void setVehicleType(Integer vehicleType) {
            this.vehicleType = vehicleType;
        }

        public String getVehicleTitle() {
            return vehicleTitle;
        }

        public void setVehicleTitle(String vehicleTitle) {
            this.vehicleTitle = vehicleTitle;
        }

        public String getVehicleNumber() {
            return vehicleNumber;
        }

        public void setVehicleNumber(String vehicleNumber) {
            this.vehicleNumber = vehicleNumber;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public String getPasswordHash() {
            return passwordHash;
        }

        public void setPasswordHash(String passwordHash) {
            this.passwordHash = passwordHash;
        }

        public String getPasswordSalt() {
            return passwordSalt;
        }

        public void setPasswordSalt(String passwordSalt) {
            this.passwordSalt = passwordSalt;
        }

        public Boolean getWhetherFirstLogin() {
            return whetherFirstLogin;
        }

        public void setWhetherFirstLogin(Boolean whetherFirstLogin) {
            this.whetherFirstLogin = whetherFirstLogin;
        }

        public String getFirstDeviceID() {
            return firstDeviceID;
        }

        public void setFirstDeviceID(String firstDeviceID) {
            this.firstDeviceID = firstDeviceID;
        }

        public Object getFirstTime() {
            return firstTime;
        }

        public void setFirstTime(Object firstTime) {
            this.firstTime = firstTime;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public List<Object> getDocumentInformation() {
            return documentInformation;
        }

        public void setDocumentInformation(List<Object> documentInformation) {
            this.documentInformation = documentInformation;
        }

        public List<Double> getPoint() {
            return point;
        }

        public void setPoint(List<Double> point) {
            this.point = point;
        }

        public String getCityTitle() {
            return cityTitle;
        }

        public void setCityTitle(String cityTitle) {
            this.cityTitle = cityTitle;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Count")
        @Expose
        private Integer count;
        @SerializedName("Data")
        @Expose
        private List<Datum> data = null;

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public List<Datum> getData() {
            return data;
        }


        public void setData(List<Datum> data) {
            this.data = data;
        }

    }

    public class ImageData {

        @SerializedName("ImageID")
        @Expose
        private String imageID;
        @SerializedName("Image50")
        @Expose
        private String image50;
        @SerializedName("Image100")
        @Expose
        private String image100;
        @SerializedName("Image250")
        @Expose
        private String image250;
        @SerializedName("Image550")
        @Expose
        private String image550;
        @SerializedName("Image900")
        @Expose
        private String image900;
        @SerializedName("ImageOriginal")
        @Expose
        private String imageOriginal;

        public String getImageID() {
            return imageID;
        }

        public void setImageID(String imageID) {
            this.imageID = imageID;
        }

        public String getImage50() {
            return image50;
        }

        public void setImage50(String image50) {
            this.image50 = image50;
        }

        public String getImage100() {
            return image100;
        }

        public void setImage100(String image100) {
            this.image100 = image100;
        }

        public String getImage250() {
            return image250;
        }

        public void setImage250(String image250) {
            this.image250 = image250;
        }

        public String getImage550() {
            return image550;
        }

        public void setImage550(String image550) {
            this.image550 = image550;
        }

        public String getImage900() {
            return image900;
        }

        public void setImage900(String image900) {
            this.image900 = image900;
        }

        public String getImageOriginal() {
            return imageOriginal;
        }

        public void setImageOriginal(String imageOriginal) {
            this.imageOriginal = imageOriginal;
        }

    }

    public class ImageInformation {

        @SerializedName("ImageID")
        @Expose
        private String imageID;
        @SerializedName("Image50")
        @Expose
        private String image50;
        @SerializedName("Image100")
        @Expose
        private String image100;
        @SerializedName("Image250")
        @Expose
        private String image250;
        @SerializedName("Image550")
        @Expose
        private String image550;
        @SerializedName("Image900")
        @Expose
        private String image900;
        @SerializedName("ImageOriginal")
        @Expose
        private String imageOriginal;

        public String getImageID() {
            return imageID;
        }

        public void setImageID(String imageID) {
            this.imageID = imageID;
        }

        public String getImage50() {
            return image50;
        }

        public void setImage50(String image50) {
            this.image50 = image50;
        }

        public String getImage100() {
            return image100;
        }

        public void setImage100(String image100) {
            this.image100 = image100;
        }

        public String getImage250() {
            return image250;
        }

        public void setImage250(String image250) {
            this.image250 = image250;
        }

        public String getImage550() {
            return image550;
        }

        public void setImage550(String image550) {
            this.image550 = image550;
        }

        public String getImage900() {
            return image900;
        }

        public void setImage900(String image900) {
            this.image900 = image900;
        }

        public String getImageOriginal() {
            return imageOriginal;
        }

        public void setImageOriginal(String imageOriginal) {
            this.imageOriginal = imageOriginal;
        }

    }

    public class LocalityProductData {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("ProductID")
        @Expose
        private String productID;
        @SerializedName("Available_Stock")
        @Expose
        private Integer availableStock;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("LocalityProductID")
        @Expose
        private String localityProductID;
        @SerializedName("MRP")
        @Expose
        private Integer mRP;
        @SerializedName("Selling_Price")
        @Expose
        private Integer sellingPrice;
        @SerializedName("Sold_Stock")
        @Expose
        private Integer soldStock;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("Total_Stock")
        @Expose
        private Integer totalStock;
        @SerializedName("Whether_Inventory")
        @Expose
        private Boolean whetherInventory;
        @SerializedName("Whether_Subscription")
        @Expose
        private Boolean whetherSubscription;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("Image_Information")
        @Expose
        private ImageInformation imageInformation;
        @SerializedName("Product_Brand")
        @Expose
        private String productBrand;
        @SerializedName("Product_Name")
        @Expose
        private String productName;
        @SerializedName("Product_Unit")
        @Expose
        private String productUnit;
        @SerializedName("Product_Description")
        @Expose
        private String productDescription;
        @SerializedName("Product_Type")
        @Expose
        private Integer productType;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("Category_Title")
        @Expose
        private String categoryTitle;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getProductID() {
            return productID;
        }

        public void setProductID(String productID) {
            this.productID = productID;
        }

        public Integer getAvailableStock() {
            return availableStock;
        }

        public void setAvailableStock(Integer availableStock) {
            this.availableStock = availableStock;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public String getLocalityProductID() {
            return localityProductID;
        }

        public void setLocalityProductID(String localityProductID) {
            this.localityProductID = localityProductID;
        }

        public Integer getMRP() {
            return mRP;
        }

        public void setMRP(Integer mRP) {
            this.mRP = mRP;
        }

        public Integer getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(Integer sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public Integer getSoldStock() {
            return soldStock;
        }

        public void setSoldStock(Integer soldStock) {
            this.soldStock = soldStock;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public Integer getTotalStock() {
            return totalStock;
        }

        public void setTotalStock(Integer totalStock) {
            this.totalStock = totalStock;
        }

        public Boolean getWhetherInventory() {
            return whetherInventory;
        }

        public void setWhetherInventory(Boolean whetherInventory) {
            this.whetherInventory = whetherInventory;
        }

        public Boolean getWhetherSubscription() {
            return whetherSubscription;
        }

        public void setWhetherSubscription(Boolean whetherSubscription) {
            this.whetherSubscription = whetherSubscription;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public ImageInformation getImageInformation() {
            return imageInformation;
        }

        public void setImageInformation(ImageInformation imageInformation) {
            this.imageInformation = imageInformation;
        }

        public String getProductBrand() {
            return productBrand;
        }

        public void setProductBrand(String productBrand) {
            this.productBrand = productBrand;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductUnit() {
            return productUnit;
        }

        public void setProductUnit(String productUnit) {
            this.productUnit = productUnit;
        }

        public String getProductDescription() {
            return productDescription;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }

        public Integer getProductType() {
            return productType;
        }

        public void setProductType(Integer productType) {
            this.productType = productType;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public String getCategoryTitle() {
            return categoryTitle;
        }

        public void setCategoryTitle(String categoryTitle) {
            this.categoryTitle = categoryTitle;
        }

    }

    public class PriceInformation {

        @SerializedName("Selling_Price")
        @Expose
        private Integer sellingPrice;
        @SerializedName("Required_Quantity")
        @Expose
        private Integer requiredQuantity;
        @SerializedName("Sub_Total_Amount")
        @Expose
        private Integer subTotalAmount;
        @SerializedName("Subscription_Service_Charge")
        @Expose
        private Integer subscriptionServiceCharge;
        @SerializedName("Final_Total_Amount")
        @Expose
        private Integer finalTotalAmount;

        public Integer getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(Integer sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public Integer getRequiredQuantity() {
            return requiredQuantity;
        }

        public void setRequiredQuantity(Integer requiredQuantity) {
            this.requiredQuantity = requiredQuantity;
        }

        public Integer getSubTotalAmount() {
            return subTotalAmount;
        }

        public void setSubTotalAmount(Integer subTotalAmount) {
            this.subTotalAmount = subTotalAmount;
        }

        public Integer getSubscriptionServiceCharge() {
            return subscriptionServiceCharge;
        }

        public void setSubscriptionServiceCharge(Integer subscriptionServiceCharge) {
            this.subscriptionServiceCharge = subscriptionServiceCharge;
        }

        public Integer getFinalTotalAmount() {
            return finalTotalAmount;
        }

        public void setFinalTotalAmount(Integer finalTotalAmount) {
            this.finalTotalAmount = finalTotalAmount;
        }

    }

    public class SubscriptionData {

        @SerializedName("SubscriptionID")
        @Expose
        private String subscriptionID;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("ProductID")
        @Expose
        private String productID;
        @SerializedName("LocalityProductID")
        @Expose
        private String localityProductID;
        @SerializedName("AddressID")
        @Expose
        private String addressID;
        @SerializedName("Required_Quantity")
        @Expose
        private Integer requiredQuantity;
        @SerializedName("Required_Frequency")
        @Expose
        private Integer requiredFrequency;
        @SerializedName("Last_Delivery_Date")
        @Expose
        private String lastDeliveryDate;
        @SerializedName("Subscription_Status")
        @Expose
        private Integer subscriptionStatus;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("Subscription_Status_Logs")
        @Expose
        private List<SubscriptionStatusLog> subscriptionStatusLogs = null;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("LocalityProductData")
        @Expose
        private LocalityProductData localityProductData;
        @SerializedName("User_Information")
        @Expose
        private UserInformation userInformation;
        @SerializedName("AddressData")
        @Expose
        private AddressData addressData;
        @SerializedName("Price_Information")
        @Expose
        private PriceInformation priceInformation;
        @SerializedName("Next_Delivery_Date")
        @Expose
        private String nextDeliveryDate;

        public String getSubscriptionID() {
            return subscriptionID;
        }

        public void setSubscriptionID(String subscriptionID) {
            this.subscriptionID = subscriptionID;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public String getProductID() {
            return productID;
        }

        public void setProductID(String productID) {
            this.productID = productID;
        }

        public String getLocalityProductID() {
            return localityProductID;
        }

        public void setLocalityProductID(String localityProductID) {
            this.localityProductID = localityProductID;
        }

        public String getAddressID() {
            return addressID;
        }

        public void setAddressID(String addressID) {
            this.addressID = addressID;
        }

        public Integer getRequiredQuantity() {
            return requiredQuantity;
        }

        public void setRequiredQuantity(Integer requiredQuantity) {
            this.requiredQuantity = requiredQuantity;
        }

        public Integer getRequiredFrequency() {
            return requiredFrequency;
        }

        public void setRequiredFrequency(Integer requiredFrequency) {
            this.requiredFrequency = requiredFrequency;
        }

        public String getLastDeliveryDate() {
            return lastDeliveryDate;
        }

        public void setLastDeliveryDate(String lastDeliveryDate) {
            this.lastDeliveryDate = lastDeliveryDate;
        }

        public Integer getSubscriptionStatus() {
            return subscriptionStatus;
        }

        public void setSubscriptionStatus(Integer subscriptionStatus) {
            this.subscriptionStatus = subscriptionStatus;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public List<SubscriptionStatusLog> getSubscriptionStatusLogs() {
            return subscriptionStatusLogs;
        }

        public void setSubscriptionStatusLogs(List<SubscriptionStatusLog> subscriptionStatusLogs) {
            this.subscriptionStatusLogs = subscriptionStatusLogs;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public LocalityProductData getLocalityProductData() {
            return localityProductData;
        }

        public void setLocalityProductData(LocalityProductData localityProductData) {
            this.localityProductData = localityProductData;
        }

        public UserInformation getUserInformation() {
            return userInformation;
        }

        public void setUserInformation(UserInformation userInformation) {
            this.userInformation = userInformation;
        }

        public AddressData getAddressData() {
            return addressData;
        }

        public void setAddressData(AddressData addressData) {
            this.addressData = addressData;
        }

        public PriceInformation getPriceInformation() {
            return priceInformation;
        }

        public void setPriceInformation(PriceInformation priceInformation) {
            this.priceInformation = priceInformation;
        }

        public String getNextDeliveryDate() {
            return nextDeliveryDate;
        }

        public void setNextDeliveryDate(String nextDeliveryDate) {
            this.nextDeliveryDate = nextDeliveryDate;
        }

    }

    public class SubscriptionPriceInformation {

        @SerializedName("Selling_Price")
        @Expose
        private Integer sellingPrice;
        @SerializedName("Required_Quantity")
        @Expose
        private Integer requiredQuantity;
        @SerializedName("Sub_Total_Amount")
        @Expose
        private Integer subTotalAmount;
        @SerializedName("Subscription_Service_Charge")
        @Expose
        private Integer subscriptionServiceCharge;
        @SerializedName("Final_Total_Amount")
        @Expose
        private Integer finalTotalAmount;

        public Integer getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(Integer sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public Integer getRequiredQuantity() {
            return requiredQuantity;
        }

        public void setRequiredQuantity(Integer requiredQuantity) {
            this.requiredQuantity = requiredQuantity;
        }

        public Integer getSubTotalAmount() {
            return subTotalAmount;
        }

        public void setSubTotalAmount(Integer subTotalAmount) {
            this.subTotalAmount = subTotalAmount;
        }

        public Integer getSubscriptionServiceCharge() {
            return subscriptionServiceCharge;
        }

        public void setSubscriptionServiceCharge(Integer subscriptionServiceCharge) {
            this.subscriptionServiceCharge = subscriptionServiceCharge;
        }

        public Integer getFinalTotalAmount() {
            return finalTotalAmount;
        }

        public void setFinalTotalAmount(Integer finalTotalAmount) {
            this.finalTotalAmount = finalTotalAmount;
        }

    }

    public class SubscriptionStatusLog {

        @SerializedName("LogID")
        @Expose
        private String logID;
        @SerializedName("Subscription_Status")
        @Expose
        private Integer subscriptionStatus;
        @SerializedName("Comment")
        @Expose
        private String comment;
        @SerializedName("Time")
        @Expose
        private String time;

        public String getLogID() {
            return logID;
        }

        public void setLogID(String logID) {
            this.logID = logID;
        }

        public Integer getSubscriptionStatus() {
            return subscriptionStatus;
        }

        public void setSubscriptionStatus(Integer subscriptionStatus) {
            this.subscriptionStatus = subscriptionStatus;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

    }

    public class TaskStatusLog {

        @SerializedName("LogID")
        @Expose
        private String logID;
        @SerializedName("Task_Status")
        @Expose
        private Integer taskStatus;
        @SerializedName("Comments")
        @Expose
        private String comments;
        @SerializedName("Time")
        @Expose
        private String time;

        public String getLogID() {
            return logID;
        }

        public void setLogID(String logID) {
            this.logID = logID;
        }

        public Integer getTaskStatus() {
            return taskStatus;
        }

        public void setTaskStatus(Integer taskStatus) {
            this.taskStatus = taskStatus;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

    }

    public class UserInformation {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("PhoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("DeviceID")
        @Expose
        private String deviceID;
        @SerializedName("EmailID")
        @Expose
        private String emailID;
        @SerializedName("Image_Data")
        @Expose
        private ImageData imageData;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("SessionID")
        @Expose
        private String sessionID;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("Wallet_Information")
        @Expose
        private WalletInformation walletInformation;
        @SerializedName("Whether_Basic_Information_Available")
        @Expose
        private Boolean whetherBasicInformationAvailable;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getDeviceID() {
            return deviceID;
        }

        public void setDeviceID(String deviceID) {
            this.deviceID = deviceID;
        }

        public String getEmailID() {
            return emailID;
        }

        public void setEmailID(String emailID) {
            this.emailID = emailID;
        }

        public ImageData getImageData() {
            return imageData;
        }

        public void setImageData(ImageData imageData) {
            this.imageData = imageData;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSessionID() {
            return sessionID;
        }

        public void setSessionID(String sessionID) {
            this.sessionID = sessionID;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public WalletInformation getWalletInformation() {
            return walletInformation;
        }

        public void setWalletInformation(WalletInformation walletInformation) {
            this.walletInformation = walletInformation;
        }

        public Boolean getWhetherBasicInformationAvailable() {
            return whetherBasicInformationAvailable;
        }

        public void setWhetherBasicInformationAvailable(Boolean whetherBasicInformationAvailable) {
            this.whetherBasicInformationAvailable = whetherBasicInformationAvailable;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public class WalletInformation {

        @SerializedName("Available_Amount")
        @Expose
        private Integer availableAmount;
        @SerializedName("Credited_Amount")
        @Expose
        private Integer creditedAmount;
        @SerializedName("Debited_Amount")
        @Expose
        private Integer debitedAmount;

        public Integer getAvailableAmount() {
            return availableAmount;
        }

        public void setAvailableAmount(Integer availableAmount) {
            this.availableAmount = availableAmount;
        }

        public Integer getCreditedAmount() {
            return creditedAmount;
        }

        public void setCreditedAmount(Integer creditedAmount) {
            this.creditedAmount = creditedAmount;
        }

        public Integer getDebitedAmount() {
            return debitedAmount;
        }

        public void setDebitedAmount(Integer debitedAmount) {
            this.debitedAmount = debitedAmount;
        }

    }

}
