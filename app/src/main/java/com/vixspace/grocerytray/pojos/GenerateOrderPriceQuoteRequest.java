package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GenerateOrderPriceQuoteRequest implements Serializable {

    public class CartInformation {

        @SerializedName("LocalityProductID")
        @Expose
        private String localityProductID;
        @SerializedName("Quantity")
        @Expose
        private Integer quantity;

        public String getLocalityProductID() {
            return localityProductID;
        }

        public void setLocalityProductID(String localityProductID) {
            this.localityProductID = localityProductID;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

    }

        @SerializedName("ApiKey")
        @Expose
        private String apiKey;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("SessionID")
        @Expose
        private String sessionID;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("AddressID")
        @Expose
        private String addressID;
        @SerializedName("SLOTID")
        @Expose
        private String sLOTID;
        @SerializedName("Selected_Date_Time")
        @Expose
        private String selectedDateTime;
        @SerializedName("Payment_Mode")
        @Expose
        private Integer paymentMode;
        @SerializedName("Cart_Information")
        @Expose
        private List<CartInformation> cartInformation = null;

        public String getApiKey() {
            return apiKey;
        }

        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public String getSessionID() {
            return sessionID;
        }

        public void setSessionID(String sessionID) {
            this.sessionID = sessionID;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getAddressID() {
            return addressID;
        }

        public void setAddressID(String addressID) {
            this.addressID = addressID;
        }

        public String getSLOTID() {
            return sLOTID;
        }

        public void setSLOTID(String sLOTID) {
            this.sLOTID = sLOTID;
        }

        public String getSelectedDateTime() {
            return selectedDateTime;
        }

        public void setSelectedDateTime(String selectedDateTime) {
            this.selectedDateTime = selectedDateTime;
        }

        public Integer getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(Integer paymentMode) {
            this.paymentMode = paymentMode;
        }

        public List<CartInformation> getCartInformation() {
            return cartInformation;
        }

        public void setCartInformation(List<CartInformation> cartInformation) {
            this.cartInformation = cartInformation;
        }

}
