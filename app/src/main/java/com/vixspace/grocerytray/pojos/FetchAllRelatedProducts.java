package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FetchAllRelatedProducts implements Serializable {

    public class Datum {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("ProductID")
        @Expose
        private String productID;
        @SerializedName("Available_Stock")
        @Expose
        private Integer availableStock;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("LocalityProductID")
        @Expose
        private String localityProductID;
        @SerializedName("MRP")
        @Expose
        private Integer mRP;
        @SerializedName("Selling_Price")
        @Expose
        private Integer sellingPrice;
        @SerializedName("Sold_Stock")
        @Expose
        private Integer soldStock;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("Total_Stock")
        @Expose
        private Integer totalStock;
        @SerializedName("Whether_Inventory")
        @Expose
        private Boolean whetherInventory;
        @SerializedName("Whether_Subscription")
        @Expose
        private Boolean whetherSubscription;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("Image_Information")
        @Expose
        private ImageInformation imageInformation;
        @SerializedName("Product_Brand")
        @Expose
        private String productBrand;
        @SerializedName("Product_Name")
        @Expose
        private String productName;
        @SerializedName("Product_Unit")
        @Expose
        private String productUnit;
        @SerializedName("Product_Description")
        @Expose
        private String productDescription;
        @SerializedName("Product_Type")
        @Expose
        private Integer productType;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("Category_Title")
        @Expose
        private String categoryTitle;
        @SerializedName("Whether_Favourite_Product")
        @Expose
        private Boolean whetherFavouriteProduct;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Datum withId(String id) {
            this.id = id;
            return this;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public Datum withCityID(String cityID) {
            this.cityID = cityID;
            return this;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public Datum withLocalityID(String localityID) {
            this.localityID = localityID;
            return this;
        }

        public String getProductID() {
            return productID;
        }

        public void setProductID(String productID) {
            this.productID = productID;
        }

        public Datum withProductID(String productID) {
            this.productID = productID;
            return this;
        }

        public Integer getAvailableStock() {
            return availableStock;
        }

        public void setAvailableStock(Integer availableStock) {
            this.availableStock = availableStock;
        }

        public Datum withAvailableStock(Integer availableStock) {
            this.availableStock = availableStock;
            return this;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public Datum withCategoryID(String categoryID) {
            this.categoryID = categoryID;
            return this;
        }

        public String getLocalityProductID() {
            return localityProductID;
        }

        public void setLocalityProductID(String localityProductID) {
            this.localityProductID = localityProductID;
        }

        public Datum withLocalityProductID(String localityProductID) {
            this.localityProductID = localityProductID;
            return this;
        }

        public Integer getMRP() {
            return mRP;
        }

        public void setMRP(Integer mRP) {
            this.mRP = mRP;
        }

        public Datum withMRP(Integer mRP) {
            this.mRP = mRP;
            return this;
        }

        public Integer getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(Integer sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public Datum withSellingPrice(Integer sellingPrice) {
            this.sellingPrice = sellingPrice;
            return this;
        }

        public Integer getSoldStock() {
            return soldStock;
        }

        public void setSoldStock(Integer soldStock) {
            this.soldStock = soldStock;
        }

        public Datum withSoldStock(Integer soldStock) {
            this.soldStock = soldStock;
            return this;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public Datum withStatus(Boolean status) {
            this.status = status;
            return this;
        }

        public Integer getTotalStock() {
            return totalStock;
        }

        public void setTotalStock(Integer totalStock) {
            this.totalStock = totalStock;
        }

        public Datum withTotalStock(Integer totalStock) {
            this.totalStock = totalStock;
            return this;
        }

        public Boolean getWhetherInventory() {
            return whetherInventory;
        }

        public void setWhetherInventory(Boolean whetherInventory) {
            this.whetherInventory = whetherInventory;
        }

        public Datum withWhetherInventory(Boolean whetherInventory) {
            this.whetherInventory = whetherInventory;
            return this;
        }

        public Boolean getWhetherSubscription() {
            return whetherSubscription;
        }

        public void setWhetherSubscription(Boolean whetherSubscription) {
            this.whetherSubscription = whetherSubscription;
        }

        public Datum withWhetherSubscription(Boolean whetherSubscription) {
            this.whetherSubscription = whetherSubscription;
            return this;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public Datum withV(Integer v) {
            this.v = v;
            return this;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Datum withCreatedAt(String createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public ImageInformation getImageInformation() {
            return imageInformation;
        }

        public void setImageInformation(ImageInformation imageInformation) {
            this.imageInformation = imageInformation;
        }

        public Datum withImageInformation(ImageInformation imageInformation) {
            this.imageInformation = imageInformation;
            return this;
        }

        public String getProductBrand() {
            return productBrand;
        }

        public void setProductBrand(String productBrand) {
            this.productBrand = productBrand;
        }

        public Datum withProductBrand(String productBrand) {
            this.productBrand = productBrand;
            return this;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public Datum withProductName(String productName) {
            this.productName = productName;
            return this;
        }

        public String getProductUnit() {
            return productUnit;
        }

        public void setProductUnit(String productUnit) {
            this.productUnit = productUnit;
        }

        public Datum withProductUnit(String productUnit) {
            this.productUnit = productUnit;
            return this;
        }

        public String getProductDescription() {
            return productDescription;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }

        public Datum withProductDescription(String productDescription) {
            this.productDescription = productDescription;
            return this;
        }

        public Integer getProductType() {
            return productType;
        }

        public void setProductType(Integer productType) {
            this.productType = productType;
        }

        public Datum withProductType(Integer productType) {
            this.productType = productType;
            return this;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public Datum withWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
            return this;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Datum withUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        public String getCategoryTitle() {
            return categoryTitle;
        }

        public void setCategoryTitle(String categoryTitle) {
            this.categoryTitle = categoryTitle;
        }

        public Datum withCategoryTitle(String categoryTitle) {
            this.categoryTitle = categoryTitle;
            return this;
        }

        public Boolean getWhetherFavouriteProduct() {
            return whetherFavouriteProduct;
        }

        public void setWhetherFavouriteProduct(Boolean whetherFavouriteProduct) {
            this.whetherFavouriteProduct = whetherFavouriteProduct;
        }

        public Datum withWhetherFavouriteProduct(Boolean whetherFavouriteProduct) {
            this.whetherFavouriteProduct = whetherFavouriteProduct;
            return this;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }


    public class Extras {
        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Count")
        @Expose
        private Integer count;
        @SerializedName("Data")
        @Expose
        private List<Datum> data = null;

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public Extras withCount(Integer count) {
            this.count = count;
            return this;
        }

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }

        public Extras withData(List<Datum> data) {
            this.data = data;
            return this;
        }

    }

    public class ImageInformation {

        @SerializedName("ImageID")
        @Expose
        private String imageID;
        @SerializedName("Image50")
        @Expose
        private String image50;
        @SerializedName("Image100")
        @Expose
        private String image100;
        @SerializedName("Image250")
        @Expose
        private String image250;
        @SerializedName("Image550")
        @Expose
        private String image550;
        @SerializedName("Image900")
        @Expose
        private String image900;
        @SerializedName("ImageOriginal")
        @Expose
        private String imageOriginal;

        public String getImageID() {
            return imageID;
        }

        public void setImageID(String imageID) {
            this.imageID = imageID;
        }

        public ImageInformation withImageID(String imageID) {
            this.imageID = imageID;
            return this;
        }

        public String getImage50() {
            return image50;
        }

        public void setImage50(String image50) {
            this.image50 = image50;
        }

        public ImageInformation withImage50(String image50) {
            this.image50 = image50;
            return this;
        }

        public String getImage100() {
            return image100;
        }

        public void setImage100(String image100) {
            this.image100 = image100;
        }

        public ImageInformation withImage100(String image100) {
            this.image100 = image100;
            return this;
        }

        public String getImage250() {
            return image250;
        }

        public void setImage250(String image250) {
            this.image250 = image250;
        }

        public ImageInformation withImage250(String image250) {
            this.image250 = image250;
            return this;
        }

        public String getImage550() {
            return image550;
        }

        public void setImage550(String image550) {
            this.image550 = image550;
        }

        public ImageInformation withImage550(String image550) {
            this.image550 = image550;
            return this;
        }

        public String getImage900() {
            return image900;
        }

        public void setImage900(String image900) {
            this.image900 = image900;
        }

        public ImageInformation withImage900(String image900) {
            this.image900 = image900;
            return this;
        }

        public String getImageOriginal() {
            return imageOriginal;
        }

        public void setImageOriginal(String imageOriginal) {
            this.imageOriginal = imageOriginal;
        }

        public ImageInformation withImageOriginal(String imageOriginal) {
            this.imageOriginal = imageOriginal;
            return this;
        }

    }
}
