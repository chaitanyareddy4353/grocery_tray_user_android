package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FetchUserWalletInformationResponse implements Serializable {

    public class Data {

        @SerializedName("Available_Amount")
        @Expose
        private Double availableAmount;
        @SerializedName("Credited_Amount")
        @Expose
        private Double creditedAmount;
        @SerializedName("Debited_Amount")
        @Expose
        private Double debitedAmount;

        public Double getReservedAmount() {
            return reservedAmount;
        }

        public void setReservedAmount(Double reservedAmount) {
            this.reservedAmount = reservedAmount;
        }

        @SerializedName("Reserved_Amount")
        @Expose
        private Double reservedAmount;

        public Double getAvailableAmount() {
            return availableAmount;
        }

        public void setAvailableAmount(Double availableAmount) {
            this.availableAmount = availableAmount;
        }

        public Double getCreditedAmount() {
            return creditedAmount;
        }

        public void setCreditedAmount(Double creditedAmount) {
            this.creditedAmount = creditedAmount;
        }

        public Double getDebitedAmount() {
            return debitedAmount;
        }

        public void setDebitedAmount(Double debitedAmount) {
            this.debitedAmount = debitedAmount;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {
        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Data")
        @Expose
        private Data data;

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }
}
