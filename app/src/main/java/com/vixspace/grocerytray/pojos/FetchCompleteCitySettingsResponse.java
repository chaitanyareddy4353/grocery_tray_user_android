package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FetchCompleteCitySettingsResponse implements Serializable {

    public class Data {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("Delivery_Price")
        @Expose
        private Integer deliveryPrice;
        @SerializedName("Free_Delivery_Order_Price")
        @Expose
        private Integer freeDeliveryOrderPrice;
        @SerializedName("Minimum_Order_Price")
        @Expose
        private Integer minimumOrderPrice;
        @SerializedName("Payment_Gateway_Percentage")
        @Expose
        private Double paymentGatewayPercentage;
        @SerializedName("Subscription_Service_Charge")
        @Expose
        private Integer subscriptionServiceCharge;
        @SerializedName("Whether_COD")
        @Expose
        private Boolean whetherCOD;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public Integer getDeliveryPrice() {
            return deliveryPrice;
        }

        public void setDeliveryPrice(Integer deliveryPrice) {
            this.deliveryPrice = deliveryPrice;
        }

        public Integer getFreeDeliveryOrderPrice() {
            return freeDeliveryOrderPrice;
        }

        public void setFreeDeliveryOrderPrice(Integer freeDeliveryOrderPrice) {
            this.freeDeliveryOrderPrice = freeDeliveryOrderPrice;
        }

        public Integer getMinimumOrderPrice() {
            return minimumOrderPrice;
        }

        public void setMinimumOrderPrice(Integer minimumOrderPrice) {
            this.minimumOrderPrice = minimumOrderPrice;
        }

        public Double getPaymentGatewayPercentage() {
            return paymentGatewayPercentage;
        }

        public void setPaymentGatewayPercentage(Double paymentGatewayPercentage) {
            this.paymentGatewayPercentage = paymentGatewayPercentage;
        }

        public Integer getSubscriptionServiceCharge() {
            return subscriptionServiceCharge;
        }

        public void setSubscriptionServiceCharge(Integer subscriptionServiceCharge) {
            this.subscriptionServiceCharge = subscriptionServiceCharge;
        }

        public Boolean getWhetherCOD() {
            return whetherCOD;
        }

        public void setWhetherCOD(Boolean whetherCOD) {
            this.whetherCOD = whetherCOD;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Data")
        @Expose
        private Data data;

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }
}
