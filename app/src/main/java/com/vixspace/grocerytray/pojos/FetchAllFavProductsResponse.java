package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FetchAllFavProductsResponse implements Serializable {

    public class Datum {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("ProductID")
        @Expose
        private String productID;
        @SerializedName("Available_Stock")
        @Expose
        private Integer availableStock;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("LocalityProductID")
        @Expose
        private String localityProductID;
        @SerializedName("MRP")
        @Expose
        private Double mRP;
        @SerializedName("Selling_Price")
        @Expose
        private Double sellingPrice;
        @SerializedName("Sold_Stock")
        @Expose
        private Integer soldStock;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("Total_Stock")
        @Expose
        private Integer totalStock;
        @SerializedName("Whether_Inventory")
        @Expose
        private Boolean whetherInventory;
        @SerializedName("Whether_Subscription")
        @Expose
        private Boolean whetherSubscription;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("Image_Information")
        @Expose
        private ImageInformation imageInformation;
        @SerializedName("Product_Brand")
        @Expose
        private String productBrand;
        @SerializedName("Product_Name")
        @Expose
        private String productName;
        @SerializedName("Product_Unit")
        @Expose
        private String productUnit;
        @SerializedName("Product_Description")
        @Expose
        private String productDescription;
        @SerializedName("Product_Type")
        @Expose
        private Integer productType;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("Category_Title")
        @Expose
        private String categoryTitle;
        @SerializedName("Whether_Favourite_Product")
        @Expose
        private Boolean whetherFavouriteProduct;
        @SerializedName("FavouriteProductData")
        @Expose
        private FavouriteProductData favouriteProductData;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getProductID() {
            return productID;
        }

        public void setProductID(String productID) {
            this.productID = productID;
        }

        public Integer getAvailableStock() {
            return availableStock;
        }

        public void setAvailableStock(Integer availableStock) {
            this.availableStock = availableStock;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public String getLocalityProductID() {
            return localityProductID;
        }

        public void setLocalityProductID(String localityProductID) {
            this.localityProductID = localityProductID;
        }

        public Double getMRP() {
            return mRP;
        }

        public void setMRP(Double mRP) {
            this.mRP = mRP;
        }

        public Double getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(Double sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public Integer getSoldStock() {
            return soldStock;
        }

        public void setSoldStock(Integer soldStock) {
            this.soldStock = soldStock;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public Integer getTotalStock() {
            return totalStock;
        }

        public void setTotalStock(Integer totalStock) {
            this.totalStock = totalStock;
        }

        public Boolean getWhetherInventory() {
            return whetherInventory;
        }

        public void setWhetherInventory(Boolean whetherInventory) {
            this.whetherInventory = whetherInventory;
        }

        public Boolean getWhetherSubscription() {
            return whetherSubscription;
        }

        public void setWhetherSubscription(Boolean whetherSubscription) {
            this.whetherSubscription = whetherSubscription;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public ImageInformation getImageInformation() {
            return imageInformation;
        }

        public void setImageInformation(ImageInformation imageInformation) {
            this.imageInformation = imageInformation;
        }

        public String getProductBrand() {
            return productBrand;
        }

        public void setProductBrand(String productBrand) {
            this.productBrand = productBrand;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductUnit() {
            return productUnit;
        }

        public void setProductUnit(String productUnit) {
            this.productUnit = productUnit;
        }

        public String getProductDescription() {
            return productDescription;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }

        public Integer getProductType() {
            return productType;
        }

        public void setProductType(Integer productType) {
            this.productType = productType;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCategoryTitle() {
            return categoryTitle;
        }

        public void setCategoryTitle(String categoryTitle) {
            this.categoryTitle = categoryTitle;
        }

        public Boolean getWhetherFavouriteProduct() {
            return whetherFavouriteProduct;
        }

        public void setWhetherFavouriteProduct(Boolean whetherFavouriteProduct) {
            this.whetherFavouriteProduct = whetherFavouriteProduct;
        }

        public FavouriteProductData getFavouriteProductData() {
            return favouriteProductData;
        }

        public void setFavouriteProductData(FavouriteProductData favouriteProductData) {
            this.favouriteProductData = favouriteProductData;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Count")
        @Expose
        private Integer count;
        @SerializedName("Data")
        @Expose
        private List<Datum> data = null;

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }

    }

    public class FavouriteProductData {

        @SerializedName("ProductID")
        @Expose
        private String productID;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("FavouriteID")
        @Expose
        private String favouriteID;
        @SerializedName("Remark")
        @Expose
        private String remark;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public String getProductID() {
            return productID;
        }

        public void setProductID(String productID) {
            this.productID = productID;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public String getFavouriteID() {
            return favouriteID;
        }

        public void setFavouriteID(String favouriteID) {
            this.favouriteID = favouriteID;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public class ImageInformation {

        @SerializedName("ImageID")
        @Expose
        private String imageID;
        @SerializedName("Image50")
        @Expose
        private String image50;
        @SerializedName("Image100")
        @Expose
        private String image100;
        @SerializedName("Image250")
        @Expose
        private String image250;
        @SerializedName("Image550")
        @Expose
        private String image550;
        @SerializedName("Image900")
        @Expose
        private String image900;
        @SerializedName("ImageOriginal")
        @Expose
        private String imageOriginal;

        public String getImageID() {
            return imageID;
        }

        public void setImageID(String imageID) {
            this.imageID = imageID;
        }

        public String getImage50() {
            return image50;
        }

        public void setImage50(String image50) {
            this.image50 = image50;
        }

        public String getImage100() {
            return image100;
        }

        public void setImage100(String image100) {
            this.image100 = image100;
        }

        public String getImage250() {
            return image250;
        }

        public void setImage250(String image250) {
            this.image250 = image250;
        }

        public String getImage550() {
            return image550;
        }

        public void setImage550(String image550) {
            this.image550 = image550;
        }

        public String getImage900() {
            return image900;
        }

        public void setImage900(String image900) {
            this.image900 = image900;
        }

        public String getImageOriginal() {
            return imageOriginal;
        }

        public void setImageOriginal(String imageOriginal) {
            this.imageOriginal = imageOriginal;
        }

    }
}
