package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GenerateOrderPriceQuoteResponse implements Serializable {

    public class CartInformation {

        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("ProductID")
        @Expose
        private String productID;
        @SerializedName("LocalityProductID")
        @Expose
        private String localityProductID;
        @SerializedName("MRP")
        @Expose
        private Double mRP;
        @SerializedName("Selling_Price")
        @Expose
        private Double sellingPrice;
        @SerializedName("Quantity")
        @Expose
        private Double quantity;
        @SerializedName("Sub_Total_Amount")
        @Expose
        private Double subTotalAmount;
        @SerializedName("_id")
        @Expose
        private String id;

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public String getProductID() {
            return productID;
        }

        public void setProductID(String productID) {
            this.productID = productID;
        }

        public String getLocalityProductID() {
            return localityProductID;
        }

        public void setLocalityProductID(String localityProductID) {
            this.localityProductID = localityProductID;
        }

        public Double getMRP() {
            return mRP;
        }

        public void setMRP(Double mRP) {
            this.mRP = mRP;
        }

        public Double getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(Double sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public Double getQuantity() {
            return quantity;
        }

        public void setQuantity(Double quantity) {
            this.quantity = quantity;
        }

        public Double getSubTotalAmount() {
            return subTotalAmount;
        }

        public void setSubTotalAmount(Double subTotalAmount) {
            this.subTotalAmount = subTotalAmount;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

    }

    public class Data {

        @SerializedName("Delivery_Information")
        @Expose
        private DeliveryInformation deliveryInformation;
        @SerializedName("Pricing_Information")
        @Expose
        private PricingInformation pricingInformation;
        @SerializedName("PriceQuoteID")
        @Expose
        private String priceQuoteID;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("SLOTID")
        @Expose
        private String sLOTID;
        @SerializedName("Selected_Date_Time")
        @Expose
        private String selectedDateTime;
        @SerializedName("Payment_Mode")
        @Expose
        private Integer paymentMode;
        @SerializedName("Whether_Order_Placed")
        @Expose
        private Boolean whetherOrderPlaced;
        @SerializedName("Order_Time")
        @Expose
        private Object orderTime;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("Delivery_Point")
        @Expose
        private List<Double> deliveryPoint = null;
        @SerializedName("Cart_Information")
        @Expose
        private List<CartInformation> cartInformation = null;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public DeliveryInformation getDeliveryInformation() {
            return deliveryInformation;
        }

        public void setDeliveryInformation(DeliveryInformation deliveryInformation) {
            this.deliveryInformation = deliveryInformation;
        }

        public PricingInformation getPricingInformation() {
            return pricingInformation;
        }

        public void setPricingInformation(PricingInformation pricingInformation) {
            this.pricingInformation = pricingInformation;
        }

        public String getPriceQuoteID() {
            return priceQuoteID;
        }

        public void setPriceQuoteID(String priceQuoteID) {
            this.priceQuoteID = priceQuoteID;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getSLOTID() {
            return sLOTID;
        }

        public void setSLOTID(String sLOTID) {
            this.sLOTID = sLOTID;
        }

        public String getSelectedDateTime() {
            return selectedDateTime;
        }

        public void setSelectedDateTime(String selectedDateTime) {
            this.selectedDateTime = selectedDateTime;
        }

        public Integer getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(Integer paymentMode) {
            this.paymentMode = paymentMode;
        }

        public Boolean getWhetherOrderPlaced() {
            return whetherOrderPlaced;
        }

        public void setWhetherOrderPlaced(Boolean whetherOrderPlaced) {
            this.whetherOrderPlaced = whetherOrderPlaced;
        }

        public Object getOrderTime() {
            return orderTime;
        }

        public void setOrderTime(Object orderTime) {
            this.orderTime = orderTime;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<Double> getDeliveryPoint() {
            return deliveryPoint;
        }

        public void setDeliveryPoint(List<Double> deliveryPoint) {
            this.deliveryPoint = deliveryPoint;
        }

        public List<CartInformation> getCartInformation() {
            return cartInformation;
        }

        public void setCartInformation(List<CartInformation> cartInformation) {
            this.cartInformation = cartInformation;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }

    public class DeliveryInformation {

        @SerializedName("AddressID")
        @Expose
        private String addressID;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("PhoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("AddressType")
        @Expose
        private Integer addressType;
        @SerializedName("Address")
        @Expose
        private String address;
        @SerializedName("Latitude")
        @Expose
        private Double latitude;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;

        public String getAddressID() {
            return addressID;
        }

        public void setAddressID(String addressID) {
            this.addressID = addressID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public Integer getAddressType() {
            return addressType;
        }

        public void setAddressType(Integer addressType) {
            this.addressType = addressType;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Data")
        @Expose
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }

    public class PricingInformation {

        @SerializedName("Total_Cart_Amount")
        @Expose
        private Double totalCartAmount;
        @SerializedName("Delivery_Price")
        @Expose
        private Double deliveryPrice;
        @SerializedName("Whether_Offer_Coupon_Applied")
        @Expose
        private Boolean whetherOfferCouponApplied;
        @SerializedName("Discount_Percentage")
        @Expose
        private Double discountPercentage;
        @SerializedName("Discount_Amount")
        @Expose
        private Double discountAmount;
        @SerializedName("Final_Transaction_Amount")
        @Expose
        private Double finalTransactionAmount;

        public Double getTotalCartAmount() {
            return totalCartAmount;
        }

        public void setTotalCartAmount(Double totalCartAmount) {
            this.totalCartAmount = totalCartAmount;
        }

        public Double getDeliveryPrice() {
            return deliveryPrice;
        }

        public void setDeliveryPrice(Double deliveryPrice) {
            this.deliveryPrice = deliveryPrice;
        }

        public Boolean getWhetherOfferCouponApplied() {
            return whetherOfferCouponApplied;
        }

        public void setWhetherOfferCouponApplied(Boolean whetherOfferCouponApplied) {
            this.whetherOfferCouponApplied = whetherOfferCouponApplied;
        }

        public Double getDiscountPercentage() {
            return discountPercentage;
        }

        public void setDiscountPercentage(Double discountPercentage) {
            this.discountPercentage = discountPercentage;
        }

        public Double getDiscountAmount() {
            return discountAmount;
        }

        public void setDiscountAmount(Double discountAmount) {
            this.discountAmount = discountAmount;
        }

        public Double getFinalTransactionAmount() {
            return finalTransactionAmount;
        }

        public void setFinalTransactionAmount(Double finalTransactionAmount) {
            this.finalTransactionAmount = finalTransactionAmount;
        }

    }
}
