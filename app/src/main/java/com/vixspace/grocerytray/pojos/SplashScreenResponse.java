package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SplashScreenResponse implements Serializable {

    public class Data {

        @SerializedName("ApiKey")
        @Expose
        private String apiKey;
        @SerializedName("Whether_Latest_Version")
        @Expose
        private Boolean whetherLatestVersion;
        @SerializedName("PubnubData")
        @Expose
        private PubnubData pubnubData;
        @SerializedName("RazorpayData")
        @Expose
        private RazorpayData razorpayData;
        @SerializedName("CUSTOMER_CARE_NUMBER")
        @Expose
        private String cUSTOMERCARENUMBER;

        public String getApiKey() {
            return apiKey;
        }

        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }

        public Boolean getWhetherLatestVersion() {
            return whetherLatestVersion;
        }

        public void setWhetherLatestVersion(Boolean whetherLatestVersion) {
            this.whetherLatestVersion = whetherLatestVersion;
        }

        public PubnubData getPubnubData() {
            return pubnubData;
        }

        public void setPubnubData(PubnubData pubnubData) {
            this.pubnubData = pubnubData;
        }

        public RazorpayData getRazorpayData() {
            return razorpayData;
        }

        public void setRazorpayData(RazorpayData razorpayData) {
            this.razorpayData = razorpayData;
        }

        public String getCUSTOMERCARENUMBER() {
            return cUSTOMERCARENUMBER;
        }

        public void setCUSTOMERCARENUMBER(String cUSTOMERCARENUMBER) {
            this.cUSTOMERCARENUMBER = cUSTOMERCARENUMBER;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Data")
        @Expose
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }

    public class PubnubData {

        @SerializedName("publishKey")
        @Expose
        private String publishKey;
        @SerializedName("subscribeKey")
        @Expose
        private String subscribeKey;
        @SerializedName("secretKey")
        @Expose
        private String secretKey;
        @SerializedName("ssl")
        @Expose
        private Boolean ssl;

        public String getMasterChannelName() {
            return masterChannelName;
        }

        public void setMasterChannelName(String masterChannelName) {
            this.masterChannelName = masterChannelName;
        }

        @SerializedName("masterChannelName")
        @Expose
        private String masterChannelName;

        public String getPublishKey() {
            return publishKey;
        }

        public void setPublishKey(String publishKey) {
            this.publishKey = publishKey;
        }

        public String getSubscribeKey() {
            return subscribeKey;
        }

        public void setSubscribeKey(String subscribeKey) {
            this.subscribeKey = subscribeKey;
        }

        public String getSecretKey() {
            return secretKey;
        }

        public void setSecretKey(String secretKey) {
            this.secretKey = secretKey;
        }

        public Boolean getSsl() {
            return ssl;
        }

        public void setSsl(Boolean ssl) {
            this.ssl = ssl;
        }

    }

    public class RazorpayData {

        @SerializedName("host")
        @Expose
        private String host;
        @SerializedName("baseURL")
        @Expose
        private String baseURL;
        @SerializedName("key_id")
        @Expose
        private String keyId;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getBaseURL() {
            return baseURL;
        }

        public void setBaseURL(String baseURL) {
            this.baseURL = baseURL;
        }

        public String getKeyId() {
            return keyId;
        }

        public void setKeyId(String keyId) {
            this.keyId = keyId;
        }

    }
}
