package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LastRechargeResponse implements Serializable {

    public class Data {

        @SerializedName("Last_Recharge_Amount")
        @Expose
        private Double lastRechargeAmount;
        @SerializedName("Balance_After_Last_Recharge")
        @Expose
        private Double balanceAfterLastRecharge;
        @SerializedName("Last_Bill_Amount")
        @Expose
        private Double lastBillAmount;

        public Double getLastRechargeAmount() {
            return lastRechargeAmount;
        }

        public void setLastRechargeAmount(Double lastRechargeAmount) {
            this.lastRechargeAmount = lastRechargeAmount;
        }

        public Double getBalanceAfterLastRecharge() {
            return balanceAfterLastRecharge;
        }

        public void setBalanceAfterLastRecharge(Double balanceAfterLastRecharge) {
            this.balanceAfterLastRecharge = balanceAfterLastRecharge;
        }

        public Double getLastBillAmount() {
            return lastBillAmount;
        }

        public void setLastBillAmount(Double lastBillAmount) {
            this.lastBillAmount = lastBillAmount;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Data")
        @Expose
        private Data data;

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }
}
