package com.vixspace.grocerytray.pojos;

public class MonthlyPojo {

    public MonthlyPojo() {
    }

    Integer pos;
    Boolean isSelect = true;

    public Integer getPos() {
        return pos;
    }

    public void setPos(Integer pos) {
        this.pos = pos;
    }

    public Boolean getSelect() {
        return isSelect;
    }

    public void setSelect(Boolean select) {
        isSelect = select;
    }
}
