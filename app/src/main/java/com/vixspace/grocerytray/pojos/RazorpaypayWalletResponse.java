package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RazorpaypayWalletResponse implements Serializable {

    public class Data {

        @SerializedName("Amount_RequestID")
        @Expose
        private String amountRequestID;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("Amount")
        @Expose
        private Double amount;
        @SerializedName("Payment_Gateway_Percentage")
        @Expose
        private Double paymentGatewayPercentage;
        @SerializedName("Payment_Gateway_Amount")
        @Expose
        private Double paymentGatewayAmount;
        @SerializedName("Total_Amount")
        @Expose
        private Double totalAmount;
        @SerializedName("Whether_Request")
        @Expose
        private Boolean whetherRequest;
        @SerializedName("Request_Time")
        @Expose
        private String requestTime;
        @SerializedName("Whether_Payment_Done")
        @Expose
        private Boolean whetherPaymentDone;
        @SerializedName("Payment_Time")
        @Expose
        private Object paymentTime;
        @SerializedName("PaymentID")
        @Expose
        private String paymentID;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public String getAmountRequestID() {
            return amountRequestID;
        }

        public void setAmountRequestID(String amountRequestID) {
            this.amountRequestID = amountRequestID;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public Double getAmount() {
            return amount;
        }

        public void setAmount(Double amount) {
            this.amount = amount;
        }

        public Double getPaymentGatewayPercentage() {
            return paymentGatewayPercentage;
        }

        public void setPaymentGatewayPercentage(Double paymentGatewayPercentage) {
            this.paymentGatewayPercentage = paymentGatewayPercentage;
        }

        public Double getPaymentGatewayAmount() {
            return paymentGatewayAmount;
        }

        public void setPaymentGatewayAmount(Double paymentGatewayAmount) {
            this.paymentGatewayAmount = paymentGatewayAmount;
        }

        public Double getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Double totalAmount) {
            this.totalAmount = totalAmount;
        }

        public Boolean getWhetherRequest() {
            return whetherRequest;
        }

        public void setWhetherRequest(Boolean whetherRequest) {
            this.whetherRequest = whetherRequest;
        }

        public String getRequestTime() {
            return requestTime;
        }

        public void setRequestTime(String requestTime) {
            this.requestTime = requestTime;
        }

        public Boolean getWhetherPaymentDone() {
            return whetherPaymentDone;
        }

        public void setWhetherPaymentDone(Boolean whetherPaymentDone) {
            this.whetherPaymentDone = whetherPaymentDone;
        }

        public Object getPaymentTime() {
            return paymentTime;
        }

        public void setPaymentTime(Object paymentTime) {
            this.paymentTime = paymentTime;
        }

        public String getPaymentID() {
            return paymentID;
        }

        public void setPaymentID(String paymentID) {
            this.paymentID = paymentID;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {
        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Data")
        @Expose
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }
}
