package com.vixspace.grocerytray.pojos;

public class WeeklyPojo {

    public WeeklyPojo() {
    }

    String Name;
    Boolean isSelect = true;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Boolean getSelect() {
        return isSelect;
    }

    public void setSelect(Boolean select) {
        isSelect = select;
    }
}
