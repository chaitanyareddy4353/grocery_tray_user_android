package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CreateSubRequestPojo implements Serializable {

        @SerializedName("ApiKey")
        @Expose
        private String apiKey;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("SessionID")
        @Expose
        private String sessionID;
        @SerializedName("CityID")
        @Expose
        private String cityID;

    public String getSubscriptionID() {
        return SubscriptionID;
    }

    public void setSubscriptionID(String subscriptionID) {
        SubscriptionID = subscriptionID;
    }

    @SerializedName("SubscriptionID")
        @Expose
        private String SubscriptionID;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("LocalityProductID")
        @Expose
        private String localityProductID;
        @SerializedName("AddressID")
        @Expose
        private String addressID;
        @SerializedName("Required_Quantity")
        @Expose
        private Integer requiredQuantity;
        @SerializedName("Subscription_Schedule_Type")
        @Expose
        private Integer subscriptionScheduleType;
        @SerializedName("Subscription_Start_Date")
        @Expose
        private String subscriptionStartDate;
        @SerializedName("Week_Array")
        @Expose
        private List<String> weekArray = null;
        @SerializedName("Day_Selection_Array")
        @Expose
        private List<Integer> daySelectionArray = null;

        public String getApiKey() {
            return apiKey;
        }

        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public String getSessionID() {
            return sessionID;
        }

        public void setSessionID(String sessionID) {
            this.sessionID = sessionID;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getLocalityProductID() {
            return localityProductID;
        }

        public void setLocalityProductID(String localityProductID) {
            this.localityProductID = localityProductID;
        }

        public String getAddressID() {
            return addressID;
        }

        public void setAddressID(String addressID) {
            this.addressID = addressID;
        }

        public Integer getRequiredQuantity() {
            return requiredQuantity;
        }

        public void setRequiredQuantity(Integer requiredQuantity) {
            this.requiredQuantity = requiredQuantity;
        }

        public Integer getSubscriptionScheduleType() {
            return subscriptionScheduleType;
        }

        public void setSubscriptionScheduleType(Integer subscriptionScheduleType) {
            this.subscriptionScheduleType = subscriptionScheduleType;
        }

        public String getSubscriptionStartDate() {
            return subscriptionStartDate;
        }

        public void setSubscriptionStartDate(String subscriptionStartDate) {
            this.subscriptionStartDate = subscriptionStartDate;
        }

        public List<String> getWeekArray() {
            return weekArray;
        }

        public void setWeekArray(List<String> weekArray) {
            this.weekArray = weekArray;
        }

        public List<Integer> getDaySelectionArray() {
            return daySelectionArray;
        }

        public void setDaySelectionArray(List<Integer> daySelectionArray) {
            this.daySelectionArray = daySelectionArray;
        }

}
