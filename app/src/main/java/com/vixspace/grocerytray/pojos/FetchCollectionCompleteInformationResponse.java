package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FetchCollectionCompleteInformationResponse implements Serializable {

    public class AllCollection {

        @SerializedName("CollectionID")
        @Expose
        private String collectionID;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("Collection_Number")
        @Expose
        private Integer collectionNumber;
        @SerializedName("Collection_Name")
        @Expose
        private String collectionName;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("Whether_Selected_Collection")
        @Expose
        private Boolean whetherSelectedCollection;

        public String getCollectionID() {
            return collectionID;
        }

        public void setCollectionID(String collectionID) {
            this.collectionID = collectionID;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public Integer getCollectionNumber() {
            return collectionNumber;
        }

        public void setCollectionNumber(Integer collectionNumber) {
            this.collectionNumber = collectionNumber;
        }

        public String getCollectionName() {
            return collectionName;
        }

        public void setCollectionName(String collectionName) {
            this.collectionName = collectionName;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public Boolean getWhetherSelectedCollection() {
            return whetherSelectedCollection;
        }

        public void setWhetherSelectedCollection(Boolean whetherSelectedCollection) {
            this.whetherSelectedCollection = whetherSelectedCollection;
        }

    }

    public class CategoryData {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("Image_Information")
        @Expose
        private ImageInformation imageInformation;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("Category_Number")
        @Expose
        private Integer categoryNumber;
        @SerializedName("Category_Title")
        @Expose
        private String categoryTitle;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public ImageInformation getImageInformation() {
            return imageInformation;
        }

        public void setImageInformation(ImageInformation imageInformation) {
            this.imageInformation = imageInformation;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public Integer getCategoryNumber() {
            return categoryNumber;
        }

        public void setCategoryNumber(Integer categoryNumber) {
            this.categoryNumber = categoryNumber;
        }

        public String getCategoryTitle() {
            return categoryTitle;
        }

        public void setCategoryTitle(String categoryTitle) {
            this.categoryTitle = categoryTitle;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }

    public class CollectionData {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("CollectionID")
        @Expose
        private String collectionID;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("Collection_Number")
        @Expose
        private Integer collectionNumber;
        @SerializedName("Collection_Name")
        @Expose
        private String collectionName;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCollectionID() {
            return collectionID;
        }

        public void setCollectionID(String collectionID) {
            this.collectionID = collectionID;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public Integer getCollectionNumber() {
            return collectionNumber;
        }

        public void setCollectionNumber(Integer collectionNumber) {
            this.collectionNumber = collectionNumber;
        }

        public String getCollectionName() {
            return collectionName;
        }

        public void setCollectionName(String collectionName) {
            this.collectionName = collectionName;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;


        @SerializedName("CollectionData")
        @Expose
        private CollectionData collectionData;
        @SerializedName("CategoryData")
        @Expose
        private CategoryData categoryData;
        @SerializedName("All_Collections")
        @Expose
        private List<AllCollection> allCollections = null;

        public CollectionData getCollectionData() {
            return collectionData;
        }

        public void setCollectionData(CollectionData collectionData) {
            this.collectionData = collectionData;
        }

        public CategoryData getCategoryData() {
            return categoryData;
        }

        public void setCategoryData(CategoryData categoryData) {
            this.categoryData = categoryData;
        }

        public List<AllCollection> getAllCollections() {
            return allCollections;
        }

        public void setAllCollections(List<AllCollection> allCollections) {
            this.allCollections = allCollections;
        }

    }

    public class ImageInformation {

        @SerializedName("ImageID")
        @Expose
        private String imageID;
        @SerializedName("Image50")
        @Expose
        private String image50;
        @SerializedName("Image100")
        @Expose
        private String image100;
        @SerializedName("Image250")
        @Expose
        private String image250;
        @SerializedName("Image550")
        @Expose
        private String image550;
        @SerializedName("Image900")
        @Expose
        private String image900;
        @SerializedName("ImageOriginal")
        @Expose
        private String imageOriginal;

        public String getImageID() {
            return imageID;
        }

        public void setImageID(String imageID) {
            this.imageID = imageID;
        }

        public String getImage50() {
            return image50;
        }

        public void setImage50(String image50) {
            this.image50 = image50;
        }

        public String getImage100() {
            return image100;
        }

        public void setImage100(String image100) {
            this.image100 = image100;
        }

        public String getImage250() {
            return image250;
        }

        public void setImage250(String image250) {
            this.image250 = image250;
        }

        public String getImage550() {
            return image550;
        }

        public void setImage550(String image550) {
            this.image550 = image550;
        }

        public String getImage900() {
            return image900;
        }

        public void setImage900(String image900) {
            this.image900 = image900;
        }

        public String getImageOriginal() {
            return imageOriginal;
        }

        public void setImageOriginal(String imageOriginal) {
            this.imageOriginal = imageOriginal;
        }

    }

}
