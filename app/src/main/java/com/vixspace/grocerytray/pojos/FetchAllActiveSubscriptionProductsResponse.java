package com.vixspace.grocerytray.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FetchAllActiveSubscriptionProductsResponse implements Serializable {

    public class AddressData {

        @SerializedName("PhoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("Address")
        @Expose
        private String address;
        @SerializedName("AddressID")
        @Expose
        private String addressID;
        @SerializedName("AddressType")
        @Expose
        private Integer addressType;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("Latitude")
        @Expose
        private Double latitude;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("Longitude")
        @Expose
        private Double longitude;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("Point")
        @Expose
        private List<Double> point = null;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAddressID() {
            return addressID;
        }

        public void setAddressID(String addressID) {
            this.addressID = addressID;
        }

        public Integer getAddressType() {
            return addressType;
        }

        public void setAddressType(Integer addressType) {
            this.addressType = addressType;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Double> getPoint() {
            return point;
        }

        public void setPoint(List<Double> point) {
            this.point = point;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public class Datum {

        @SerializedName("SubscriptionID")
        @Expose
        private String subscriptionID;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("ProductID")
        @Expose
        private String productID;
        @SerializedName("LocalityProductID")
        @Expose
        private String localityProductID;
        @SerializedName("AddressID")
        @Expose
        private String addressID;
        @SerializedName("Required_Quantity")
        @Expose
        private Integer requiredQuantity;
        @SerializedName("Subscription_Schedule_Type")
        @Expose
        private Integer subscriptionScheduleType;
        @SerializedName("Subscription_Start_Date")
        @Expose
        private String subscriptionStartDate;
        @SerializedName("Week_Array")
        @Expose
        private List<String> weekArray = null;
        @SerializedName("Day_Selection_Array")
        @Expose
        private List<Integer> daySelectionArray = null;
        @SerializedName("Subscription_Day_Calendar_Array")
        @Expose
        private List<String> subscriptionDayCalendarArray = null;
        @SerializedName("Last_Delivery_Date")
        @Expose
        private String lastDeliveryDate;
        @SerializedName("Subscription_Status")
        @Expose
        private Integer subscriptionStatus;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("Subscription_Status_Logs")
        @Expose
        private List<SubscriptionStatusLog> subscriptionStatusLogs = null;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("LocalityProductData")
        @Expose
        private LocalityProductData localityProductData;
        @SerializedName("User_Information")
        @Expose
        private UserInformation userInformation;
        @SerializedName("AddressData")
        @Expose
        private AddressData addressData;
        @SerializedName("Price_Information")
        @Expose
        private PriceInformation priceInformation;
        @SerializedName("Whether_Next_Delivery_Date")
        @Expose
        private Boolean whetherNextDeliveryDate;
        @SerializedName("Next_Delivery_Date")
        @Expose
        private String nextDeliveryDate;

        public String getSubscriptionID() {
            return subscriptionID;
        }

        public void setSubscriptionID(String subscriptionID) {
            this.subscriptionID = subscriptionID;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public String getProductID() {
            return productID;
        }

        public void setProductID(String productID) {
            this.productID = productID;
        }

        public String getLocalityProductID() {
            return localityProductID;
        }

        public void setLocalityProductID(String localityProductID) {
            this.localityProductID = localityProductID;
        }

        public String getAddressID() {
            return addressID;
        }

        public void setAddressID(String addressID) {
            this.addressID = addressID;
        }

        public Integer getRequiredQuantity() {
            return requiredQuantity;
        }

        public void setRequiredQuantity(Integer requiredQuantity) {
            this.requiredQuantity = requiredQuantity;
        }

        public Integer getSubscriptionScheduleType() {
            return subscriptionScheduleType;
        }

        public void setSubscriptionScheduleType(Integer subscriptionScheduleType) {
            this.subscriptionScheduleType = subscriptionScheduleType;
        }

        public String getSubscriptionStartDate() {
            return subscriptionStartDate;
        }

        public void setSubscriptionStartDate(String subscriptionStartDate) {
            this.subscriptionStartDate = subscriptionStartDate;
        }

        public List<String> getWeekArray() {
            return weekArray;
        }

        public void setWeekArray(List<String> weekArray) {
            this.weekArray = weekArray;
        }

        public List<Integer> getDaySelectionArray() {
            return daySelectionArray;
        }

        public void setDaySelectionArray(List<Integer> daySelectionArray) {
            this.daySelectionArray = daySelectionArray;
        }

        public List<String> getSubscriptionDayCalendarArray() {
            return subscriptionDayCalendarArray;
        }

        public void setSubscriptionDayCalendarArray(List<String> subscriptionDayCalendarArray) {
            this.subscriptionDayCalendarArray = subscriptionDayCalendarArray;
        }

        public String getLastDeliveryDate() {
            return lastDeliveryDate;
        }

        public void setLastDeliveryDate(String lastDeliveryDate) {
            this.lastDeliveryDate = lastDeliveryDate;
        }

        public Integer getSubscriptionStatus() {
            return subscriptionStatus;
        }

        public void setSubscriptionStatus(Integer subscriptionStatus) {
            this.subscriptionStatus = subscriptionStatus;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public List<SubscriptionStatusLog> getSubscriptionStatusLogs() {
            return subscriptionStatusLogs;
        }

        public void setSubscriptionStatusLogs(List<SubscriptionStatusLog> subscriptionStatusLogs) {
            this.subscriptionStatusLogs = subscriptionStatusLogs;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public LocalityProductData getLocalityProductData() {
            return localityProductData;
        }

        public void setLocalityProductData(LocalityProductData localityProductData) {
            this.localityProductData = localityProductData;
        }

        public UserInformation getUserInformation() {
            return userInformation;
        }

        public void setUserInformation(UserInformation userInformation) {
            this.userInformation = userInformation;
        }

        public AddressData getAddressData() {
            return addressData;
        }

        public void setAddressData(AddressData addressData) {
            this.addressData = addressData;
        }

        public PriceInformation getPriceInformation() {
            return priceInformation;
        }

        public void setPriceInformation(PriceInformation priceInformation) {
            this.priceInformation = priceInformation;
        }

        public Boolean getWhetherNextDeliveryDate() {
            return whetherNextDeliveryDate;
        }

        public void setWhetherNextDeliveryDate(Boolean whetherNextDeliveryDate) {
            this.whetherNextDeliveryDate = whetherNextDeliveryDate;
        }

        public String getNextDeliveryDate() {
            return nextDeliveryDate;
        }

        public void setNextDeliveryDate(String nextDeliveryDate) {
            this.nextDeliveryDate = nextDeliveryDate;
        }

    }

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("extras")
        @Expose
        private Extras extras;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Extras getExtras() {
            return extras;
        }

        public void setExtras(Extras extras) {
            this.extras = extras;
        }

    public class Extras {

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("msg")
        @Expose
        private String msg;

        @SerializedName("Data")
        @Expose
        private List<Datum> data = null;

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }

    }

    public class ImageData {

        @SerializedName("ImageID")
        @Expose
        private String imageID;
        @SerializedName("Image50")
        @Expose
        private String image50;
        @SerializedName("Image100")
        @Expose
        private String image100;
        @SerializedName("Image250")
        @Expose
        private String image250;
        @SerializedName("Image550")
        @Expose
        private String image550;
        @SerializedName("Image900")
        @Expose
        private String image900;
        @SerializedName("ImageOriginal")
        @Expose
        private String imageOriginal;

        public String getImageID() {
            return imageID;
        }

        public void setImageID(String imageID) {
            this.imageID = imageID;
        }

        public String getImage50() {
            return image50;
        }

        public void setImage50(String image50) {
            this.image50 = image50;
        }

        public String getImage100() {
            return image100;
        }

        public void setImage100(String image100) {
            this.image100 = image100;
        }

        public String getImage250() {
            return image250;
        }

        public void setImage250(String image250) {
            this.image250 = image250;
        }

        public String getImage550() {
            return image550;
        }

        public void setImage550(String image550) {
            this.image550 = image550;
        }

        public String getImage900() {
            return image900;
        }

        public void setImage900(String image900) {
            this.image900 = image900;
        }

        public String getImageOriginal() {
            return imageOriginal;
        }

        public void setImageOriginal(String imageOriginal) {
            this.imageOriginal = imageOriginal;
        }

    }

    public class ImageInformation {

        @SerializedName("ImageID")
        @Expose
        private String imageID;
        @SerializedName("Image50")
        @Expose
        private String image50;
        @SerializedName("Image100")
        @Expose
        private String image100;
        @SerializedName("Image250")
        @Expose
        private String image250;
        @SerializedName("Image550")
        @Expose
        private String image550;
        @SerializedName("Image900")
        @Expose
        private String image900;
        @SerializedName("ImageOriginal")
        @Expose
        private String imageOriginal;

        public String getImageID() {
            return imageID;
        }

        public void setImageID(String imageID) {
            this.imageID = imageID;
        }

        public String getImage50() {
            return image50;
        }

        public void setImage50(String image50) {
            this.image50 = image50;
        }

        public String getImage100() {
            return image100;
        }

        public void setImage100(String image100) {
            this.image100 = image100;
        }

        public String getImage250() {
            return image250;
        }

        public void setImage250(String image250) {
            this.image250 = image250;
        }

        public String getImage550() {
            return image550;
        }

        public void setImage550(String image550) {
            this.image550 = image550;
        }

        public String getImage900() {
            return image900;
        }

        public void setImage900(String image900) {
            this.image900 = image900;
        }

        public String getImageOriginal() {
            return imageOriginal;
        }

        public void setImageOriginal(String imageOriginal) {
            this.imageOriginal = imageOriginal;
        }

    }

    public class LocalityProductData {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("LocalityID")
        @Expose
        private String localityID;
        @SerializedName("ProductID")
        @Expose
        private String productID;
        @SerializedName("Available_Stock")
        @Expose
        private Integer availableStock;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("LocalityProductID")
        @Expose
        private String localityProductID;
        @SerializedName("MRP")
        @Expose
        private Double mRP;
        @SerializedName("Selling_Price")
        @Expose
        private Double sellingPrice;
        @SerializedName("Sold_Stock")
        @Expose
        private Integer soldStock;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("Total_Stock")
        @Expose
        private Integer totalStock;
        @SerializedName("Whether_Inventory")
        @Expose
        private Boolean whetherInventory;
        @SerializedName("Whether_Subscription")
        @Expose
        private Boolean whetherSubscription;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("Image_Information")
        @Expose
        private ImageInformation imageInformation;
        @SerializedName("Product_Brand")
        @Expose
        private String productBrand;
        @SerializedName("Product_Name")
        @Expose
        private String productName;
        @SerializedName("Product_Unit")
        @Expose
        private String productUnit;
        @SerializedName("Product_Description")
        @Expose
        private String productDescription;
        @SerializedName("Product_Type")
        @Expose
        private Integer productType;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("Category_Title")
        @Expose
        private String categoryTitle;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getLocalityID() {
            return localityID;
        }

        public void setLocalityID(String localityID) {
            this.localityID = localityID;
        }

        public String getProductID() {
            return productID;
        }

        public void setProductID(String productID) {
            this.productID = productID;
        }

        public Integer getAvailableStock() {
            return availableStock;
        }

        public void setAvailableStock(Integer availableStock) {
            this.availableStock = availableStock;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public String getLocalityProductID() {
            return localityProductID;
        }

        public void setLocalityProductID(String localityProductID) {
            this.localityProductID = localityProductID;
        }

        public Double getMRP() {
            return mRP;
        }

        public void setMRP(Double mRP) {
            this.mRP = mRP;
        }

        public Double getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(Double sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public Integer getSoldStock() {
            return soldStock;
        }

        public void setSoldStock(Integer soldStock) {
            this.soldStock = soldStock;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public Integer getTotalStock() {
            return totalStock;
        }

        public void setTotalStock(Integer totalStock) {
            this.totalStock = totalStock;
        }

        public Boolean getWhetherInventory() {
            return whetherInventory;
        }

        public void setWhetherInventory(Boolean whetherInventory) {
            this.whetherInventory = whetherInventory;
        }

        public Boolean getWhetherSubscription() {
            return whetherSubscription;
        }

        public void setWhetherSubscription(Boolean whetherSubscription) {
            this.whetherSubscription = whetherSubscription;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public ImageInformation getImageInformation() {
            return imageInformation;
        }

        public void setImageInformation(ImageInformation imageInformation) {
            this.imageInformation = imageInformation;
        }

        public String getProductBrand() {
            return productBrand;
        }

        public void setProductBrand(String productBrand) {
            this.productBrand = productBrand;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductUnit() {
            return productUnit;
        }

        public void setProductUnit(String productUnit) {
            this.productUnit = productUnit;
        }

        public String getProductDescription() {
            return productDescription;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }

        public Integer getProductType() {
            return productType;
        }

        public void setProductType(Integer productType) {
            this.productType = productType;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public String getCategoryTitle() {
            return categoryTitle;
        }

        public void setCategoryTitle(String categoryTitle) {
            this.categoryTitle = categoryTitle;
        }

    }

    public class PriceInformation {

        @SerializedName("Selling_Price")
        @Expose
        private Double sellingPrice;
        @SerializedName("Required_Quantity")
        @Expose
        private Double requiredQuantity;
        @SerializedName("Sub_Total_Amount")
        @Expose
        private Double subTotalAmount;
        @SerializedName("Subscription_Service_Charge")
        @Expose
        private Double subscriptionServiceCharge;
        @SerializedName("Final_Total_Amount")
        @Expose
        private Double finalTotalAmount;

        public Double getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(Double sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public Double getRequiredQuantity() {
            return requiredQuantity;
        }

        public void setRequiredQuantity(Double requiredQuantity) {
            this.requiredQuantity = requiredQuantity;
        }

        public Double getSubTotalAmount() {
            return subTotalAmount;
        }

        public void setSubTotalAmount(Double subTotalAmount) {
            this.subTotalAmount = subTotalAmount;
        }

        public Double getSubscriptionServiceCharge() {
            return subscriptionServiceCharge;
        }

        public void setSubscriptionServiceCharge(Double subscriptionServiceCharge) {
            this.subscriptionServiceCharge = subscriptionServiceCharge;
        }

        public Double getFinalTotalAmount() {
            return finalTotalAmount;
        }

        public void setFinalTotalAmount(Double finalTotalAmount) {
            this.finalTotalAmount = finalTotalAmount;
        }

    }

    public class SubscriptionStatusLog {

        @SerializedName("LogID")
        @Expose
        private String logID;
        @SerializedName("Subscription_Status")
        @Expose
        private Integer subscriptionStatus;
        @SerializedName("Comment")
        @Expose
        private String comment;
        @SerializedName("Time")
        @Expose
        private String time;

        public String getLogID() {
            return logID;
        }

        public void setLogID(String logID) {
            this.logID = logID;
        }

        public Integer getSubscriptionStatus() {
            return subscriptionStatus;
        }

        public void setSubscriptionStatus(Integer subscriptionStatus) {
            this.subscriptionStatus = subscriptionStatus;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

    }

    public class UserInformation {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("PhoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("DeviceID")
        @Expose
        private String deviceID;
        @SerializedName("EmailID")
        @Expose
        private String emailID;
        @SerializedName("Image_Data")
        @Expose
        private ImageData imageData;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("SessionID")
        @Expose
        private String sessionID;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("USERID")
        @Expose
        private String uSERID;
        @SerializedName("Wallet_Information")
        @Expose
        private WalletInformation walletInformation;
        @SerializedName("Whether_Basic_Information_Available")
        @Expose
        private Boolean whetherBasicInformationAvailable;
        @SerializedName("Whether_Image_Available")
        @Expose
        private Boolean whetherImageAvailable;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getDeviceID() {
            return deviceID;
        }

        public void setDeviceID(String deviceID) {
            this.deviceID = deviceID;
        }

        public String getEmailID() {
            return emailID;
        }

        public void setEmailID(String emailID) {
            this.emailID = emailID;
        }

        public ImageData getImageData() {
            return imageData;
        }

        public void setImageData(ImageData imageData) {
            this.imageData = imageData;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSessionID() {
            return sessionID;
        }

        public void setSessionID(String sessionID) {
            this.sessionID = sessionID;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getUSERID() {
            return uSERID;
        }

        public void setUSERID(String uSERID) {
            this.uSERID = uSERID;
        }

        public WalletInformation getWalletInformation() {
            return walletInformation;
        }

        public void setWalletInformation(WalletInformation walletInformation) {
            this.walletInformation = walletInformation;
        }

        public Boolean getWhetherBasicInformationAvailable() {
            return whetherBasicInformationAvailable;
        }

        public void setWhetherBasicInformationAvailable(Boolean whetherBasicInformationAvailable) {
            this.whetherBasicInformationAvailable = whetherBasicInformationAvailable;
        }

        public Boolean getWhetherImageAvailable() {
            return whetherImageAvailable;
        }

        public void setWhetherImageAvailable(Boolean whetherImageAvailable) {
            this.whetherImageAvailable = whetherImageAvailable;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public class WalletInformation {

        @SerializedName("Available_Amount")
        @Expose
        private Double availableAmount;
        @SerializedName("Credited_Amount")
        @Expose
        private Double creditedAmount;
        @SerializedName("Debited_Amount")
        @Expose
        private Double debitedAmount;
        @SerializedName("Reserved_Amount")
        @Expose
        private Double reservedAmount;

        public Double getAvailableAmount() {
            return availableAmount;
        }

        public void setAvailableAmount(Double availableAmount) {
            this.availableAmount = availableAmount;
        }

        public Double getCreditedAmount() {
            return creditedAmount;
        }

        public void setCreditedAmount(Double creditedAmount) {
            this.creditedAmount = creditedAmount;
        }

        public Double getDebitedAmount() {
            return debitedAmount;
        }

        public void setDebitedAmount(Double debitedAmount) {
            this.debitedAmount = debitedAmount;
        }

        public Double getReservedAmount() {
            return reservedAmount;
        }

        public void setReservedAmount(Double reservedAmount) {
            this.reservedAmount = reservedAmount;
        }

    }
}
