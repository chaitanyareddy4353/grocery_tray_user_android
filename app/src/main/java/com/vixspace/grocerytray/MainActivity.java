package com.vixspace.grocerytray;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.razorpay.PaymentResultListener;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.LoginSignUpActivity;
import com.vixspace.grocerytray.activities.ProductDetailsActivity;
import com.vixspace.grocerytray.fragments.HomeFragment;
import com.vixspace.grocerytray.fragments.MyCartFragment;
import com.vixspace.grocerytray.fragments.OrdersFragment;
import com.vixspace.grocerytray.fragments.SubscriptionFragment;
import com.vixspace.grocerytray.fragments.WalletFragment;
import com.vixspace.grocerytray.fragments.WishlistFragment;
import com.vixspace.grocerytray.utils.Allsharedpreference;
import com.vixspace.grocerytray.utils.CommonMethods;

public class MainActivity extends AppCompatActivity implements PaymentResultListener {

    BottomNavigationView bottomNavigationView;
    private long mLastClickTime = 0;
    PaymentResult paymentResult;
    Allsharedpreference allsharedpreference;
    CommonMethods commonMethods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        allsharedpreference = new Allsharedpreference(MainActivity.this);
        commonMethods = new CommonMethods(MainActivity.this);

        bottomNavigationView = findViewById(R.id.bottomNavView);

        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Fragment fragment = new HomeFragment();
        loadFragment(fragment);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            bottomNavigationView.getMenu().setGroupCheckable(0, true, true);

            if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
                return false;
            }
            mLastClickTime = SystemClock.elapsedRealtime();

            Fragment fragment;

            switch (menuItem.getItemId()) {

                case R.id.home :
                    fragment = new HomeFragment();
                    loadFragment(fragment);
                    return true;

                case R.id.orders :

                    if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
                        fragment = new OrdersFragment();
                        loadFragment(fragment);
                    } else {
                        Intent intent = new Intent(MainActivity.this, LoginSignUpActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }

                    return true;

                case R.id.wallet :
                    if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
                        fragment = new WalletFragment();
                        loadFragment(fragment);
                    } else {
                        Intent intent = new Intent(MainActivity.this, LoginSignUpActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                    return true;

                case R.id.wishlist :
                    if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
                        fragment = new SubscriptionFragment();
                        loadFragment(fragment);
                    } else {
                        Intent intent = new Intent(MainActivity.this, LoginSignUpActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                    return true;

                case R.id.cart :
                    fragment = new MyCartFragment();
                    loadFragment(fragment);
                    return true;

            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        if(bottomNavigationView.getSelectedItemId() ==R.id.home){
            super.onBackPressed();
        }else{
            bottomNavigationView.setSelectedItemId(R.id.home);
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        paymentResult.onResultRecieved(true);
    }

    @Override
    public void onPaymentError(int i, String s) {
        paymentResult.onResultRecieved(false);
    }

    public interface PaymentResult {
        void onResultRecieved(boolean isSuccess);
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof PaymentResult) {
            paymentResult = (PaymentResult) fragment;
        }
    }
}
