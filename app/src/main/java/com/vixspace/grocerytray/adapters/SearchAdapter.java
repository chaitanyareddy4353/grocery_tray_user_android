package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.ProductDetailsActivity;
import com.vixspace.grocerytray.pojos.FetchAllRelatedProducts;

import java.util.List;

public class SearchAdapter extends  RecyclerView.Adapter<SearchAdapter.MyViewHolder> {
    private Context context;
    List<FetchAllRelatedProducts.Datum> datumList;

    public SearchAdapter(Context context, List<FetchAllRelatedProducts.Datum> datumList) {
        this.context = context;
        this.datumList = datumList;

    }

    @Override
    public SearchAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.individual_search,parent,false);
        return new SearchAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final SearchAdapter.MyViewHolder holder, final int position) {

        holder.cardWhole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductDetailsActivity.class);
                intent.putExtra("productID",datumList.get(position).getLocalityProductID());
                context.startActivity(intent);
            }
        });


        Picasso.get().load(datumList.get(position).getImageInformation().getImage250()).into(holder.imgItem);
        holder.txtProductName.setText(datumList.get(position).getProductName());
        holder.txtproductAmount.setText("₹ "+datumList.get(position).getMRP());
        holder.txtproductSize.setText(datumList.get(position).getProductUnit());
//        holder.txtAmount.setText("₹"+datumList.get(position).getSellingPrice());
//        holder.txtBeforeDiscount.setText("₹"+datumList.get(position).getMRP());


    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtProductName,txtproductSize,txtproductAmount,txtSelectedQuantity;
        ImageView imgItem,imgSub,imgAdd;
        CardView cardWhole;


        public MyViewHolder(View itemView) {
            super(itemView);

            cardWhole = itemView.findViewById(R.id.cardWhole);
            imgItem = itemView.findViewById(R.id.imgItem);
            imgSub = itemView.findViewById(R.id.imgSub);
            imgAdd = itemView.findViewById(R.id.imgAdd);
            txtProductName = itemView.findViewById(R.id.txtProductName);
            txtproductSize = itemView.findViewById(R.id.txtproductSize);
            txtproductAmount = itemView.findViewById(R.id.txtproductAmount);
            txtSelectedQuantity = itemView.findViewById(R.id.txtSelectedQuantity);

        }
    }



}
