package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.CreateSubscriptionActivity;
import com.vixspace.grocerytray.activities.LoginSignUpActivity;
import com.vixspace.grocerytray.activities.ProductDetailsActivity;
import com.vixspace.grocerytray.pojos.FetchAllCategoryCollectionResponse;
import com.vixspace.grocerytray.pojos.FetchAllCollectionproductsResponse;
import com.vixspace.grocerytray.utils.Allsharedpreference;

import java.util.List;

public class CollectionProductsAdapter extends  RecyclerView.Adapter<CollectionProductsAdapter.MyViewHolder> {
    private Context context;
    List<FetchAllCollectionproductsResponse.Datum> datumList;
    Allsharedpreference allsharedpreference;

    public CollectionProductsAdapter(Context context, List<FetchAllCollectionproductsResponse.Datum> datumList) {
        this.context = context;
        this.datumList = datumList;
        this.allsharedpreference = new Allsharedpreference(context);
    }

    @Override
    public CollectionProductsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.individual_item_check,parent,false);
        return new CollectionProductsAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final CollectionProductsAdapter.MyViewHolder holder, final int position) {

        holder.cardWhole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductDetailsActivity.class);
                intent.putExtra("productID",datumList.get(position).getLocalityProductID());
                context.startActivity(intent);
            }
        });


        if(datumList.get(position).getImageInformation().getImage250().equals("")||datumList.get(position).getImageInformation().getImage250().equals(null)){

        } else {
            Picasso.get().load(datumList.get(position).getImageInformation().getImage250()).into(holder.imgProduct);
        }

        holder.txtProductName.setText(datumList.get(position).getProductName());
        holder.txtCompany.setText(datumList.get(position).getProductBrand());
        holder.txtWeight.setText(datumList.get(position).getProductUnit());
        holder.txtAmount.setText("₹"+datumList.get(position).getSellingPrice());
        holder.txtBeforeDiscount.setText("₹"+datumList.get(position).getMRP());
//        holder.txtDiscountPercent.setText((datumList.get(position).getMRP()-datumList.get(position).getSellingPrice()
//                /datumList.get(position).getMRP())*100+" % off");


        if (datumList.get(position).getWhetherSubscription()){
            holder.txtSubscribeAmount.setText("Subscribe @ ₹ "+datumList.get(position).getSellingPrice());
            holder.txtSubscribeAmount.setVisibility(View.VISIBLE);
        } else {
            holder.txtSubscribeAmount.setVisibility(View.INVISIBLE);
        }

        if (datumList.get(position).getCategoryTitle().equals("News Paper")||datumList.get(position).getCategoryTitle().equals("Water Cans")){
            if (datumList.get(position).getWhetherSubscription()){
                holder.txtBuyOnce.setVisibility(View.INVISIBLE);
                holder.txtSubscribeAmount.setVisibility(View.VISIBLE);
            } else {
                holder.txtBuyOnce.setVisibility(View.INVISIBLE);
                holder.txtSubscribeAmount.setVisibility(View.INVISIBLE);
            }
        }

        holder.txtSubscribeAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (allsharedpreference.getShared_Booleen(Allsharedpreference.Registered)){
                    Intent intent = new Intent(context, CreateSubscriptionActivity.class);
                    intent.putExtra("productID",datumList.get(position).getLocalityProductID());
                    intent.putExtra("brand",datumList.get(position).getProductBrand());
                    intent.putExtra("name",datumList.get(position).getProductName());
                    intent.putExtra("size",datumList.get(position).getProductUnit());
                    intent.putExtra("img",datumList.get(position).getImageInformation().getImage250());
                    intent.putExtra("sp","₹"+datumList.get(position).getSellingPrice());
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, LoginSignUpActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);
                }
            }
        });

        if (datumList.get(position).getMRP()==datumList.get(position).getSellingPrice()){
            holder.txtBeforeDiscount.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imgProduct,imgPlus,imgMinus;
        TextView txtCompany,txtProductName,txtWeight,txtAmount,txtBeforeDiscount,txtDiscountPercent,
                txtSubscribeAmount,txtBuyOnce,txtQuantity;
        RelativeLayout relativeCounter;
        CardView cardWhole;

        public MyViewHolder(View itemView) {
            super(itemView);

            cardWhole = itemView.findViewById(R.id.cardWhole);
            imgProduct = itemView.findViewById(R.id.imgProduct);
            imgPlus = itemView.findViewById(R.id.imgPlus);
            imgMinus = itemView.findViewById(R.id.imgMinus);
            relativeCounter = itemView.findViewById(R.id.relativeCounter);
            txtCompany = itemView.findViewById(R.id.txtCompany);
            txtProductName = itemView.findViewById(R.id.txtProductName);
            txtWeight = itemView.findViewById(R.id.txtWeight);
            txtAmount = itemView.findViewById(R.id.txtAmount);
            txtBeforeDiscount = itemView.findViewById(R.id.txtBeforeDiscount);
            txtDiscountPercent = itemView.findViewById(R.id.txtDiscountPercent);
            txtSubscribeAmount = itemView.findViewById(R.id.txtSubscribeAmount);
            txtBuyOnce = itemView.findViewById(R.id.txtBuyOnce);
            txtQuantity = itemView.findViewById(R.id.txtQuantity);

        }
    }



}
