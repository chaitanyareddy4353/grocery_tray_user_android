package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.MonthlyPojo;
import com.vixspace.grocerytray.pojos.WeeklyPojo;

import java.util.List;

public class MonthlyAdapter extends  RecyclerView.Adapter<MonthlyAdapter.MyViewHolder> {
    private Context context;
    List<MonthlyPojo> datumList;
    ToucherM toucherm;
    int lastSelectedPosition=0;

    public MonthlyAdapter(Context context, List<MonthlyPojo> datumList, ToucherM toucherm) {
        this.context = context;
        this.datumList = datumList;
        this.toucherm = toucherm;
    }

    @Override
    public MonthlyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.week_individual,parent,false);
        return new MonthlyAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MonthlyAdapter.MyViewHolder holder, final int position) {

        holder.txtUnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datumList.get(position).setSelect(true);
                notifyItemChanged(position,datumList.get(position));
                holder.txtSelect.setVisibility(View.VISIBLE);
                holder.txtUnSelect.setVisibility(View.GONE);
                toucherm.onToucherm((position),datumList);
            }
        });

        holder.txtSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datumList.get(position).setSelect(false);
                notifyItemChanged(position,datumList.get(position));
                holder.txtSelect.setVisibility(View.GONE);
                holder.txtUnSelect.setVisibility(View.VISIBLE);
                toucherm.onToucherm((position),datumList);
            }
        });

        holder.txtUnSelect.setText(String.valueOf(position+1));
        holder.txtSelect.setText(String.valueOf(position+1));

        if (datumList.get(position).getSelect()){
            holder.txtSelect.setVisibility(View.VISIBLE);
            holder.txtUnSelect.setVisibility(View.GONE);
        } else {
            holder.txtSelect.setVisibility(View.GONE);
            holder.txtUnSelect.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtUnSelect,txtSelect;
//        RelativeLayout relativeWhole;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtSelect = itemView.findViewById(R.id.txtSelect);
//            relativeWhole = itemView.findViewById(R.id.relativeWhole);
            txtUnSelect = itemView.findViewById(R.id.txtUnSelect);

        }
    }

    public interface ToucherM {
        void onToucherm(Integer pos, List<MonthlyPojo> weeklyPojos);
    }

}
