package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.FetchAllActiveSubscriptionProductsResponse;

import java.util.List;

public class TerminatedSubsAdapter extends  RecyclerView.Adapter<TerminatedSubsAdapter.MyViewHolder> {
    private Context context;
    List<FetchAllActiveSubscriptionProductsResponse.Datum> datumList;
    Subs toucher;
    int lastSelectedPosition=0;

    public TerminatedSubsAdapter(Context context, List<FetchAllActiveSubscriptionProductsResponse.Datum> datumList, Subs toucher) {
        this.context = context;
        this.datumList = datumList;
        this.toucher = toucher;
    }

    @Override
    public TerminatedSubsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.individual_terminate_subs,parent,false);
        return new TerminatedSubsAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final TerminatedSubsAdapter.MyViewHolder holder, final int position) {

//        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                toucher.onModify(datumList.get(position).getSubscriptionID(),false,false,true);
//            }
//        });
//        holder.txtModify.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                toucher.onModify(datumList.get(position).getSubscriptionID(),false,true,false);
//            }
//        });
        holder.txtResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toucher.onModify(datumList.get(position).getSubscriptionID(),true,false,false);
            }
        });

        holder.linearDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(context,)
            }
        });

        holder.txtTotal.setText("₹"+String.valueOf(Double.valueOf(datumList.get(position).getRequiredQuantity())*datumList.get(position).getLocalityProductData().getSellingPrice()));

        if (datumList.get(position).getSubscriptionScheduleType()==1){
            holder.txtFrequency.setText("Daily");
        } else if (datumList.get(position).getSubscriptionScheduleType()==2){
            holder.txtFrequency.setText("Alternate days");
        } else if (datumList.get(position).getSubscriptionScheduleType()==3){
            holder.txtFrequency.setText("Every 3 days");
        } else if (datumList.get(position).getSubscriptionScheduleType()==4){
//            holder.txtFrequency.setText("Weekly");
            if (datumList.get(position).getWeekArray()!=null){
                if (datumList.get(position).getWeekArray().size()==2){
                    if (datumList.get(position).getWeekArray().get(0).equals("Saturday")||datumList.get(position).getWeekArray().get(0).equals("Sunday")){
                        if (datumList.get(position).getWeekArray().get(1).equals("Saturday")||datumList.get(position).getWeekArray().get(1).equals("Sunday")){
                            holder.txtFrequency.setText("Weekends");
                        }
                    } else {
                        holder.txtFrequency.setText("Weekly");
                    }
                } else {
                    holder.txtFrequency.setText("Weekly");
                }
            }
        } else if (datumList.get(position).getSubscriptionScheduleType()==5){
            holder.txtFrequency.setText("Monthly");
        }


        Picasso.get().load(datumList.get(position).getLocalityProductData().getImageInformation().getImage250()).into(holder.imgProduct);

        holder.txtProductDetails.setText(datumList.get(position).getLocalityProductData().getProductBrand() +" - "+
                datumList.get(position).getLocalityProductData().getProductName()+"\n"+datumList.get(position).getLocalityProductData().getProductUnit());

        holder.txtQty.setText("Qty "+datumList.get(position).getRequiredQuantity());
    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtModify,txtDelete,txtPause,txtProductDetails,txtTotal,txtFrequency,txtQty,txtResume;
        LinearLayout linearDetails;
        ImageView imgProduct;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtResume = itemView.findViewById(R.id.txtResume);
            txtQty = itemView.findViewById(R.id.txtQty);
            txtFrequency = itemView.findViewById(R.id.txtFrequency);
            txtTotal = itemView.findViewById(R.id.txtTotal);
            txtProductDetails = itemView.findViewById(R.id.txtProductDetails);
            txtModify = itemView.findViewById(R.id.txtModify);
            txtPause = itemView.findViewById(R.id.txtPause);
            txtDelete = itemView.findViewById(R.id.txtDelete);
            linearDetails = itemView.findViewById(R.id.linearDetails);
            imgProduct = itemView.findViewById(R.id.imgProduct);

        }
    }

    public interface Subs {
        void onModify(String Id, Boolean isPaused, Boolean isModified, Boolean isDelete);
    }

}
