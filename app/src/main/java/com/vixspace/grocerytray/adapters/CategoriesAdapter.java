package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.AllCollectionProductsActivity;
import com.vixspace.grocerytray.pojos.FetchAllLocalityCategoriesResponse;

import java.util.List;

public class CategoriesAdapter extends  RecyclerView.Adapter<CategoriesAdapter.MyViewHolder> {
    private Context context;
    List<FetchAllLocalityCategoriesResponse.Datum> datumList;

    public CategoriesAdapter(Context context, List<FetchAllLocalityCategoriesResponse.Datum> datumList) {
        this.context = context;
        this.datumList = datumList;

    }

    @Override
    public CategoriesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.individual_category,parent,false);
        return new CategoriesAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoriesAdapter.MyViewHolder holder, final int position) {

        Picasso.get().load(datumList.get(position).getImageInformation().getImageOriginal()).into(holder.imgCat);
        holder.txtCat.setText(datumList.get(position).getCategoryTitle());
        holder.cardWhole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //call api no 19
                Intent intent = new Intent(context, AllCollectionProductsActivity.class);
                intent.putExtra("CategoryName",datumList.get(position).getCategoryTitle());
                intent.putExtra("CategoryID",datumList.get(position).getCategoryID());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtCat;
        ImageView imgCat;
        CardView cardWhole;

        public MyViewHolder(View itemView) {
            super(itemView);

            cardWhole = itemView.findViewById(R.id.cardWhole);
            imgCat = itemView.findViewById(R.id.imgCat);
            txtCat = itemView.findViewById(R.id.txtCat);

        }
    }



}
