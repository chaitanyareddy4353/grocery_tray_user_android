package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.FetchAllAvailableDatesResponse;
import com.vixspace.grocerytray.pojos.FetchAllCollectionproductsResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SelecteddatesAdapter extends  RecyclerView.Adapter<SelecteddatesAdapter.MyViewHolder> {
    private Context context;
    List<FetchAllAvailableDatesResponse.Datum> datumList;
    Dates dates;
    String newDateStr,newDateStr1;
    int lastSelectedPosition=0;

    public SelecteddatesAdapter(Context context, List<FetchAllAvailableDatesResponse.Datum> datumList,Dates dates) {
        this.context = context;
        this.datumList = datumList;
        this.dates = dates;

    }

    @Override
    public SelecteddatesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.individual_dates,parent,false);
        return new SelecteddatesAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final SelecteddatesAdapter.MyViewHolder holder, final int position) {

        holder.relativeWhole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastSelectedPosition = position;
                notifyDataSetChanged();
//                dates.onDateTouched(datumList.get(position).getDate());
            }
        });

        SimpleDateFormat form = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = form.parse(datumList.get(position).getDate());
        } catch (ParseException e) {

            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd");
        newDateStr = postFormater.format(date);

        holder.txtDay.setText(datumList.get(position).getDay());
        holder.txtDate.setText(newDateStr);

        if(lastSelectedPosition == position) {
            holder.linearWholeBackground.setVisibility(View.VISIBLE);
            dates.onDateTouched(datumList.get(position).getDate());

        } else {
            holder.linearWholeBackground.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtDay,txtDate;
        LinearLayout linearWholeBackground;
        RelativeLayout relativeWhole,linearWhole;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtDay = itemView.findViewById(R.id.txtDay);
            txtDate = itemView.findViewById(R.id.txtDate);
            linearWhole = itemView.findViewById(R.id.linearWhole);
            linearWholeBackground = itemView.findViewById(R.id.linearWholeBackground);
            relativeWhole = itemView.findViewById(R.id.relativeWhole);

        }
    }

    public interface Dates {
        void onDateTouched(String Id);
    }

}
