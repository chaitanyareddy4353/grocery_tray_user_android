package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.FetchAllAvailableSlotsResponse;
import com.vixspace.grocerytray.pojos.FetchAllCollectionproductsResponse;

import java.util.List;

public class TimeSlotsAdapter extends  RecyclerView.Adapter<TimeSlotsAdapter.MyViewHolder> {
    private Context context;
    List<FetchAllAvailableSlotsResponse.Datum> datumList;
    Toucher toucher;
    int lastSelectedPosition=0;

    public TimeSlotsAdapter(Context context, List<FetchAllAvailableSlotsResponse.Datum> datumList,Toucher toucher) {
        this.context = context;
        this.datumList = datumList;
        this.toucher = toucher;
    }

    @Override
    public TimeSlotsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.individual_slots,parent,false);
        return new TimeSlotsAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final TimeSlotsAdapter.MyViewHolder holder, final int position) {

        holder.relativeWhole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                toucher.onToucher(datumList.get(position).getSLOTID(),datumList.get(position).getTime());
                lastSelectedPosition = position;
                notifyDataSetChanged();
            }
        });

        holder.txtTime.setText(datumList.get(position).getTime());

        if(lastSelectedPosition == position) {
            holder.txtTimeBackground.setVisibility(View.VISIBLE);
            toucher.onToucher(datumList.get(position).getSLOTID(),datumList.get(position).getTime());

        } else {
            holder.txtTimeBackground.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtTime,txtTimeBackground;
        RelativeLayout relativeWhole;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtTime = itemView.findViewById(R.id.txtTime);
            relativeWhole = itemView.findViewById(R.id.relativeWhole);
            txtTimeBackground = itemView.findViewById(R.id.txtTimeBackground);

        }
    }

    public interface Toucher {
        void onToucher(String Id,String time);
    }

}
