package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.CartInfoModel;
import com.vixspace.grocerytray.pojos.FetchAllFavProductsResponse;
import com.vixspace.grocerytray.pojos.UpcomingOrdersResponse;
import com.vixspace.grocerytray.utils.Allsharedpreference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class WishListAdapter extends  RecyclerView.Adapter<WishListAdapter.MyViewHolder> {
    private Context context;
    List<FetchAllFavProductsResponse.Datum> datumList;
    String newDateStr,newDateStr1;
    Allsharedpreference allsharedpreference;
    Integer Indicator = 0;
    Delete delete;

    public WishListAdapter(Context context, List<FetchAllFavProductsResponse.Datum> datumList, Integer indicator,Delete delete) {
        this.context = context;
        this.datumList = datumList;
        this.allsharedpreference = new Allsharedpreference(context);
        this.Indicator = indicator;
        this.delete = delete;
    }

    @Override
    public WishListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.individual_wishlist,parent,false);
        return new WishListAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final WishListAdapter.MyViewHolder holder, final int position) {

            SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = form.parse(datumList.get(position).getCreatedAt());
            } catch (ParseException e) {

                e.printStackTrace();
            }
            SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM yyyy");
            newDateStr = postFormater.format(date);

            holder.txtDate.setText("Item added on " +newDateStr);
            holder.txtProductName.setText(datumList.get(position).getProductName());
            holder.txtTotal.setText(""+datumList.get(position).getSellingPrice());
            holder.txtQuantity.setText(datumList.get(position).getProductUnit());

        Picasso.get().load(datumList.get(position).getImageInformation().getImage250()).into(holder.imgProduct);

        holder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete.onDeleted(datumList.get(position).getLocalityProductID(),position,false,datumList.get(position));
            }
        });

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete.onDeleted(datumList.get(position).getFavouriteProductData().getFavouriteID(),position,true,datumList.get(position));
            }
        });



    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtTotal,txtDate,txtSelectedQuantity,txtQuantity,txtProductName;
        ImageView imgDelete,imgProduct;
        Button btnAddToCart;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtTotal = itemView.findViewById(R.id.txtTotal);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtSelectedQuantity = itemView.findViewById(R.id.txtSelectedQuantity);
            txtQuantity = itemView.findViewById(R.id.txtQuantity);
            txtProductName = itemView.findViewById(R.id.txtProductName);
            imgDelete = itemView.findViewById(R.id.imgDelete);
            imgProduct = itemView.findViewById(R.id.imgProduct);
            btnAddToCart = itemView.findViewById(R.id.btnAddToCart);
        }
    }

    public interface Delete {
        void onDeleted(String cartInfoModel, Integer position,Boolean isDelete,FetchAllFavProductsResponse.Datum datum);
    }

}
