package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.FetchAllAvailableDatesResponse;
import com.vixspace.grocerytray.pojos.ListAllUserAddressResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AddressAdapter extends  RecyclerView.Adapter<AddressAdapter.MyViewHolder> {
    private Context context;
    List<ListAllUserAddressResponse.Datum> datumList;
    Dates dates;
    String newDateStr,newDateStr1;
    int lastSelectedPosition=0;

    public AddressAdapter(Context context, List<ListAllUserAddressResponse.Datum> datumList, Dates dates) {
        this.context = context;
        this.datumList = datumList;
        this.dates = dates;

    }

    @Override
    public AddressAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.individual_address,parent,false);
        return new AddressAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final AddressAdapter.MyViewHolder holder, final int position) {

        holder.linearWhole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dates.onDateTouched(datumList.get(position).getAddressID(),datumList.get(position).getAddress());
                lastSelectedPosition = position;
                notifyDataSetChanged();
            }
        });

        holder.radioDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dates.onDateTouched(datumList.get(position).getAddressID(),datumList.get(position).getAddress());
                lastSelectedPosition = position;
                notifyDataSetChanged();
            }
        });

        if (datumList.get(position).getAddressType()==1){
            holder.radioDefault.setText("Home");
        } else if (datumList.get(position).getAddressType()==2){
            holder.radioDefault.setText("Work");
        } else if (datumList.get(position).getAddressType()==3){
            holder.radioDefault.setText("Other");
        }


        if(lastSelectedPosition == position) {
//            dates.onDateTouched(datumList.get(position).getAddressID(),datumList.get(position).getAddress());
            holder.radioDefault.setChecked(true);

        } else {
            holder.radioDefault.setChecked(false);
        }

        holder.txtAddress.setText(datumList.get(position).getName()+"\n"+datumList.get(position).getAddress()+"\n"+
                datumList.get(position).getPhoneNumber());
    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtAddress;
        LinearLayout linearWhole;
        RadioButton radioDefault;

        public MyViewHolder(View itemView) {
            super(itemView);

            radioDefault = itemView.findViewById(R.id.radioDefault);
            txtAddress = itemView.findViewById(R.id.txtAddress);
            linearWhole = itemView.findViewById(R.id.linearWhole);
        }
    }

    public interface Dates {
        void onDateTouched(String Id,String name);
    }

}
