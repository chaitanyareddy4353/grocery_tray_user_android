package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.FetchOrderCompleteResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class TrackAdapter extends  RecyclerView.Adapter<TrackAdapter.MyViewHolder> {
    private Context context;
    List<FetchOrderCompleteResponse.OrderStatusLog> datumList;
    String newDateStr,newDateStrs;

    public TrackAdapter(Context context, List<FetchOrderCompleteResponse.OrderStatusLog> datumList) {
        this.context = context;
        this.datumList = datumList;
    }

    @Override
    public TrackAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.individual_track,parent,false);
        return new TrackAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final TrackAdapter.MyViewHolder holder, final int position) {

        if (position==0){
            holder.viewBottom.setVisibility(View.GONE);
            holder.viewFollowing.setVisibility(View.GONE);
        }

        if (position==(datumList.size()-1)){
            holder.viewTop.setVisibility(View.GONE);
            holder.txtOrderComment.setTextColor(context.getResources().getColor(R.color.black));
            holder.txtDate.setTextColor(context.getResources().getColor(R.color.black));
            holder.txtTime.setTextColor(context.getResources().getColor(R.color.black));
        }

        holder.txtOrderComment.setText(datumList.get(position).getComment());


        SimpleDateFormat form = new SimpleDateFormat("dd-MM-yyyy,HH:mm:ss a");
        java.util.Date date = null;
        try {
            date = form.parse(datumList.get(position).getTime());
        } catch (ParseException e) {

            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("HH:mm a");
        newDateStr = postFormater.format(date);


        SimpleDateFormat postFormaters = new SimpleDateFormat("dd.MM.yyyy");
        newDateStrs = postFormaters.format(date);

        holder.txtTime.setText(newDateStr);
        holder.txtDate.setText(newDateStrs);

    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtTime,txtDate,txtOrderComment;
        View viewTop,viewBottom,viewFollowing;

        public MyViewHolder(View itemView) {
            super(itemView);

            viewBottom = itemView.findViewById(R.id.viewBottom);
            viewTop = itemView.findViewById(R.id.viewTop);
            txtTime = itemView.findViewById(R.id.txtTime);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtOrderComment = itemView.findViewById(R.id.txtOrderComment);
            viewFollowing = itemView.findViewById(R.id.viewFollowing);

        }
    }

}
