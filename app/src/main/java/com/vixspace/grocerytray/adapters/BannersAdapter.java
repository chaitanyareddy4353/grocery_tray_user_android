package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.AllCategoriesActivity;
import com.vixspace.grocerytray.activities.AllCollectionProductsActivity;
import com.vixspace.grocerytray.activities.ApplyCoupon;
import com.vixspace.grocerytray.activities.ProductDetailsActivity;
import com.vixspace.grocerytray.activities.TermsAndConditionsActivity;
import com.vixspace.grocerytray.pojos.FetchAllHomeSliderItemsResponse;
import com.vixspace.grocerytray.pojos.FetchAllLocalityCategoriesResponse;

import java.util.List;

public class BannersAdapter extends  RecyclerView.Adapter<BannersAdapter.MyViewHolder> {
    private Context context;
    List<FetchAllHomeSliderItemsResponse.Datum> datumList;

    public BannersAdapter(Context context, List<FetchAllHomeSliderItemsResponse.Datum> datumList) {
        this.context = context;
        this.datumList = datumList;

    }

    @Override
    public BannersAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.individual_banner,parent,false);
        return new BannersAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final BannersAdapter.MyViewHolder holder, final int position) {

        Picasso.get().load(datumList.get(position).getDocumentInformation().getDocumentURL()).into(holder.imgBanner);
//        holder.imgBanner.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //call api no 19
//            }
//        });
        //1.Category 2.Collection 3. Product 4.Coupon 5.Web Page
        holder.imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (datumList.get(position).getItemType().equals("1")){
                    Intent intent = new Intent(context, AllCategoriesActivity.class);
                    context.startActivity(intent);
                } else if (datumList.get(position).getItemType().equals("2")){
                    Intent intent = new Intent(context, AllCollectionProductsActivity.class);
                    intent.putExtra("CategoryName",datumList.get(position).getCategoryData().getCategoryTitle());
                    intent.putExtra("CategoryID",datumList.get(position).getCategoryID());
                    context.startActivity(intent);
                } else if (datumList.get(position).getItemType().equals("3")){
                    Intent intent = new Intent(context, ProductDetailsActivity.class);
                    intent.putExtra("productID",datumList.get(position).getProductData().getProductID());
                    context.startActivity(intent);
                } else if (datumList.get(position).getItemType().equals("4")){
                    Intent intent = new Intent(context, ApplyCoupon.class);
                    context.startActivity(intent);
                } else if (datumList.get(position).getItemType().equals("5")){
                    Intent intent = new Intent(context, TermsAndConditionsActivity.class);
                    intent.putExtra("indicator","1");
                    intent.putExtra("web",datumList.get(position).getPageURL());
                    context.startActivity(intent);

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imgBanner;

        public MyViewHolder(View itemView) {
            super(itemView);

            imgBanner = itemView.findViewById(R.id.imgBanner);

        }
    }



}
