package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.FetchAllAvailableSlotsResponse;
import com.vixspace.grocerytray.pojos.WeeklyPojo;

import java.util.List;

public class WeeklyAdapter extends  RecyclerView.Adapter<WeeklyAdapter.MyViewHolder> {
    private Context context;
    List<WeeklyPojo> datumList;
    Toucher toucher;
    int lastSelectedPosition=0;

    public WeeklyAdapter(Context context, List<WeeklyPojo> datumList, Toucher toucher) {
        this.context = context;
        this.datumList = datumList;
        this.toucher = toucher;
    }

    @Override
    public WeeklyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.week_individual,parent,false);
        return new WeeklyAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final WeeklyAdapter.MyViewHolder holder, final int position) {

        holder.txtUnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datumList.get(position).setSelect(true);
                notifyItemChanged(position,datumList.get(position));
                holder.txtSelect.setVisibility(View.VISIBLE);
                holder.txtUnSelect.setVisibility(View.GONE);
                toucher.onToucher(datumList.get(position).getName(),(position),datumList);
            }
        });

        holder.txtSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datumList.get(position).setSelect(false);
                notifyItemChanged(position,datumList.get(position));
                holder.txtSelect.setVisibility(View.GONE);
                holder.txtUnSelect.setVisibility(View.VISIBLE);
                toucher.onToucher(datumList.get(position).getName(),(position),datumList);
            }
        });

        holder.txtUnSelect.setText(datumList.get(position).getName());
        holder.txtSelect.setText(datumList.get(position).getName());

        if (datumList.get(position).getSelect()){
            holder.txtSelect.setVisibility(View.VISIBLE);
            holder.txtUnSelect.setVisibility(View.GONE);
        } else {
            holder.txtSelect.setVisibility(View.GONE);
            holder.txtUnSelect.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtUnSelect,txtSelect;
//        RelativeLayout relativeWhole;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtSelect = itemView.findViewById(R.id.txtSelect);
//            relativeWhole = itemView.findViewById(R.id.relativeWhole);
            txtUnSelect = itemView.findViewById(R.id.txtUnSelect);

        }
    }

    public interface Toucher {
        void onToucher(String Id, Integer pos,List<WeeklyPojo> weeklyPojos);
    }

}
