package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.WalletLogsResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;


public class TransactionsAdapter extends  RecyclerView.Adapter<TransactionsAdapter.MyViewHolder> {
    private Context context;
    List<WalletLogsResponse.Datum> datumList;
    Toucher toucher;
    String newDateStr,newDateStrs;

    public TransactionsAdapter(Context context, List<WalletLogsResponse.Datum> datumList, Toucher toucher) {
        this.context = context;
        this.toucher = toucher;
        this.datumList = datumList;

    }

    @Override
    public TransactionsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_history_individual,parent,false);
        return new TransactionsAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final TransactionsAdapter.MyViewHolder holder, final int position) {

        if (datumList.get(position).getType()==1){
            holder.txtReason.setText("Amount added from Razorpay");
        } else if (datumList.get(position).getType()==2){
            holder.txtReason.setText("Debited Payment gateway charges");
        } else if (datumList.get(position).getType()==3){
            holder.txtReason.setText("Debited for Placing Order");
        } else if (datumList.get(position).getType()==4){
            holder.txtReason.setText("Debited for Subscription Product Delivery from Wallet");
        } else if (datumList.get(position).getType()==5){
            holder.txtReason.setText("Credit by Paying to Driver at Subscription Delivery");
        } else if (datumList.get(position).getType()==6){
            holder.txtReason.setText("Cancel order refund credit");
        } else if (datumList.get(position).getType()==7){
            holder.txtReason.setText("Reserve Amount");
        } else if (datumList.get(position).getType()==8){
            holder.txtReason.setText("Debited for Subscription Product Delivery from Reserve Amount");
        } else if (datumList.get(position).getType()==9){
            holder.txtReason.setText("Debited for Subscription Product Delivery from Complete Reserve Amount and partial from Wallet");
        }

        holder.txtAmt.setText("₹ "+String.valueOf(datumList.get(position).getAmount()));

        String str1 = datumList.get(position).getTime();
        char[] ch1 = new char[str1.length()];

        String one1= String.valueOf(str1.charAt(0));

//        SimpleDateFormat form = new SimpleDateFormat("dd-MM-yyyy,HH:mm:ss a");
//        java.util.Date date = null;
//        try {
//            date = form.parse(datumList.get(position).getTime());
//        } catch (ParseException e) {
//
//            e.printStackTrace();
//        }
//        SimpleDateFormat postFormater = new SimpleDateFormat("HH:mm a");
//        newDateStr = postFormater.format(date);
//
//
//        SimpleDateFormat postFormaters = new SimpleDateFormat("dd.MM.yyyy");
//        newDateStrs = postFormaters.format(date);
//
//        holder.txtTranId.setText(newDateStr);

    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtTranId,txtReason,txtAmt;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtAmt = itemView.findViewById(R.id.txtAmt);
            txtReason = itemView.findViewById(R.id.txtReason);
            txtTranId = itemView.findViewById(R.id.txtTranId);

        }
    }

    public interface Toucher {
        void onToucher(String Id, Boolean added);
    }

}
