package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.vixspace.grocerytray.MainActivity;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.CartActivity;
import com.vixspace.grocerytray.pojos.CartInfoModel;
import com.vixspace.grocerytray.pojos.CartModelClass;
import com.vixspace.grocerytray.pojos.FetchAllLocalityCategoriesResponse;
import com.vixspace.grocerytray.pojos.GeneratePriceQuoteRequest;
import com.vixspace.grocerytray.utils.Allsharedpreference;

import java.util.ArrayList;
import java.util.List;

public class CartAdapter extends  RecyclerView.Adapter<CartAdapter.MyViewHolder> {
    private Context context;
    CartInfoModel cartInfoModel;
    Items items;
    Allsharedpreference allsharedpreference;

    public CartAdapter(Context context, CartInfoModel cartInfoModel,Items items) {
        this.context = context;
        this.cartInfoModel = cartInfoModel;
        this.items = items;
        this.allsharedpreference = new Allsharedpreference(context);
    }

    @Override
    public CartAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.individual_cart,parent,false);
        return new CartAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final CartAdapter.MyViewHolder holder, final int position) {

        Picasso.get().load(cartInfoModel.getData().get(position).getImageId()).into(holder.imgItem);
        holder.txtproductAmount.setText("₹ "+cartInfoModel.getData().get(position).getMRP());
        holder.txtproductSize.setText(cartInfoModel.getData().get(position).getItemWeight());
        holder.txtSelectedQuantity.setText(""+cartInfoModel.getData().get(position).getQuantity());
        holder.txtProductName.setText(cartInfoModel.getData().get(position).getProductName());

        holder.imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (cartInfoModel.getData().get(position).)
//                cartInfoModel.getData().get(position).setQuantity(cartInfoModel.getData().get(position).getQuantity()+1);
//                notifyItemChanged(position, cartInfoModel.getData().get(position));
                items.onItemTouched(cartInfoModel,position,true,cartInfoModel.getData());
            }
        });

        holder.imgSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (cartInfoModel.getData().get(position).getQuantity()==1){
//                    if (cartInfoModel.getData().size()==1){
//                        CartInfoModel cartInfoModel1 = new CartInfoModel();
//                        List<CartModelClass> list = new ArrayList<>();
//                        cartInfoModel1.setData(list);
//                        Gson gsons = new Gson();
//                        String jsons = gsons.toJson(cartInfoModel1); // myObject - instance of MyObject
//                        allsharedpreference.setShared_String(Allsharedpreference.CartData,jsons);
//                        Intent intent = new Intent(context, MainActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        context.startActivity(intent);
//                    } else {
//                        items.onItemTouched(cartInfoModel,position,false,cartInfoModel.getData());
//                    }
////                    cartInfoModel.getData().remove(position);
////                    notifyItemRemoved(position);
//
////                    notifyDataSetChanged();
//                } else {
////                    cartInfoModel.getData().get(position).setQuantity(cartInfoModel.getData().get(position).getQuantity()-1);
////                    notifyItemChanged(position, cartInfoModel.getData().get(position));
//                    items.onItemTouched(cartInfoModel,position,false,cartInfoModel.getData());
//                }
                items.onItemTouched(cartInfoModel,position,false,cartInfoModel.getData());
            }
        });

    }

    @Override
    public int getItemCount() {
        return cartInfoModel.getData().size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtProductName,txtproductSize,txtproductAmount,txtSelectedQuantity;
        ImageView imgItem,imgSub,imgAdd;
        CardView cardWhole;

        public MyViewHolder(View itemView) {
            super(itemView);

            cardWhole = itemView.findViewById(R.id.cardWhole);
            imgItem = itemView.findViewById(R.id.imgItem);
            imgSub = itemView.findViewById(R.id.imgSub);
            imgAdd = itemView.findViewById(R.id.imgAdd);
            txtProductName = itemView.findViewById(R.id.txtProductName);
            txtproductSize = itemView.findViewById(R.id.txtproductSize);
            txtproductAmount = itemView.findViewById(R.id.txtproductAmount);
            txtSelectedQuantity = itemView.findViewById(R.id.txtSelectedQuantity);

        }
    }

    public interface Items {
        void onItemTouched(CartInfoModel cartInfoModel, Integer position, Boolean isAdd, List<CartModelClass> cartModelClasses);
    }


}
