package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.pojos.FetchAllAvailableOfferCouponsResponse;

import java.util.List;


public class CouponsAdapter extends  RecyclerView.Adapter<CouponsAdapter.MyViewHolder> {
    private Context context;
    List<FetchAllAvailableOfferCouponsResponse.Datum> datumList;
    Toucher toucher;

    public CouponsAdapter(Context context, List<FetchAllAvailableOfferCouponsResponse.Datum> datumList, Toucher toucher) {
        this.context = context;
        this.toucher = toucher;
        this.datumList = datumList;

    }

    @Override
    public CouponsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.coupons_individual,parent,false);
        return new CouponsAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final CouponsAdapter.MyViewHolder holder, final int position) {
        holder.txtCopounName.setText(datumList.get(position).getCouponCode());
        holder.txtDescription.setText(datumList.get(position).getTermsAndCondition());
        holder.btnCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toucher.onToucher(datumList.get(position).getCouponCode());
            }
        });
    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtCopounName,txtDescription;
        Button btnCopy;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtCopounName = itemView.findViewById(R.id.txtCopounName);
            txtDescription = itemView.findViewById(R.id.txtDescription);
            btnCopy = itemView.findViewById(R.id.btnCopy);

        }
    }

    public interface Toucher {
        void onToucher(String Id);
    }

}
