package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.OrderDetailsActivity;
import com.vixspace.grocerytray.activities.TrackActivity;
import com.vixspace.grocerytray.pojos.UpcomingOrdersResponse;
import com.vixspace.grocerytray.utils.Allsharedpreference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class CurrentOrdersAdapter extends  RecyclerView.Adapter<CurrentOrdersAdapter.MyViewHolder> {
    private Context context;
    List<UpcomingOrdersResponse.Datum> datumList;
    String newDateStr,newDateStr1;
    Allsharedpreference allsharedpreference;
    Integer indicator = 0;

    public CurrentOrdersAdapter(Context context, List<UpcomingOrdersResponse.Datum> datumList, Integer indicator) {
        this.context = context;
        this.datumList = datumList;
        this.allsharedpreference = new Allsharedpreference(context);
        this.indicator = indicator;
    }

    @Override
    public CurrentOrdersAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.individual_orders,parent,false);
        return new CurrentOrdersAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final CurrentOrdersAdapter.MyViewHolder holder, final int position) {

            SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = form.parse(datumList.get(position).getSelectedDateTime());
            } catch (ParseException e) {

                e.printStackTrace();
            }
            SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM yyyy");
            newDateStr = postFormater.format(date);

            SimpleDateFormat forms = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = null;
            try {
                date1 = forms.parse(datumList.get(position).getSelectedDateTime());
            } catch (ParseException e) {

                e.printStackTrace();
            }
            SimpleDateFormat postFormater1 = new SimpleDateFormat("EEE");
            newDateStr1 = postFormater1.format(date1);

            holder.txtDay.setText(newDateStr1);
            holder.txtDate.setText(newDateStr);
            holder.txtOrderId.setText("#"+datumList.get(position).getOrderNumber());
            holder.txtSlot.setText("On "+newDateStr);

            if (datumList.get(position).getOrderStatusLogs().size()>0){
                holder.txtStatus.setText("Status - "+datumList.get(position).getOrderStatusLogs().get(datumList.get(position).getOrderStatusLogs().size()-1).getComment());
            } else {
                holder.txtStatus.setVisibility(View.GONE);
            }

            holder.relativeViewDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, OrderDetailsActivity.class);
                    intent.putExtra("OrderId",datumList.get(position).getOrderID());
                    intent.putExtra("indicator",indicator);
                    context.startActivity(intent);
                }
            });

            holder.relativeTrack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, TrackActivity.class);
                    intent.putExtra("OrderId",datumList.get(position).getOrderID());
                    intent.putExtra("OrderNum",datumList.get(position).getOrderNumber());
                    context.startActivity(intent);
                }
            });


    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout relativeViewDetails,relativeTrack;
        TextView txtOrderId,txtSlot,txtStatus,txtDate,txtDay;

        public MyViewHolder(View itemView) {
            super(itemView);
            relativeViewDetails = itemView.findViewById(R.id.relativeViewDetails);
            txtOrderId = itemView.findViewById(R.id.txtOrderId);
            txtSlot = itemView.findViewById(R.id.txtSlot);
            txtStatus = itemView.findViewById(R.id.txtStatus);
            relativeTrack = itemView.findViewById(R.id.relativeTrack);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtDay = itemView.findViewById(R.id.txtDay);
        }
    }

}
