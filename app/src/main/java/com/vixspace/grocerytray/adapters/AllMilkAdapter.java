package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.ProductDetailsActivity;
import com.vixspace.grocerytray.pojos.FetchAllDealOfDayDetailsResponse;

import java.util.List;

public class AllMilkAdapter extends  RecyclerView.Adapter<AllMilkAdapter.MyViewHolder> {
    private Context context;
    List<FetchAllDealOfDayDetailsResponse.Datum> datumList;

    public AllMilkAdapter(Context context, List<FetchAllDealOfDayDetailsResponse.Datum> datumList) {
        this.context = context;
        this.datumList = datumList;

    }

    @Override
    public AllMilkAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.individual_item,parent,false);
        return new AllMilkAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final AllMilkAdapter.MyViewHolder holder, final int position) {

        holder.cardWhole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductDetailsActivity.class);
                intent.putExtra("productID",datumList.get(position).getLocalityProductID());
                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imgProduct,imgPlus,imgMinus;
        TextView txtCompany,txtProductName,txtWeight,txtAmount,txtBeforeDiscount,txtDiscountPercent,
                txtSubscribeAmount,txtBuyOnce,txtQuantity;
        RelativeLayout relativeCounter;
        CardView cardWhole;

        public MyViewHolder(View itemView) {
            super(itemView);

            cardWhole = itemView.findViewById(R.id.cardWhole);
            imgProduct = itemView.findViewById(R.id.imgProduct);
            imgPlus = itemView.findViewById(R.id.imgPlus);
            imgMinus = itemView.findViewById(R.id.imgMinus);
            relativeCounter = itemView.findViewById(R.id.relativeCounter);
            txtCompany = itemView.findViewById(R.id.txtCompany);
            txtProductName = itemView.findViewById(R.id.txtProductName);
            txtWeight = itemView.findViewById(R.id.txtWeight);
            txtAmount = itemView.findViewById(R.id.txtAmount);
            txtBeforeDiscount = itemView.findViewById(R.id.txtBeforeDiscount);
            txtDiscountPercent = itemView.findViewById(R.id.txtDiscountPercent);
            txtSubscribeAmount = itemView.findViewById(R.id.txtSubscribeAmount);
            txtBuyOnce = itemView.findViewById(R.id.txtBuyOnce);
            txtQuantity = itemView.findViewById(R.id.txtQuantity);

        }
    }



}
