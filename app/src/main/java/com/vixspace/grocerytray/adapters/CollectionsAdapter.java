package com.vixspace.grocerytray.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.vixspace.grocerytray.R;
import com.vixspace.grocerytray.activities.AllCollectionProductsActivity;
import com.vixspace.grocerytray.activities.ProductDetailsActivity;
import com.vixspace.grocerytray.pojos.FetchAllCategoryCollectionResponse;
import com.vixspace.grocerytray.pojos.FetchAllLocalityCategoriesResponse;

import java.util.List;

public class CollectionsAdapter extends  RecyclerView.Adapter<CollectionsAdapter.MyViewHolder> {
    private Context context;
    Collect collect;
    List<FetchAllCategoryCollectionResponse.Datum> datumList;
    int lastSelectedPosition=0;

    public CollectionsAdapter(Context context, List<FetchAllCategoryCollectionResponse.Datum> datumList,Collect collect) {
        this.context = context;
        this.datumList = datumList;
        this.collect = collect;

    }

    @Override
    public CollectionsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.individual_collections_header,parent,false);
        return new CollectionsAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final CollectionsAdapter.MyViewHolder holder, final int position) {

        holder.layout_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastSelectedPosition = position;
                notifyDataSetChanged();
                collect.onCollect(datumList.get(position).getCollectionID(),datumList.get(position).getCollectionName());
//                Intent intent = new Intent(context, AllCollectionProductsActivity.class);
//                intent.putExtra("CollectionID",datumList.get(position).getCollectionID());
//                context.startActivity(intent);
            }
        });

        if(lastSelectedPosition == position) {
            holder.view_red.setVisibility(View.VISIBLE);
            holder.txtCollection.setTextColor(context.getResources().getColor(R.color.black));
        } else {
            holder.view_red.setVisibility(View.GONE);
            holder.txtCollection.setTextColor(context.getResources().getColor(R.color.warm_grey_two));
        }


        holder.txtCollection.setText(datumList.get(position).getCollectionName());

    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtCollection;
        LinearLayout layout_cat;
        View view_red;

        public MyViewHolder(View itemView) {
            super(itemView);

            layout_cat = itemView.findViewById(R.id.layout_cat);
            txtCollection = itemView.findViewById(R.id.txtCollection);
            view_red = itemView.findViewById(R.id.view_red);

        }
    }

    public interface Collect {
        void onCollect(String Id,String name);
    }

}
